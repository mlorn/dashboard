﻿interface Person {
  firstName: string;
  lastName: string;
}

function greeter(persion: Person) {
  return "Hello, " + persion.firstName;
}

let user =  { firstName: "Mao", lastName: "Lorn" };


document.body.innerHTML = greeter(user);