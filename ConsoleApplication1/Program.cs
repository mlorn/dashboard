﻿using System;
using System.Data.Common;
using System.Diagnostics;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DataAccessLayer;
using DataAccessLayer;
using static ConsoleApplication1.StaticClass;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Net.Http;
using DataAccessLayer.BusinessLogic;
using ObjectDumper;
using NomadLetter.UILayer;

namespace ConsoleApplication1
{


	class Program
	{

		static void Main(string[] args)
		{
			const int opTypeLead = 983;  // FCC for lead
			const int opTypeClientProspect = 390;
			const int opTypeService = 982;

			string authKey = "967fc379-f7d8-4dad-833f-611dbf9cc1c1";
			int resvAgent = 186;
			const int dashboardAdmin = 195;
			int userID = 10976;
			DateTime startDate = Convert.ToDateTime("09/30/2019");
			DateTime endDate = DateTime.Today;
			string userName = "SUZANNEG";
			string result;
			decimal temp = 0.0m;

			//ProcessLetterRevision letterRevision = new ProcessLetterRevision();
			//result = letterRevision.Process(userName);

			DashboardData dashboardData = new DashboardData(userID, resvAgent, userName);
			////DashboardData dashboardData = new DashboardData();
			//dashboardData.GroupID = 0;
			//dashboardData.UserName = userName;
			//if (dashboardData.DashboardDataset.tblLetter.Count > 0)
			//{
			//  foreach(var l in dashboardData.DashboardDataset.tblLetter.AsEnumerable())
			//  {
			//    Console.WriteLine($"letter type ={l.LetterType}, sentDate={l.LetterSentDate}");
			//  }
			//}

			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				//dashboardData.FillResvAgent(magnumDb);
				//if (dashboardData.DashboardDataset.tblFCC.Rows.Count == 0)
				//{
				//  dashboardData.FillFutureClientCallback(magnumDb, startDate, endDate);
				//}
				//Console.WriteLine($"start={DateTime.Now}");
				magnumDb.CommandTimeout = 0;
				//var fccData = magnumDb.spDashboardFCC_New(startDate, endDate, userName).ToList();
				//if (fccData != null)
				//{
				//  Console.WriteLine($"count={fccData.Count}");
				//  foreach (var f in fccData)
				//  {
				//    Console.WriteLine($"{f.UserName}");
				//  }
				//}
				dashboardData.FillReservation(magnumDb, userName);
				if (dashboardData.DashboardDataset.tblReservation.Count > 0)
					Console.WriteLine($"end={DateTime.Now}");



				//using (MagnumDataContext magnumDb = new MagnumDataContext())
				//{
				//  var letterData = (from d in magnumDb.spDashboardLetterRevision(userName).AsEnumerable()
				//                   select d);
				//  if (letterData != null)
				//  {
				//    foreach(var l in letterData)
				//    {
				//      WriteLine($"ResvNo={l.ReservationNumber} UniqueID={l.UniqueID}");
				//    }

				//  }
				//}

				//}

				DateTime currentDate = DateTime.Today;

				var sunday = currentDate.AddDays(-(int)currentDate.DayOfWeek);


				WriteLine($"first day={sunday}");
				WriteLine($"last day={currentDate}");

				Console.WriteLine("================================");


				Console.WriteLine("Press any key to continue...");
				Console.ReadKey();



			}
		}
	}
}
