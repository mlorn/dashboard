﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1
{
    public static class StaticClass
    {
        private static string firstName = "Mao";
        private static string lastName = "Lorn";

        public static string ClientName()
        {
            return $"{lastName}, {firstName}";
        }
    }
}
