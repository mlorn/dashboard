using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data;
using System.Reflection;
using System.Web;
//using System.Windows.Forms;
//using DevExpress.XtraReports;
//using DevExpress.XtraReports.UI;
using QuickRes.BusinessLayer;
using QuickRes.DataAccessLayer;
using QuickRes.HelperClass;
using WimcoHelper;
using DataAccessLayer;

namespace NomadLetter.UILayer
{
	/// <summary>
	/// Print revision letters or Refund Letters or Remittance Letters
	/// Letters can be printed individually by a Resv No or all 
	/// letters that have been changed and approved or a deposit/payment
	/// reference number.
	/// </summary>
	public class ProcessLetterRevision
	{
		private DataSet dsResv;
		private DataSet dsAirResv = null;
		private string resvNo;
		//private string depositNo;
		private string revisionTag;
		private int printTo;
		private List<PrintedLetter> printedLetter = new List<PrintedLetter>();
		private LetterData letterData;

		private string GetRevisionLetter(string resvNo)
		{
			string filterString;
			string result = string.Empty;
			string letterType = string.Empty;
			string clientConfirmationWithTA = "Confirmation With TA";
			string clientConfirmationWithoutTA = "Confirmation Without TA";
			string clientInsufficientDepositWithoutTA = "Insufficient Deposit Without TA";
			string clientInsufficientDepositWithTA = "Insufficient Deposit With TA";
			string depositRequestLetter = "Deposit Request";
			string carReservationConfirmation = "Car Confirmation";

			#region Print H Reservation

			/// <summary>
			/// Client Confirmation Without TA - H Reservation
			/// </summary>
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += " AND ResvPrefix = 'H' AND IsTravelAgent = 'N' ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorKindID <> 4) ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithoutTA_HM.ToString();
				//result += letterType;
				result = clientConfirmationWithoutTA;
			}

			/// <summary>
			/// Client Confirmation With TA - H Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'H' AND IsTravelAgent = 'Y' ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithTA_HM.ToString();
				result = clientConfirmationWithTA;
			}

			#endregion

			#region Print M Reservation
			/// <summary>
			/// Client Confirmation Without TA - M Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithoutTA_HM.ToString();
				result = clientConfirmationWithoutTA;
			}

			/// <summary>
			/// Client Confirmation Without TA - M Reservation
			/// Deposit due is charged to a credit card
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithoutTA_HM.ToString();
				result = clientConfirmationWithoutTA;
			}

			/// <summary>
			/// Client Insufficient Deposit Without TA - M Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientInsufficientDepositWithoutTA_HM.ToString();
				result = clientInsufficientDepositWithoutTA;
			}

			/// <summary>
			/// Client Confirmation With TA - M Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithTA_HM.ToString();
				result = clientConfirmationWithTA;
			}


			/// <summary>
			/// Client Confirmation With TA - M Reservation
			/// Deposit Due is charged to a credit card
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithTA_HM.ToString();
				result = clientConfirmationWithTA;
			}

			/// <summary>
			/// Client Insufficient Deposit With TA - M Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientInsufficientDepositWithTA_HM.ToString();
				result = clientInsufficientDepositWithTA;
			}

			#endregion

			#region Print M Reservation for WSBH (vendor base ID 7684)
			/// <summary>
			/// Client Confirmation Without TA - M Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithoutTA_HM.ToString();
				result = clientConfirmationWithoutTA;
			}


			/// <summary>
			/// Client Confirmation Without TA - M Reservation
			/// Deposit due is charged to a credit card
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithoutTA_HM.ToString();
				result = clientConfirmationWithoutTA;
			}

			/// <summary>
			/// Client Insufficient Deposit Without TA - M Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientInsufficientDepositWithoutTA_HM.ToString();
				result = clientInsufficientDepositWithoutTA;
			}

			/// <summary>
			/// Client Confirmation With TA - M Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithTA_HM.ToString();
				result = clientConfirmationWithTA;
			}

			/// <summary>
			/// Client Confirmation With TA - M Reservation
			/// Deposit Due is charged to a credit card
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithTA_HM.ToString();
				result = clientConfirmationWithTA;
			}

			/// <summary>
			/// Client Insufficient Deposit With TA - M Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientInsufficientDepositWithTA_HM.ToString();
				result = clientInsufficientDepositWithTA;
			}

			#endregion

			#region H Reservation Deposit Request
			/// <summary>
			/// Deposit Request Letter - H Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND (ResvPrefix = 'H') AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
			filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//HttpContext.Current.Session["IsNonDeposit"] = "Y";
				//letterType = LetterType.DepositRequestLetter_HM.ToString();
				result = depositRequestLetter;
			}

			#endregion

			#region M Reservation Deposit Request
			/// <summary>
			/// Deposit Request Letter - M Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND (ResvPrefix = 'M') AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
			filterString += "AND (VendorBaseID <> 7684) ";  //Vendor Base ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.DepositRequestLetter_HM.ToString();
				result = depositRequestLetter;
			}

			#endregion

			#region M Reservation Deposit Request for WSBH (vendor base id 7684)
			/// <summary>
			/// Deposit Request Letter - M Reservation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND (ResvPrefix = 'M') AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.DepositRequestLetter_HM.ToString();
				result = depositRequestLetter;
			}

			#endregion

			#region Print R Reservations

			/// <summary>
			/// Client Confirmation without TA - R Resv
			/// </summary>
			/// <param name="resvNo">The resv no.</param>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'R' AND IsTravelAgent = 'N' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithoutTA_R.ToString();
				result = clientConfirmationWithoutTA;
			}

			/// <summary>
			/// Client Confirmation without TA - R Resv
			/// to make sure that insufficient deposit letter not print
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'R' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND IsDepositPaid = 'Y'";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithoutTA_R.ToString();
				result = clientConfirmationWithoutTA;
			}

			/// <summary>
			/// Client Insufficient Deposit without TA - R Resv
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'R' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND IsDepositPaid = 'N'";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientInsufficientDepositWithoutTA_R.ToString();
				result = clientInsufficientDepositWithoutTA;
			}

			/// <summary>
			/// Client Confirmation with TA - R Resv
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'R' AND IsTravelAgent = 'Y' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithTA_R.ToString();
				result = clientConfirmationWithTA;
			}

			/// <summary>
			/// Client Confirmation With TA - R Resv
			/// Take care of not to print insufficient deposit letter
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'R' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND isDepositPaid = 'Y' ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientConfirmationWithTA_R.ToString();
				result = clientConfirmationWithTA;
			}

			/// <summary>
			/// Client Insufficient Deposit With TA - R Resv
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'R' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND isDepositPaid = 'N'";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.ClientInsufficientDepositWithTA_R.ToString();
				result = clientInsufficientDepositWithTA;
			}

			/// <summary>
			/// Deposit Request Letter - R Resv
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'R' AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//HttpContext.Current.Session["IsNonDeposit"] = "Y";
				//letterType = LetterType.DepositRequestLetter_R.ToString();
				result = depositRequestLetter;
			}

			#endregion

			#region Car Reservation Confirmation
			/// <summary>
			/// Car Reservation Confirmation
			/// </summary>
			letterType = string.Empty;
			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND (ResvPrefix = 'H') AND VendorKindID = 4 ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//letterType = LetterType.CarReservationConfirmation.ToString();
				result = carReservationConfirmation;
			}

			#endregion

			return result;
		}

		#region Print H Reservation
		/// <summary>
		/// Client Confirmation Without TA - H Reservation
		/// </summary>
		private string clientConfirmationWithoutTA_H()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'H' AND IsTravelAgent = 'N' ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorKindID <> 4) ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithoutTA_HM.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Confirmation With TA - H Reservation
		/// </summary>
		private string clientConfirmationWithTA_H()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'H' AND IsTravelAgent = 'Y' ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithTA_HM.ToString();
			}

			return result;
		}

		#endregion

		#region Print M Reservation
		/// <summary>
		/// Client Confirmation Without TA - M Reservation
		/// </summary>
		private string clientConfirmationWithoutTA_M(string resvNo)
		{
			string filterString;
			string result = string.Empty;

			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithoutTA_HM.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Confirmation Without TA - M Reservation
		/// Deposit due is charged to a credit card
		/// </summary>
		private string clientConfirmationWithoutTA_M2()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithoutTA_HM.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Insufficient Deposit Without TA - M Reservation
		/// </summary>
		private string clientInsufficientDepositWithoutTA_M()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientInsufficientDepositWithoutTA_HM.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Confirmation With TA - M Reservation
		/// </summary>
		private string clientConfirmationWithTA_M()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithTA_HM.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Confirmation With TA - M Reservation
		/// Deposit Due is charged to a credit card
		/// </summary>
		private string clientConfirmationWithTA_M2()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithTA_HM.ToString();
			}
			return result;
		}
		/// <summary>
		/// Client Insufficient Deposit With TA - M Reservation
		/// </summary>
		private string clientInsufficientDepositWithTA_M()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID <> 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientInsufficientDepositWithTA_HM.ToString();
			}
			return result;
		}
		#endregion

		#region Print M Reservation for WSBH (vendor base ID 7684)
		/// <summary>
		/// Client Confirmation Without TA - M Reservation
		/// </summary>
		private string clientConfirmationWithoutTA_M_7684(string resvNo)
		{
			string filterString;
			string result = string.Empty;

			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithoutTA_HM.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Confirmation Without TA - M Reservation
		/// Deposit due is charged to a credit card
		/// </summary>
		private string clientConfirmationWithoutTA_M2_7684(string resvNo)
		{
			string filterString;
			string result = string.Empty;

			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithoutTA_HM.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Insufficient Deposit Without TA - M Reservation
		/// </summary>
		private string clientInsufficientDepositWithoutTA_M_7684(string resvNo)
		{
			string filterString;
			string result = string.Empty;

			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientInsufficientDepositWithoutTA_HM.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Confirmation With TA - M Reservation
		/// </summary>
		private string clientConfirmationWithTA_M_7684(string resvNo)
		{
			string filterString;
			string result = string.Empty;

			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithTA_HM.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Confirmation With TA - M Reservation
		/// Deposit Due is charged to a credit card
		/// </summary>
		private string clientConfirmationWithTA_M2_7684(string resvNo)
		{
			string filterString;
			string result = string.Empty;

			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction = 2 OR CreditCardAction = 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithTA_HM.ToString();
			}
			return result;
		}
		/// <summary>
		/// Client Insufficient Deposit With TA - M Reservation
		/// </summary>
		private string clientInsufficientDepositWithTA_M_7684(string resvNo)
		{
			string filterString;
			string result = string.Empty;

			filterString = string.Format("ReservationNumber = '{0}' ", resvNo);
			filterString += "AND ResvPrefix = 'M' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND (CreditCardAction <> 2 AND CreditCardAction <> 3) ";  //Deposit due is charged to a credit card OR guarantee
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientInsufficientDepositWithTA_HM.ToString();
			}
			return result;
		}
		#endregion


		#region H Reservation Deposit Request
		/// <summary>
		/// Deposit Request Letter - H Reservation
		/// </summary>
		private string depositRequestLetter_H()
		{
			string filterString;
			string result = string.Empty;

			filterString = "(ResvPrefix = 'H') AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
			filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//HttpContext.Current.Session["IsNonDeposit"] = "Y";
				result = LetterType.DepositRequestLetter_HM.ToString();
			}
			return result;
		}
		#endregion

		#region M Reservation Deposit Request
		/// <summary>
		/// Deposit Request Letter - M Reservation
		/// </summary>
		private string depositRequestLetter_M()
		{
			string filterString;
			string result = string.Empty;

			filterString = "(ResvPrefix = 'M') AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
			filterString += "AND (VendorBaseID <> 7684) ";  //Vendor Base ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//HttpContext.Current.Session["IsNonDeposit"] = "Y";
				result = LetterType.DepositRequestLetter_HM.ToString();
			}
			return result;
		}

		#endregion

		#region M Reservation Deposit Request for WSBH (vendor base id 7684)
		/// <summary>
		/// Deposit Request Letter - M Reservation
		/// </summary>
		private string depositRequestLetter_M_7684()
		{
			string filterString;
			string result = string.Empty;

			filterString = "(ResvPrefix = 'M') AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0 ";
			filterString += "AND (VendorBaseID = 7684) ";  //vendorbase ID 7684 is WSBH
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//HttpContext.Current.Session["IsNonDeposit"] = "Y";
				result = LetterType.DepositRequestLetter_HM.ToString();
			}
			return result;
		}

		#endregion


		#region Print R Reservations

		/// <summary>
		/// Client Confirmation without TA - R Resv
		/// </summary>
		/// <param name="resvNo">The resv no.</param>
		private string clientConfirmationWithoutTA_R()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithoutTA_R.ToString();
				//HttpContext.Current.Session.Add("ReportData", dsResv);
			}
			return result;
		}

		/// <summary>
		/// Client Confirmation without TA - R Resv
		/// to make sure that insufficient deposit letter not print
		/// </summary>
		private string clientConfirmationWithoutTA_R2()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND IsDepositPaid = 'Y'";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithoutTA_R.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Insufficient Deposit without TA - R Resv
		/// </summary>
		private string clientInsufficientDepositWithoutTA_R()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND IsDepositPaid = 'N'";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientInsufficientDepositWithoutTA_R.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Confirmation with TA - R Resv
		/// </summary>
		private string clientConfirmationWithTA_R()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' AND depositDue <= TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithTA_R.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Confirmation With TA - R Resv
		/// Take care of not to print insufficient deposit letter
		/// </summary>
		private string clientConfirmationWithTA_R2()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND isDepositPaid = 'Y' ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientConfirmationWithTA_R.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Insufficient Deposit With TA - R Resv
		/// </summary>
		private string clientInsufficientDepositWithTA_R()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' AND depositDue > TotalReceived ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND isDepositPaid = 'N'";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientInsufficientDepositWithTA_R.ToString();
			}
			return result;
		}

		/// <summary>
		/// Deposit Request Letter French - R Resv, Not being called anymore
		/// </summary>
		private string depositRequestLetter_R()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//HttpContext.Current.Session["IsNonDeposit"] = "Y";
				result = LetterType.DepositRequestLetter_R.ToString();
			}
			return result;
		}
		/// <summary>
		/// Deposit Request Letter French - R Resv
		/// </summary>
		private string depositRequestLetterFrench_R()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND WimcoTotalRecdClient = 0 AND PMTotalRecdClient = 0";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				//HttpContext.Current.Session["IsNonDeposit"] = "Y";
				result = LetterType.DepositRequestLetterFrench_R.ToString();
			}
			return result;
		}

		#endregion


		#region Air Confirmation Letter

		private string airConfirmationLetter()
		{
			string result = string.Empty;

			if (this.dsAirResv == null)
			{
				this.dsAirResv = this.letterData.GetLetterAirData(this.resvNo, this.revisionTag);
			}
			if (this.dsAirResv.Tables[0].Rows.Count > 0)
			{
				result = LetterType.AirConfirmationLetter.ToString();
			}
			return result;
		}

		#endregion


		#region Print Client Refund Letter - H or M Reservation
		/// <summary>
		/// Client Refund Letter without TA - H or M Resv
		/// </summary>
		private string clientRefundLetterWithoutTA_HM()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'H' OR ResvPrefix = 'M' AND IsTravelAgent = 'N' ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND Status = 'CXL' ";
			filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientRefundLetterWithoutTA_HM.ToString();
			}
			return result;
		}


		/// <summary>
		/// Client Refund Letter with TA - H or M Resv
		/// </summary>
		private string clientRefundLetterWithTA_HM()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'H' OR ResvPrefix = 'M' AND IsTravelAgent = 'Y' ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND Status = 'CXL' ";
			filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientRefundLetterWithTA_HM.ToString();
			}
			return result;
		}
		#endregion

		#region Print Client Refund Letter - R Reservations

		/// <summary>
		/// Client Refund Letter without TA - R Resv
		/// </summary>
		private string clientRefundLetterWithoutTA_R()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND Status = 'CXL' ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientRefundLetterWithoutTA_R.ToString();
			}
			return result;
		}

		/// <summary>
		/// Client Refund Letter with TA - R Resv
		/// </summary>
		private string clientRefundLetterWithTA_R()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' ";
			filterString += "AND (TransactionKindID = 6 OR TransactionKindID = 8) ";
			filterString += "AND Status = 'CXL' ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.ClientRefundLetterWithTA_R.ToString();
			}
			return result;
		}
		#endregion

		#region PM Remittance Letters

		/// <summary>
		/// PM Remittance with Gross Deal
		/// </summary>
		private string pmRemittanceWithGrossDeal()
		{
			string filterString;
			string result = string.Empty;

			filterString = "TransactionKindID = 7 AND IsNetDeal = 'N' ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.PMRemittanceWithGrossDeal.ToString();
			}
			return result;
		}

		/// <summary>
		/// PM Remittance with Net Deal
		/// </summary>
		private string pmRemittanceWithNetDeal()
		{
			string filterString;
			string result = string.Empty;

			filterString = "TransactionKindID = 7 AND IsNetDeal = 'Y' ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.PMRemittanceWithNetDeal.ToString();
			}
			return result;
		}
		#endregion

		#region PM Confirmation Letter
		/// <summary>
		/// PM Confirmation Letter Without TA - R Resv
		/// </summary>
		private string pmConfirmationWithoutTA_R()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'N' ";
			filterString += "AND (TransactionKindID = 7) ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.PMConfirmationWithoutTA_R.ToString();
			}
			return result;
		}

		/// <summary>
		/// PM Confirmation Letter With TA - R Resv
		/// </summary>
		private string pmConfirmationWithTA_R()
		{
			string filterString;
			string result = string.Empty;

			filterString = "ResvPrefix = 'R' AND IsTravelAgent = 'Y' ";
			filterString += "AND (TransactionKindID = 7) ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.PMConfirmationWithTA_R.ToString();
			}
			return result;
		}
		#endregion

		#region PM Confirmation H and M Resv
		/// <summary>
		/// PM Confirm without TA - H & M Resv
		/// </summary>
		private string pmConfirmationWithoutTA_HM()
		{
			string filterString;
			string result = string.Empty;

			filterString = "(ResvPrefix = 'H' OR ResvPrefix = 'M') AND IsTravelAgent = 'N' ";
			filterString += "AND (TransactionKindID = 7) ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.PMConfirmationWithoutTA_HM.ToString();
			}
			return result;
		}
		#endregion

		/// <summary>
		/// PM Confirm with TA - H & M Resv
		/// </summary>
		private string pmConfirmationWithTA_HM()
		{
			string filterString;
			string result = string.Empty;

			filterString = "(ResvPrefix = 'H' OR ResvPrefix = 'M') AND IsTravelAgent = 'Y' ";
			filterString += "AND (TransactionKindID = 7) ";
			filterString += "AND (VendorKindID <> 4) ";  // vendor kind 4 is car rental
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.PMConfirmationWithTA_HM.ToString();
			}
			return result;
		}

		private string CarReservationConfirmation()
		{
			string filterString;
			string result = string.Empty;

			filterString = "(ResvPrefix = 'H') AND VendorKindID = 4 ";
			this.dsResv.Tables[0].DefaultView.RowFilter = filterString;
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.CarReservationConfirmation.ToString();
			}
			return result;
		}

		#region Check By Phone Form

		private string CheckByPhoneForm()
		{
			string result = string.Empty;

			DataRow dr = this.dsResv.Tables[0].Rows[0];
			this.dsResv.Tables[0].Clear();
			this.dsResv.Tables[0].Rows.Add(dr);
			if (this.dsResv.Tables[0].DefaultView.Count > 0)
			{
				result = LetterType.CheckByPhoneForm.ToString();
			}
			return result;
		}
		#endregion

		public DataSet CreateDataSet<T>(List<T> list)
		{
			//it does so create the dataset and table
			DataSet dataSet;
			DataTable dataTable;

			//list is nothing or has nothing, return nothing (or add exception handling)
			if (list == null || list.Count == 0)
			{
				return null;
			}

			//get the type of the first obj in the list
			var obj = list[0].GetType();

			//now grab all properties
			var properties = obj.GetProperties();

			//make sure the obj has properties, return nothing (or add exception handling)
			if (properties.Length == 0)
			{
				return null;
			}

			//it does so create the dataset and table
			dataSet = new DataSet();
			dataTable = new DataTable();

			//now build the columns from the properties
			var columns = new DataColumn[properties.Length];
			for (int i = 0; i < properties.Length; i++)
			{

				columns[i] = new DataColumn(properties[i].Name, Nullable.GetUnderlyingType(properties[i].PropertyType) ?? properties[i].PropertyType);
			}

			//add columns to table
			dataTable.Columns.AddRange(columns);

			//now add the list values to the table
			foreach (var item in list)
			{
				//create a new row from table
				var dataRow = dataTable.NewRow();

				//now we have to iterate thru each property of the item and retrieve it's value for the corresponding row's cell
				var itemProperties = item.GetType().GetProperties();

				for (int i = 0; i < itemProperties.Length; i++)
				{

					dataRow[i] = itemProperties[i].GetValue(item) ?? DBNull.Value;
				}

				//now add the populated row to the table
				dataTable.Rows.Add(dataRow);
			}

			//add table to dataset
			dataSet.Tables.Add(dataTable);

			//return dataset
			return dataSet;
		}


		/// <summary>
		/// Processes the revision letters
		/// </summary>
		/// <param name="resvNo">The resv no.</param>
		/// <param name="revsionTag">The revsion tag.</param>
		/// <param name="printTo">The print to.</param>
		/// <param name="languageID">English and French.</param>
		/// <returns></returns>
		public string Process(string userName)
		{
			string result = string.Empty;
			bool processRevision = true;
			string letterType = string.Empty;


			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var letterData = magnumDb.spDashboardLetterRevision(userName).ToList();

				if (letterData != null)
				{
					this.dsResv = CreateDataSet(letterData);

					var resv = (from r in dsResv.Tables[0].AsEnumerable()
											select new { ReservationNumber = r.Field<string>("ReservationNumber") }).Distinct().ToList();

					//var resv = (from r in letterData.AsEnumerable()
					//            select new {r.ReservationNumber}).Distinct().ToList();



					Console.WriteLine($"count = {resv.Count}");
					foreach (var l in resv)
					{
						Console.WriteLine($"Resv No= {l.ReservationNumber}");
						result = GetRevisionLetter(l.ReservationNumber);
						Console.WriteLine($"Result={result}");
						/*
            result = clientConfirmationWithoutTA_M_7684(l.ReservationNumber);
            Console.WriteLine($"Result={result}");
            result = clientConfirmationWithoutTA_M2_7684(l.ReservationNumber);
            Console.WriteLine($"Result={result}");
            result = clientConfirmationWithTA_M_7684(l.ReservationNumber);
            Console.WriteLine($"Result={result}");
            result = clientConfirmationWithTA_M2_7684(l.ReservationNumber);
            Console.WriteLine($"Result={result}");
            result = clientInsufficientDepositWithTA_M_7684(l.ReservationNumber);
            Console.WriteLine($"Result={result}");
            result = clientConfirmationWithoutTA_M(l.ReservationNumber);
            Console.WriteLine($"Result={result}");
            */
					}
					/*
          var resv = (from r in dsResv.Tables[0].AsEnumerable()
                      select new { ReservationNumber = r.Field<string>("ReservationNumber") }).Distinct().ToList();

          foreach (var l in resv)
          {
            Console.WriteLine($"ResvNo={l.ReservationNumber}");
          }
          */
				}
			}

			/*
      this.letterData = new LetterData();
      this.dsResv = this.letterData.GetRevisionData(this.resvNo, this.revisionTag, languageID);
      // if there is no regular resv, there may be a car resv.
      if (this.dsResv.Tables[0].Rows.Count == 0)
      {
        result = "There is no letter to print.";
        this.dsAirResv = this.letterData.GetLetterAirData(this.resvNo, this.revisionTag);
        if (this.dsAirResv.Tables[0].Rows.Count == 0)
        {
          return result;
        }
        else
        {
          result = string.Empty;
        }
        processRevision = false;
      }
      */
			/*
      try
      {
        if (processRevision)
        {
          // clear reservation from the list.
          //ReservationList.ListOfReservations.Clear();

          //HttpContext.Current.Session["RptDataset"] = this.dsResv;

          #region Print H Reservation
          letterType = clientConfirmationWithoutTA_H();

          clientConfirmationWithTA_H();
          #endregion

          #region Print M Reservation

          clientConfirmationWithoutTA_M();

          clientConfirmationWithoutTA_M2();  // deposit due is charged to a credit card.

          clientInsufficientDepositWithoutTA_M();

          clientConfirmationWithTA_M();

          clientConfirmationWithTA_M2();   // Deposit due is charged to a credit card

          clientInsufficientDepositWithTA_M();
          #endregion

          #region Print M Reservation for WSBH (VendorBase ID 7684)
          // not running because WSBH reservations use R letter kinds

          //clientConfirmationWithoutTA_M_7684();

          //clientConfirmationWithoutTA_M2_7684();  // deposit due is charged to a credit card.

          //clientInsufficientDepositWithoutTA_M_7684();

          //clientConfirmationWithTA_M_7684();

          //clientConfirmationWithTA_M2_7684();   // Deposit due is charged to a credit card

          //clientInsufficientDepositWithTA_M_7684();

          #endregion

          #region H Reservation Deposit Request

          depositRequestLetter_H();
          #endregion

          #region M Reservation Deposit Request

          depositRequestLetter_M();
          #endregion

          #region M Reservation Deposit Request for WSBH (VendorBase ID 7684)

          depositRequestLetter_M_7684();
          #endregion

          #region Print R Reservations
          clientConfirmationWithoutTA_R();
          clientConfirmationWithoutTA_R2();   // Take care of not to print insufficient deposit letter

          clientInsufficientDepositWithoutTA_R();

          clientConfirmationWithTA_R();
          clientConfirmationWithTA_R2();     // Take care of not to print insufficient deposit letter

          clientInsufficientDepositWithTA_R();
          if (languageID == 1)
          {
            depositRequestLetter_R();
          }
          else
          {
            #region R Reservation Deposit Request French

            depositRequestLetterFrench_R();
            #endregion
          }

          #endregion
        }

        #region Air Confirmation Letter
        airConfirmationLetter();

        #endregion

        #region Print Car Reservavation Confirmation

        CarReservationConfirmation();
        #endregion

        CheckByPhoneForm();
      }
      catch (Exception ex)
      {
        result = string.Format("Error has been occurred while printing letters. With error message: {0}", ex.Message);
      }
      */

			return result;
		}

		public bool ProcessFrenchDepositRequest(string resvNo, string revisionTag, int printTo, int languageID)
		{
			bool result = false;
			bool processRevision = true;

			// if it a non-deposit letter, use a deposit balance due, instead of resv balance due.
			//HttpContext.Current.Session.Remove("IsNonDeposit");

			//HttpContext.Current.Session.Remove("IsTA");
			//HttpContext.Current.Session.Remove("ReportObjectTA");

			this.resvNo = resvNo;
			this.revisionTag = revisionTag;
			this.printTo = printTo;
			this.letterData = new LetterData();
			this.dsResv = this.letterData.GetRevisionData(this.resvNo, this.revisionTag, languageID);
			if (this.dsResv.Tables[0].Rows.Count == 0)
			{
				if (this.dsAirResv.Tables[0].Rows.Count == 0)
				{
					//MessageBox.Show("There is no letter to print.", "No Letter",
					//  MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
					return result;
				}
				processRevision = false;
			}

			try
			{
				if (processRevision)
				{
					// clear reservation from the list.
					//ReservationList.ListOfReservations.Clear();

					#region Print H Reservation
					/*
          clientConfirmationWithoutTA_H();

          clientConfirmationWithTA_H();
          */
					#endregion

					#region Print M Reservation

					//clientConfirmationWithoutTA_M();

					//clientConfirmationWithoutTA_M2();  // deposit due is charged to a credit card.

					//clientConfirmationWithTA_M();

					//clientConfirmationWithTA_M2();   // Deposit due is charged to a credit card

					#endregion

					#region Print M Reservation for WSBH (VendorBase ID 7684)
					/*
          clientConfirmationWithoutTA_M_7684();

          clientConfirmationWithoutTA_M2_7684();  // deposit due is charged to a credit card.

          clientInsufficientDepositWithoutTA_M_7684();

          clientConfirmationWithTA_M_7684();

          clientConfirmationWithTA_M2_7684();   // Deposit due is charged to a credit card

          clientInsufficientDepositWithTA_M_7684();
          */
					#endregion

					#region H Reservation Deposit Request

					//depositRequestLetter_H();
					#endregion

					#region M Reservation Deposit Request

					//depositRequestLetter_M();
					#endregion

					#region M Reservation Deposit Request for WSBH (VendorBase ID 7684)

					depositRequestLetter_M_7684();
					#endregion

					#region Print R Reservations
					/*
          clientConfirmationWithoutTA_R();
          clientConfirmationWithoutTA_R2();   // Take care of not to print insufficient deposit letter

          depositRequestLetter_R();
          */
					#endregion
				}

			}
			//catch (Exception ex)
			//{
			//  result = false;
			//  MessageBox.Show("Error has been occurred while printing letters. With error message: " + ex.Message);
			//}
			finally
			{
				// set this to true so that it will ask to select a printer
				//PrintLetter.FirstTime = true;
				result = true;
				//ReservationList.AddReservationToList(this.dsResv);
			}

			return result;

		}

	}
}
