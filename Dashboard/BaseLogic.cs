﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Dashboard
{
  [DataObject(true)]
  public class BaseLogic
  {

    public struct UserData
    {
      public bool Loggedin;
      public string username;
      public string password;
      public int UserID;
      public int OrganizationID;
      public bool NomadAdmin;
      public bool Manager;
      public bool SalesAgent;
      public string EmailAddress;
      public string FullName;
      public int[] Destinations;
      public string[] ExtendedOptions;
      public string IPAddress;
    }


    public static void AddSessionData(string Key, object value)
    {
      System.Web.HttpContext.Current.Session.Add(Key, value);
    }

    public static Object ReadSessionData(string key)
    {
      Object obj = null;
      try
      {
        obj = System.Web.HttpContext.Current.Session[key];
      }
      catch { }
      return obj;
    }
    public static void KillSession() //Kills all session items except UserData and UserID
    {
      for (short i = (short)HttpContext.Current.Session.Count; i > 0; i--)
      {
        string sMe = HttpContext.Current.Session.Keys[i - 1].ToString();
        if (sMe != "UserData" && sMe != "UserID")
        { HttpContext.Current.Session.Remove(sMe); }
      }
    }
    public static UserData GetUserData()
    {
      UserData ud = new UserData();
      try
      {
        ud = (BaseLogic.UserData)BaseLogic.ReadSessionData("UserData");
      }
      catch { }
      return ud;
    }


    public static UserData CheckLogin(string Username, string Password)
    {
      UserData ud = new UserData();
      ud.Loggedin = false;
      ud.NomadAdmin = false;

      // locking out user 'Guest' 10.14.11 per DW/TW
      if (Username.ToLower() == "guest")
      {
        goto jmp;
      }

      DataSet ds = new DataSet();

      ds = AdHoc("SELECT MagUser.UserID, MagUser.UserName, UserPwd.Pwd, MagUser.OrganizationID, MagUser.EmailAddress FROM MagUser INNER JOIN UserPwd ON MagUser.UserID = UserPwd.UserID WHERE (LOWER(MagUser.UserName) = '" + Username.ToLower() + "') AND (LOWER(UserPwd.Pwd) = '" + Password.ToLower() + "') AND (MagUser.OrganizationID IN(1, 37, 55)) AND (MagUser.IsEnabled = 'Y')");
      try
      {
        if (ds.Tables[0].Rows.Count > 0)
        {

          string sUserID = ds.Tables[0].Rows[0]["UserID"].ToString();
          string sOrgID = ds.Tables[0].Rows[0]["OrganizationID"].ToString();
          int OrgID = Convert.ToInt32(sOrgID);
          int UserID = Convert.ToInt32(sUserID);
          string EmailAddress = string.Empty;
          try
          {
            EmailAddress = ds.Tables[0].Rows[0]["EmailAddress"].ToString();
          }
          catch { }
          ud.EmailAddress = EmailAddress;
          ud.UserID = UserID;
          ud.Loggedin = true;
          ud.OrganizationID = OrgID;



          // populate destinations 07.08.14
          //DataSet dst = AdHoc("SELECT DestinationID FROM  OrgDestination WHERE (OrgID = " + ud.OrganizationID + ")");
          // changed to OrganizationDestination per IT meeting 7.21.14 -jk
          DataSet dst = AdHoc("SELECT DestinationID FROM  OrganizationDestination WHERE (IsEnabled = 'Y') AND (OrganizationID = " + ud.OrganizationID + ")");
          int[] dx = new int[dst.Tables[0].Rows.Count];
          int ii = 0;
          foreach (DataRow dxx in dst.Tables[0].Rows)
          {
            dx[ii] = (int)dxx[0];
          }
          ud.Destinations = dx;
          // end destinations population.

          ud.username = Username.ToLower();
          ud.password = Password.ToLower();

          // is the user a Nomad Admin?

          ud.NomadAdmin = false;
          ud.Manager = false;

          DataSet ug = new DataSet();
          ug = AdHoc("SELECT MagGroup.Name FROM  UserGroup INNER JOIN MagGroup ON UserGroup.GroupID = MagGroup.GroupID WHERE (UserGroup.UserID = " + UserID + ")");
          foreach (DataRow r in ug.Tables[0].Rows)
          {
            if (r[0].ToString() == "NomadAdmin")
            { ud.NomadAdmin = true; }

            if (r[0].ToString() == "Managers")
            { ud.Manager = true; }

            if (r[0].ToString() == "Sales Agent")
            { ud.SalesAgent = true; }

          }

          ud.FullName = AdHocSeek("SELECT FullName FROM  MagUser WHERE (UserID = " + ud.UserID + ")", "FullName");
          ud.IPAddress = HttpContext.Current.Request.UserHostAddress;

          AddSessionData("UserData", ud);
        }
      }
      catch (Exception e)
      {
        BaseLogic.sendEmail("villa owner login error", "jkelly@wimco.com", "err: " + e.Message + " : " + e.Source);
        LogEvent("Error in Login: " + e.Message);
      }

    jmp:;

      return ud;
    }

    public static void CheckIfLoggedIn()
    {
      BaseLogic.UserData ud = BaseLogic.GetUserData();
      if (!ud.Loggedin)
      {

        HttpCookie c = HttpContext.Current.Request.Cookies["udv"];
        if (c != null)
        {
          string uds = BaseLogic.DecodeFrom64(c.Value);
          System.Type type = ud.GetType();
          ud = (BaseLogic.UserData)BaseLogic.DeSerializeAnObject(uds, type);
          AddSessionData("UserData", ud);
          goto jmp;
        }
        HttpContext.Current.Response.Redirect("Login.aspx");
      jmp:;
      }
    }


    public static BusinessLogic.WimcoBusinessLogic businessLogic;
    public static BusinessLogic.LoginData loginData;

    public static string GetAuthKey()
    {
      return Properties.Settings.Default.AuthKey;
    }

    public static string GetAuthKey_d()
    {

      try
      {
        if (loginData.AuthKey == null)
        {
          loginData = businessLogic.LoginProxyUser("RESTEMP", "password2006", "Nomad", System.Web.HttpContext.Current.Request.UserHostAddress);
          //loginData = businessLogic.Login("RESTEMP", "password2006");
        }
      }
      catch
      {
        try
        {
          // no key, let's get one.
          businessLogic = new BusinessLogic.WimcoBusinessLogic();
          loginData = new BusinessLogic.LoginData();
          loginData = businessLogic.LoginProxyUser("RESTEMP", "password2006", "Nomad", System.Web.HttpContext.Current.Request.UserHostAddress);
          //loginData = businessLogic.Login("RESTEMP", "password2006");
        }
        catch
        {

        }
      }
      if (loginData.Expires <= DateTime.Now)
      {
        try
        {
          loginData = businessLogic.LoginProxyUser("RESTEMP", "password2006", "Nomad", System.Web.HttpContext.Current.Request.UserHostAddress);
          //loginData = businessLogic.Login("RESTEMP", "password2006");
        }
        catch
        {

        }
      }
      return loginData.AuthKey;
    }




    public static DataSet AdHoc(string SQL)
    {

      string DevAuth = Properties.Settings.Default.DevAuth;
      BusinessLogic.WimcoBusinessLogic businessLogic = new BusinessLogic.WimcoBusinessLogic();
      DataSet ds = new DataSet();
      try
      {
        string AuthKey = GetAuthKey();
        ds = businessLogic.AdHocQuery(DevAuth, AuthKey, SQL);
      }
      catch (Exception e)
      {
        BaseLogic.sendEmail("nomad adhoc err", "jkelly@wimco.com", "nomad adhoc err: " + e.Message + " : " + e.Source);
        LogEvent("nomad adhoc err: " + e.Message + " : " + e.Source + " sql: " + SQL);
      }
      return ds;
    }

    public static string AdHocSeek(string SQL, string SeekValue)
    {


      string DevAuth = Properties.Settings.Default.DevAuth;
      BusinessLogic.WimcoBusinessLogic businessLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = GetAuthKey();
      string seek = null;
      seek = businessLogic.AdHocQuerySeek(DevAuth, AuthKey, SQL, SeekValue);
      return seek;
    }

    public static BusinessLogic.UpdateDataSetResults UpdateDataSet(DataSet ds)
    {
      string DevAuth = Properties.Settings.Default.DevAuth;
      BusinessLogic.UpdateDataSetResults udr = new BusinessLogic.UpdateDataSetResults();
      string AuthKey = GetAuthKey();
      if (ds.HasChanges())
      {
        try
        {
          //udr = businessLogic.DAL_UpdateDataSet(AuthKey, @DevAuth, ds);
          //businessLogic.DAL_UpdateDataSet("", "", ds);
          BusinessLogic.WimcoBusinessLogic businessLogic = new BusinessLogic.WimcoBusinessLogic();
          udr = businessLogic.DAL_UpdateDataSet(AuthKey, DevAuth, ds.GetChanges());
        }
        catch (System.Net.WebException webEx)
        {
          udr.Success = false;
          udr.msg = webEx.Message;
        }
      }
      if (!udr.Success)
      {
        string result = DataSettoString(ds);
        BaseLogic.sendEmail("nomad udr error", "jkelly@wimco.com", "udr err: " + udr.msg + " data: " + result);
      }
      return udr;
    }




    public static string DataSettoString(DataSet ds)
    {
      string r = string.Empty;
      try
      {
        System.IO.StringWriter sw = new System.IO.StringWriter();
        ds.WriteXml(sw);
        r = sw.ToString();
      }
      catch { }
      return r;

    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public App_Data.OwnerSearchDS GetData(string EmailAddress)
    {
      App_Data.OwnerSearchDS os = new App_Data.OwnerSearchDS();
      os.LoadDataByEmailAddress(EmailAddress);
      return os;
    }


    public static Int32 GetProductBaseID_FromVillaCode(string VillaCode)
    {
      BusinessLogic.WimcoBusinessLogic businessLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      int ProductBaseID = businessLogic.GetProductBaseIDFromVillaCode(AuthKey, VillaCode);
      return ProductBaseID;
    }

    public static string GetVillaNameFromProductBaseID(Int32 ProductBaseID)
    {
      BusinessLogic.WimcoBusinessLogic businessLogic = new BusinessLogic.WimcoBusinessLogic();
      string villa = businessLogic.utils_GetVillaCodeFromProductBaseID(GetAuthKey(), ProductBaseID);
      return villa;
    }

    public static int GetNextKey(string TableName)
    {
      BusinessLogic.WimcoBusinessLogic businessLogic = new BusinessLogic.WimcoBusinessLogic();
      string DevAuth = Properties.Settings.Default.DevAuth;
      int key = 0;
      string AuthKey = GetAuthKey();
      key = businessLogic.GetNextKey(DevAuth, AuthKey, TableName);
      return key;
    }


    public static string[] Split(string linein, string delim)
    {

      string[] returnD = null;
      try
      {
        char[] d = delim.ToCharArray();
        returnD = linein.Split(d);
      }
      catch { }
      return returnD;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.Reservation GetReservation(string ReservationNumber)
    {
      BusinessLogic.WimcoBusinessLogic businessLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.Reservation res = new BusinessLogic.Reservation();
      res = businessLogic.GetResevation(GetAuthKey(), ReservationNumber);
      return res;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.ItineraryObject GetItineraryByItineraryNumber(int ItineraryNumber)
    {
      BusinessLogic.WimcoBusinessLogic businessLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.ItineraryObject io = new BusinessLogic.ItineraryObject();
      io = businessLogic.GenerateItinerary(GetAuthKey(), ItineraryNumber);
      return io;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.ItineraryObject GetItineraryByReservation(string ReservationNumber)
    {
      BusinessLogic.WimcoBusinessLogic businessLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.ItineraryObject io = new BusinessLogic.ItineraryObject();
      io = businessLogic.GenerateItineraryByReservationNumber(GetAuthKey(), ReservationNumber);
      return io;
    }

    public static BusinessLogic.Reservation.ReservationLineItemRow GetRLI(BusinessLogic.Reservation res)
    {
      BusinessLogic.Reservation.ReservationLineItemRow rli = (BusinessLogic.Reservation.ReservationLineItemRow)res.ReservationLineItem.Rows[0];
      return rli;
    }

    public static BusinessLogic.Reservation.ResvWimTempRow GetRWT(BusinessLogic.Reservation res)
    {
      BusinessLogic.Reservation.ResvWimTempRow rwt = (BusinessLogic.Reservation.ResvWimTempRow)res.ResvWimTemp.Rows[0];
      return rwt;
    }

    #region ResInput




    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.ReservationMinimalReservation GetMinimalReservation(string ReservationNumber)
    {
      BusinessLogic.ReservationMinimalReservation mr = new BusinessLogic.ReservationMinimalReservation();
      try
      {
        BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
        string AuthKey = BaseLogic.GetAuthKey();
        mr = biz.GetMinimalReservation(AuthKey, ReservationNumber);
      }
      catch { }
      return mr;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
    public static BusinessLogic.MinimalResDataReturn InsertMinimalReservation(BusinessLogic.ReservationMinimalReservation minRes)
    {
      BusinessLogic.MinimalResDataReturn mrdr = new BusinessLogic.MinimalResDataReturn();
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      mrdr = biz.InsertMinimalReservation(AuthKey, minRes);
      return mrdr;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public static BusinessLogic.MinimalResDataReturn UpdateMinimalReservation(BusinessLogic.ReservationMinimalReservation minRes)
    {
      BusinessLogic.MinimalResDataReturn mrdr = new BusinessLogic.MinimalResDataReturn();
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      mrdr = biz.UpdateMinimalReservation(AuthKey, minRes);
      return mrdr;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
    public static BusinessLogic.MinimalResDataReturn DeleteMinimalReservation(BusinessLogic.ReservationMinimalReservation minRes)
    {
      BusinessLogic.MinimalResDataReturn mrdr = new BusinessLogic.MinimalResDataReturn();
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      mrdr = biz.DeleteMinimalReservation(AuthKey, minRes);
      return mrdr;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public BusinessLogic.FlatReservation ws_GetFlatReservation(string ReservationNumber)
    {
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.FlatReservation flat = new BusinessLogic.FlatReservation();
      string AuthKey = BaseLogic.GetAuthKey();
      flat = biz.GetFlatReservation(AuthKey, ReservationNumber);
      return flat;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public BusinessLogic.FlatDataReturn ws_UpdateFlatReservation(BusinessLogic.FlatReservation flatReservation)
    {
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.FlatDataReturn fdr = new BusinessLogic.FlatDataReturn();
      string AuthKey = BaseLogic.GetAuthKey();
      fdr = biz.UpdateFlatReservation(AuthKey, flatReservation);
      return fdr;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
    public BusinessLogic.FlatDataReturn ws_InsertFlatReservation(BusinessLogic.FlatReservation flatReservation)
    {
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.FlatDataReturn fdr = new BusinessLogic.FlatDataReturn();
      string AuthKey = BaseLogic.GetAuthKey();
      fdr = biz.InsertFlatReservation(AuthKey, flatReservation);
      return fdr;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
    public BusinessLogic.FlatDataReturn ws_DeleteFlatReservation(BusinessLogic.FlatReservation flatReservation)
    {
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.FlatDataReturn fdr = new BusinessLogic.FlatDataReturn();
      string AuthKey = BaseLogic.GetAuthKey();
      fdr = biz.DeleteFlatReservation(AuthKey, flatReservation);
      return fdr;
    }



    //[System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
    //public static BusinessLogic.MinimalResDataReturn InsertVendorReservation(BusinessLogic.ReservationMinimalReservation res)
    //{
    //  BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
    //  string AuthKey = BaseLogic.GetAuthKey();
    //  BusinessLogic.MinimalResDataReturn mrd = new BusinessLogic.MinimalResDataReturn();
    //  mrd = bizLogic.InsertMinimalVendorReservation(AuthKey, res);
    //  return mrd;
    //}

    //[System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    //public static BusinessLogic.MinimalResDataReturn UpdateVendorReservation(BusinessLogic.ReservationMinimalReservation res)
    //{
    //  BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
    //  string AuthKey = BaseLogic.GetAuthKey();
    //  BusinessLogic.MinimalResDataReturn mrd = new BusinessLogic.MinimalResDataReturn();
    //  mrd = bizLogic.UpdateMinimalVendorReservation(AuthKey, res);
    //  return mrd;
    //}

    //[System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
    //public static BusinessLogic.MinimalResDataReturn DeleteVendorReservation(BusinessLogic.ReservationMinimalReservation res)
    //{
    //  BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
    //  string AuthKey = BaseLogic.GetAuthKey();
    //  BusinessLogic.MinimalResDataReturn mrd = new BusinessLogic.MinimalResDataReturn();
    //  mrd = bizLogic.DeleteMinimalVendorReservation(AuthKey, res);
    //  return mrd;
    //}


    #endregion

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.ExtendedMessages GetExtendedMessagesByReservationNumber(string ReservationNumber)
    {
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.ExtendedMessages ex = new BusinessLogic.ExtendedMessages();
      string AuthKey = BaseLogic.GetAuthKey();
      ex = biz.LoadExtendedMessagesByReservationNumber(AuthKey, ReservationNumber);
      return ex;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static DataView GetExtendedMessagesByReservationNumber(string ReservationNumber, string sortExpression)
    {
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.ExtendedMessages ex = new BusinessLogic.ExtendedMessages();
      string AuthKey = BaseLogic.GetAuthKey();
      ex = biz.LoadExtendedMessagesByReservationNumber(AuthKey, ReservationNumber);
      DataView dv = new DataView(ex.Messages);
      dv.Sort = sortExpression;
      return dv;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.Messages GetMessagesByMessageID(int MessageID)
    {
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.Messages msg = new global::Dashboard.BusinessLogic.Messages();
      string AuthKey = BaseLogic.GetAuthKey();
      msg = biz.LoadMessagesByMessageID(AuthKey, MessageID);
      return msg;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.Client GetClientByClientID(int ClientID)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.Client Client = new BusinessLogic.Client();
      Client = bizLogic.GenerateClientObjectByClientID(BaseLogic.GetAuthKey(), ClientID);
      return Client;
    }


    public static BusinessLogic.Client GetEmptyClient()
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.Client Client = new BusinessLogic.Client();
      return Client;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public static void UpdateClient(BusinessLogic.Client client)
    {

    }


    #region ArrivalData

    /*
    // depricated 06.27.13 switch to ArrivalDS call. 

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.Arrival GetArrivalDataByReservationLineItemID(int ReservationLineItemID)
    {
        BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
        BusinessLogic.Arrival arrival = new BusinessLogic.Arrival();
        string AuthKey = BaseLogic.GetAuthKey();
        arrival = biz.GetArrivalDataByReservationLineItemID(AuthKey, ReservationLineItemID);            



        return arrival;
    }
    */

    // returns ArrivalID

    /*
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
    public static int InsertArrivalData(int ReservationLineItemID, int ArrivalSeq, string Airline, string Flight, string FromCity, string ToCity, DateTime ArrivalTime, DateTime DepartureTime, int NumberOfPassenger, string Remarks)
    {
        BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();            
        string AuthKey = BaseLogic.GetAuthKey();
        int n = biz.InsertArrivalData(AuthKey,ReservationLineItemID, ArrivalSeq, Airline, Flight, FromCity, ToCity, ArrivalTime, DepartureTime, NumberOfPassenger, Remarks);
        return n;
    }
    */

    /*
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public static bool UpdateArrivalDataByArrivalID(int ArrivalID, int ArrivalSeq, string Airline, string Flight, string FromCity, string ToCity, DateTime ArrivalTime, DateTime DepartureTime, int NumberOfPassenger, string Remarks, string Enabled)
    {
        BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
        string AuthKey = BaseLogic.GetAuthKey();
        bool b = biz.UpdateArrivalDataByArrivalID(AuthKey, ArrivalID, ArrivalSeq, Airline, Flight, FromCity, ToCity, ArrivalTime, DepartureTime, NumberOfPassenger, Remarks, Enabled);
        return b;
    }
    */

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
    public static bool DisableArrivalDataByArrivalID(int ArrivalID)
    {
      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      bool b = biz.DisableArrivalDataByArrivalID(AuthKey, ArrivalID);
      return b;
    }

    #endregion

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public App_Data.MaidAssignmentDS.MaidAssignmentDataTable GetMaidAssignment(int MaidID)
    {
      App_Data.MaidAssignmentDS ma = new App_Data.MaidAssignmentDS();
      ma.LoadDataByMaidID(MaidID);
      return ma.MaidAssignment;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public int GetCurrentUserID()
    {
      BaseLogic.UserData ud = BaseLogic.GetUserData();
      return ud.UserID;
    }



    public static void InsertNomadLog(string Page, string Type, string Terms)
    {
      string IP = HttpContext.Current.Request.UserHostAddress;
      BaseLogic.UserData ud = BaseLogic.GetUserData();
      int UserID = ud.UserID;
      BaseLogic.AdHoc("EXEC spNomadInsertLog '" + Page + "', '" + Type + "', '" + Terms + "', '" + IP + "', " + UserID + string.Empty);
    }

    public static void LogEvent(string Event)
    {
      App_Data.EventLogDS eventLog = new App_Data.EventLogDS();
      App_Data.EventLogDS.EventLogRow evr = eventLog.EventLog.NewEventLogRow();
      evr.ID = Guid.NewGuid().ToString();
      evr.DateTime = DateTime.Now;
      evr.Event = Event;
      eventLog.EventLog.AddEventLogRow(evr);
      BaseLogic.UpdateDataSet(eventLog);
    }

    public static void LogVendorChange(string TableName, int ID, int VendorBaseID)
    {
      BaseLogic.UserData ud = BaseLogic.GetUserData();
      App_Data.VendorChangeLogDS v = new App_Data.VendorChangeLogDS();
      App_Data.VendorChangeLogDS.VendorChangeLogRow r = v.VendorChangeLog.NewVendorChangeLogRow();
      r.VendorChangeLog_ID = BaseLogic.GetNextKey("VendorChangeLog");
      r.TableName = TableName;
      r.ID = ID;
      r.MagUserID = ud.UserID;
      r.ChangeDateTime = DateTime.Now;
      r.VendorBaseID = VendorBaseID;
      v.VendorChangeLog.AddVendorChangeLogRow(r);
      try
      {
        BaseLogic.UpdateDataSet(v);
      }
      catch { }
    }

    public static void LogVendorChange(string TableName, string ID, string VendorBaseID)
    {
      BaseLogic.UserData ud = BaseLogic.GetUserData();
      App_Data.VendorChangeLogDS v = new App_Data.VendorChangeLogDS();
      App_Data.VendorChangeLogDS.VendorChangeLogRow r = v.VendorChangeLog.NewVendorChangeLogRow();
      r.VendorChangeLog_ID = BaseLogic.GetNextKey("VendorChangeLog");
      r.TableName = TableName;
      r.ID = Convert.ToInt32(ID);
      r.MagUserID = ud.UserID;
      r.ChangeDateTime = DateTime.Now;
      r.VendorBaseID = Convert.ToInt32(VendorBaseID);
      v.VendorChangeLog.AddVendorChangeLogRow(r);
      BaseLogic.UpdateDataSet(v);
    }

    public static string GetLastVendorChange(int VendorBaseID)
    {
      DataSet ds = BaseLogic.AdHoc("SELECT TOP 1 VendorChangeLog.ChangeDateTime, MagUser.FullName FROM VendorChangeLog INNER JOIN MagUser ON VendorChangeLog.MagUserID = MagUser.UserID WHERE (VendorChangeLog.VendorBaseID = " + VendorBaseID + ") ORDER BY VendorChangeLog.ChangeDateTime DESC");
      string LastChange = string.Empty;
      try
      {
        DataRow r = ds.Tables[0].Rows[0];
        DateTime dt = Convert.ToDateTime(r[0]);

        LastChange = "Last Change: " + dt + ".    Changed By: " + r[1].ToString();
      }
      catch { }
      return LastChange;



    }

    [Obsolete("Obsolete, use BaseLogic.CopyProperies instead", true)]
    public static void CloneMinRes(BusinessLogic.ReservationMinimalReservation rmr, BusinessLogic.ReservationMinimalReservation rmr2)
    {
      rmr.ChangeDescription = rmr2.ChangeDescription;
      rmr.ClientID = rmr2.ClientID;
      rmr.ContactAgentID = rmr2.ContactAgentID;
      rmr.Currency = rmr2.Currency;
      rmr.DateFrom = rmr2.DateFrom;
      rmr.DateTo = rmr2.DateTo;
      rmr.GrossPrice = rmr2.GrossPrice;
      rmr.ItineraryID = rmr2.ItineraryID;
      rmr.MessageStatusKindID = rmr2.MessageStatusKindID;
      rmr.MessageToVendor = rmr2.MessageToVendor;
      rmr.Note = rmr2.Note;
      rmr.NumAdults = rmr2.NumAdults;
      rmr.NumChildren = rmr2.NumChildren;
      rmr.NumRooms = rmr2.NumRooms;
      rmr.OtherChargeAmount = rmr2.OtherChargeAmount;
      rmr.OtherChargeDesc = rmr2.OtherChargeDesc;
      rmr.ParentRLIID = rmr2.ParentRLIID;
      rmr.ProductBaseID = rmr2.ProductBaseID;
      rmr.RateRemarks = rmr2.RateRemarks;
      rmr.ReservationKindID = rmr2.ReservationKindID;
      rmr.ReservationNumber = rmr2.ReservationNumber;
      rmr.ServiceCharge = rmr2.ServiceCharge;
      rmr.Status = rmr2.Status;
      rmr.Tax = rmr2.Tax;
      rmr.VendorBaseID = rmr2.VendorBaseID;
    }


    public static void CopyProperties(object dest, object src)
    {
      foreach (PropertyDescriptor item in TypeDescriptor.GetProperties(src))
      {
        item.SetValue(dest, item.GetValue(src));
      }
    }



    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.Message GetNewMessageObject()
    {
      BusinessLogic.Message m = new BusinessLogic.Message();
      return m;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
    public static BusinessLogic.MessageReturnData InsertNewMessageObject(BusinessLogic.Message message)
    {
      BusinessLogic.MessageReturnData mrd = new BusinessLogic.MessageReturnData();
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      mrd = bizLogic.InsertNewMessageObject(AuthKey, message);
      return mrd;
    }



    // PhoneNumberContact support. -jk 010714.

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Fill, true)]
    public static BusinessLogic.PhoneNumberContactDS phonenumbercontact_GetAllNumbers()
    {
      BusinessLogic.PhoneNumberContactDS pn = new BusinessLogic.PhoneNumberContactDS();
      return pn;
    }



    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.PhoneNumberContactDS phonenumbercontact_GetAllNumbers(int PhoneNumberSourceID, int SourceKindID)
    {
      BusinessLogic.PhoneNumberContactDS pn = new BusinessLogic.PhoneNumberContactDS();
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      pn = bizLogic.phonenumbercontact_GetAllNumbers(AuthKey, PhoneNumberSourceID, SourceKindID);
      return pn;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public static BusinessLogic.PhoneNumberContactDS phonenumbercontact_UpdateRow(BusinessLogic.PhoneNumberContactDS.PhoneNumberContactRow pcr)
    {
      BusinessLogic.PhoneNumberContactDS pn = new BusinessLogic.PhoneNumberContactDS();
      //BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      //string AuthKey = BaseLogic.GetAuthKey();
      //pn = bizLogic.phonenumbercontact_GetAllNumbers(AuthKey, PhoneNumberSourceID, SourceKindID);
      return pn;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
    public static BusinessLogic.PhoneNumberContactDS phonenumbercontact_InsertRow(BusinessLogic.PhoneNumberContactDS.PhoneNumberContactRow pcr)
    {
      BusinessLogic.PhoneNumberContactDS pn = new BusinessLogic.PhoneNumberContactDS();
      //BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      //string AuthKey = BaseLogic.GetAuthKey();
      //pn = bizLogic.phonenumbercontact_GetAllNumbers(AuthKey, PhoneNumberSourceID, SourceKindID);
      return pn;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
    public static BusinessLogic.PhoneNumberContactDS phonenumbercontact_DeleteRow(BusinessLogic.PhoneNumberContactDS.PhoneNumberContactRow pcr)
    {
      BusinessLogic.PhoneNumberContactDS pn = new BusinessLogic.PhoneNumberContactDS();
      //BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      //string AuthKey = BaseLogic.GetAuthKey();
      //pn = bizLogic.phonenumbercontact_GetAllNumbers(AuthKey, PhoneNumberSourceID, SourceKindID);
      return pn;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Fill, true)]
    public static BusinessLogic.PhoneNumberContactDS phonenumbercontact_fill()
    {
      BusinessLogic.PhoneNumberContactDS pn = new BusinessLogic.PhoneNumberContactDS();
      //BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      //string AuthKey = BaseLogic.GetAuthKey();
      //pn = bizLogic.phonenumbercontact_GetAllNumbers(AuthKey, PhoneNumberSourceID, SourceKindID);
      return pn;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.PhoneNumberContact phonenumbercontact_GetPhoneNumber(int PhoneNumberContactID, string SourceTable)
    {
      BusinessLogic.PhoneNumberContact pnc = new BusinessLogic.PhoneNumberContact();
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      pnc = bizLogic.phonenumbercontact_GetPhoneNumberContact(AuthKey, PhoneNumberContactID, SourceTable);
      return pnc;



    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
    public static BusinessLogic.PhoneNumberContactDataReturn phonenumbercontact_InsertPhoneNumberContact(BusinessLogic.PhoneNumberContact pnc)
    {
      BusinessLogic.PhoneNumberContactDataReturn pncdr = new BusinessLogic.PhoneNumberContactDataReturn();
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      pncdr = bizLogic.phonenumbercontact_InsertPhoneNumberContact(AuthKey, pnc);
      return pncdr;



    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public static BusinessLogic.PhoneNumberContactDataReturn phonenumbercontact_UpdatePhoneNumberContact(BusinessLogic.PhoneNumberContact pnc)
    {
      BusinessLogic.PhoneNumberContactDataReturn pncdr = new BusinessLogic.PhoneNumberContactDataReturn();
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      pncdr = bizLogic.phonenumbercontact_UpdatePhoneNumberContact(AuthKey, pnc);
      return pncdr;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
    public static BusinessLogic.PhoneNumberContactDataReturn phonenumbercontact_DeletePhoneNumberContact(BusinessLogic.PhoneNumberContact pnc)
    {
      BusinessLogic.PhoneNumberContactDataReturn pncdr = new BusinessLogic.PhoneNumberContactDataReturn();
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      pncdr = bizLogic.phonenumbercontact_DeletePhoneNumberContact(AuthKey, pnc);
      return pncdr;
    }


    // FCC object support. -jk 120513.

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Fill, true)]
    public static BusinessLogic.FCCobj fcc_GetEmptyFCC()
    {
      BusinessLogic.FCCobj fcc = new BusinessLogic.FCCobj();
      return fcc;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.FCCobj fcc_GetFCC(int CallbackID)
    {
      BusinessLogic.FCCobj fcc = new BusinessLogic.FCCobj();
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      fcc = bizLogic.fcc_GetFCC(AuthKey, CallbackID);
      return fcc;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
    public static BusinessLogic.FCCDataReturn fcc_InsertFCC(BusinessLogic.FCCobj fcc)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      BusinessLogic.FCCDataReturn fccDR = new BusinessLogic.FCCDataReturn();
      fccDR = bizLogic.fcc_InsertFCC(AuthKey, fcc);
      return fccDR;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public static BusinessLogic.FCCDataReturn fcc_UpdateFCC(BusinessLogic.FCCobj fcc)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      BusinessLogic.FCCDataReturn fccDR = new BusinessLogic.FCCDataReturn();
      fccDR = bizLogic.fcc_UpdateFCC(AuthKey, fcc);
      return fccDR;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
    public static BusinessLogic.FCCDataReturn fcc_DeleteFCC(BusinessLogic.FCCobj fcc)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      string AuthKey = BaseLogic.GetAuthKey();
      BusinessLogic.FCCDataReturn fccDR = new BusinessLogic.FCCDataReturn();
      fccDR = bizLogic.fcc_DeleteFCC(AuthKey, fcc);
      return fccDR;
    }
    // flat Client
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.flatClient flatclient_GetClient(int ClientID)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.flatClient fc = new BusinessLogic.flatClient();
      string AuthKey = BaseLogic.GetAuthKey();
      fc = bizLogic.flatclient_GetClient(AuthKey, ClientID);

      return fc;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
    public static BusinessLogic.flatClientDataReturn flat_client_InsertClient(BusinessLogic.flatClient flatClient)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.flatClientDataReturn cdr = new BusinessLogic.flatClientDataReturn();
      string AuthKey = BaseLogic.GetAuthKey();
      cdr = bizLogic.flat_client_InsertClient(AuthKey, flatClient);

      return cdr;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public static BusinessLogic.flatClientDataReturn flat_client_UpdateClient(BusinessLogic.flatClient flatClient)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.flatClientDataReturn cdr = new BusinessLogic.flatClientDataReturn();
      string AuthKey = BaseLogic.GetAuthKey();
      cdr = bizLogic.flat_client_UpdateClient(AuthKey, flatClient);

      return cdr;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
    public static BusinessLogic.flatClientDataReturn flat_client_DisableClient(BusinessLogic.flatClient flatClient)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.flatClientDataReturn cdr = new BusinessLogic.flatClientDataReturn();
      string AuthKey = BaseLogic.GetAuthKey();
      //   cdr = bizLogic.flat_client_DisableClient(AuthKey, BusinessLogic.flatClient flatClient);
      cdr = bizLogic.flat_client_DisableClient(AuthKey, flatClient);
      return cdr;
    }


    // full client
    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public static BusinessLogic.FullClient fullclient_GetClient(int ClientID)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.FullClient fc = new BusinessLogic.FullClient();
      string AuthKey = BaseLogic.GetAuthKey();
      fc = bizLogic.fullclient_GetClient(AuthKey, ClientID);
      return fc;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
    public static BusinessLogic.FullClientDataReturn fullclient_InsertClient(BusinessLogic.FullClient fullClient)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.FullClientDataReturn cdr = new BusinessLogic.FullClientDataReturn();
      string AuthKey = BaseLogic.GetAuthKey();
      cdr = bizLogic.fullclient_InsertClient(AuthKey, fullClient);
      return cdr;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public static BusinessLogic.FullClientDataReturn fullclient_UpdateClient(BusinessLogic.FullClient fullClient)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.FullClientDataReturn cdr = new BusinessLogic.FullClientDataReturn();
      string AuthKey = BaseLogic.GetAuthKey();
      cdr = bizLogic.fullclient_UpdateClient(AuthKey, fullClient);
      return cdr;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
    public static BusinessLogic.FullClientDataReturn fullclient_DeleteClient(BusinessLogic.FullClient fullClient)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.FullClientDataReturn cdr = new BusinessLogic.FullClientDataReturn();
      string AuthKey = BaseLogic.GetAuthKey();
      cdr = bizLogic.fullclient_DeleteClient(AuthKey, fullClient);
      return cdr;
    }

    // msg thread insert hook here. -jk 052112.

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public static BusinessLogic.MessageReturnData UpdateMessageObject(BusinessLogic.Message message)
    {
      BusinessLogic.MessageReturnData mrd = new BusinessLogic.MessageReturnData();
      return mrd;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
    public static BusinessLogic.MessageReturnData DeleteMessageObject(BusinessLogic.Message message)
    {
      BusinessLogic.MessageReturnData mrd = new BusinessLogic.MessageReturnData();
      return mrd;
    }


    public static BusinessLogic.UpdateDataSetResults InsertNewMessage(string ReservationNumber, string MessageBody, int MessageKindID, int MocID, int ByWho, int FromWho, int ToWho, string FromContact, string ToContact, int MessageType, int MessageStatusID, int EventKindID)
    {
      BusinessLogic.WimcoBusinessLogic bizLogic = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.UpdateDataSetResults udr = new BusinessLogic.UpdateDataSetResults();
      string AuthKey = BaseLogic.GetAuthKey();
      udr = bizLogic.InsertNewMessage(AuthKey, ReservationNumber, MessageBody, MessageKindID, MocID, ByWho, FromWho, ToWho, FromContact, ToContact, MessageType, MessageStatusID, EventKindID);
      return udr;
    }


    // lbRedirect section

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public App_Data.OpenCampaignsDS GetAllOpenCampaigns()
    {
      App_Data.OpenCampaignsDS oc = new App_Data.OpenCampaignsDS();
      oc.LoadOpenCampaigns();
      return oc;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
    public App_Data.CampaignLinksDS GetCampaignLinks(string EmailCampaignID)
    {
      App_Data.CampaignLinksDS cl = new App_Data.CampaignLinksDS();
      cl.GetCampainLinks(EmailCampaignID);
      return cl;
    }

    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
    public void UpdateCampainLink(string Key, string TargetURL, string Note)
    {


      UserData ud = GetUserData();

      string SQL = "UPDATE EmailCampaignLink SET  LinkURL = '" + TargetURL + "', Note = '" + Note + "', ModifiedDate = { fn NOW() }, ModifiedBy = " + ud.UserID + " WHERE (EmailCampaignRowID = '" + Key + "')";
      BaseLogic.AdHoc(SQL);




    }


    // lbRedirect section ends.


    public static void sendEmail(string Subject, string Email, string Message)
    {
      System.Net.Mail.MailMessage emsg = new System.Net.Mail.MailMessage();
      System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("mail.wimco.com");

      System.Net.Mail.MailAddress add = new System.Net.Mail.MailAddress(Email, Email);
      emsg.To.Add(add);
      emsg.From = new System.Net.Mail.MailAddress("villaowner.e@wimco.com", "villaowner.e@wimco.com");
      emsg.ReplyTo = new System.Net.Mail.MailAddress("villaowner.e@wimco.com", "villaowner.e@wimco.com");
      emsg.Subject = Subject;
      emsg.IsBodyHtml = false;
      emsg.Body = Message;
      try
      {
        lock (smtp)
        {
          smtp.Send(emsg);
        }

      }
      catch { }
    }

    public static void sendHTMLEmail(string FromAddress, string FromDisplayName, string Subject, string Email, string Message)
    {
      System.Net.Mail.MailMessage emsg = new System.Net.Mail.MailMessage();
      System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("mail.wimco.com");

      System.Net.Mail.MailAddress add = new System.Net.Mail.MailAddress(Email, Email);
      emsg.To.Add(add);
      emsg.From = new System.Net.Mail.MailAddress(FromAddress, FromDisplayName);
      emsg.ReplyTo = new System.Net.Mail.MailAddress(FromAddress, FromDisplayName);

      emsg.Subject = Subject;
      emsg.IsBodyHtml = true;
      emsg.Body = Message;
      try
      {
        lock (smtp)
        {
          smtp.Send(emsg);
        }

      }
      catch { }
    }
    public static void sendHTMLEmail(string FromAddress, string FromDisplayName, string Subject, string Email, string CCAddress, string Message)
    {
      System.Net.Mail.MailMessage emsg = new System.Net.Mail.MailMessage();
      System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.wimco.com");
      char[] delimiterChars = { ',', ';' };

      string EmailTo = Email;
      string[] to;

      to = EmailTo.Split(delimiterChars);

      foreach (string s in to)
      {
        System.Net.Mail.MailAddress t = new System.Net.Mail.MailAddress(s, s);
        emsg.To.Add(t);
      }

      emsg.From = new System.Net.Mail.MailAddress(FromAddress, FromDisplayName);
      emsg.ReplyTo = new System.Net.Mail.MailAddress(FromAddress, FromDisplayName);

      if (CCAddress != string.Empty)
      {
        System.Net.Mail.MailAddress CC = new System.Net.Mail.MailAddress(CCAddress, CCAddress);


        string[] CCs = CCAddress.Split(delimiterChars);
        foreach (string s2 in CCs)
        {
          System.Net.Mail.MailAddress t2 = new System.Net.Mail.MailAddress(s2, s2);

          emsg.CC.Add(t2);
          //  esmg.BCC.Add(esmg.From);


        }
        if (!emsg.CC.Contains(new System.Net.Mail.MailAddress(FromAddress, FromAddress)))
        {
          emsg.CC.Add(emsg.From);
        }
      }
      else
      {

        emsg.CC.Add(emsg.From);
      }

      emsg.Bcc.Add(new System.Net.Mail.MailAddress("dwhite@wimco.com", "dwhite@wimco.com"));
      emsg.Subject = Subject;
      emsg.IsBodyHtml = true;
      emsg.Body = Message;
      try
      {
        emsg.Headers.Add("X-Wimco-ClientID", HttpContext.Current.Session["ClientID"].ToString());
      }
      catch { }
      try
      {
        emsg.Headers.Add("X-Wimco-ReservationID", HttpContext.Current.Session["ParentRLI"].ToString());
      }
      catch { }
      try
      {
        emsg.Headers.Add("X-Wimco-ItineraryID", HttpContext.Current.Session["CurrentItinerary"].ToString());
      }
      catch { }
      try
      {
        lock (smtp)
        {
          smtp.Send(emsg);
        }

      }
      catch { }
      InsertNomadLog(HttpContext.Current.Request.Url.ToString(), "Send Email", EmailTo);
    }
    public static void sendHTMLEmail(string FromAddress, string FromDisplayName, string Subject, string Email, string CCAddress, string Message, Attachment[] attach)
    {
      System.Net.Mail.MailMessage emsg = new System.Net.Mail.MailMessage();
      System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.wimco.com");
      char[] delimiterChars = { ',', ';' };

      string EmailTo = Email;
      string[] to;

      to = EmailTo.Split(delimiterChars);

      foreach (string s in to)
      {
        System.Net.Mail.MailAddress t = new System.Net.Mail.MailAddress(s, s);
        emsg.To.Add(t);
      }

      emsg.From = new System.Net.Mail.MailAddress(FromAddress, FromDisplayName);
      emsg.ReplyTo = new System.Net.Mail.MailAddress(FromAddress, FromDisplayName);

      if (CCAddress != string.Empty)
      {
        System.Net.Mail.MailAddress CC = new System.Net.Mail.MailAddress(CCAddress, CCAddress);


        string[] CCs = CCAddress.Split(delimiterChars);
        foreach (string s2 in CCs)
        {
          System.Net.Mail.MailAddress t2 = new System.Net.Mail.MailAddress(s2, s2);

          emsg.CC.Add(t2);
          //  esmg.BCC.Add(esmg.From);


        }
        if (!emsg.CC.Contains(new System.Net.Mail.MailAddress(FromAddress, FromAddress)))
        {
          emsg.CC.Add(emsg.From);
        }
      }
      else
      {

        emsg.CC.Add(emsg.From);
      }
      try
      {
        for (int i = 0; i < attach.Length; i++)
          emsg.Attachments.Add(attach[i]);
      }
      catch { }
      emsg.Bcc.Add(new System.Net.Mail.MailAddress("dwhite@wimco.com", "dwhite@wimco.com"));
      emsg.Subject = Subject;
      emsg.IsBodyHtml = true;
      emsg.Body = Message;
      try
      {
        emsg.Headers.Add("X-Wimco-ClientID", HttpContext.Current.Session["ClientID"].ToString());
      }
      catch { }
      try
      {
        emsg.Headers.Add("X-Wimco-ReservationID", HttpContext.Current.Session["ParentRLI"].ToString());
      }
      catch { }
      try
      {
        emsg.Headers.Add("X-Wimco-ItineraryID", HttpContext.Current.Session["CurrentItinerary"].ToString());
      }
      catch { }
      try
      {
        lock (smtp)
        {
          smtp.Send(emsg);
        }

      }
      catch { }
      InsertNomadLog(HttpContext.Current.Request.Url.ToString(), "Send Email", EmailTo);
    }
    public static WimcoBaseLogic.BusinessLogic.GWEmailToSendReturn sendGWEmail(string FromAddress, string FromDisplayName, string Subject, string Email, string CCAddress, string Message, WimcoBaseLogic.BusinessLogic.UploadTicket[] attach)
    {
      UserData ud = GetUserData();
      WimcoBaseLogic.BusinessLogic.GWEmailToSendReturn gwsr = new WimcoBaseLogic.BusinessLogic.GWEmailToSendReturn();
      int MagUserID = ud.UserID;
      string body = Message;
      string ParentRLI;
      string CurrentItinerary;
      string ClientID;


      try
      {
        ClientID = HttpContext.Current.Session["ClientID"].ToString();
      }
      catch
      {
        ClientID = "";
      }
      try
      {
        CurrentItinerary = HttpContext.Current.Session["CurrentItinerary"].ToString();
      }
      catch
      {
        CurrentItinerary = "";
      }
      try
      {
        ParentRLI = HttpContext.Current.Session["ParentRLI"].ToString();
      }
      catch
      {
        ParentRLI = "";
      }
      List<string> xHeadersList = new List<string>();
      DataSet dsXHeaders = AdHoc("Select XHeaderName From Groupwise_XheaderID");
      for (int x = 0; x < dsXHeaders.Tables[0].Rows.Count; x++)
      {
        string xHeader = dsXHeaders.Tables[0].Rows[x][0].ToString();
        switch (xHeader)
        {
          case "X-Wimco-ClientID":
            if (ClientID != "")
            {
              xHeadersList.Add("X-Wimco-ClientID=" + ClientID);
            }
            break;
          case "X-Wimco-ReservationNumber":
            if (ParentRLI != "")
            {
              xHeadersList.Add("X-Wimco-ReservationNumber=" + ParentRLI);
            }
            break;
          case "X-Wimco-ItineraryID":
            if (CurrentItinerary != "")
            {
              xHeadersList.Add("X-Wimco-ItineraryID=" + CurrentItinerary);
            }
            break;
          case "X-Wimco-DestinationID":
            if (HttpContext.Current.Session["DID"] != null)
            {
              string DID = HttpContext.Current.Session["DID"].ToString();
              xHeadersList.Add("X-Wimco-DestinationID=" + DID);
            }
            break;
          case "X-Wimco-ProductBaseID":
            break;
          case "X-Wimco-ReservationStartDate":
            if (HttpContext.Current.Session["ParentStartDate"] != null)
            {
              string PSD = HttpContext.Current.Session["PSD"].ToString();
              xHeadersList.Add("X-Wimco-ReservationStartDate=" + PSD);
            }


            break;
          case "X-Wimco-ReservationEndDate":
            if (HttpContext.Current.Session["ParentEndDate"] != null)
            {
              string PED = HttpContext.Current.Session["PED"].ToString();
              xHeadersList.Add("X-Wimco-ReservationEndDate=" + PED);
            }
            break;
          case "X-Wimco-ItineraryStartDate":
            break;
          case "X-Wimco-ItineraryEndDate":
            break;
          case "X-Wimco-ReservationVendorBaseID":
            break;
          case "X-Wimco-AgentID":
            xHeadersList.Add("X-Wimco-AgentID=" + MagUserID.ToString());
            break;
          case "X-Wimco-TravelAgentID":
            break;
          case "X-Wimco-TravelAgentVendorID":
            break;
          case "X-Wimco-AdditionalTravelerID":
            break;
          case "X-Wimco-Heat":
          default: break;


        }
      }

      string[] xHeaders = xHeadersList.ToArray();
      string[] CCs;
      //   char[] delimiterChars = { ',', ';' };
      int ccCount = 0;
      if (CCAddress != "")
      {
        CCs = CCAddress.Split(',', ';');
        ccCount = CCs.Length;

      }
      else
      {
        CCs = null;
      }
      string EmailTo = Email;
      string[] to;

      to = EmailTo.Split(',', ';');
      int recipientCount = to.Length + ccCount;
      int c = 0;
      WimcoBaseLogic.BusinessLogic.Recipient[] recipient = new WimcoBaseLogic.BusinessLogic.Recipient[recipientCount + 1];
      foreach (string s in to)
      {
        String RecipientName = "";

        WimcoBaseLogic.BusinessLogic.Recipient r = new WimcoBaseLogic.BusinessLogic.Recipient();
        try
        {
          if (s.ToLower() == HttpContext.Current.Session["ClientEmail"].ToString().ToLower())
          {
            if (HttpContext.Current.Session["ClientName"] != null)
            {
              RecipientName = HttpContext.Current.Session["ClientName"].ToString();
            }
          }
          else
          {
            RecipientName = s;
          }
        }
        catch
        {
          RecipientName = s;
        }
        r.email = s;
        r.displayName = RecipientName;
        r.distType = WimcoBaseLogic.BusinessLogic.DistributionType.TO;
        r.recipType = WimcoBaseLogic.BusinessLogic.RecipientType.User;
        recipient[c] = r;
        c++;
      }
      if (CCAddress != "")
      {
        foreach (string s2 in CCs)
        {
          WimcoBaseLogic.BusinessLogic.Recipient r = new WimcoBaseLogic.BusinessLogic.Recipient();


          r.email = s2;
          r.displayName = s2;
          r.distType = WimcoBaseLogic.BusinessLogic.DistributionType.CC;
          r.recipType = WimcoBaseLogic.BusinessLogic.RecipientType.User;
          recipient[c] = r;
          c++;
        }
      }
      WimcoBaseLogic.BusinessLogic.Recipient bcc = new WimcoBaseLogic.BusinessLogic.Recipient();
      bcc.email = "webmaster@wimco.com";
      bcc.displayName = "Webmaster";
      bcc.distType = WimcoBaseLogic.BusinessLogic.DistributionType.BC;
      bcc.recipType = WimcoBaseLogic.BusinessLogic.RecipientType.User;
      recipient[c] = bcc;

      // debug point 051017 jk/da   dump content.
      //System.IO.StreamWriter sw = new StreamWriter("c:/emailtest.txt");
      //sw.WriteLine(body);
      //sw.Flush();
      //sw.Close();
      // end debug point


      gwsr = WimcoBaseLogic.BaseLogic.Groupwise_SendGWEmail(MagUserID, Subject, body, null, recipient, xHeaders, attach);
      if (!gwsr.Success)
      {
        string ErrMsg = gwsr.Message;
        int StatusCode = gwsr.StatusCode;
        WimcoBaseLogic.BaseLogic.SendSlackDebugMessage("GW Email Send Fail: " + ErrMsg + "  StatusCode: " + StatusCode.ToString());
      }

      InsertNomadLog(HttpContext.Current.Request.Url.ToString(), "Send Email", EmailTo);
      return gwsr;
    }
    public static string SerializeAnObject(object AnObject)
    {
      XmlSerializer Xml_Serializer = new XmlSerializer(AnObject.GetType());
      StringWriter Writer = new StringWriter();
      Xml_Serializer.Serialize(Writer, AnObject);
      return Writer.ToString();
    }

    public static Object DeSerializeAnObject(string XmlOfAnObject, Type ObjectType)
    {
      StringReader StrReader = new StringReader(XmlOfAnObject);
      XmlSerializer Xml_Serializer = new XmlSerializer(ObjectType);
      XmlTextReader XmlReader = new XmlTextReader(StrReader);
      try
      {
        Object AnObject = Xml_Serializer.Deserialize(XmlReader);
        return AnObject;
      }
      finally
      {
        XmlReader.Close();
        StrReader.Close();
      }
    }

    public static string EncodeTo64(string toEncode)
    {
      byte[] toEncodeAsBytes = System.Text.Encoding.Unicode.GetBytes(toEncode);
      string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
      return returnValue;
    }

    public static string DecodeFrom64(string encodedData)
    {
      byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
      string returnValue = System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);
      return returnValue;
    }

  }



}
