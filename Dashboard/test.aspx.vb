Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls

Partial Public Class villas_test
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ShowSessions()
        ShowHttpHeader()
        ShowCookie()
        ShowFormValues()
        If Not Request.Cookies("WimcoReferrer") Is Nothing Then
            litCookie.Text = Mid(Server.HtmlEncode(Request.Cookies("WimcoReferrer").Value), 10)
        End If
        ShowHeaders()
    End Sub
#Region "Headers"
    Protected Sub ShowHeaders()
        Dim headers As String = String.Empty
        Dim loop1, loop2 As Integer
        Dim arr1(), arr2() As String
        Dim coll As NameValueCollection


        ' Load Header collection into NameValueCollection object.
        coll = Request.Headers

        ' Put the names of all keys into a string array.
        arr1 = coll.AllKeys
        For loop1 = 0 To arr1.GetUpperBound(0)
            headers = String.Format("{0}{1}", headers, "Key: " & arr1(loop1) & "<br>")
            arr2 = coll.GetValues(loop1)
            ' Get all values under this key. 
            For loop2 = 0 To arr2.GetUpperBound(0)
                headers = String.Format("{0}{1}", headers, "Value " & CStr(loop2) & ": " & Server.HtmlEncode(arr2(loop2)) & "<br>")
            Next loop2
        Next loop1


        'Dim headers As String = String.Empty
        'For Each key As String In Request.Headers.AllKeys
        '    headers &= key & "=" & Request.Headers(key) & "<br />"
        'Next
        litHeaderValues.Text = headers

    End Sub

    Protected Sub ShowHttpHeader()
        'Dim sMe As String = HttpContext.Current.Response.Headers.Item("User-Agent")
        'lblUserAgent.Text = sMe
    End Sub
#End Region

#Region "Cookies"
    Protected Sub btnKillCookies_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        HttpContext.Current.Response.Cookies.Clear()
        Dim lCookieNames As New List(Of String)

        For Each sCookieName As String In HttpContext.Current.Request.Cookies
            lCookieNames.Add(sCookieName)
        Next
        For Each sCookieName As String In lCookieNames
            Dim objCookie As HttpCookie = New HttpCookie(sCookieName)
            objCookie.Expires = DateTime.Today.AddMonths(-1)
            HttpContext.Current.Request.Cookies.Add(objCookie)
            HttpContext.Current.Response.Cookies.Add(objCookie)
        Next
    End Sub
    Protected Sub btnKillOneCookie_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If tbCookieName.Text <> "" Then
            Dim sCookieName As String = tbCookieName.Text
            Dim objCookie As HttpCookie = New HttpCookie(sCookieName)
            objCookie.Expires = DateTime.Today.AddDays(-1)
            HttpContext.Current.Request.Cookies.Add(objCookie)
            HttpContext.Current.Response.Cookies.Add(objCookie)
        End If
    End Sub
    Protected Sub ShowCookie()
        Dim sCookie As String = ""
        'cMyCookie = HttpContext.Current.Request.Cookies("WimcoLegacy")
        Dim MyCookieCollection As HttpCookieCollection
        Dim MyCookie As HttpCookie
        Dim MyKeyNames() As String
        Dim MyValues() As String
        Dim loop1 As Integer
        Response.Write("<table border=1><tr><td width=10%>&nbsp;</td><td width=10%>&nbsp;</td><td width=80%>&nbsp;</td></tr>")
        MyCookieCollection = Request.Cookies
        For loop1 = 0 To MyCookieCollection.Count - 1
            MyCookie = MyCookieCollection(loop1)
            Response.Write("<tr><th colspan=3 align=left>Cookie: " & MyCookie.Name & "</th></tr>")
            Response.Write("<tr><td>&nbsp;</td><td colspan=2>Secure:" & MyCookie.Secure & "</td></tr>")
            Response.Write("<tr><td>&nbsp;</td><td colspan=2>Value: " & myCookie.Value.ToString & "</td></tr>")
            ' Grab all values for single cookie into an object array.

            If MyCookie.HasKeys Then
                Dim MyCookieValues As NameValueCollection = New NameValueCollection(MyCookie.Values)
                MyKeyNames = MyCookieValues.AllKeys
                For Each KeyName As String In MyKeyNames
                    MyValues = MyCookieValues.GetValues(KeyName)
                    Dim sValues As String = ""
                    If MyValues.Length > 1 Then
                        For Each sValue As String In MyValues
                            sValues = String.Format("{0}, {1}", sValues, sValue)
                        Next
                    Else
                        sValues = myValues(0)
                    End If
                    Response.Write("<tr><td>&nbsp;</td><td>&nbsp;</td><td>" & KeyName & ": " & sValues & "</td></tr>")
                Next
            End If
        Next loop1
        Response.Write("</table>")
    End Sub

#End Region

#Region "Sessions"
    Protected Sub ShowSessions()

        Dim arS, sKey As String
        arS = ""
        sKey = ""
        For Each sKey In HttpContext.Current.Session.Keys
            Try
                arS = arS & sKey.ToString() & ": " & HttpContext.Current.Session(sKey).ToString() & "<br />"
            Catch
                arS = arS & "Nothing <br />"
            End Try

        Next
        lblSession.Text = arS
    End Sub

    Protected Sub btnKillSwitch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session.RemoveAll()
    End Sub
    Protected Sub btnKillOneSession_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btnMe As Button = sender
        Dim tbKillSessionName As TextBox = btnMe.NamingContainer.FindControl("tbKillSessionName")
        Dim sKillName As String = tbKillSessionName.Text
        HttpContext.Current.Session.Remove(sKillName)
    End Sub
    Protected Sub btnAddSession_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim btnMe As Button = sender
        Dim tbSessionName As TextBox = btnMe.NamingContainer.FindControl("tbSessionName")
        Dim tbSessionValue As TextBox = btnMe.NamingContainer.FindControl("tbSessionValue")
        Dim sName As String = tbSessionName.Text
        Dim sValue As String = tbSessionValue.Text
        Session(sName) = sValue
        Response.Redirect("test.aspx")
    End Sub
#End Region

    Protected Sub ShowFormValues()
        Dim loop1 As Integer
        Dim arr1() As String
        Dim coll As NameValueCollection
        Dim sbMy As StringBuilder = New StringBuilder

        ' Load Form variables into NameValueCollection variable.
        coll = Request.Form

        ' Get names of all forms into a stringbuilder.
        arr1 = coll.AllKeys
        For loop1 = 0 To arr1.GetUpperBound(0)
            Dim sKey As String = arr1(loop1)
            sbMy.Append("Form: " & arr1(loop1) & ": " & coll.Item(sKey).ToString() & "<br>")
        Next
        litFormValues.Text = sbMy.ToString()

    End Sub

    Protected Sub btnMakeMessage_Click(ByVal sender As Object, ByVal e As System.EventArgs)


    End Sub

End Class
