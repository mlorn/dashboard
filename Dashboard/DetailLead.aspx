﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetailLead.aspx.cs" Inherits="Dashboard.DetailLead" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Detail Lead</title>
	<script>
		function pageLoad() {
			setInterval("KeepSessionAlive()", 60000);
		}

		function KeepSessionAlive() {
			url = "/keepalive.ashx?";
			var xmlHttp = new XMLHttpRequest();
			xmlHttp.open("GET", url, true);
			xmlHttp.send();
		}

	</script>

	<style type="text/css">
		.auto-style2 {
			height: 23px;
		}

		.auto-style7 {
			width: 82px;
		}

		.auto-style15 {
			width: 39px;
			height: 26px;
		}

		.auto-style16 {
			height: 26px;
		}

		.auto-style22 {
			width: 14px;
			height: 26px;
		}

		.auto-style24 {
			height: 26px;
			width: 96px;
		}

		.auto-style25 {
			width: 72px;
		}

		.auto-style26 {
			width: 96px;
		}
		.auto-style27 {
			width: 10px;
		}
		.auto-style28 {
			width: 1021px;
		}
	</style>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<form id="form1" runat="server" title="Detail Lead">
		<div>
			<asp:ScriptManager ID="ScriptManager1" runat="server">
			</asp:ScriptManager>
			<asp:UpdatePanel ID="updPnlLeadDetail" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<table style="width: 100%;">
						<tr>
							<td class="auto-style27">&nbsp;</td>
							<td class="auto-style28">
								<div style="width: 600px; height: auto;">


									<asp:DetailsView ID="dvLeadDetail" runat="server" Height="50px" Width="500px" AutoGenerateRows="False" GridLines="None" DataKeyNames="ClientEmailAddress">
										<FieldHeaderStyle HorizontalAlign="Right" Width="180px" />
										<Fields>
											<asp:TemplateField HeaderText="Lead ID:&amp;nbsp;&amp;nbsp;" SortExpression="LeadID">
												<ItemTemplate>
													<asp:Label ID="lblLeadID" runat="server" Text='<%# Eval("LeadID") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Client ID:&amp;nbsp;&amp;nbsp;" SortExpression="ClientID">
												<ItemTemplate>
													<asp:Label ID="lblClientID" runat="server" Text='<%# Eval("ClientID") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Name:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblClientName" runat="server" Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Received Date:&amp;nbsp;&amp;nbsp;" SortExpression="RequestDate">
												<ItemTemplate>
													<asp:Label ID="lblRequestDate" runat="server" Text='<%# string.Format("{0:MMM dd, yyyy h:mm tt}", Eval("RequestDate")) %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Villa Code/Hotel Name:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblVillaCode" runat="server" Text='<%# Eval("VillaCode") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Email Address:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblMailAddress" runat="server" Text='<%# Eval("ClientEMailAddress") %>'> </asp:Label>
													<asp:LinkButton ID="lnkbtnEMailAddress" runat="server" CommandArgument='<%# string.Format("{0}:{1}", Eval("LeadID"), Eval("ClientEmailAddress")) %>' OnClick="lnkbtnEMailAddress_Click" Text="Open Nomad" ToolTip="Open the client record in Nomad."></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Phone Number:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblPhoneNumber" runat="server" Text='<%# Eval("PhoneNumber") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Comment:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblComment" runat="server" Text='<%# Eval("Comment") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="First Choice:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblDestinationName" runat="server" Text='<%# Eval("DestinationName") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Second Choice:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblSecondDestination" runat="server" Text='<%# Eval("SecondDestination") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Begin Date:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblDateFrom" runat="server" Text='<%# string.Format("{0:MMM dd, yyyy}", Eval("DateFrom")) %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="End Date:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblDateTo" runat="server" Text='<%# string.Format("{0:MMM dd, yyyy}", Eval("DateTo")) %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Number of Bedrooms:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblNumberOfBedroom" runat="server" Text='<%# Eval("NumberOfBedroom") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Minimum Price:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblMinimumPrice" runat="server" Text='<%# Eval("MinimumPrice") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Maximum Price:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblMaximumPrice" runat="server" Text='<%# Eval("MaximumPrice") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Must Have a Pool:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblRequirePool" runat="server" Text='<%# (Boolean.Parse(Eval("RequirePool").ToString())) ? "Yes" : string.Empty %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Travel with Children:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblTravelWithChildren" runat="server" Text='<%# Boolean.Parse(Eval("TravelWithChildren").ToString()) ? "Yes" : string.Empty %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Client Source Code:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblContactCodeName" runat="server" Text='<%# Eval("ContactCode") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Web-Inq Source Code:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblWebInqContactSourceCodeName" runat="server" Text='<%# Eval("WebInqContactSourceCode") %>'></asp:Label>
												</ItemTemplate>
												<HeaderStyle Wrap="False" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Recent Agent:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblRecentResvAgentName" runat="server" Text='<%# Eval("RecentResvAgentName") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Claim Agent:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblOwned" runat="server" Text='<%# Eval("ResvAgent") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Claim Agent:&amp;nbsp;&amp;nbsp;" Visible="false">
												<ItemTemplate>
													<asp:Label ID="lblMagUserID1" runat="server" Text='<%# Bind("MagUserID") %>' Visible="false" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Claim Date:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblAssignDate" runat="server" Text='<%# string.Format("{0:MMM dd, yyyy h:mm tt}", Eval("AssignDate")) %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="Status:&amp;nbsp;&amp;nbsp;">
												<ItemTemplate>
													<asp:Label ID="lblLeadStatusCode" runat="server" Text='<%# Eval("LeadStatusCode") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
										</Fields>

										<PagerSettings
											FirstPageText=""
											LastPageText=""
											PageButtonCount="6"
											Position="Bottom" NextPageText="" PreviousPageText="" />

										<RowStyle Width="325px" />
									</asp:DetailsView>
									<table>
										<tr>
											<td class="auto-style2">
												<asp:Label ID="lblRecNo" runat="server" Text="Record: "></asp:Label>
											</td>
											<td class="auto-style2">
												<asp:LinkButton ID="Rec1" runat="server" OnClick="Paging"></asp:LinkButton>
											</td>
											<td class="auto-style1">
												<asp:LinkButton ID="Rec2" runat="server" OnClick="Paging"></asp:LinkButton>
											</td>
											<td class="auto-style1">
												<asp:LinkButton ID="Rec3" runat="server" OnClick="Paging"></asp:LinkButton>
											</td>
											<td class="auto-style1">
												<asp:LinkButton ID="Rec4" runat="server" OnClick="Paging"></asp:LinkButton>
											</td>
											<td class="auto-style1">
												<asp:LinkButton ID="Rec5" runat="server" OnClick="Paging"></asp:LinkButton>
											</td>
											<td class="auto-style1">
												<asp:LinkButton ID="Rec6" runat="server" OnClick="Paging"></asp:LinkButton>
											</td>
											<td class="auto-style1">
												<asp:LinkButton ID="Rec7" runat="server" OnClick="Paging"></asp:LinkButton>
											</td>
											<td class="auto-style1">
												<asp:LinkButton ID="Rec8" runat="server" OnClick="Paging"></asp:LinkButton>
											</td>
											<td class="auto-style1">
												<asp:LinkButton ID="Rec9" runat="server" OnClick="Paging"></asp:LinkButton>
											</td>
											<td>
												<asp:LinkButton ID="Rec10" runat="server" OnClick="Paging"></asp:LinkButton>
											</td>
										</tr>
									</table>
									<table style="width: 100%;">
										<tr>
											<td>
												<asp:Label ID="lblFirstChoice" runat="server" Font-Bold="True" ForeColor="Red" Text="Assign Destination:" Width="175px"></asp:Label>
											</td>
											<td>
												<asp:DropDownList ID="ddlFirstChoice" runat="server" AppendDataBoundItems="True" Width="130px" Style="margin-left: 0px" AutoPostBack="True">
													<asp:ListItem Value="">Select a dest.</asp:ListItem>
												</asp:DropDownList>
											</td>
											<td class="auto-style26">
												<asp:LinkButton ID="btnAssignDestination" runat="server" CausesValidation="true" OnClick="btnAssignDestination_Click" Text="Save"></asp:LinkButton>
											</td>
											<td class="auto-style25">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<asp:Label ID="lblAssignLead" runat="server" Font-Bold="True" ForeColor="Red" Text="Assign Lead to Agent:" Width="175px"></asp:Label>
											</td>
											<td>
												<asp:DropDownList ID="ddlResvAgent" runat="server" AppendDataBoundItems="True" Width="130px" Style="margin-left: 0px" AutoPostBack="True">
													<asp:ListItem Value="">Select a RA</asp:ListItem>
												</asp:DropDownList>
											</td>
											<td class="auto-style26">
												<asp:LinkButton ID="btnAssignLead" runat="server" CausesValidation="true" OnClick="btnAssignLead_Click" Text="Save" ToolTip="Take this Lead"></asp:LinkButton>
												<asp:LinkButton ID="btnClaimLead" runat="server" CausesValidation="true" OnClick="btnClaimLead_Click" Text="Claim Lead(s)" ToolTip="Take this Lead"></asp:LinkButton>
											</td>
											<td width="30px">
												<asp:LinkButton ID="btnPrint" runat="server" OnClick="btnPrint_Click" Text="Print"></asp:LinkButton>
											</td>
										</tr>
										<tr>
											<td class="auto-style22">
												<asp:Label ID="lblCloseLeadInfo" runat="server" Font-Bold="True" ForeColor="Red" Text="Order or Resv Number:" Width="186px"></asp:Label>
											</td>
											<td class="auto-style15">
												<asp:TextBox ID="txtFCCNumber" runat="server" Width="130px"></asp:TextBox>
											</td>
											<td class="auto-style24">
												<asp:LinkButton ID="btnCloseLead" runat="server" CausesValidation="true" OnClick="btnCloseLead_Click" Text="Close Lead" ToolTip="Close this lead by entering order or resv number."></asp:LinkButton>
											</td>
											<td width="30px" class="auto-style16">
												<asp:LinkButton ID="btnGoBack" runat="server" OnClick="btnGoBack_Click" Text="Go Back" Width="70px" Visible="False"></asp:LinkButton>
											</td>
										</tr>
										<tr>
											<td colspan="4" width="100%">
												<asp:Label ID="lblMsg" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
											</td>
										</tr>
										<tr>
											<td colspan="4" width="100%"></td>
										</tr>
										<tr>
											<td colspan="4" width="100%">

												<br />

											</td>
										</tr>
									</table>
									<br />

								</div>
							</td>
							<td class="auto-style7">&nbsp;</td>
						</tr>
					</table>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="btnCloseLead" EventName="Click" />
				</Triggers>
			</asp:UpdatePanel>
		</div>
	</form>
</body>
</html>
