﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintFCC.aspx.cs" Inherits="Dashboard.PrintFCC" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Print FCC</title>
  <script src="Scripts/jquery-1.8.2.min.js"></script>
  <script>
    $(document).ready(function () {
      window.print();
    });
  </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
          <asp:GridView ID="grdPrintFCC" runat="server" AutoGenerateColumns="False"
            CssClass="AltRow TableHeader" DataKeyNames="ClientID" Caption="Future Client Callback" Width="100%" EnableModelValidation="True">
            <Columns>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                <ItemTemplate>
                  <asp:Label ID="lblFCCRaName" runat="server" 
                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' ></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:Label ID="lblFCCRaEmail" runat="server" Text='<%# Eval("EMailAddress") %>'></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="CallbackDate" SortExpression="CallbackDate" HeaderText="Callback Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
              <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
              <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="Remarks" />
            </Columns>
          </asp:GridView>
    
    </div>
    </form>
</body>
</html>
