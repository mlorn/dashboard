﻿using fccCustom;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Dashboard
{
  public partial class EditFCC : System.Web.UI.UserControl
  {


    public int WimcoClientID { get; set; }
    public int WimcoAgentID { get; set; }

    public int CallBackID { get; set; }
    public int FCCForLead = 0;
    public Control nc;
    protected void Page_Load(object sender, EventArgs e)
    {
      if (Session["ClientID"] != null)
      {

        WimcoClientID = Int32.Parse(Session["ClientID"].ToString());
      }
      BaseLogic.UserData ud = BaseLogic.GetUserData();
      if (WimcoAgentID == 0)
      {
        WimcoAgentID = ud.UserID;
      }
      nc = ((EditFCC)sender).NamingContainer;
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {


    }
    public void loadFCC()
    {

      lblTempData.Text = CallBackID.ToString();

      WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
      fcc = WimcoBaseLogic.BaseLogic.GetFCC(CallBackID);
      ListItem li639 = new ListItem("NTI-EML New Inquiry-E-MAIL", "639");
      ListItem li648 = new ListItem("NTI-ENL New Inquiry-ENewsletter", "648");
      ListItem li640 = new ListItem("NTI-PHO New Inquiry-PHONE", "640");
      ListItem li646 = new ListItem("NTI-CUS New Inquiry-Contact Us", "646");
      ListItem li647 = new ListItem("NTI-CHA New Inquiry-CHAT", "647");
      ListItem li701 = new ListItem("NTI-RFA New Inquiry-Req For Avail", "701");
      ListItem li826 = new ListItem("NTI-HOME New Inquiry-Homeaway", "826");
      ListItem li904 = new ListItem("NTI-CAN New Inquiry-canadastays", "904");
      ListItem li728 = new ListItem("NTI_FLP New Inquiry-Flipkey", "728");
      ListItem li915 = new ListItem("NTI-ABNB New Inquiry-AirBnB", "915");
      ListItem li722 = new ListItem("NTI-TA New Inquiry-TA", "722");
      ListItem li390 = new ListItem("CLP Client Prospecting", "390");
      ListItem li803 = new ListItem("NTI-HTL New Inquiry-Hotel RFA", "803");
      ListItem li673 = new ListItem("NL New Lead Worked by Agent", "673");
      txtFccDisplay.Text = fcc.Description;
      ddlFCCType.SelectedIndex = 0;
      if (fcc.CallbackContactSourceID != null)
      {
        for (int i = 0; i < ddlFCCType.Items.Count; i++)
        {
          if (ddlFCCType.Items[i].Value == fcc.CallbackContactSourceID.ToString())
          {
            ddlFCCType.SelectedIndex = i;
            break;
          }
        }
      }
      txtCalEdit.Text = fcc.CallbackDate.ToString("yyyy-MM-dd");
      RangeValidator1.Type = ValidationDataType.Date;
      if (fcc.CallbackDate.CompareTo(DateTime.Now) < 0)
      {
        RangeValidator1.MinimumValue = fcc.CallbackDate.ToShortDateString();
      }
      else
      {
        RangeValidator1.MinimumValue = DateTime.Now.ToShortDateString();
      }

      RangeValidator1.MaximumValue = DateTime.Now.AddMonths(18).ToShortDateString();

      ddlDest.Items.Clear();
      //  DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("SELECT Name + ' - ' + DestinationCode AS Name, DestinationID FROM  Destination ORDER BY DestinationCode");
      DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("Exec spGetApprovedDestinationList @External = 0");

      //DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("SELECT DestinationCode, DestinationID FROM Destination ORDER BY DestinationCode");
      foreach (DataRow dr in dsDest.Tables[0].Rows)
      {
        ListItem li = new ListItem(dr[2].ToString(), dr[1].ToString());

        if (fcc.DestinationID.ToString() == li.Value)
        {
          li.Selected = true;

        }

        ddlDest.Items.Add(li);

      }
      //ddlDest.Items.Insert(0, li1109);
      //ddlDest.Items.Insert(0, li1108);
      //ddlDest.Items.Insert(0, li558);
      //ddlDest.Items.Insert(0, li1260);
      //ddlDest.Items.Insert(0, li537);
      //ddlDest.Items.Insert(0, li514);
      //ddlDest.Items.Insert(0, li549);
      //ddlDest.Items.Insert(0, li553);
      //ddlDest.Items.Insert(0, li542);
      //ddlDest.Items.Insert(0, li540);
      //ddlDest.Items.Insert(0, li1033);
      //ddlDest.Items.Insert(0, li1234);
      //ddlDest.Items.Insert(0, li536);
      //ddlDest.Items.Insert(0, li535);
      //ddlDest.Items.Insert(0, li1177);
      //ddlDest.Items.Insert(0, li561);
      //ddlDest.Items.Insert(0, li1158);
      //ddlDest.Items.Insert(0, li529);
      //ddlDest.Items.Insert(0, li527);
      //ddlDest.Items.Insert(0, li522);
      //ddlDest.Items.Insert(0, li519);
      //ddlDest.Items.Insert(0, li518);
      //ddlDest.Items.Insert(0, li506);
      //ddlDest.Items.Insert(0, li503);
      //ddlDest.Items.Insert(0, li487);



      ddlAssigned.Items.Clear();
      DataSet ds = WimcoBaseLogic.BaseLogic.AdHoc("SELECT MagUser.UserID, MagUser.FullName FROM  MagUser INNER JOIN UserGroup ON MagUser.UserID = UserGroup.UserID INNER JOIN MagGroup ON UserGroup.GroupID = MagGroup.GroupID WHERE (MagUser.IsEnabled = 'Y') AND (MagUser.OrganizationID = 1) AND (UserGroup.GroupID = 186) ORDER BY MagUser.FullName");
      foreach (DataRow dr in ds.Tables[0].Rows)
      {
        ListItem li = new ListItem(dr[1].ToString(), dr[0].ToString());

        if (fcc.AgentID.ToString() == li.Value)
        {
          li.Selected = true;
        }
        ddlAssigned.Items.Add(li);
      }
      //ddlAssigned.Enabled = false;


      ddlSourceName.ClearSelection();
      DataSet dscsn = WimcoBaseLogic.BaseLogic.AdHoc("SELECT Name, ContactSourceID FROM    ContactSource WHERE IsEnabled = 'Y' AND (NOT (Name IS NULL)) OR (NOT (Name = '')) ORDER BY Name");
      foreach (DataRow dr in dscsn.Tables[0].Rows)
      {
        ListItem li = new ListItem(dr[0].ToString(), dr[1].ToString());
        if (dr[0].ToString().Length > 0)
        {
          if (fcc.ContactSourceID.ToString() == li.Value)
          {
            li.Selected = true;
          }

          ddlSourceName.Items.Add(li);
        }
      }

      ddlSourceName.Items.Insert(0, "=======================");

      ddlSourceName.Items.Insert(0, li390);
      ddlSourceName.Items.Insert(0, li722);
      ddlSourceName.Items.Insert(0, li915);
      ddlSourceName.Items.Insert(0, li728);
      ddlSourceName.Items.Insert(0, li904);
      ddlSourceName.Items.Insert(0, li826);
      ddlSourceName.Items.Insert(0, li640);
      ddlSourceName.Items.Insert(0, li647);
      ddlSourceName.Items.Insert(0, li646);
      ddlSourceName.Items.Insert(0, li701);
      ddlSourceName.Items.Insert(0, li648);
      ddlSourceName.Items.Insert(0, li639);
      ddlSourceName.Items.Insert(0, "SELECT A SOURCE");

      ddlMocEdit.ClearSelection();
      foreach (ListItem li in ddlMocEdit.Items)
      {
        if (li.Value == fcc.MocID.ToString())
        {
          li.Selected = true;
        }
      }



      ddlLeadScore.ClearSelection();

      foreach (ListItem li in ddlLeadScore.Items)
      {
        if (li.Value == fcc.LeadScoreID.ToString())
        {
          li.Selected = true;
        }
      }



    }
    protected void btnModify_Click(object sender, EventArgs e)
    {
      bool b = ModifyFCC();
    }
    public bool ModifyFCC()
    {
      BaseLogic.UserData udo = BaseLogic.GetUserData();
      //if (ddlMocEdit.SelectedValue == "10")
      //{
      //   goto icCloseJmp;
      // }

      //   else
      // {



      if (!FCCIsValid())
      {


        return false;

      }

    icCloseJmp:;

      Int32 CallBackID = Convert.ToInt32(lblTempData.Text);
      WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
      fcc = WimcoBaseLogic.BaseLogic.GetFCC(CallBackID);
      fcc.AgentID = Convert.ToInt32(ddlAssigned.SelectedValue);
      fcc.MocID = Convert.ToInt32(ddlMocEdit.SelectedValue);
      fcc.ContactSourceID = Convert.ToInt32(ddlSourceName.SelectedValue);
      fcc.CallbackDate = Convert.ToDateTime(txtCalEdit.Text);
      fcc.LeadScoreID = Convert.ToInt32(ddlLeadScore.SelectedValue);
      fcc.DestinationID = Convert.ToInt32(ddlDest.SelectedValue);
      fcc.Description = txtFccDisplay.Text;
      if (ddlFCCType.SelectedValue != "")
      {
        fcc.CallbackContactSourceID = Convert.ToInt32(ddlFCCType.SelectedValue);
      }
      else
      {
        ddlFCCType.BorderColor = System.Drawing.Color.Red;
        ddlFCCType.BorderWidth = 2;
        return false;
      }
      WimcoBaseLogic.BusinessLogic.FCCDataReturn fdr = WimcoBaseLogic.BaseLogic.UpdateFCC(fcc);

      if (fdr.Success)
      {
        lblInformation.Text = "Your FCC has been Modified.<br/>";
        lblInformation.Visible = true;
        //  UpdateFCCHistory(fcc);

        FCCForLead = fcc.CallbackID;

        //  }


        //  RebindGrid();




        if (Session["LeadID"] != null)
        {
          CloseLead(fcc.CallbackID.ToString());

        }


        if (nc.GetType().ToString() == "ASP.wimcofcc_ascx")
        {
          nc.FindControl("pnlMainFCC").Visible = true;

          WimcoFCC wimcoFCC = (WimcoFCC)nc;

          nc.FindControl("pnlFCCEdit").Visible = false;
          wimcoFCC.RebindGrid();
        }


        return true;



      }
      else
      {
        SendErrorEmail(fdr.Message, fcc);
        lblInformation.Text = fdr.Message + "<br/>";
        lblInformation.Visible = true;
        return false;
      }

      //if (ddlMocEdit.SelectedValue.ToString() == "10" && !udo.Manager) //if Closing FCC and if user isn't a manager, contact Sales Manager
      //{

      //    string strSMEmail = "ACaye@wimco.com";
      //    // strSMEmail = "damaral@wimco.com"; //for testing
      //    BusinessLogic.flatClient fc = BaseLogic.flatclient_GetClient(WimcoClientID);
      //    string clientinfo = "Client: " + fc.LastName + ", " + fc.FirstName + " (" + WimcoClientID.ToString() + ")<br />";
      //    string fccdetails = "Order Number: " + fcc.CallbackID + "<br />";
      //    fccdetails += "Callback: " + fcc.CallbackDate.ToString("{0:d}") + "<br />";

      //   // fccdetails += "Created: " + lblCreate.Text + "<br />";
      //    fccdetails += "Lead: " + ddlLeadScore.SelectedItem.Text + "<br />";
      //    fccdetails += "Notes: " + fcc.Description + "<br />";
      //    fccdetails += "Assigned: " + ddlAssigned.SelectedItem.Text + "<br />";
      //    fccdetails += "Source: " + ddlSourceName.SelectedItem.Text + "<br />";
      //    fccdetails += "Destination: " + ddlDest.SelectedItem.Text + "<br />";

      //    //        VillaOwner.BaseLogic.sendHTMLEmail(udo.EmailAddress, udo.FullName, "Nomad: Closing FCC #" + lblTempData.Text, strSMEmail, udo.EmailAddress, clientinfo + fccdetails);


      //}

      return false;

    jmp:;
      //  }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
      //pnlMainFCC.Visible = true;
      WimcoFCC wimcoFCC = (WimcoFCC)nc;


      nc.FindControl("pnlMainFCC").Visible = true;

      nc.FindControl("pnlFCCEdit").Visible = false;
      wimcoFCC.RebindGrid();
    }

    protected void btnAddNewFCC_Click(object sender, EventArgs e)
    {
      AddFCC wimAddFCC = (AddFCC)nc.FindControl("wimAddFCC");
      wimAddFCC.Attributes["purpose"] = "addFCC";
      wimAddFCC.WimcoClientID = WimcoClientID;
      if (nc.GetType().ToString() == "ASP.wimcofcc_ascx")
      {
        nc.FindControl("pnlFCCEdit").Visible = false;
        nc.FindControl("pnlFCCMain").Visible = false;
        wimAddFCC.DataBind(); //It's getting bound on the first load for the email control, don't need to bind again.
      }
      else
      {
        ((Button)nc.FindControl("wimMail").FindControl("btnSend")).Enabled = false;
        this.Visible = false;
      }


      wimAddFCC.Visible = true;



    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {


      Int32 CallBackID = Convert.ToInt32(lblTempData.Text);
      WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
      fcc = WimcoBaseLogic.BaseLogic.GetFCC(CallBackID);
      fcc.MocID = 6;
      fcc.IsEnabled = "N";
      WimcoBaseLogic.BusinessLogic.FCCDataReturn fccd = new WimcoBaseLogic.BusinessLogic.FCCDataReturn();
      fccd = WimcoBaseLogic.BaseLogic.DeleteFCC(fcc);
      UpdateFCCHistory(fcc);
      if (nc.GetType().ToString() == "ASP.wimcofcc_ascx")
      {
        nc.FindControl("pnlMainFCC").Visible = true;
        WimcoFCC wimcoFCC = (WimcoFCC)nc;
        wimcoFCC.RebindGrid();

        nc.FindControl("pnlFCCEdit").Visible = false;
        Label lblInfo = (Label)nc.FindControl("lblInformation");
        lblInfo.Text = "Your FCC has been Deleted.<br/>";
        lblInfo.Visible = true;

      }
      else
      {
        lblInformation.Text = "Your FCC has been Deleted.<br/>";
        lblInformation.Visible = true;
      }

    }
    public Boolean FCCIsValid()
    {
      Boolean isValid = true;
      BaseLogic.UserData udo = BaseLogic.GetUserData();
      //if (ddlMocEdit.SelectedValue == "10")
      //{

      //   goto icCloseJmp;
      // }
      if (ddlSourceName.SelectedIndex > 0)
      {
        ddlSourceName.BorderColor = System.Drawing.Color.Gray;
        ddlSourceName.BorderWidth = 1;
      }
      else
      {
        ddlSourceName.BorderColor = System.Drawing.Color.Red;
        ddlSourceName.BorderWidth = 2;

        isValid = false;
      }
      if (txtFccDisplay.Text == "")
      {
        lblInformation.Text = "Your FCC has <bold>NOT</bold> been Created. Add a Description!<br/>";
        lblInformation.Visible = true;
        txtFccDisplay.BorderColor = System.Drawing.Color.Red;
        txtFccDisplay.BorderWidth = 2;

        isValid = false;
        //    timer.Interval = 8000;
        //   timer.Enabled = true;
        //  goto jmp;
      }
      else
      {
        txtFccDisplay.BorderColor = System.Drawing.Color.Gray;
        txtFccDisplay.BorderWidth = 1;
      }



      if (txtFccDisplay.Text.Length > 7999)
      {
        lblInformation.Text = "Your FCC has <bold>NOT</bold> been Created. Description too long. (8000max)<br/>";
        lblInformation.Visible = true;
        txtFccDisplay.BorderColor = System.Drawing.Color.Red;
        txtFccDisplay.BorderWidth = 2;

        isValid = false;
        //    timer.Interval = 8000;
        //     timer.Enabled = true;
        //  goto jmp;

      }
      else
      {
        txtFccDisplay.BorderColor = System.Drawing.Color.Gray;
        txtFccDisplay.BorderWidth = 1;
      }
      if (ddlLeadScore.SelectedIndex < 1)
      {
        lblInformation.Text = "Your FCC has <bold>NOT</bold> been Created. Lead Heat Is Required<br/>";
        lblInformation.Visible = true;
        ddlLeadScore.BorderColor = System.Drawing.Color.Red;
        ddlLeadScore.BorderWidth = 2;

        isValid = false;
        //    timer.Interval = 8000;
        //     timer.Enabled = true;
        //   goto jmp;

      }
      else
      {
        ddlLeadScore.BorderColor = System.Drawing.Color.Gray;
        ddlLeadScore.BorderWidth = 1;
      }
      if (ddlFCCType.SelectedIndex < 1)
      {
        lblInformation.Text = "Your FCC has <bold>NOT</bold> been Created. FCC Type is Required<br/>";
        lblInformation.Visible = true;
        ddlFCCType.BorderColor = System.Drawing.Color.Red;
        ddlFCCType.BorderWidth = 2;

        isValid = false;
        //    timer.Interval = 8000;
        //     timer.Enabled = true;
        //  goto jmp;

      }
      else
      {
        ddlFCCType.BorderColor = System.Drawing.Color.Gray;
        ddlFCCType.BorderWidth = 1;
      }

      if (txtCalEdit.Text != "")
      {
        if (Convert.ToDateTime(txtCalEdit.Text) < DateTime.Today)
        {
          lblInformation.Text = "Your FCC has <bold>NOT</bold> been Created. Choose a date in the Future!<br/>";
          lblInformation.Visible = true;
          txtCalEdit.BorderColor = System.Drawing.Color.Red;
          txtCalEdit.BorderWidth = 2;

          isValid = false;
          //    timer.Interval = 8000;
          //    timer.Enabled = true;
          // goto jmp;

        }
        else
        {
          txtCalEdit.BorderColor = System.Drawing.Color.Gray;
          txtCalEdit.BorderWidth = 1;
        }
        if (Convert.ToDateTime(txtCalEdit.Text) > DateTime.Today.AddMonths(18))
        {
          lblInformation.Text = "Your FCC has <bold>NOT</bold> been Created. Choose a date with 18 months of today!<br/>";
          lblInformation.Visible = true;
          txtCalEdit.BorderColor = System.Drawing.Color.Red;
          txtCalEdit.BorderWidth = 2;

          isValid = false;
          //    timer.Interval = 8000;
          //    timer.Enabled = true;
          // goto jmp;

        }
        else
        {
          txtCalEdit.BorderColor = System.Drawing.Color.Gray;
          txtCalEdit.BorderWidth = 1;
        }

      }
      else
      {
        lblInformation.Text = "Your FCC has <bold>NOT</bold> been Created. Choose a date with 18 months of today!<br/>";
        lblInformation.Visible = true;
        txtCalEdit.BorderColor = System.Drawing.Color.Red;
        txtCalEdit.BorderWidth = 2;

        isValid = false;
      }


      return isValid;
    }

    public void UpdateFCCHistory(WimcoBaseLogic.BusinessLogic.FCCobj fcc)
    {
      WimcoBaseLogic.BusinessLogic.FCC_HistoryDS hist = new WimcoBaseLogic.BusinessLogic.FCC_HistoryDS();
      WimcoBaseLogic.BusinessLogic.FCC_HistoryDS.FCC_HistoryRow hr = (WimcoBaseLogic.BusinessLogic.FCC_HistoryDS.FCC_HistoryRow)hist.FCC_History.NewFCC_HistoryRow();
      try
      {
        hr.CallbackDate = fcc.CallbackDate;
      }
      catch { }

      hr.CallbackID = fcc.CallbackID;
      hr.ChangeUserID = WimcoAgentID;
      hr.DateModified = DateTime.Now;
      hr.Description = fcc.Description;
      hr.FCC_HistoryID = Guid.NewGuid().ToString();
      hr.MagUserID = fcc.AgentID;
      hr.LeadScore = fcc.LeadScoreID;
      hr.MocID = fcc.MocID;

      if (hr.MocID == 5 | hr.MocID == 9)
      {
        hr.SetCallbackDateNull();
      }


      hist.FCC_History.AddFCC_HistoryRow(hr);
      //try
      //{
      //     WimcoBaseLogic.BaseLogic.UpdateDataSet(hist);
      //}
      //catch (Exception err) {

      //    String m = err.Message;
      //}
    }
    protected Boolean SendErrorEmail(String err, WimcoBaseLogic.BusinessLogic.FCCobj fcc)
    {
      BaseLogic.UserData ud = (BaseLogic.UserData)Session["UserData"];
      string sUserName = ud.username.ToString();
      string sUserID = ud.UserID.ToString();
      Boolean bResults = true;
      string sSubject = string.Format("Add FCC Error");
      string sBody = "Body Text";
      //build message string
      sBody = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
      sBody += "<HTML><HEAD><META http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">";
      sBody += "</HEAD><BODY>";
      sBody += "<table align='center' width='400' style='border: 1px solid #d9d9d9'><tr><td style='padding: 20px'><p style='font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #4071CF'><strong>Error Information:</strong></p><p style='font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #505050; line-height: 18px'>";

      sBody += "<hr /><p style='font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #4071CF'><strong>Client Information:</strong></p><p style='font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #505050; line-height: 18px'>";

      sBody += string.Format("ClientID:<strong> {0}</strong><br />", fcc.ClientID);
      sBody += string.Format("Error:<strong> {0}</strong><br />", err);
      sBody += string.Format("CallbackID:<strong> {0}</strong><br />", fcc.CallbackID);
      sBody += " Source URL:<strong>" + HttpContext.Current.Request.Url.AbsoluteUri + " </strong><br />";
      sBody += string.Format("Agent: <strong>{0} / {1}</strong></p></td></tr></table>", sUserName, sUserID);
      sBody += "</body></html>";
      string sTo = "damaral@wimco.com";//"concierge@wimco.com";
      string sFrom = "wsbhvillas@wimco.com";
      string sCC = "webmaster@wimco.com;jkelly@wimco.com";
      try
      {

        SendEmail.SendMessage(sSubject, sBody, sFrom, sTo, sCC, true);
      }
      catch { bResults = false; }
      return bResults;
    }

    protected void CloseLead(String OrderID)
    {
      String LeadSuccess = "<br/>Lead Was Not Closed";
      if (Session["LeadID"] != null)
      {
        String LeadID = Session["LeadID"].ToString();
        BaseLogic.UserData ud = BaseLogic.GetUserData();
        String AuthKey = BaseLogic.GetAuthKey();
        BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
        BusinessLogic.Leads l = biz.leads_GetLead(AuthKey, Int32.Parse(LeadID));
        if (l.MagUserID == ud.UserID)
        {
          if (l.LeadStatusID == 2)
          {
            WimcoBaseLogic.BusinessLogic.FCCobj fcc2 = new WimcoBaseLogic.BusinessLogic.FCCobj();

            String strOrderNumber = BaseLogic.AdHocSeek("Select mo.OrderNumber From MagOrder mo Join Callback cb on mo.OrderID = cb.BusinessID where cb.CallbackID = " + OrderID, "OrderNumber");


            //  Boolean setCallBack = biz2.leads_SetCallBackID(AuthKey, l.LeadID, fccd.CallbackID.ToString());
            Boolean setCallBack = biz.leads_SetCallBackID(AuthKey, Int32.Parse(LeadID), strOrderNumber);
            if (setCallBack) { LeadSuccess = "<br / >Lead was Closed"; }
            Session.Remove("LeadID");
          }

        }

      }
      lblInformation.Text += LeadSuccess;


    }
  }
}