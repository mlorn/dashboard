﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace fccCustom
{
  enum purpose
  {
    addFCC, addNote, cancel, none, submitted, redo
  }
  public partial class AddFCC : System.Web.UI.UserControl
  {
    private const string AuthKey = "967fc379-f7d8-4dad-833f-611dbf9cc1c1";
    public int WimcoClientID { get; set; }
    public int WimcoAgentID { get; set; }
    public string ContactSourceID { get; set; }
    public string FCCDateRule { get; set; }
    public string FCCMethod { get; set; }
    public string FCCDID { get; set; }
    public string FCCNote { get; set; }
    public string purpose { get; set; }
    public int FCCForLead = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

      if (Session["ClientID"] != null)
      {

        WimcoClientID = Int32.Parse(Session["ClientID"].ToString());
      }
      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      if (WimcoAgentID == 0)
      {
        WimcoAgentID = ud.UserID;
      }

    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
      try
      {
        if (purpose != "cancel" && purpose != "redo")
        {
          if (((UserControl)sender).Attributes["purpose"] != null)
          {
            purpose = ((UserControl)sender).Attributes["purpose"].ToString();
          }
        }
        else
        {
          if (purpose == "redo")
          {
            if (this.NamingContainer.ClientID.ToLower().Contains("wimcofcc"))
            {
              this.NamingContainer.FindControl("pnlMainFCC").Visible = false;
            }
          }

          purpose = "none";
          ((UserControl)sender).Attributes["purpose"] = purpose;
        }
        if (purpose == "addFCC")
        {
          if (ddlNewDest.Items.Count == 0)
          {
            addFCCLoad();
          }
          RangeValidator1.Type = ValidationDataType.Date;
          RangeValidator1.MinimumValue = DateTime.Now.ToShortDateString();
          RangeValidator1.MaximumValue = DateTime.Now.AddMonths(18).ToShortDateString();
          if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Contains("mail"))
          {
            if (HttpContext.Current.Session["fccPastValues"] != null)
            {
              try
              {
                Array a = ((Array)HttpContext.Current.Session["fccPastValues"]);
                ddlNewSource.SelectedIndex = Int32.Parse(a.GetValue(1).ToString());


              }
              catch { }
            }

            String[] FCCValuesInitial = new String[3];
            FCCValuesInitial[0] = txtNewFCCDate.Text;
            FCCValuesInitial[1] = ddlNewSource.SelectedIndex.ToString();
            FCCValuesInitial[2] = txtNewFCCDisplay.Text;
            Session["FCCValuesInitial"] = FCCValuesInitial;
          }

        }
        else
        {
          if (purpose == "addNote")
          {
            addNoteLoad();

          }
        }
        if (purpose == "submitted")
        {
          pnlNewFCC.Visible = false;
          ((UserControl)sender).Attributes["purpose"] = "none";


        }
      }
      catch
      {

      }

    }
    protected void addFCCLoad()
    {
      //fccCalendarNew.SelectedDate = DateTime.Today;
      //fccCalendarNew.TodaysDate = DateTime.Today;            
      //fccCalendarNew.DataBind();
      if (FCCDateRule != null)
      {
        if (Session["FCCDateInitial"] != null)
        {
          DateTime FCCDateInitial = DateTime.Parse(Session["FCCDateInitial"].ToString());
          DateTime FCCDate = FCCDateInitial.AddDays(int.Parse(FCCDateRule));
          if (DateTime.Today > FCCDate)
          {
            FCCDate = DateTime.Today.AddDays(1);
          }
          // calExNewFCCDate.SelectedDate = FCCDate;
          txtNewFCCDate.Text = FCCDate.ToString("yyyy-MM-dd");
        }
        else
        {
          // calExNewFCCDate.SelectedDate = DateTime.Today.AddDays(int.Parse(FCCDateRule));
          txtNewFCCDate.Text = DateTime.Today.AddDays(int.Parse(FCCDateRule)).ToString("yyyy-MM-dd");
        }
      }
      else
      {

        if (Session["RetainFCC"] == null)
        {

          txtNewFCCDate.Text = DateTime.Today.AddDays(1).ToString("yyyy-MM-dd"); //DateTime.Today.ToShortDateString();


        }
        else
        {
          if (Session["RetainFCC"].ToString() != "Y")
          {
            txtNewFCCDate.Text = DateTime.Today.AddDays(1).ToString("yyyy-MM-dd"); //DateTime.Today.ToShortDateString();
          }


        }
      }
      txtNewFCCDisplay.Text = "";
      ddlHeat.ClearSelection();
      ddlHeat.Items[0].Selected = true;
      // ddlNewSource.BorderColor = System.Drawing.Color.Gray;
      //   ddlNewSource.BorderWidth = 1;
      Int32 LeadID = 0;
      try
      {
        LeadID = Int32.Parse(Session["ControlLeadID"].ToString());
      }
      catch
      {
        LeadID = 0;
        btnFCCLead.Visible = false;
      }
      if (LeadID > 0)
      {
        String AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();

        WimcoBaseLogic.BusinessLogic.Leads lead = WimcoBaseLogic.BaseLogic.businessLogic.leads_GetLead(AuthKey, LeadID);
        //  VillaOwner.BaseLogic.UserData ud = VillaOwner.BaseLogic.GetUserData();
        if (lead.MagUserID == WimcoAgentID && lead.LeadStatusID == 2)
        {
          btnFCCLead.Visible = true;
        }
        else
        {
          btnFCCLead.Visible = false;
        }
      }


      ddlNewSource.Items.Clear();
      DataSet dscsn = WimcoBaseLogic.BaseLogic.AdHoc("SELECT Code + ' ' + Name, ContactSourceID, IsEnabled FROM  ContactSource WHERE IsEnabled = 'Y' AND ((NOT (Name IS NULL)) OR  (NOT (Name = ''))) ORDER BY Code");
      foreach (DataRow dr in dscsn.Tables[0].Rows)
      {
        ListItem li = new ListItem(dr[0].ToString(), dr[1].ToString());
        if (dr[0].ToString().Length > 0)
        {
          if (li.Value == "889")
          {
            li.Selected = true;
          }

          ddlNewSource.Items.Add(li);
        }
      }
      //Removed 8-30-18 per Tim -DA


      //Load Preferred Destinations:
      ListItem li487 = new ListItem("St Barthelemy - SBH", "487");
      ListItem li503 = new ListItem("Anguilla - AXA", "503");
      ListItem li506 = new ListItem("Barbados - BGI", "506");
      ListItem li518 = new ListItem("France - FRA", "518");
      ListItem li519 = new ListItem("Grand - Cayman GCM", "519");
      ListItem li522 = new ListItem("Greece - GRC", "522");
      ListItem li527 = new ListItem("Italy - ITA", "527");
      ListItem li529 = new ListItem("Jamaica - MBJ", "529");
      ListItem li1158 = new ListItem("Mexico - MEX", "1158");
      ListItem li561 = new ListItem("Mustique - MQS", "561");
      ListItem li1177 = new ListItem("Nantucket - ACK", "1177");
      ListItem li535 = new ListItem("Necker - NEC", "535");
      ListItem li536 = new ListItem("Nevis - NEV", "536");
      ListItem li1234 = new ListItem("New York - NYS", "1234");
      ListItem li1033 = new ListItem("Peter Island - PET", "1033");
      ListItem li540 = new ListItem("Saba - SAB", "540");
      ListItem li542 = new ListItem("St John - SJF", "542");
      ListItem li553 = new ListItem("St Martin - SXM", "553");
      ListItem li549 = new ListItem("St Thomas - STT", "549");
      ListItem li514 = new ListItem("Tortola - EIS", "514");
      ListItem li537 = new ListItem("Turks & Caicos - PLS", "537");
      ListItem li1260 = new ListItem("Uruguay - URU", "1260");
      ListItem li558 = new ListItem("Virgin Gorda - VIJ", "558");
      ListItem li1108 = new ListItem("Caribbean - CCC", "1108");
      ListItem li1109 = new ListItem("Europe - EEE", "1109");


      //Load preferred Sources:

      ListItem li639 = new ListItem("NTI-EML New Inquiry-E-MAIL", "639");
      ListItem li648 = new ListItem("NTI-ENL New Inquiry-ENewsletter", "648");
      ListItem li646 = new ListItem("NTI-CUS New Inquiry-Contact Us", "646");
      ListItem li647 = new ListItem("NTI-CHA New Inquiry-CHAT", "647");
      ListItem li701 = new ListItem("NTI-RFA New Inquiry-Req For Avail", "701");
      ListItem li640 = new ListItem("NTI-PHO New Inquiry-PHONE", "640");

      ListItem li826 = new ListItem("NTI-HOME New Inquiry-Homeaway", "826");
      ListItem li904 = new ListItem("NTI-CAN New Inquiry-canadastays", "904");
      ListItem li728 = new ListItem("NTI-FLP New Inquiry-Flipkey", "728");
      ListItem li915 = new ListItem("NTI-ABNB New Inquiry-AirBnB", "915");
      ListItem li722 = new ListItem("NTI-TA New Inquiry-TA", "722");
      ListItem li390 = new ListItem("CLP Client Prospecting", "390");
      ListItem li803 = new ListItem("NTI-HTL New Inquiry-Hotel RFA", "803");
      ListItem li673 = new ListItem("NL New Lead Worked by Agent", "673");
      ListItem li688 = new ListItem("NTI-WEBRES New Lead Worked by Agent", "688");
      ddlNewSource.Items.Insert(0, "=======================");
      ddlNewSource.Items.Insert(0, li803);
      ddlNewSource.Items.Insert(0, li673);
      ddlNewSource.Items.Insert(0, li390);
      ddlNewSource.Items.Insert(0, li722);
      ddlNewSource.Items.Insert(0, li915);
      ddlNewSource.Items.Insert(0, li728);
      ddlNewSource.Items.Insert(0, li904);
      ddlNewSource.Items.Insert(0, li826);
      ddlNewSource.Items.Insert(0, li688);
      ddlNewSource.Items.Insert(0, li640);
      ddlNewSource.Items.Insert(0, li646);
      ddlNewSource.Items.Insert(0, li647);
      ddlNewSource.Items.Insert(0, li701);
      ddlNewSource.Items.Insert(0, li648);
      ddlNewSource.Items.Insert(0, li639);
      ddlNewSource.Items.Insert(0, "SELECT A SOURCE");
      try
      {
        // if(Session["FCCContactSource"] != null){
        if (ContactSourceID != null)
        {
          for (int i = 0; i < ddlNewSource.Items.Count; i++)
          {
            if (ContactSourceID == ddlNewSource.Items[i].Value)
            {
              try
              {
                ddlNewSource.SelectedItem.Selected = false;
              }
              catch { }
              //    if (Session["FCCContactSource"].ToString() == ddlNewSource.Items[i].Value) {
              ddlNewSource.SelectedIndex = i;
              break;
            }

          }

        }
      }
      catch { }
      if (Session["TemplateKindID"] != null)
      {
        if (!this.IsPostBack)
        {
          if (Session["TemplateKindID"].ToString() == "1" || Session["TemplateKindID"].ToString() == "6" || Session["TemplateKindID"].ToString() == "7")
          {
            ddlNewFCCType.SelectedValue = "390";
            if (Session["TemplateKindID"].ToString() == "7")
            {
              ddlNewSource.SelectedValue = "1123";
            }
          }
          else
          {
            //   ddlNewFCCType.SelectedValue = "983"; //Removed 8-28-2018 per request
          }
        }
      }
      else
      {
        //     ddlNewFCCType.SelectedValue = "983";  //Removed 8-28-2018 per request
      }



      ddlAssignNew.Items.Clear();
      DataSet ds = WimcoBaseLogic.BaseLogic.AdHoc("SELECT MagUser.UserID, MagUser.FullName FROM  MagUser INNER JOIN UserGroup ON MagUser.UserID = UserGroup.UserID INNER JOIN MagGroup ON UserGroup.GroupID = MagGroup.GroupID WHERE (MagUser.IsEnabled = 'Y') AND (MagUser.OrganizationID = 1) AND (UserGroup.GroupID = 186) ORDER BY MagUser.FullName");
      foreach (DataRow dr in ds.Tables[0].Rows)
      {
        ListItem li = new ListItem(dr[1].ToString(), dr[0].ToString());


        if (WimcoAgentID.ToString() == li.Value)
        {
          li.Selected = true;
        }
        ddlAssignNew.Items.Add(li);
      }
      ddlAssignNew.Items.Add(new ListItem("Dan Amaral", "11199"));
      //ddlAssignNew.Enabled = false;


      ddlNewDest.Items.Clear();
      // DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("SELECT LocaleName + ' - ' + LocaleCode AS Name, LocaleID FROM Locale ORDER BY LocaleName");
      DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("Exec spGetApprovedDestinationList @External = 0");
      //DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("SELECT DestinationCode, DestinationID FROM Destination ORDER BY DestinationCode");
      foreach (DataRow dr in dsDest.Tables[0].Rows)
      {
        ListItem li = new ListItem(dr[2].ToString(), dr[1].ToString());

        //if ("487" == li.Value)
        //{
        //    li.Selected = true;

        //}

        ddlNewDest.Items.Add(li);

      }


      //ddlNewDest.Items.Insert(0, li1109);
      //ddlNewDest.Items.Insert(0, li1108);
      //ddlNewDest.Items.Insert(0, li558);
      //ddlNewDest.Items.Insert(0, li1260);
      //ddlNewDest.Items.Insert(0, li537);
      //ddlNewDest.Items.Insert(0, li514);
      //ddlNewDest.Items.Insert(0, li549);
      //ddlNewDest.Items.Insert(0, li553);
      //ddlNewDest.Items.Insert(0, li542);
      //ddlNewDest.Items.Insert(0, li540);
      //ddlNewDest.Items.Insert(0, li1033);
      //ddlNewDest.Items.Insert(0, li1234);
      //ddlNewDest.Items.Insert(0, li536);
      //ddlNewDest.Items.Insert(0, li535);
      //ddlNewDest.Items.Insert(0, li1177);
      //ddlNewDest.Items.Insert(0, li561);
      //ddlNewDest.Items.Insert(0, li1158);
      //ddlNewDest.Items.Insert(0, li529);
      //ddlNewDest.Items.Insert(0, li527);
      //ddlNewDest.Items.Insert(0, li522);
      //ddlNewDest.Items.Insert(0, li519);
      //ddlNewDest.Items.Insert(0, li518);
      //ddlNewDest.Items.Insert(0, li506);
      //ddlNewDest.Items.Insert(0, li503);
      //ddlNewDest.Items.Insert(0, li487);
      try
      {
        //FCCDID = "553";
        // if(Session["FCCContactSource"] != null){
        if (FCCDID != null)
        {

          for (int i = 0; i < ddlNewDest.Items.Count; i++)
          {

            if (FCCDID == ddlNewDest.Items[i].Value)
            {
              //    if (Session["FCCContactSource"].ToString() == ddlNewSource.Items[i].Value) {
              ddlNewDest.SelectedIndex = i;
              break;
            }
            else
            {
              ddlNewDest.Items[i].Selected = false;
            }

          }

        }
        else
        {
          ddlNewDest.Items[0].Selected = true;
        }
      }
      catch { ddlNewDest.Items[0].Selected = true; }
      try
      {
        // if(Session["FCCContactSource"] != null){
        if (FCCMethod != null)
        {
          for (int i = 0; i < ddlMoc.Items.Count; i++)
          {
            if (FCCMethod == ddlMoc.Items[i].Value)
            {
              //    if (Session["FCCContactSource"].ToString() == ddlNewSource.Items[i].Value) {
              ddlMoc.SelectedIndex = i;
              break;
            }

          }

        }
        else
        {
          ddlMoc.SelectedIndex = 1;

        }
      }
      catch { ddlMoc.Items[1].Selected = true; }
      if (FCCNote != null)
      {
        txtNewFCCDisplay.Text = FCCNote;
      }
      pnlNewNote.Visible = false;
      pnlNewFCC.Visible = true;


    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
      AddFCC fcc = this;
      WimcoFCC grid = (WimcoFCC)fcc.Parent;

      ((Panel)grid.FindControl("pnlFCCHistory")).Visible = false;
      grid.RebindGrid();
      pnlNewFCC.Visible = false;
      pnlNewNote.Visible = false;
      purpose = "cancel";
      // pnlFCCHistory.Visible = false;

      AddFCC AF = ((AddFCC)((Button)sender).Parent.Parent);
      AF.Attributes["purpose"] = "cancel";
      //  pnlNewFCC.Visible = false;
      //   RebindGrid();

      lblInfo.Text = "Cancel. No Changes Made.<br/>";
      lblInfo.Visible = true;
      //  timer.Interval = 2000;
      //  timer.Enabled = true;
    }
    protected void addNoteLoad()
    {
      pnlNewFCC.Visible = false;
      pnlNewNote.Visible = true;
      // pnlFCCEdit.Visible = false;
      // pnlNewFCC.Visible = false;

      txtNewNote.Text = "";



      ddlNewNoteAssigned.Items.Clear();
      DataSet ds = WimcoBaseLogic.BaseLogic.AdHoc("SELECT MagUser.UserID, MagUser.FullName FROM  MagUser INNER JOIN UserGroup ON MagUser.UserID = UserGroup.UserID INNER JOIN MagGroup ON UserGroup.GroupID = MagGroup.GroupID WHERE (MagUser.IsEnabled = 'Y') AND (MagUser.OrganizationID = 1) AND (UserGroup.GroupID = 186) ORDER BY MagUser.FullName");
      foreach (DataRow dr in ds.Tables[0].Rows)
      {
        ListItem li = new ListItem(dr[1].ToString(), dr[0].ToString());

        if (WimcoAgentID.ToString() == li.Value)
        {
          li.Selected = true;
        }
        ddlNewNoteAssigned.Items.Add(li);

      }


      ddlNewSourceNote.Items.Clear();
      DataSet dscsn = WimcoBaseLogic.BaseLogic.AdHoc("SELECT Name, ContactSourceID, IsEnabled FROM  ContactSource WHERE (NOT (Name IS NULL)) AND (IsEnabled = 'Y') OR  (NOT (Name = '')) ORDER BY Name");
      foreach (DataRow dr in dscsn.Tables[0].Rows)
      {
        ListItem li = new ListItem(dr[0].ToString(), dr[1].ToString());
        if (dr[0].ToString().Length > 0)
        {
          if (li.Value == "889")
          {
            li.Selected = true;
          }

          ddlNewSourceNote.Items.Add(li);
        }
      }

      //Load Preferred Destinations:
      ListItem li487 = new ListItem("St Barthelemy - SBH", "487");
      ListItem li503 = new ListItem("Anguilla - AXA", "503");
      ListItem li506 = new ListItem("Barbados - BGI", "506");
      ListItem li518 = new ListItem("France - FRA", "518");
      ListItem li519 = new ListItem("Grand - Cayman GCM", "519");
      ListItem li522 = new ListItem("Greece - GRC", "522");
      ListItem li527 = new ListItem("Italy - ITA", "527");
      ListItem li529 = new ListItem("Jamaica - MBJ", "529");
      ListItem li1158 = new ListItem("Mexico - MEX", "1158");
      ListItem li561 = new ListItem("Mustique - MQS", "561");
      ListItem li1177 = new ListItem("Nantucket - ACK", "1177");
      ListItem li535 = new ListItem("Necker - NEC", "535");
      ListItem li536 = new ListItem("Nevis - NEV", "536");
      ListItem li1234 = new ListItem("New York - NYS", "1234");
      ListItem li1033 = new ListItem("Peter Island - PET", "1033");
      ListItem li540 = new ListItem("Saba - SAB", "540");
      ListItem li542 = new ListItem("St John - SJF", "542");
      ListItem li553 = new ListItem("St Martin - SXM", "553");
      ListItem li549 = new ListItem("St Thomas - STT", "549");
      ListItem li514 = new ListItem("Tortola - EIS", "514");
      ListItem li537 = new ListItem("Turks & Caicos - PLS", "537");
      ListItem li1260 = new ListItem("Uruguay - URU", "1260");
      ListItem li558 = new ListItem("Virgin Gorda - VIJ", "558");
      ListItem li1108 = new ListItem("Caribbean - CCC", "1108");
      ListItem li1109 = new ListItem("Europe - EEE", "1109");



      ListItem li639 = new ListItem("NTI-EML New Inquiry-E-MAIL", "639");
      ListItem li648 = new ListItem("NTI-ENL New Inquiry-ENewsletter", "648");
      ListItem li646 = new ListItem("NTI-CUS New Inquiry-Contact Us", "646");
      ListItem li647 = new ListItem("NTI-CHA New Inquiry-CHAT", "647");
      ListItem li701 = new ListItem("NTI-RFA New Inquiry-Req For Avail", "701");
      ListItem li640 = new ListItem("NTI-PHO New Inquiry-PHONE", "640");

      ListItem li826 = new ListItem("NTI-HOME New Inquiry-Homeaway", "826");
      ListItem li904 = new ListItem("NTI-CAN New Inquiry-canadastays", "904");
      ListItem li728 = new ListItem("NTI-FLP New Inquiry-Flipkey", "728");
      ListItem li915 = new ListItem("NTI-ABNB New Inquiry-AirBnB", "915");
      ddlNewSourceNote.Items.Insert(0, "=======================");
      ddlNewSourceNote.Items.Insert(0, li915);
      ddlNewSourceNote.Items.Insert(0, li728);
      ddlNewSourceNote.Items.Insert(0, li904);
      ddlNewSourceNote.Items.Insert(0, li826);
      ddlNewSourceNote.Items.Insert(0, li646);
      ddlNewSourceNote.Items.Insert(0, li701);
      ddlNewSourceNote.Items.Insert(0, li640);
      ddlNewSourceNote.Items.Insert(0, li647);
      ddlNewSourceNote.Items.Insert(0, li648);
      ddlNewSourceNote.Items.Insert(0, li639);
      ddlNewSourceNote.Items.Insert(0, "SELECT A SOURCE");

      ddlNewNoteDest.Items.Clear();
      //DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("SELECT Name + ' - ' + DestinationCode AS Name, DestinationID FROM  Destination ORDER BY DestinationCode");
      DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("Exec spGetApprovedDestinationList @External = 0");

      //DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("SELECT DestinationCode, DestinationID FROM Destination ORDER BY DestinationCode");
      foreach (DataRow dr in dsDest.Tables[0].Rows)
      {
        ListItem li = new ListItem(dr[2].ToString(), dr[1].ToString());

        //if ("487" == li.Value)
        //{
        //    li.Selected = true;

        //}

        ddlNewNoteDest.Items.Add(li);

      }
      //ddlNewNoteDest.Items.Insert(0, li1109);
      //ddlNewNoteDest.Items.Insert(0, li1108);
      //ddlNewNoteDest.Items.Insert(0, li558);
      //ddlNewNoteDest.Items.Insert(0, li1260);
      //ddlNewNoteDest.Items.Insert(0, li537);
      //ddlNewNoteDest.Items.Insert(0, li514);
      //ddlNewNoteDest.Items.Insert(0, li549);
      //ddlNewNoteDest.Items.Insert(0, li553);
      //ddlNewNoteDest.Items.Insert(0, li542);
      //ddlNewNoteDest.Items.Insert(0, li540);
      //ddlNewNoteDest.Items.Insert(0, li1033);
      //ddlNewNoteDest.Items.Insert(0, li1234);
      //ddlNewNoteDest.Items.Insert(0, li536);
      //ddlNewNoteDest.Items.Insert(0, li535);
      //ddlNewNoteDest.Items.Insert(0, li1177);
      //ddlNewNoteDest.Items.Insert(0, li561);
      //ddlNewNoteDest.Items.Insert(0, li1158);
      //ddlNewNoteDest.Items.Insert(0, li529);
      //ddlNewNoteDest.Items.Insert(0, li527);
      //ddlNewNoteDest.Items.Insert(0, li522);
      //ddlNewNoteDest.Items.Insert(0, li519);
      //ddlNewNoteDest.Items.Insert(0, li518);
      //ddlNewNoteDest.Items.Insert(0, li506);
      //ddlNewNoteDest.Items.Insert(0, li503);
      //ddlNewNoteDest.Items.Insert(0, li487);
      //ddlNewNoteDest.Items[0].Selected = true;
      rdoNewNote.Items[0].Selected = true;

    }
    protected void btnNewNoteAdd_Click(object sender, EventArgs e)
    {

      if (txtNewNote.Text == "")
      {
        lblInfo.Text = "Your record has <bold>NOT</bold> been Created. Add a Description!<br/>";
        lblInfo.Visible = true;
        //    timer.Interval = 8000;
        //    timer.Enabled = true;
        goto jmp;
      }

      if (txtNewNote.Text.Length > 7999)
      {
        lblInfo.Text = "Your record has <bold>NOT</bold> been Created. Description too long. (8000max)<br/>";
        lblInfo.Visible = true;
        //   timer.Interval = 8000;
        //   timer.Enabled = true;
        goto jmp;

      }





      WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
      fcc.AgentID = Convert.ToInt32(ddlNewNoteAssigned.SelectedValue);
      fcc.ClientID = WimcoClientID;
      fcc.ContactSourceID = 6;
      fcc.Description = txtNewNote.Text;

      fcc.MocID = Convert.ToInt32(rdoNewNote.SelectedValue);
      fcc.IsEnabled = "Y";
      fcc.LeadScoreID = 3;
      fcc.ProductBaseID = 600492;
      fcc.DestinationID = Convert.ToInt32(ddlNewNoteDest.SelectedValue);
      fcc.ContactSourceID = Convert.ToInt32(ddlNewSourceNote.SelectedValue);
      WimcoBaseLogic.BusinessLogic.FCCDataReturn fccd = new WimcoBaseLogic.BusinessLogic.FCCDataReturn();
      fccd = WimcoBaseLogic.BaseLogic.InsertFCC(fcc);
      fcc.CallbackID = fccd.CallbackID;
      UpdateFCCHistory(fcc);


    jmp:;

      if (HttpContext.Current.Request.Url.ToString().Contains("wclientsearch"))
      {
        RebindGrid();
      }

      pnlNewNote.Visible = false;
      // pnlMainFCC.Visible = true;

    }
    public void RebindGrid()
    {
      AddFCC fcc = this;
      WimcoFCC grid = (WimcoFCC)fcc.Parent;
      //  ((ObjectDataSource)grid.FindControl("odsFCC")).DataBind();
      //  ((GridView)grid.FindControl("grdFCC")).DataBind();
      grid.RebindGrid();

      //grdFCC.DataBind();
    }

    protected void btnNoteCancel_Click(object sender, EventArgs e)
    {
      //  pnlMainFCC.Visible = true;
      //   pnlFCCEdit.Visible = false;
      pnlNewFCC.Visible = false;
      //   pnlEditNote.Visible = false;
      pnlNewNote.Visible = false;
      //  RebindGrid();
      purpose = "cancel";
      AddFCC AF = ((AddFCC)((Button)sender).Parent.Parent);
      AF.Attributes["purpose"] = "cancel";
      lblInfo.Text = "Cancel. No Changes Made.<br/>";
      lblInfo.Visible = true;
      //    timer.Interval = 2000;
      //   timer.Enabled = true;
      AddFCC fcc = this;
      WimcoFCC grid = (WimcoFCC)fcc.Parent;

      ((Panel)grid.FindControl("pnlFCCHistory")).Visible = false;
    }
    protected void btnNewFCC_Click(object sender, EventArgs e)
    {
      AddNewFCC();
    }
    public bool AddNewFCC()
    {
      Dashboard.BaseLogic.UserData udo = Dashboard.BaseLogic.GetUserData();
      // adding new FCC here.
      if (FCCIsValid())
      { // This whole chunk got moved into the FCCIsValid method

        //VillaOwner.BaseLogic.UserData udo = VillaOwner.BaseLogic.GetUserData();
        ////if (ddlMocEdit.SelectedValue == "10")
        ////{

        ////   goto icCloseJmp;
        //// }

        //if (txtNewFCCDisplay.Text == "")
        //{
        //    lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. Add a Description!<br/>";
        //    lblInfo.Visible = true;
        //    txtNewFCCDisplay.BorderColor = System.Drawing.Color.Red;
        //    txtNewFCCDisplay.BorderWidth = 2;
        //    purpose = "redo";
        //    //    timer.Interval = 8000;
        //    //   timer.Enabled = true;
        //    //  goto jmp;
        //}
        //else {
        //    txtNewFCCDisplay.BorderColor = System.Drawing.Color.Gray;
        //    txtNewFCCDisplay.BorderWidth = 1;
        //}



        //if (txtNewFCCDisplay.Text.Length > 7999)
        //{
        //    lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. Description too long. (8000max)<br/>";
        //    lblInfo.Visible = true;
        //    txtNewFCCDisplay.BorderColor = System.Drawing.Color.Red;
        //    txtNewFCCDisplay.BorderWidth = 2;
        //    purpose = "redo";
        //    //    timer.Interval = 8000;
        //    //     timer.Enabled = true;
        //  //  goto jmp;

        //}
        //else
        //{
        //    txtNewFCCDisplay.BorderColor = System.Drawing.Color.Gray;
        //    txtNewFCCDisplay.BorderWidth = 1;
        //}
        //if (ddlHeat.SelectedIndex < 1)
        //{
        //    lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. Lead Heat Is Required<br/>";
        //    lblInfo.Visible = true;
        //    ddlHeat.BorderColor = System.Drawing.Color.Red;
        //    ddlHeat.BorderWidth = 2;
        //    purpose = "redo";
        //    //    timer.Interval = 8000;
        //    //     timer.Enabled = true;
        // //   goto jmp;

        //}
        //else
        //{
        //    ddlHeat.BorderColor = System.Drawing.Color.Gray;
        //    ddlHeat.BorderWidth = 1;
        //}
        //if (ddlNewFCCType.SelectedIndex < 1)
        //{
        //    lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. FCC Type is Required<br/>";
        //    lblInfo.Visible = true;
        //    ddlNewFCCType.BorderColor = System.Drawing.Color.Red;
        //    ddlNewFCCType.BorderWidth = 2;
        //    purpose = "redo";
        //    //    timer.Interval = 8000;
        //    //     timer.Enabled = true;
        //  //  goto jmp;

        //}
        //else
        //{
        //    ddlNewFCCType.BorderColor = System.Drawing.Color.Gray;
        //    ddlNewFCCType.BorderWidth = 1;
        //}


        //if (Convert.ToDateTime(txtNewFCCDate.Text) < DateTime.Today)
        //{
        //    lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. Choose a date in the Future!<br/>";
        //    lblInfo.Visible = true;
        //    txtNewFCCDate.BorderColor = System.Drawing.Color.Red;
        //   txtNewFCCDate.BorderWidth = 2;
        //   purpose = "redo";
        //    //    timer.Interval = 8000;
        //    //    timer.Enabled = true;
        //   // goto jmp;

        //}
        //else
        //{
        //    txtNewFCCDate.BorderColor = System.Drawing.Color.Gray;
        //    txtNewFCCDate.BorderWidth = 1;
        //}
        //if (Convert.ToDateTime(txtNewFCCDate.Text) > DateTime.Today.AddMonths(18))
        //{
        //    lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. Choose a date with 18 months of today!<br/>";
        //    lblInfo.Visible = true;
        //    txtNewFCCDate.BorderColor = System.Drawing.Color.Red;
        //    txtNewFCCDate.BorderWidth = 2;
        //    purpose = "redo";
        //    //    timer.Interval = 8000;
        //    //    timer.Enabled = true;
        //   // goto jmp;

        //}
        //else 
        //{
        //    txtNewFCCDate.BorderColor = System.Drawing.Color.Gray;
        //    txtNewFCCDate.BorderWidth = 1;
        //}
        //if (purpose == "redo") {
        // //   if (this.NamingContainer.ClientID.ToLower().Contains("wimcofcc")) { 
        // //   this.NamingContainer.FindControl("pnlMainFCC").Visible = false;
        //    //}
        //    goto jmp;
        //}
        WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
        //fcc.AgentID = WimcoAgentID;


        fcc.CallbackDate = Convert.ToDateTime(txtNewFCCDate.Text);
        //fcc.CallbackDate = fccCalendarNew.SelectedDate;
        //fcc.CallbackID;            

        fcc.AgentID = Convert.ToInt32(ddlAssignNew.SelectedValue);
        fcc.CallbackContactSourceID = Convert.ToInt32(ddlNewFCCType.SelectedValue);
        fcc.ClientID = WimcoClientID;
        fcc.ContactSourceID = Convert.ToInt32(ddlNewSource.SelectedValue);
        fcc.Description = txtNewFCCDisplay.Text;
        fcc.DestinationID = Convert.ToInt32(ddlNewDest.SelectedValue);
        fcc.MocID = Convert.ToInt32(ddlMoc.SelectedValue);
        fcc.IsEnabled = "Y";
        fcc.LeadScoreID = Convert.ToInt32(ddlHeat.SelectedValue);
        fcc.ProductBaseID = 600492;

        WimcoBaseLogic.BusinessLogic.FCCDataReturn fccd = new WimcoBaseLogic.BusinessLogic.FCCDataReturn();
        fccd = WimcoBaseLogic.BaseLogic.InsertFCC(fcc);
        if (fccd.Success)
        {
          fcc.CallbackID = fccd.CallbackID;
          //    UpdateFCCHistory(fcc);
          AddFCC AF = ((AddFCC)btnNewFCC.Parent.Parent);
          AF.Attributes["purpose"] = "submitted";

          if (ddlMoc.SelectedValue.ToString() == "10" && !udo.Manager) //if Closing FCC and if user isn't a manager, contact Sales Manager
          {
            Dashboard.BusinessLogic.flatClient fc = Dashboard.BaseLogic.flatclient_GetClient(WimcoClientID);
            string clientinfo = "Client: " + fc.LastName + ", " + fc.FirstName + " (" + WimcoClientID.ToString() + ")<br />";
            string fccdetails = "Order Number: " + fcc.CallbackID + "<br />";

            fccdetails += "Callback: " + fcc.CallbackDate.ToString("{0:d}") + "<br />";
            fccdetails += "Created: " + DateTime.Today.ToString("{0:d}") + "<br />";
            fccdetails += "Lead: " + ddlHeat.SelectedItem.Text + "<br />";
            fccdetails += "Notes: " + fcc.Description + "<br />";
            fccdetails += "Assigned: " + ddlAssignNew.SelectedItem.Text + "<br />";
            fccdetails += "Source: " + ddlNewSource.SelectedItem.Text + "<br />";
            fccdetails += "Destination: " + ddlNewDest.SelectedItem.Text + "<br />";
            string strSMEmail = "ACaye@wimco.com";
            //  strSMEmail = "damaral@wimco.com"; //for testing
            //  VillaOwner.BaseLogic.sendHTMLEmail(udo.EmailAddress, udo.FullName, "Nomad: Closing FCC #" + fcc.CallbackID, strSMEmail, udo.EmailAddress, clientinfo + fccdetails);


          }
          //if (source == "leadForm") //sender is called from the close lead button function
          //{
          if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("wclientsearch"))
          {

            RebindGrid();
          }
          //}
          //else
          //{

          FCCForLead = fcc.CallbackID;

          //  }


          //  RebindGrid();


          lblInfo.Text = "Your FCC has been Created.<br/>";
          lblInfo.Visible = true;

          if (Session["LeadID"] != null)
          {
            CloseLead(fcc.CallbackID.ToString());
          }

          //  timer.Interval = 2000;
          //  timer.Enabled = true;




          //  pnlMainFCC.Visible = true;
          //   pnlFCCEdit.Visible = false;
          pnlNewFCC.Visible = false;
          return true;
        }
        else
        {
          SendErrorEmail(fccd.Message, fcc);
          lblInfo.Text = fccd.Message + "<br/>";
          lblInfo.Visible = true;
          return false;
        }

      jmp:;
      }
      //else {
      //    ddlNewSource.BorderColor = System.Drawing.Color.Red;
      //    ddlNewSource.BorderWidth = 2;
      //    purpose = "redo";
      //}
      return false;
    }


    protected void btnFCCLead_Click(object sender, EventArgs e)
    {

      Int32 LeadID = 0;
      try
      {
        LeadID = Int32.Parse(Session["ControlLeadID"].ToString());
      }
      catch
      {
        LeadID = 0;
      }
      if (LeadID > 0)
      {
        String AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();

        WimcoBaseLogic.BusinessLogic.Leads lead = WimcoBaseLogic.BaseLogic.businessLogic.leads_GetLead(AuthKey, LeadID);
        //  VillaOwner.BaseLogic.UserData ud = VillaOwner.BaseLogic.GetUserData();
        if (lead.MagUserID == WimcoAgentID)
        {
          AddNewFCC();
          if (FCCForLead > 0)
          {
            if (lead.LeadStatusID == 2)//pending
            {
              //Leadboard.LeadData ld = new Leadboard.LeadData();
              DataSet DsOrd = WimcoBaseLogic.BaseLogic.AdHoc("SELECT dbo.MagOrder.OrderNumber FROM  dbo.Callback, dbo.OrderLineItem, dbo.MagOrder WHERE dbo.Callback.BusinessID = dbo.OrderLineItem.OrderID AND dbo.OrderLineItem.OrderID = dbo.MagOrder.OrderID AND (dbo.Callback.CallbackID = " + FCCForLead.ToString() + ")");
              String OrderID = DsOrd.Tables[0].Rows[0][0].ToString();
              //bool success = ld.CloseLead(LeadID, OrderID);
              bool success = CloseLead(LeadID, OrderID);

              if (success)
              {
                lblInfo.Text = "This lead has been closed and the FCC has beeen added.<br/>";
                lblInfo.Visible = true;


              }
              else
              {
                lblInfo.Text = "This lead has not been closed, but the FCC has been created.<br/>";
                lblInfo.Visible = true;

              }

            }

          }





        }
        else
        {
          lblInfo.Text = "This lead has not been closed<br/>";
          lblInfo.Visible = true;
        }
      }
      else
      {
        lblInfo.Text = "This lead has not been closed<br/>";
        lblInfo.Visible = true;
      }


    }

    /// <summary>
    /// Closes the lead by letting a RA entering a Order Number.
    /// </summary>
    /// <param name="leadID">The lead identifier.</param>
    /// <param name="FCCNumber">The FCC (order number) number or reservation number.</param>
    /// <returns></returns>
    public bool CloseLead(int leadID, string FCCNumber)
    {
      bool temp;
      // declare the web service
      Dashboard.BusinessLogic.WimcoBusinessLogic biz = new Dashboard.BusinessLogic.WimcoBusinessLogic();
      // declare the lead obj.
      Dashboard.BusinessLogic.Leads lead = new Dashboard.BusinessLogic.Leads();
      // declare the lead data return obj.
      Dashboard.BusinessLogic.LeadsDataReturn ldr = new Dashboard.BusinessLogic.LeadsDataReturn();

      // get a lead.
      //lead = biz.leads_GetLead(AuthKey, 1);
      //lead.MagUserID = magUserID;
      //biz.leads_UpdateLead(AuthKey, lead);

      temp = biz.leads_SetCallBackID(AuthKey, leadID, FCCNumber);
      /*
      if (temp == true)
      {
        result = "Lead has been closed";
      }
      else
      {
        result = "Could not the lead";
      }
      */
      return temp;
    }
    protected void CloseLead(String OrderID)
    {
      String LeadSuccess = "<br/>Lead Was Not Closed";
      if (Session["LeadID"] != null)
      {
        String LeadID = Session["LeadID"].ToString();
        Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
        String AuthKey = Dashboard.BaseLogic.GetAuthKey();
        Dashboard.BusinessLogic.WimcoBusinessLogic biz = new Dashboard.BusinessLogic.WimcoBusinessLogic();
        Dashboard.BusinessLogic.Leads l = biz.leads_GetLead(AuthKey, Int32.Parse(LeadID));
        if (l.MagUserID == ud.UserID)
        {
          if (l.LeadStatusID == 2)
          {
            WimcoBaseLogic.BusinessLogic.FCCobj fcc2 = new WimcoBaseLogic.BusinessLogic.FCCobj();

            String strOrderNumber = Dashboard.BaseLogic.AdHocSeek("Select mo.OrderNumber From MagOrder mo Join Callback cb on mo.OrderID = cb.BusinessID where cb.CallbackID = " + OrderID, "OrderNumber");


            //  Boolean setCallBack = biz2.leads_SetCallBackID(AuthKey, l.LeadID, fccd.CallbackID.ToString());
            Boolean setCallBack = biz.leads_SetCallBackID(AuthKey, Int32.Parse(LeadID), strOrderNumber);
            if (setCallBack) { LeadSuccess = "<br / >Lead was Closed"; }
            Session.Remove("LeadID");
          }

        }

      }
      lblInfo.Text += LeadSuccess;


    }
    public void UpdateFCCHistory(WimcoBaseLogic.BusinessLogic.FCCobj fcc)
    {
      WimcoBaseLogic.BusinessLogic.FCC_HistoryDS hist = new WimcoBaseLogic.BusinessLogic.FCC_HistoryDS();
      WimcoBaseLogic.BusinessLogic.FCC_HistoryDS.FCC_HistoryRow hr = (WimcoBaseLogic.BusinessLogic.FCC_HistoryDS.FCC_HistoryRow)hist.FCC_History.NewFCC_HistoryRow();
      try
      {
        hr.CallbackDate = fcc.CallbackDate;
      }
      catch { }

      hr.CallbackID = fcc.CallbackID;
      hr.ChangeUserID = WimcoAgentID;
      hr.DateModified = DateTime.Now;
      hr.Description = fcc.Description;
      hr.FCC_HistoryID = Guid.NewGuid().ToString();
      hr.MagUserID = fcc.AgentID;
      hr.LeadScore = fcc.LeadScoreID;
      hr.MocID = fcc.MocID;

      if (hr.MocID == 5 | hr.MocID == 9)
      {
        hr.SetCallbackDateNull();
      }


      hist.FCC_History.AddFCC_HistoryRow(hr);

      WimcoBaseLogic.BaseLogic.UpdateDataSet(hist);

    }
    public Boolean FCCIsValid()
    {
      Boolean isValid = true;
      Dashboard.BaseLogic.UserData udo = Dashboard.BaseLogic.GetUserData();
      //if (ddlMocEdit.SelectedValue == "10")
      //{

      //   goto icCloseJmp;
      // }
      if (ddlNewSource.SelectedIndex > 0)
      {
        ddlNewSource.BorderColor = System.Drawing.Color.Gray;
        ddlNewSource.BorderWidth = 1;
      }
      else
      {
        ddlNewSource.BorderColor = System.Drawing.Color.Red;
        ddlNewSource.BorderWidth = 2;
        purpose = "redo";
        isValid = false;
      }
      if (txtNewFCCDisplay.Text == "")
      {
        lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. Add a Description!<br/>";
        lblInfo.Visible = true;
        txtNewFCCDisplay.BorderColor = System.Drawing.Color.Red;
        txtNewFCCDisplay.BorderWidth = 2;
        purpose = "redo";
        isValid = false;
        //    timer.Interval = 8000;
        //   timer.Enabled = true;
        //  goto jmp;
      }
      else
      {
        txtNewFCCDisplay.BorderColor = System.Drawing.Color.Gray;
        txtNewFCCDisplay.BorderWidth = 1;
      }



      if (txtNewFCCDisplay.Text.Length > 7999)
      {
        lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. Description too long. (8000max)<br/>";
        lblInfo.Visible = true;
        txtNewFCCDisplay.BorderColor = System.Drawing.Color.Red;
        txtNewFCCDisplay.BorderWidth = 2;
        purpose = "redo";
        isValid = false;
        //    timer.Interval = 8000;
        //     timer.Enabled = true;
        //  goto jmp;

      }
      else
      {
        txtNewFCCDisplay.BorderColor = System.Drawing.Color.Gray;
        txtNewFCCDisplay.BorderWidth = 1;
      }
      if (ddlHeat.SelectedIndex < 1)
      {
        lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. Lead Heat Is Required<br/>";
        lblInfo.Visible = true;
        ddlHeat.BorderColor = System.Drawing.Color.Red;
        ddlHeat.BorderWidth = 2;
        purpose = "redo";
        isValid = false;
        //    timer.Interval = 8000;
        //     timer.Enabled = true;
        //   goto jmp;

      }
      else
      {
        ddlHeat.BorderColor = System.Drawing.Color.Gray;
        ddlHeat.BorderWidth = 1;
      }
      if (ddlNewFCCType.SelectedIndex < 1)
      {
        lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. FCC Type is Required<br/>";
        lblInfo.Visible = true;
        ddlNewFCCType.BorderColor = System.Drawing.Color.Red;
        ddlNewFCCType.BorderWidth = 2;
        purpose = "redo";
        isValid = false;
        //    timer.Interval = 8000;
        //     timer.Enabled = true;
        //  goto jmp;

      }
      else
      {
        ddlNewFCCType.BorderColor = System.Drawing.Color.Gray;
        ddlNewFCCType.BorderWidth = 1;
      }


      if (Convert.ToDateTime(txtNewFCCDate.Text) < DateTime.Today)
      {
        lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. Choose a date in the Future!<br/>";
        lblInfo.Visible = true;
        txtNewFCCDate.BorderColor = System.Drawing.Color.Red;
        txtNewFCCDate.BorderWidth = 2;
        purpose = "redo";
        isValid = false;
        //    timer.Interval = 8000;
        //    timer.Enabled = true;
        // goto jmp;

      }
      else
      {
        txtNewFCCDate.BorderColor = System.Drawing.Color.Gray;
        txtNewFCCDate.BorderWidth = 1;
      }
      if (Convert.ToDateTime(txtNewFCCDate.Text) > DateTime.Today.AddMonths(18))
      {
        lblInfo.Text = "Your FCC has <bold>NOT</bold> been Created. Choose a date with 18 months of today!<br/>";
        lblInfo.Visible = true;
        txtNewFCCDate.BorderColor = System.Drawing.Color.Red;
        txtNewFCCDate.BorderWidth = 2;
        purpose = "redo";
        isValid = false;
        //    timer.Interval = 8000;
        //    timer.Enabled = true;
        // goto jmp;

      }
      else
      {
        txtNewFCCDate.BorderColor = System.Drawing.Color.Gray;
        txtNewFCCDate.BorderWidth = 1;
      }
      return isValid;
    }


    protected Boolean SendErrorEmail(String err, WimcoBaseLogic.BusinessLogic.FCCobj fcc)
    {
      Dashboard.BaseLogic.UserData ud = (Dashboard.BaseLogic.UserData)Session["UserData"];
      string sUserName = ud.username.ToString();
      string sUserID = ud.UserID.ToString();
      Boolean bResults = true;
      string sSubject = string.Format("Add FCC Error");
      string sBody = "Body Text";
      //build message string
      sBody = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
      sBody += "<HTML><HEAD><META http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">";
      sBody += "</HEAD><BODY>";
      sBody += "<table align='center' width='400' style='border: 1px solid #d9d9d9'><tr><td style='padding: 20px'><p style='font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #4071CF'><strong>Error Information:</strong></p><p style='font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #505050; line-height: 18px'>";

      sBody += "<hr /><p style='font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #4071CF'><strong>Client Information:</strong></p><p style='font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #505050; line-height: 18px'>";

      sBody += string.Format("ClientID:<strong> {0}</strong><br />", fcc.ClientID);
      sBody += string.Format("Error:<strong> {0}</strong><br />", err);
      sBody += " Source URL:<strong>" + HttpContext.Current.Request.Url.AbsoluteUri + " </strong><br />";
      sBody += string.Format("Agent: <strong>{0} / {1}</strong></p></td></tr></table>", sUserName, sUserID);
      sBody += "</body></html>";
      string sTo = "damaral@wimco.com";//"concierge@wimco.com";
      string sFrom = "wsbhvillas@wimco.com";
      string sCC = "webmaster@wimco.com;jkelly@wimco.com";
      try
      {

        Dashboard.SendEmail.SendMessage(sSubject, sBody, sFrom, sTo, sCC, true);
      }
      catch { bResults = false; }
      return bResults;
    }

  }
}