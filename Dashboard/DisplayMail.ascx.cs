﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using WimcoBaseLogic;
using WimcoBaseLogic.BusinessLogic;
using System.Net.Mail;
namespace Dashboard
{
  public partial class DisplayMail : System.Web.UI.UserControl
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().Contains("showall"))
      {

        try
        {

          WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS cel = new WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS();
          String[] emails = WimcoBaseLogic.BaseLogic.clientemail_GetEmailAddressArray(Int32.Parse(Session["ClientID"].ToString()));

          DataSet dsShowAll = ((DataSet)WimcoBaseLogic.BaseLogic.SearchByEmailAddresses(emails));

          DataView dvShowAll = dsShowAll.Tables[0].DefaultView;
          dvShowAll.Sort = "Date DESC";

          RptEmails.DataSource = dvShowAll;
          RptEmails.DataBind();


        }
        catch { }
        try
        {
          lblTempStorageEmailTo.Text = HttpContext.Current.Session["EmailToSearch"].ToString();

        }
        catch
        {
        }

        upEmailbody.Visible = true;
        pnlEmail.Visible = false;
        btnShowAll.Visible = false;
        btnReplyAll.Visible = false;
        btnReply.Visible = true;// false;
      }
      else
      {

        upEmailbody.Visible = false;
        if (!IsPostBack)
        {

          string GroupWiseID = "";



          if (Request.Url.AbsoluteUri.ToLower().Contains("hid="))
          { //This will handle the dashboard hover piece using ClientID
            Session["ClientID"] = Request.QueryString["hid"].ToString();
            String[] emails = WimcoBaseLogic.BaseLogic.clientemail_GetEmailAddressArray(Int32.Parse(Session["ClientID"].ToString()));
            for (int i = 0; i < emails.Length; i++)
            {
              WimcoBaseLogic.BaseLogic.GWSearch gws = new WimcoBaseLogic.BaseLogic.GWSearch();
              gws = WimcoBaseLogic.BaseLogic.Groupwise_SearchByEmail(Int32.Parse(Session["ClientID"].ToString()), emails[i].ToString());


            }

            DataRow[] drgw = WimcoBaseLogic.BaseLogic.SearchByEmailAddresses(emails).Tables[0].Select("", "Date DESC", DataViewRowState.CurrentRows);
            if (drgw.Length > 0)
            {
              GroupWiseID = drgw[0]["GroupwiseID"].ToString();
            }
            else
            {
              GroupWiseID = Request.QueryString["m"];
            }
          }
          else
          {
            GroupWiseID = Request.QueryString["m"];
          }

          if (Session["ClientID"] != null)
          {
            btnFWD.Visible = true;
          }
          else
          {
            btnFWD.Visible = false;
          }

          WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();
          GWMailObject mail = new GWMailObject();
          string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
          bizLogic.Timeout = 5000;
          try
          {
            mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
          }
          catch
          {
            mail.StatusCode = -1144;
          }
          if (mail.StatusCode == 0)
          {
            Page.Title = mail.Subject;
            string m = string.Empty;
            Boolean HideDetails = false; //Use cookie to determine if mail details should be hidden
            HttpCookie c = HttpContext.Current.Request.Cookies["conmob"];
            if (c != null)
            {

              if (c.Values["ConMob"] != null)
              {
                HideDetails = Boolean.Parse(c.Values["ConMob"].ToString());
              }
              if (HideDetails)
              {
                btnReply.Visible = false;
                btnReplyAll.Visible = false;
              }
            }

            foreach (Recipient dd in mail.Distribution.recipients)
            {
              if (HideDetails)
              {
                if (!dd.email.ToLower().Contains("@wimco"))
                {
                  dd.email = "Client";
                }
              }
              if (dd.distType.ToString() == "TO")
              {
                m = m + dd.distType.ToString() + " : " + dd.email + "<br/>";

              }
            }

            foreach (Recipient dd in mail.Distribution.recipients)
            {
              if (HideDetails)
              {
                if (!dd.email.ToLower().Contains("@wimco"))
                {
                  dd.email = "Client";
                }
              }
              if (dd.distType.ToString() != "TO" && dd.distType != DistributionType.BC)
              {
                m = m + dd.distType.ToString() + " : " + dd.email + "<br/>";
                lblTempStorageEmailFrom.Text = dd.email;
              }
              if (dd.distType.ToString() == "TO" && !dd.email.ToLower().Contains("@wimco"))
              {
                lblTempStorageEmailToClient.Text = dd.email.ToLower();
              }
            }

            if (HideDetails)
            {
              if (!mail.FromEmail.ToLower().Contains("@wimco"))
              {
                mail.FromEmail = "Client";
              }
              if (!mail.Distribution.to.ToLower().Contains("@wimco"))
              {
                mail.Distribution.to = "Client";
              }

            }

            m = m + "From            : " + mail.FromName + " <" + mail.FromEmail + "><br/>";
            m = m + "Subj            : " + mail.Subject + "<br/>";
            m = m + "Date            : " + mail.DateSent + "<br/>";

            lblTempStorageSubject.Text = mail.Subject;
            lblTempStorageEmailFromName.Text = mail.Distribution.to;
            lblEmail.Text = m;

            lblTempStorageEmailTo.Text = mail.FromEmail;
            //lblTempStorageEmailFrom.Text = 

            // attachments start


            Session.Remove("s1");
            Session.Remove("s2");
            Session.Remove("s3");
            Session.Remove("s4");
            Session.Remove("s5");
            lblAttachment.Visible = false;
            lnkBtnAtt1.Text = "";
            lnkBtnAtt1.Visible = false;
            lnkBtnAtt2.Text = "";
            lnkBtnAtt2.Visible = false;
            lnkBtnAtt3.Text = "";
            lnkBtnAtt3.Visible = false;
            lnkBtnAtt4.Text = "";
            lnkBtnAtt4.Visible = false;
            lnkBtnAtt5.Text = "";
            lnkBtnAtt5.Visible = false;

            try
            {
              int i = 1;
              foreach (AttachmentItemInfo ao in mail.Attachments)
              {

                if (ao.size == 0)
                {
                  goto jmp;
                }


                if (ao.name.Contains(".htm"))
                {
                  goto jmp;
                }

                if (ao.name.Contains("Mime"))
                {
                  goto jmp;
                }

                if (i == 1)
                {
                  lnkBtnAtt1.Text = ao.name;
                  lnkBtnAtt1.CommandArgument = "s1";
                  lnkBtnAtt1.Visible = true;
                  Session.Add("s1", ao);
                  Session.Add("s1gwid", GroupWiseID);
                  lblAttachment.Visible = true;
                }

                if (i == 2)
                {
                  lnkBtnAtt2.Text = ao.name;
                  lnkBtnAtt2.CommandArgument = "s2";
                  lnkBtnAtt2.Visible = true;
                  Session.Add("s2", ao);
                  Session.Add("s2gwid", GroupWiseID);
                  lblAttachment.Visible = true;
                }

                if (i == 3)
                {
                  lnkBtnAtt3.Text = ao.name;
                  lnkBtnAtt3.CommandArgument = "s3";
                  lnkBtnAtt3.Visible = true;
                  Session.Add("s3", ao);
                  Session.Add("s3gwid", GroupWiseID);
                  lblAttachment.Visible = true;
                }

                if (i == 4)
                {
                  lnkBtnAtt4.Text = ao.name;
                  lnkBtnAtt4.CommandArgument = "s4";
                  lnkBtnAtt4.Visible = true;
                  Session.Add("s4", ao);
                  Session.Add("s4gwid", GroupWiseID);
                  lblAttachment.Visible = true;
                }

                if (i == 5)
                {
                  lnkBtnAtt5.Text = ao.name;
                  lnkBtnAtt5.CommandArgument = "s5";
                  lnkBtnAtt5.Visible = true;
                  Session.Add("s5", ao);
                  Session.Add("s5gwid", GroupWiseID);
                  lblAttachment.Visible = true;
                }



                i++;

              jmp:;

              }
            }
            catch (Exception exx)
            {
              string err = exx.Message;
            }

            // attachments end


            if (mail.MessageContainsHTML)
            {

              if (mail.MessageInHTML.Contains("<img src=\"cid:"))
              {
                mail.MessageInHTML = mail.MessageInHTML.Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href");
              }


              lblHTML.Text = mail.MessageInHTML.Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href");
              lblTempStorage.Text = mail.MessageInHTML.Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href");
              txtEmailMsg.Text = "";
              txtEmailMsg.Visible = false;
            }

            if (!mail.MessageContainsHTML)
            {
              lblHTML.Text = txtEmailMsg.Text = "<pre>" + HttpUtility.HtmlDecode(mail.MessageContent.Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href")) + "</pre>";
              //txtEmailMsg.Text = mail.MessageContent.Replace("text/javascript", "text/noscript"); 
              lblTempStorage.Text = "<pre>" + mail.MessageContent.Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href") + "</pre>";
              txtEmailMsg.Visible = false;
              txtEmailMsg.Text = "";
            }

          }

          if (mail.StatusCode != 0)
          {
            txtEmailMsg.Visible = false;
            lblEmail.Text = "<br/><strong> **** Email has been deleted from Server.. Sorry :(   </strong><br/>";
          }

          if (mail.StatusCode == -1144)
          {
            txtEmailMsg.Visible = false;
            lblEmail.Text = "<br/><strong> **** I Can't reach Groupwise to read this email.. Perhaps the server is down. Please try again later. Sorry :(   </strong><br/>";

          }


        }
      }
    }
    protected void lnkBtnAtt1_Click(object sender, EventArgs e)
    {

      LinkButton li = (LinkButton)sender;
      string op = li.CommandArgument;
      string op2 = op + "gwid";


      AttachmentItemInfo ao = (AttachmentItemInfo)Session[op];
      string GroupWiseID = (string)Session[op2];


      WimcoBusinessLogic bizLogic = new WimcoBusinessLogic();

      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
      string UserName = WimcoBaseLogic.BaseLogic.AdHocSeek("SELECT Groupwise_User.UserName FROM  Groupwise_Recipients INNER JOIN Groupwise_User ON Groupwise_Recipients.MsgOwner = Groupwise_User.GroupwiseUserID WHERE (Groupwise_Recipients.GroupwiseID = N'" + GroupWiseID + "')", "UserName");
      string Password = WimcoBaseLogic.BaseLogic.AdHocSeek("SELECT Groupwise_User.Password FROM  Groupwise_Recipients INNER JOIN Groupwise_User ON Groupwise_Recipients.MsgOwner = Groupwise_User.GroupwiseUserID WHERE (Groupwise_Recipients.GroupwiseID = N'" + GroupWiseID + "')", "Password");
      GWAttachmentObject att = new GWAttachmentObject();
      att = bizLogic.Groupwise_GetAttachment(AuthKey, UserName, Password, ao);


      if (att.Content.Length == 0) // got an empty attachment.
      {
        /*
        lblAction.Text = "Cant Access this attachment on Groupwise. Sorry :(";
        lblAction.Visible = true;
        timer.Interval = 5000;
        timer.Enabled = true;
        */
      }

      if (att.Content.Length > 0)
      {
        Response.ClearContent();
        Response.ClearHeaders();

        Response.Buffer = true;
        //Response.BufferOutput = true;


        Response.Cache.SetCacheability(HttpCacheability.Public);
        Response.Cache.SetMaxAge(new TimeSpan(1, 0, 0));

        Response.AddHeader("Content-Disposition", "filename=" + att.AttachmentName.Replace(" ", "_"));
        Response.AddHeader("Content-Length", att.Lenght.ToString());


        Response.ContentType = "application/unknown";
        Response.BinaryWrite(att.Content);

        Response.Flush();

        Response.End();
        //Response.Close();

      }
    }
    protected void btnShowAll_Click(object sender, EventArgs e)
    {
      Response.Redirect("viewmail.aspx?showall=true");

    }
    protected void btnReply_Click(object sender, EventArgs e)
    {
      String m = "";
      WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();

      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
      GWMailObject mail = new GWMailObject();
      BaseLogic.UserData ud = BaseLogic.GetUserData();
      if (HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().Contains("showall"))
      {
        HiddenField hfGWID = ((HiddenField)RptEmails.Items[0].FindControl("hfEmailID"));
        mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, hfGWID.Value.ToString());
        try
        {
          lblTempStorage.Text = Session["emailBody"].ToString();
        }
        catch { }
      }
      else
      {
        String GroupWiseID = "";
        try
        {
          GroupWiseID = Request.QueryString["m"];

          //     bizLogic.Timeout = 5000;
          try
          {
            mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
          }
          catch
          {
            mail.StatusCode = -1144;
          }
          if (mail.StatusCode == 0)
          {

          }

        }
        catch
        {

        }
      }

      try
      {
        m += ">>> " + mail.FromName + " &lt;" + mail.FromEmail + "&gt; ";
        m = m + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + " >>> <br/>";
      }
      catch { }
      //EmailForm.Visible = true;
      //EmailForm.EmailTo = lblTempStorageEmailTo.Text.ToLower();
      //EmailForm.EmailName = ud.FullName; //lblTempStorageEmailFromName.Text;
      //EmailForm.EmailFrom = ud.EmailAddress;//lblTempStorageEmailFrom.Text;
      //EmailForm.EmailSubject = "Re: " + lblTempStorageSubject.Text;

      // EmailForm.EmailCC = ud.EmailAddress;
      string replydata = "";
      // EmailForm.EmailBody = replydata;
      if (lblTempStorage.Text.Contains("INSERT SIG HERE"))
      {
        replydata = lblTempStorage.Text;
      }
      else
      {
        replydata = "<br><br><w99><br></w99>" + m + lblTempStorage.Text;
      }
      MailMessage emsg = new MailMessage();
      if (ud.EmailAddress.ToLower() == lblTempStorageEmailTo.Text.ToLower())
      {
        emsg.To.Add(new MailAddress(lblTempStorageEmailToClient.Text.ToLower(), lblTempStorageEmailToClient.Text.ToLower()));
      }
      else
      {
        emsg.To.Add(new MailAddress(lblTempStorageEmailTo.Text.ToLower(), lblTempStorageEmailTo.Text.ToLower()));
      }
      emsg.From = new MailAddress(ud.EmailAddress, ud.FullName);
      emsg.Subject = "Re: " + lblTempStorageSubject.Text;
      if (lblTempStorage.Text != "")
      {
        emsg.Body = replydata;
      }
      else
      {
        emsg.Body = "";
      }

      Session["EmailObject"] = emsg;

      Response.Redirect("sendemail.aspx");

      //     Response.Redirect(HttpContext.Current.Request.Url.ToString().Replace("viewmail", "sendemail"));

      //  pnlEmail.Visible = false;
      //  upEmailbody.Visible = false;
      //  //pnlSearch.Visible = false;
      //  pnlEmailButtons.Visible = false;
      //  //  pnlReply.Visible = true;
      //  //   pnlFCC.Visible = true;
      //  //   wimAddFCC.Attributes["purpose"] = "addFCC";
      //  //    ((TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = lblTempStorageSubject.Text;

    }
    protected void btnReplyAll_Click(object sender, EventArgs e)
    {
      string m = "";
      string strEmails = "";
      string strCCs = "";
      String EmailToSearch = "";
      DataTable dtEmails;

      try
      {
        EmailToSearch = Session["EmailToSearch"].ToString();
      }
      catch
      {
        EmailToSearch = lblTempStorageEmailTo.Text.ToLower();
      }


      ObjectDataSource1.SelectParameters["Email"].DefaultValue = EmailToSearch;
      ObjectDataSource1.DataBind();


      dtEmails = (((DataView)ObjectDataSource1.Select()).ToTable());

      WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();
      GWMailObject mail = new GWMailObject();
      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();


      if (HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().Contains("showall"))
      {
        HiddenField hfGWID = ((HiddenField)RptEmails.Items[0].FindControl("hfEmailID"));
        mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, hfGWID.Value.ToString());
        try
        {
          lblTempStorage.Text = Session["emailBody"].ToString();

        }
        catch { }
        foreach (DataRow dr in dtEmails.Rows)
        {
          if (strEmails.IndexOf(dr["FromEmail"].ToString().ToLower()) == -1)
          {
            strEmails += dr["FromEmail"].ToString().ToLower() + "; ";

          }
          if (strEmails.IndexOf(dr["Recipient"].ToString().ToLower()) == -1)
          {
            strEmails += dr["Recipient"].ToString().ToLower() + "; ";

          }

        }
        if (strEmails.Length == 0)
        {

          strEmails = EmailToSearch;
        }

      }
      else
      {
        String GroupWiseID = "";
        try
        {
          GroupWiseID = Request.QueryString["m"];

          //     bizLogic.Timeout = 5000;
          try
          {
            mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
          }
          catch
          {
            mail.StatusCode = -1144;
          }
          if (mail.StatusCode == 0)
          {
            strEmails = mail.FromEmail + ";"; //+ lblTempStorageEmailFrom.Text;
            foreach (Recipient dd1 in mail.Distribution.recipients)
            {
              if (dd1.distType == DistributionType.TO && !strEmails.ToLower().Contains(dd1.email.ToLower()))
              {

                strEmails += dd1.email + ";";

              }
            }

            foreach (Recipient dd in mail.Distribution.recipients)
            {
              if (dd.distType == DistributionType.CC && !strEmails.ToLower().Contains(dd.email.ToLower()))
              {

                strCCs += dd.email + ";";

              }
            }
          }

        }
        catch
        {
          strEmails = lblTempStorageEmailFrom.Text;
        }
      }

      m += "<br/> >>> " + mail.FromName + " &lt;" + mail.FromEmail + "&gt; ";
      m = m + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + " >>> <br/>";

      //   strEmails = "damaral@wimco.com;" + strEmails; //For testing only
      //     strEmails = "dsamaral@wimco.com;" + strEmails; //For testing only

      BaseLogic.UserData ud = BaseLogic.GetUserData();
      GWMailObject mail2 = new GWMailObject();



      //   EmailForm.Visible = true;
      //  EmailForm.EmailName = ud.FullName; //lblTempStorageEmailFromName.Text;
      //  EmailForm.EmailFrom = ud.EmailAddress;//lblTempStorageEmailFrom.Text;
      if (strEmails.Contains(ud.EmailAddress.ToLower()))
      {
        //       EmailForm.EmailTo = strEmails.Replace(ud.EmailAddress.ToLower(), "").Replace(";;", ";").Trim(';');
      }
      else
      {
        //      EmailForm.EmailTo = strEmails.Trim(';');
      }

      //  EmailForm.EmailSubject = "Re: " + lblTempStorageSubject.Text;
      if (strCCs.Contains(ud.EmailAddress.ToLower()))
      {
        //    EmailForm.EmailCC = strCCs.Replace(ud.EmailAddress.ToLower(), "").Replace(";;", ";").Trim(';');
      }
      else
      {
        //   EmailForm.EmailCC = strCCs.Trim(';');
      }


      // EmailForm.EmailCC = strCCs;  //ud.EmailAddress;
      // string replydata = "<br><br><br><hr><br><w99></w99>" + lblTempStorage.Text;
      string replydata = "";
      // EmailForm.EmailBody = replydata;
      if (lblTempStorage.Text.Contains("INSERT SIG HERE"))
      {
        replydata = "<br><hr><br><w99></w99>" + lblTempStorage.Text;
      }
      else
      {
        replydata = "<br><br><w99></w99>" + m + lblTempStorage.Text;
      }

      if (lblTempStorage.Text != "")
      {
        //   EmailForm.EmailBody = replydata;
        mail2.MessageInHTML = replydata;
      }
      else
      {
        mail2.MessageInHTML = "";
        //    EmailForm.EmailBody = "";
      }

      MailMessage emsg = new MailMessage();
      foreach (string s in strEmails.Split(';'))
      {
        if (s != "" && s.ToLower() != ud.EmailAddress.ToLower())
        {
          emsg.To.Add(new MailAddress(s, s));
        }
      }

      emsg.From = new MailAddress(ud.EmailAddress, ud.FullName);
      emsg.Subject = "Re: " + lblTempStorageSubject.Text;

      foreach (string s in strCCs.Split(';'))
      {
        if (s != "" && s.ToLower() != ud.EmailAddress.ToLower())
        {
          emsg.CC.Add(new MailAddress(s, s));
        }
      }
      if (lblTempStorage.Text != "")
      {
        emsg.Body = replydata;
      }
      else
      {
        emsg.Body = "";
      }
      Session["EmailObject"] = emsg;



      //  Response.Redirect(HttpContext.Current.Request.Url.ToString().Replace("viewmail", "sendemail"));
      Response.Redirect("sendemail.aspx");
      //  txtEmailToAddress.Text = strEmails; //lblTempStorageEmailTo.Text.ToLower();
      //  txtEmailFromName.Text = ud.FullName; //lblTempStorageEmailFromName.Text;


      //   txtEmailFromEmailAddress.Text = ud.EmailAddress;//lblTempStorageEmailFrom.Text;


      //   txtEmailSubject.Text = "Re: " + lblTempStorageSubject.Text;
      //   txtEmailCCEmailAddress.Text = ud.EmailAddress;
      //  string replydata = "<br><br><br><hr><br><w99></w99>" + lblTempStorage.Text;

      //  txtEditor.Text = replydata;
      //   pnlEmail.Visible = false;
      //pnlSearch.Visible = false;
      //   pnlReply.Visible = true;
      //     pnlFCC.Visible = true;
      // pnlEmailButtons.Visible = false;

    }

    protected void btnFWD_Click(object sender, EventArgs e)
    {
      Session["TemplateKindID"] = "6";
      String sClientID = Session["ClientID"].ToString();
      String[] emails = WimcoBaseLogic.BaseLogic.clientemail_GetEmailAddressArray(Int32.Parse(sClientID));
      String MagUserEmail = "";

      //bool correctClient = false;
      //foreach (string s in emails) {
      //    if (s.ToLower() == lblTempStorageEmailToClient.Text.ToLower()) {
      //        correctClient = true;
      //        break;
      //    }
      //}
      //if (correctClient)
      //{


      MagUserEmail = WimcoBaseLogic.BaseLogic.AdHocSeek(String.Format("Select Top 1 MagUser.EmailAddress From ReservationLineItem rli Join MagUser on rli.ContactAgentID = MagUser.UserID Join ProductBase pb on pb.ProductBaseID = rli.ProductBaseID  Where rli.ClientID = {0} and pb.ProductKindID = 4 order by DateEntered DESC", sClientID), "EmailAddress");
      if (MagUserEmail == "")
      {
        MagUserEmail = WimcoBaseLogic.BaseLogic.AdHocSeek(String.Format("Select Top 1 mu2.EmailAddress, cb.CreateDate From Callback cb Join MagOrder mo on mo.OrderID = cb.BusinessID  Join MagUser mu2 on mu2.UserID = mo.AgentID Where mo.ClientID = {0} Order By cb.CreateDate DESC", sClientID), "EmailAddress");
      }
      //  }




      String m = "";
      WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();

      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
      GWMailObject mail = new GWMailObject();
      BaseLogic.UserData ud = BaseLogic.GetUserData();
      if (HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().Contains("showall"))
      {
        HiddenField hfGWID = ((HiddenField)RptEmails.Items[0].FindControl("hfEmailID"));
        mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, hfGWID.Value.ToString());
        try
        {
          lblTempStorage.Text = Session["emailBody"].ToString();
        }
        catch { }
      }
      else
      {
        String GroupWiseID = "";
        try
        {
          GroupWiseID = Request.QueryString["m"];

          //     bizLogic.Timeout = 5000;
          try
          {
            mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
          }
          catch
          {
            mail.StatusCode = -1144;
          }
          if (mail.StatusCode == 0)
          {

          }

        }
        catch
        {

        }
      }

      try
      {
        m += ">>> " + mail.FromName + " &lt;" + mail.FromEmail + "&gt; ";
        m = m + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + " >>> <br/>";
      }
      catch { }
      //EmailForm.Visible = true;
      //EmailForm.EmailTo = lblTempStorageEmailTo.Text.ToLower();
      //EmailForm.EmailName = ud.FullName; //lblTempStorageEmailFromName.Text;
      //EmailForm.EmailFrom = ud.EmailAddress;//lblTempStorageEmailFrom.Text;
      //EmailForm.EmailSubject = "Re: " + lblTempStorageSubject.Text;

      // EmailForm.EmailCC = ud.EmailAddress;
      string replydata = "";
      // EmailForm.EmailBody = replydata;
      if (lblTempStorage.Text.Contains("INSERT SIG HERE"))
      {
        replydata = lblTempStorage.Text;
      }
      else
      {
        replydata = "<br><br><w99><br></w99>" + m + lblTempStorage.Text;
      }
      MailMessage emsg = new MailMessage();
      if (MagUserEmail != "")
      {
        emsg.To.Add(new MailAddress(MagUserEmail.ToLower(), MagUserEmail.ToLower()));

      }
      emsg.From = new MailAddress(ud.EmailAddress, ud.FullName);
      emsg.Subject = "Re: " + lblTempStorageSubject.Text;
      if (lblTempStorage.Text != "")
      {
        emsg.Body = replydata;
      }
      else
      {
        emsg.Body = "";
      }

      Session["EmailObject"] = emsg;
      Session["EmailSendToAddress"] = MagUserEmail.ToLower();
      Response.Redirect("sendemail.aspx");
    }
  }
}