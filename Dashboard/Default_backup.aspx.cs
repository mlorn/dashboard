﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using System.Globalization;

namespace Dashboard
{
  public partial class Default_backup : System.Web.UI.Page
  {
    //private NumberFormatInfo nfi;
    DashboardData dashboard = null;
    dsDashboard ds = null;
    //string filterString = string.Empty;
    //store all the grid names.
    List<string> gridList = new List<string>
      {
        "CurrentFinalPayment", 
        "PastFinalPayment",
        "CurrentPastNonDeposit", 
        "CurrentReturnClientCallback", 
        "PastReturnClientCallback",
        "CurrentFCC",
        "PastFCC",
        "CurrentLead",
        "PastLead",
        "InqRLI",
        "BkdRLI",
        "WtlRLI",
        "RAClient",
        "AllGrids"
      };

    int currentFinalPayment = 0;
    int pastFinalPayment = 0;
    int currentPastNonDeposit = 0;
    int currentReturnClientCallback = 0;
    int pastReturnClientCallback = 0;
    int currentFCC = 0;
    int pastFCC = 0;
    int currentLead = 0;
    int pastLead = 0;
    int inqRLI = 0;
    int bkdRLI = 0;
    int wtlRLI = 0;
    int rowTotal = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
      int userID = 0;
      int groupID = 0;
      string userName = string.Empty;
      string adminUserName = string.Empty;
      string filterString = string.Empty;
      string sortBy = string.Empty;

      try
      {
        userID = (int)Session["UserID"];
        groupID = (int)Session["GroupID"];
        userName = Session["UserName"].ToString();
        adminUserName = Session["AdminUserName"].ToString();
      }
      catch
      {
        //TODO: handle exception correctly
        Response.Redirect("login.aspx");
      }

      //if a user is belong to admin group, use ADMIN user name.
      if (adminUserName.Length > 0)
      {
        userName = adminUserName;
      }


      if (!IsPostBack)
      {
        dashboard = new DashboardData(userID, groupID, userName);
        
        //testing
        //dashboard.DashboardDataset.WriteXml("c:/dashTemp.xml");
        //testing 
        //dashboard = new DashboardData();
        //dashboard.DashboardDataset.ReadXml("c:/dashTemp.xml");

        ds = dashboard.DashboardDataset;

        Session["DataSet"] = ds;
        grdRA.DataSource = ds.Tables["tblResAgent"];
        grdRA.DataBind();
      }
      else
      {
        ds = Session["DataSet"] as dsDashboard;
        //grdRA.LoadClientLayout(grdRA.SettingsCookies.CookiesID);
      }
      /*
      if (ds != null)
      {
        grdRA.DataSource = ds.Tables["tblResAgent"];
        //grdRA.KeyFieldName = "UserID";

        grdRA.DataBind();

      }
      */

      //if it is the Sales Agent Group, do not show foot total
      if (groupID == 4 || groupID == 195)  // admin group
      {
        //grdRA.FooterRow.Visible = true;
        grdRA.Visible = true;
      }
      else
      {
        filterString = string.Format("UserID = {0}", userID);
        //grdRA.FooterRow.Visible = false;
        assignData(grdFinalAndNonDepositRA, "tblFinalAndNonDeposit", filterString, sortBy);
        grdFinalAndNonDepositRA.ToolTip = filterString;

        assignData(grdReturnClientCallbackRA, "tblClientCallback", filterString, sortBy);
        grdReturnClientCallbackRA.ToolTip = filterString;

        assignData(grdFCCRA, "tblFCC", filterString, sortBy);
        grdFCCRA.ToolTip = filterString;

        filterString = string.Format("UserID = {0} AND (Status = 'INQ' OR Status = 'WTL' OR Status = 'WTX')", userID);
        assignData(grdInqAndWaitListRA, "tblReservation", filterString, sortBy);
        grdInqAndWaitListRA.ToolTip = filterString;

        filterString = string.Format("UserID = {0} AND (Status = 'BKD' OR Status = 'NEW')", userID);
        assignData(grdInHouseAndArrivalRA, "tblInHouseAndArrival", filterString, sortBy);
        grdInHouseAndArrivalRA.ToolTip = filterString;

        filterString = string.Format("UserID = {0} AND (Status = 'BKD' OR Status = 'NEW')", userID);
        sortBy = "DateFrom";
        assignData(grdReservationRA, "tblReservation", filterString, sortBy);
        grdReservationRA.ToolTip = filterString;
        sortBy = string.Empty;
      }
    }

    /// <summary>
    /// Shows or hides all grids when plus/minus button is pressed.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideAllGrids(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;
      string sortBy = string.Empty;

      //remove the current the active grid from the list so that it will not be closed,
      //but close the rest of the opened grid.
      gridList.Remove("AllGrids");
      closeOpenedGrid();

      ImageButton imgbtnShowHide = (sender as ImageButton);
      GridViewRow row = (imgbtnShowHide.NamingContainer as GridViewRow);
      if (imgbtnShowHide.CommandArgument == "Show")
      {
      row.FindControl("pnlAllGrids").Visible = true;
      imgbtnShowHide.CommandArgument = "Hide";
      imgbtnShowHide.ImageUrl = "~/images/minus.gif";

      userID = (int)grdRA.DataKeys[row.RowIndex].Value;

      filterString = string.Format("UserID = {0}", userID);

      //GridView grdClient = row.FindControl("grdClient") as GridView;
      //assignData(grdClient, "tblClient", filterString);
      //grdClient.ToolTip = filterString;

      GridView grdFinalAndNonDeposit = row.FindControl("grdFinalAndNonDepositAdmin") as GridView;
      assignData(grdFinalAndNonDeposit, "tblFinalAndNonDeposit", filterString, sortBy);
      grdFinalAndNonDeposit.ToolTip = filterString;

      //GridView grdNonDeposit = row.FindControl("grdNonDeposit") as GridView;
      //assignData(grdNonDeposit, "tblNonDeposit", filterString, sortBy);
      //grdNonDeposit.ToolTip = filterString;

      GridView grdReturnClientCallback = row.FindControl("grdReturnClientCallbackAdmin") as GridView;
      assignData(grdReturnClientCallback, "tblClientCallback", filterString, sortBy);
      grdReturnClientCallback.ToolTip = filterString;

      //GridView grdLead = row.FindControl("grdLead") as GridView;
      //assignData(grdLead, "tblClientCallbackLead", filterString, sortBy);
      //grdLead.ToolTip = filterString;

      GridView grdFCC = row.FindControl("grdFCCAdmin") as GridView;
      assignData(grdFCC, "tblFCC", filterString, sortBy);
      grdFCC.ToolTip = filterString;

      filterString = string.Format("UserID = {0} AND (Status = 'INQ' OR Status = 'WTL' OR Status = 'WTX')", userID);
      GridView grdInqAndWaitlist = row.FindControl("grdInqAndWaitlistAdmin") as GridView;
      assignData(grdInqAndWaitlist, "tblReservation", filterString, sortBy);
      grdInqAndWaitlist.ToolTip = filterString;

      filterString = string.Format("UserID = {0} AND (Status = 'BKD' OR Status = 'NEW')", userID);
      GridView grdInHouseAndArrival = row.FindControl("grdInHouseAndArrivalAdmin") as GridView;
      assignData(grdInHouseAndArrival, "tblInHouseAndArrival", filterString, sortBy);
      grdInHouseAndArrival.ToolTip = filterString;

      filterString = string.Format("UserID = {0} AND (Status = 'BKD' OR Status = 'NEW')", userID);
      GridView grdReservation = row.FindControl("grdReservationAdmin") as GridView;
      sortBy = "DateFrom";
      assignData(grdReservation, "tblReservation", filterString, sortBy);
      grdReservation.ToolTip = filterString;

      }
      else
      {
        row.FindControl("pnlAllGrids").Visible = false;
        imgbtnShowHide.CommandArgument = "Show";
        imgbtnShowHide.ImageUrl = "~/images/plus.gif";
      }
    }

    protected void grdClient_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      //int userID;

      GridView grdClient = (sender as GridView);
      grdClient.PageIndex = e.NewPageIndex;
      //userID = Convert.ToInt32(grdClient.ToolTip);
      filterString = grdClient.ToolTip;
      assignData(grdClient, "tblClient", filterString, sortBy);
      
      //grdClient.ToolTip = userID.ToString();
    }


    protected void grdFinalAndNonDeposit_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;

      GridView grdFinalAndNonDeposit = (sender as GridView);
      grdFinalAndNonDeposit.PageIndex = e.NewPageIndex;
      //int userID = Convert.ToInt32(grdFinalPayment.ToolTip);
      filterString = grdFinalAndNonDeposit.ToolTip;
      assignData(grdFinalAndNonDeposit, "tblFinalAndNonDeposit", filterString, sortBy);

      //grdFinalPayment.ToolTip = filterString;
    }

    protected void grdInHouseAndArrival_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;

      GridView grdInHouseAndArrival = (sender as GridView);
      grdInHouseAndArrival.PageIndex = e.NewPageIndex;

      filterString = grdInHouseAndArrival.ToolTip;
      assignData(grdInHouseAndArrival, "tblInHouseAndArrival", filterString, sortBy);

      //grdNonDeposit.ToolTip = userID.ToString();
    }

    protected void grdReservation_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = "DateFrom";

      GridView grdReservation = (sender as GridView);
      grdReservation.PageIndex = e.NewPageIndex;

      filterString = grdReservation.ToolTip;
      assignData(grdReservation, "tblReservation", filterString, sortBy);

      //grdNonDeposit.ToolTip = userID.ToString();
    }

    protected void grdReturnClientCallback_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;

      GridView grdReturnClientCallback = (sender as GridView);
      grdReturnClientCallback.PageIndex = e.NewPageIndex;

      filterString = grdReturnClientCallback.ToolTip;
      assignData(grdReturnClientCallback, "tblClientCallback", filterString, sortBy);

    }

    protected void grdLead_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;

      GridView grdLead = (sender as GridView);
      grdLead.PageIndex = e.NewPageIndex;

      filterString = grdLead.ToolTip;
      assignData(grdLead, "tblClientCallbackLead", filterString, sortBy);
    }


    protected void grdFCC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;

      GridView grdFCC = (sender as GridView);
      grdFCC.PageIndex = e.NewPageIndex;
      filterString = grdFCC.ToolTip;
      assignData(grdFCC, "tblFCC", filterString, sortBy);
    }

    protected void grdInqAndWaitList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;

      GridView grdInqAndWaitList = (sender as GridView);
      grdInqAndWaitList.PageIndex = e.NewPageIndex;

      filterString = grdInqAndWaitList.ToolTip;
      assignData(grdInqAndWaitList, "tblReservation", filterString, sortBy);
    }


    /// <summary>
    /// Shows or hides all grids that belong to a client.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideClientGrids(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int clientID;
      string emailAddress = string.Empty;

      ImageButton imgShowHide = (sender as ImageButton);
      GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
      Session["ClientRow"] = row;
      if (imgShowHide.CommandArgument == "Show")
      {
        row.FindControl("pnlClientGrids").Visible = true;
        imgShowHide.CommandArgument = "Hide";
        imgShowHide.ImageUrl = "~/images/minus.gif";

        clientID = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Values["ClientID"]);
        emailAddress = (row.NamingContainer as GridView).DataKeys[row.RowIndex].Values["EMailAddress"].ToString();

        filterString = string.Format("ClientID = {0}", clientID);

        GridView grdFinalPayment = row.FindControl("grdFinalPayment2") as GridView;
        assignData(grdFinalPayment, "tblFinalPayment", filterString, sortBy);
        grdFinalPayment.ToolTip = filterString;

        GridView grdNonDeposit = row.FindControl("grdNonDeposit2") as GridView;
        assignData(grdNonDeposit, "tblNonDeposit", filterString, sortBy);
        grdNonDeposit.ToolTip = filterString;

        GridView grdReturnClientCallback = row.FindControl("grdReturnClientCallback2") as GridView;
        assignData(grdReturnClientCallback, "tblClientCallback", filterString, sortBy);
        grdReturnClientCallback.ToolTip = filterString;

        GridView grdLead = row.FindControl("grdLead2") as GridView;
        assignData(grdLead, "tblClientCallbackLead", filterString, sortBy);
        grdLead.ToolTip = filterString;

        GridView grdFCC = row.FindControl("grdFCC2") as GridView;
        assignData(grdFCC, "tblFCC", filterString, sortBy);
        grdFCC.ToolTip = filterString;


        GridView grdResvHistory = row.FindControl("grdResvHistory2") as GridView;
        assignData(grdResvHistory, "tblReservation", filterString, sortBy);
        grdResvHistory.ToolTip = filterString;

        // find GroupWise custom control and assign e-mail address for searching by email address.
        GWCustom.GWWimco gw = row.FindControl("gwGrid2") as GWCustom.GWWimco;

        if (gw != null)
        {
          try
          {
            gw.SeachByClientID = false;
            gw.SearchByEmail = true;
            gw.EmailToSearch = emailAddress;
            gw.ReplyButtonVisible = true;

            Session["EMailAddress"] = emailAddress;
            //gw.EmailFromDisplayName = "Mao Lorn";
            //gw.EmailFromEmailAddress = "mlorn@wimco.com";

            if (gw.EmailToSearch != null)
            {
              gw.SearchEmail();
            }

          }
          catch
          { }
        }
      }
      else
      {
        row.FindControl("pnlClientGrids").Visible = false;
        imgShowHide.CommandArgument = "Show";
        imgShowHide.ImageUrl = "~/images/plus.gif";
      }
    }


    /// <summary>
    /// Shows or hides Reservation history.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideGrdResvHistory2(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int clientID;

      ImageButton imgShowHide = (sender as ImageButton);
      GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
      if (imgShowHide.CommandArgument == "Show")
      {
        row.FindControl("pnlResvHistory2").Visible = true;
        imgShowHide.CommandArgument = "Hide";
        imgShowHide.ImageUrl = "~/images/minus.gif";

        clientID = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Value);

        filterString = string.Format("ClientID = {0}", clientID);

        GridView grdResvHistory = row.FindControl("grdResvHistory2") as GridView;
        assignData(grdResvHistory, "tblReservation", filterString, sortBy);
        grdResvHistory.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlResvHistory2").Visible = false;
        imgShowHide.CommandArgument = "Show";
        imgShowHide.ImageUrl = "~/images/plus.gif";
      }
    }

    protected void grdFinalPayment2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int clientID;

      GridView grdView = (sender as GridView);
      grdView.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdView.ToolTip);
      filterString = string.Format("ClientID = {0}", clientID);
      assignData(grdView, "tblFinalPayment", filterString, sortBy);

      grdView.ToolTip = filterString;
    }

    protected void grdNonDeposit2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int clientID;

      GridView grdView = (sender as GridView);
      grdView.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdView.ToolTip);
      filterString = string.Format("ClientID = {0}", clientID);
      assignData(grdView, "tblNodDeposit", filterString, sortBy);

      grdView.ToolTip = filterString;
    }

    protected void grdReturnClientCallback2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int clientID;

      GridView grdView = (sender as GridView);
      grdView.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdView.ToolTip);
      filterString = string.Format("ClientID = {0}", clientID);
      assignData(grdView, "tblClientCallback", filterString, sortBy);

      grdView.ToolTip = filterString;
    }

    protected void grdLead2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int clientID;

      GridView grdView = (sender as GridView);
      grdView.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdView.ToolTip);
      filterString = string.Format("ClientID = {0}", clientID);
      assignData(grdView, "tblClientCallbackLead", filterString, sortBy);

      grdView.ToolTip = filterString;
    }

    protected void grdFCC2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int clientID;

      GridView grdView = (sender as GridView);
      grdView.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdView.ToolTip);
      filterString = string.Format("ClientID = {0}", clientID);
      assignData(grdView, "tblFCC", filterString, sortBy);

      grdView.ToolTip = filterString;
    }

    protected void grdResvHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int clientID;

      GridView grdResvHistory = (sender as GridView);
      grdResvHistory.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdResvHistory.ToolTip);
      filterString = string.Format("ClientID = {0}", clientID);
      assignData(grdResvHistory, "tblReservation", filterString, sortBy);

      grdResvHistory.ToolTip = filterString;
    }


    /// <summary>
    /// Hide grids
    /// </summary>
    protected void closeOpenedGrid()
    {
      ImageButton imgbtnControl;
      //find a link button control
      LinkButton lnkbtnControl;

      foreach (string ctr in gridList)
      {
        lnkbtnControl = ControlExtensions.FindControlRecursive(grdRA, "lnkbtn" + ctr) as LinkButton;
        if (lnkbtnControl != null)
        {
          GridViewRow row = (lnkbtnControl.NamingContainer as GridViewRow);
          //find a control panel and make it invisible
          row.FindControl("pnl" + ctr).Visible = false;
          lnkbtnControl.CommandArgument = "Show";
        }
        else
        {
          imgbtnControl = ControlExtensions.FindControlRecursive(grdRA, "imgbtn" + ctr) as ImageButton;
          if (imgbtnControl != null)
          {
            GridViewRow row = (imgbtnControl.NamingContainer as GridViewRow);
            //find a control panel and make it invisible
            row.FindControl("pnl" + ctr).Visible = false;
            imgbtnControl.CommandArgument = "Show";
            imgbtnControl.ImageUrl = "~/images/plus.gif";
          }
        }
      }

      // reset 
      /*
      if (!gridList.Contains("AllGrids"))
      {
        imgbtnControl = ControlExtensions.FindControlRecursive(grdRA, "AllGrids") as ImageButton;
        if (imgbtnControl != null)
        {
          GridViewRow row = (imgbtnControl.NamingContainer as GridViewRow);
          //find a control panel and make it invisible
          row.FindControl("pnlAllGrids").Visible = false;
          imgbtnControl.CommandArgument = "Show";
          imgbtnControl.ImageUrl = "~/images/plus.gif";
        }
      }
      */
    }

    /// <summary>
    /// Shows or hides current final payment.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideCurrentFinalPayment(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("CurrentFinalPayment");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);

      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";
        //lnkbtnShowHide.ImageUrl = "~/images/minus.gif";

        row.FindControl("pnlCurrentFinalPayment").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='P' AND [PastOrFuture] = 'N'", userID);

        GridView grdFinalPayment = row.FindControl("grdCurrentFinalPayment") as GridView;
        assignData(grdFinalPayment, "tblFinalPayment", filterString, sortBy);
        grdFinalPayment.ToolTip = filterString;

      }
      else
      {
        row.FindControl("pnlCurrentFinalPayment").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
        //lnkbtnShowHide.ImageUrl = "~/images/plus.gif";
      }

    }


    /// <summary>
    /// Shows or hides past final payment.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHidePastFinalPayment(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;

      int userID;
      gridList.Remove("PastFinalPayment");
      closeOpenedGrid();
      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlPastFinalPayment").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='P' AND [PastOrFuture] = 'Y'", userID);

        GridView grdFinalPayment = row.FindControl("grdPastFinalPayment") as GridView;
        assignData(grdFinalPayment, "tblFinalPayment", filterString, sortBy);
        grdFinalPayment.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlPastFinalPayment").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides current past non deposit.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideCurrentPastNonDeposit(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("CurrentPastNonDeposit");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlCurrentPastNonDeposit").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='D' AND [PastOrFuture] = 'N'", userID);

        GridView grdFinalPayment = row.FindControl("grdCurrentPastNonDeposit") as GridView;
        assignData(grdFinalPayment, "tblNonDeposit", filterString, sortBy);
        grdFinalPayment.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlCurrentPastNonDeposit").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides current return client callback.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideCurrentReturnClientCallback(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("CurrentReturnClientCallback");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlCurrentReturnClientCallback").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='R' AND [PastOrFuture] = 'N'", userID);

        GridView grdFinalPayment = row.FindControl("grdCurrentReturnClientCallback") as GridView;
        assignData(grdFinalPayment, "tblClientCallback", filterString, sortBy);
        grdFinalPayment.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlCurrentReturnClientCallback").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides past return client callback.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHidePastReturnClientCallback(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("PastReturnClientCallback");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlPastReturnClientCallback").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='R' AND [PastOrFuture] = 'Y'", userID);

        GridView grdFinalPayment = row.FindControl("grdPastReturnClientCallback") as GridView;
        assignData(grdFinalPayment, "tblClientCallback", filterString, sortBy);
        grdFinalPayment.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlPastReturnClientCallback").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides current FCC.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideCurrentFCC(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("CurrentFCC");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlCurrentFCC").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='F' AND [PastOrFuture] = 'C'", userID);

        GridView grdView = row.FindControl("grdCurrentFCC") as GridView;
        assignData(grdView, "tblFCC", filterString, sortBy);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlCurrentFCC").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides past FCC.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHidePastFCC(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("PastFCC");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlPastFCC").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='F' AND [PastOrFuture] = 'P'", userID);

        GridView grdView = row.FindControl("grdPastFCC") as GridView;
        assignData(grdView, "tblFCC", filterString, sortBy);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlPastFCC").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides current lead.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideCurrentLead(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("CurrentLead");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlCurrentLead").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='L' AND [PastOrFuture] = 'C'", userID);

        GridView grdView = row.FindControl("grdCurrentLead") as GridView;
        assignData(grdView, "tblClientCallbackLead", filterString, sortBy);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlCurrentLead").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides past lead.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHidePastLead(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("PastLead");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlPastLead").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='L' AND [PastOrFuture] = 'P'", userID);

        GridView grdView = row.FindControl("grdPastLead") as GridView;
        assignData(grdView, "tblClientCallbackLead", filterString, sortBy);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlPastLead").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides inq rli.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideInqRLI(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("InqRLI");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlInqRLI").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='I'", userID);

        GridView grdView = row.FindControl("grdInqRLI") as GridView;
        assignData(grdView, "tblReservation", filterString, sortBy);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlInqRLI").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides BKD rli.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideBkdRLI(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("BkdRLI");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlBkdRLI").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='B'", userID);

        GridView grdView = row.FindControl("grdBkdRLI") as GridView;
        assignData(grdView, "tblReservation", filterString, sortBy);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlBkdRLI").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides WTL rli.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideWtlRLI(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("WtlRLI");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlWtlRLI").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0} AND [Kind]='W'", userID);

        GridView grdView = row.FindControl("grdWtlRLI") as GridView;
        assignData(grdView, "tblReservation", filterString, sortBy);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlWtlRLI").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }


    /// <summary>
    /// Shows or hides client grid for a specific RA
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideRAClient(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      gridList.Remove("RAClient");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlRAClient").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = string.Format("UserID = {0}", userID);

        GridView grdView = row.FindControl("grdRAClient") as GridView;
        assignData(grdView, "TblClient", filterString, sortBy);
        grdView.ToolTip = filterString;

      }
      else
      {
        row.FindControl("pnlRAClient").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }


    /// <summary>
    /// Shows or hides ra client grids.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideRAClientGrids(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int clientID;
      string emailAddress = string.Empty;

      ImageButton imgShowHide = (sender as ImageButton);
      GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
      if (imgShowHide.CommandArgument == "Show")
      {
        row.FindControl("pnlRAClientGrids").Visible = true;
        imgShowHide.CommandArgument = "Hide";
        imgShowHide.ImageUrl = "~/images/minus.gif";

        clientID = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Values["ClientID"]);
        emailAddress = (row.NamingContainer as GridView).DataKeys[row.RowIndex].Values["EMailAddress"].ToString();

        filterString = string.Format("ClientID = {0}", clientID);

        Label lblTesting = row.FindControl("lblTest") as Label;

        //GWCustom.GWWimco gw = (GWCustom.GWWimco)row.FindControl("gwGrid");

        //gw.EmailToSearch = "sdsd";
        //gw.SearchEmail();

        GridView grdFinalPayment = row.FindControl("grdRAClntFinalPayment2") as GridView;
        assignData(grdFinalPayment, "tblFinalPayment", filterString, sortBy);
        grdFinalPayment.ToolTip = filterString;

        GridView grdNonDeposit = row.FindControl("grdRAClntNonDeposit2") as GridView;
        assignData(grdNonDeposit, "tblNonDeposit", filterString, sortBy);
        grdNonDeposit.ToolTip = filterString;

        GridView grdReturnClientCallback = row.FindControl("grdRAClntReturnClientCallback2") as GridView;
        assignData(grdReturnClientCallback, "tblClientCallback", filterString, sortBy);
        grdReturnClientCallback.ToolTip = filterString;

        GridView grdLead = row.FindControl("grdRAClntLead2") as GridView;
        assignData(grdLead, "tblClientCallbackLead", filterString, sortBy);
        grdLead.ToolTip = filterString;

        GridView grdFCC = row.FindControl("grdRAClntFCC2") as GridView;
        assignData(grdFCC, "tblFCC", filterString, sortBy);
        grdFCC.ToolTip = filterString;


        GridView grdResvHistory = row.FindControl("grdRAClntResvHistory2") as GridView;
        assignData(grdResvHistory, "tblReservation", filterString, sortBy);
        grdResvHistory.ToolTip = filterString;
        /*
        filterString = string.Format("EmailAddress = '{0}'", "lfrankfort@coach.com");
        GridView gwEmail = row.FindControl("grdRAClntGroupWiseEmail") as GridView;
        assignData(gwEmail, "tblClient", filterString);
        gwEmail.ToolTip = filterString;
        */

        // find GroupWise custom control and assign e-mail address for searching by email address.
        GWCustom.GWWimco gw = row.FindControl("gwGrid") as GWCustom.GWWimco;
        if (gw != null)
        {
          try
          {

            gw.SeachByClientID = false;
            gw.SearchByEmail = true;
            gw.ReplyButtonVisible = true;
            gw.EmailToSearch = emailAddress;


            Session["EMailAddress"] = emailAddress;

            //gw.EmailFromDisplayName = "Mao Lorn";
            //gw.EmailFromEmailAddress = "mlorn@wimco.com";

            if (gw.EmailToSearch != null)
            {
              gw.SearchEmail();
            }
          }
          catch
          { }

        }
      }
      else
      {
        row.FindControl("pnlRAClientGrids").Visible = false;
        imgShowHide.CommandArgument = "Show";
        imgShowHide.ImageUrl = "~/images/plus.gif";
      }
    }


    /// <summary>
    /// Assigns the data.
    /// </summary>
    /// <param name="gridView">The grid view.</param>
    /// <param name="tableName">Name of the table.</param>
    /// <param name="filterString">The filter string.</param>
    private void assignData(GridView gridView, string tableName, string filterString, string sortBy)
    {
      DataTable dt;
      DataView dv;

      //get DataSet from the session
      ds = Session["DataSet"] as dsDashboard;

      if (ds == null)
      {
        return;
      }

      try
      {
        //get a data table
        dt = ds.Tables[tableName];

        dv = dt.DefaultView;

        if (sortBy.Length > 0)
        {
          dv.Sort = sortBy;
        }
        //filter data table 
        dv.RowFilter = filterString;
        //assign detail grid with the filtered DataView
        if (dv.Count > 0)
        {
          gridView.DataSource = dv;
          gridView.DataBind();
          gridView.Visible = true;
        }
        else
        {
          gridView.Visible = false;
        }
      }
      catch (Exception ex)
      {
        string errMsg = ex.Message;
        //TODO:  better handle exception
      }

    }

    /// <summary>
    /// Sorts data.
    /// </summary>
    /// <param name="gridView">The grid view.</param>
    /// <param name="tableName">Name of the table.</param>
    /// <param name="filterString">The filter string.</param>
    /// <param name="sortExpression">The sort expression.</param>
    private void sortData(GridView gridView, string tableName, string filterString, string sortExpression)
    {
      DataTable dt;
      DataView dv;

      //get DataSet from the session
      ds = Session["DataSet"] as dsDashboard;

      if (ds == null)
      {
        return;
      }

      try
      {
        //get a data table
        dt = ds.Tables[tableName];
        //filter data table 
        dv = dt.DefaultView;
        dv.RowFilter = filterString;
        dv.Sort = sortExpression;
        //assign detail grid with the filtered DataView
        if (dv.Count > 0)
        {
          gridView.DataSource = dv;
          gridView.DataBind();
          gridView.Visible = true;
        }
        else
        {
          gridView.Visible = false;
        }
      }
      catch (Exception ex)
      {
        string errMsg = ex.Message;
        //TODO:  better handle exception
      }

    }

    /// <summary>
    /// Creates the URL for Resv No.
    /// </summary>
    /// <param name="refType">Type of the ref.</param>
    /// <param name="refNumber">The ref number.</param>
    /// <returns></returns>
    private string CreateURL(string refType, string refNumber)
    {
      string userName;
      string url = string.Empty;

      userName = Session["UserName"].ToString();
      url = SecureJump.JumpToNomad.CreateJumpURL(userName, refType, refNumber);
      return url;
    }

    /// <summary>
    /// Jumps to other site.
    /// </summary>
    /// <param name="refType">Type of the ref.</param>
    /// <param name="refNumber">The ref number.</param>
    private void jumpToOtherSite(string refType, string refNumber)
    {
      string userName;
      userName = Session["UserName"].ToString();
      string url = SecureJump.JumpToNomad.CreateJumpURL(userName, refType, refNumber);
      Response.Write("<script>window.open('" + url + "','_blank');</script>");

    }

    #region Not being used
    /// <summary>
    /// Handles the RowDataBound event of the grdRA control for adding each cell 
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void grdRA_RowDataBound(object sender, GridViewRowEventArgs e)
    {

      /*
      int temp = 0;

      if (e.Row.RowType == DataControlRowType.DataRow)
      {

        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FinalPayment"));
        currentFinalPayment += temp;

        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FinalPaymentPast"));
        pastFinalPayment += temp;

        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "NonDeposit"));
        currentPastNonDeposit += temp;

        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ReturnClientCallback"));
        currentReturnClientCallback += temp;

        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ReturnClientCallbackPast"));
        pastReturnClientCallback += temp;

        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FCCCurrent"));
        currentFCC += temp;

        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FCCPast"));
        pastFCC += temp;

        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "CurrentLead"));
        currentLead += temp;

        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "PastLead"));
        pastLead += temp;


        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "InqRLI"));
        inqRLI += temp;


        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "BkdRLI"));
        bkdRLI += temp;


        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "WtlRLI"));
        wtlRLI += temp;

        temp = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "RowTotal"));
        rowTotal += temp;
         

      }

      //update row total 
      if (e.Row.RowType == DataControlRowType.Footer)
      {
        GridViewRow row = e.Row;
        row.Cells[2].Text = currentFinalPayment.ToString();
        row.Cells[3].Text = pastFinalPayment.ToString();
        row.Cells[4].Text = currentPastNonDeposit.ToString();
        row.Cells[5].Text = currentReturnClientCallback.ToString();
        row.Cells[6].Text = pastReturnClientCallback.ToString();
        row.Cells[7].Text = currentFCC.ToString();
        row.Cells[8].Text = pastFCC.ToString();
        row.Cells[9].Text = currentLead.ToString();
        row.Cells[10].Text = pastLead.ToString();
        row.Cells[11].Text = inqRLI.ToString();
        row.Cells[12].Text = bkdRLI.ToString();
        row.Cells[13].Text = wtlRLI.ToString();
        row.Cells[14].Text = rowTotal.ToString();
      }
      */
    }
    #endregion


    protected void grdRA_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {

      int index = Convert.ToInt32(e.CommandArgument);
      GridViewRow gvRow = grdRA.Rows[index];
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
      int ID = Convert.ToInt32(((LinkButton)sender).CommandArgument);
    }


    /// <summary>
    /// Handles the Click event of the lnkbtnItinerary control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void lnkbtnItinerary_Click(object sender, EventArgs e)
    {
      string itinerary = ((LinkButton)sender).CommandArgument;
      jumpToOtherSite("I", itinerary);
    }

    /// <summary>
    /// Handles the Click event of the lnkbtnResvNo control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void lnkbtnResvNo_Click(object sender, EventArgs e)
    {
      string resvNo = ((LinkButton)sender).CommandArgument;
      jumpToOtherSite("R", resvNo);
    }

    /// <summary>
    /// Handles the Click event of the lnkbtnClientID control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
     protected void lnkbtnClientID_Click(object sender, EventArgs e)
    {
      string clientID = ((LinkButton)sender).CommandArgument;
      jumpToOtherSite("C", clientID);
    }

     /// <summary>
     /// Shows the hidelnkbtn group wise email.
     /// </summary>
     /// <param name="sender">The sender.</param>
     /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
     protected void showHidelnkbtnGroupWiseEmail(object sender, EventArgs e)
     {
       string filterString = string.Empty;
       string sortBy = string.Empty;
       string emailAddress = string.Empty;

       LinkButton lnkbtnShowHide = (sender as LinkButton);
       GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
       if (lnkbtnShowHide.CommandArgument == "Show")
       {
         lnkbtnShowHide.CommandArgument = "Hide";

         row.FindControl("pnlRAClntGroupWiseEmail").Visible = true;
         //emailAddress = (row.NamingContainer as GridView).DataKeys[row.RowIndex].Value.ToString();
         //GWCustom.GWWimco grdView = row.FindControl("GWWimcoEmailGrid1") as GWCustom.GWWimco;
         //grdView.SearchByEmail = true;
         //grdView.EmailToSearch = "louisduquesne@hotmail.com";
         //grdView.ReplyButtonVisible = true;
         //grdView.SearchEmail();

         //userID = (int)grdRA.DataKeys[row.RowIndex].Value;

         filterString = string.Format("EmailAddress = '{0}'", "lfrankfort@coach.com");

         GridView grdView = row.FindControl("grdRAClntGroupWiseEmail") as GridView;
         assignData(grdView, "tblClient", filterString, sortBy);
         grdView.ToolTip = filterString;

       }
       else
       {
         row.FindControl("pnlRAClntGroupWiseEmail").Visible = false;
         lnkbtnShowHide.CommandArgument = "Show";
       }
     }

     /// <summary>
     /// Gets or sets the grid view sort direction.
     /// </summary>
     /// <value>
     /// The grid view sort direction.
     /// </value>
    private string GridViewSortDirection
    {
      get 
      { 
        return ViewState["SortDirection"] as string ?? "ASC";
      }
      set 
      { 
        ViewState["SortDirection"] = value;
      }

    }

    /// <summary>
    /// Changes the sort direction.
    /// </summary>
    /// <returns></returns>
    private string ChangeSortDirection()
    {
      switch (GridViewSortDirection)
      {
        case "ASC":
          GridViewSortDirection = "DESC";
          break;
        case "DESC":
          GridViewSortDirection = "ASC";
          break;
        default:
          GridViewSortDirection = "ASC";
          break;
      }

      return GridViewSortDirection;
    }

    /// <summary>
    /// Handles the Sorting event of the grdClient control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdClient_Sorting(object sender, GridViewSortEventArgs e)
     {
       string filterString = string.Empty;
       string sortExpression = string.Empty;

       GridView grdClient_ = (sender as GridView);
       filterString = grdClient_.ToolTip;

       sortExpression = e.SortExpression + " " + GridViewSortDirection;
       sortData(grdClient_, "tblClient", filterString, sortExpression);
       ChangeSortDirection();
     }


    /// <summary>
    /// Handles the Sorting event of the grdFinalPayment control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdFinalAndNonDeposit_Sorting(object sender, GridViewSortEventArgs e)
     {
       string filterString = string.Empty;
       string sortExpression = string.Empty;

       GridView grdFinalAndNonDeposit = (sender as GridView);
       filterString = grdFinalAndNonDeposit.ToolTip;
       sortExpression = e.SortExpression + " " + GridViewSortDirection;
       sortData(grdFinalAndNonDeposit, "tblFinalAndNonDeposit", filterString, sortExpression);
       ChangeSortDirection();
     }

    /// <summary>
    /// Handles the Sorting event of the grdNonDeposit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdInHouseAndArrival_Sorting(object sender, GridViewSortEventArgs e)
     {
       string filterString = string.Empty;
       string sortExpression = string.Empty;

       GridView grdInHouseAndArrival = (sender as GridView);
       filterString = grdInHouseAndArrival.ToolTip;
       sortExpression = e.SortExpression + " " + GridViewSortDirection;
       sortData(grdInHouseAndArrival, "tblInHouseAndArrival", filterString, sortExpression);
       ChangeSortDirection();
     }

    /// <summary>
    /// Handles the Sorting event of the grdReturnClientCallback control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdReturnClientCallback_Sorting(object sender, GridViewSortEventArgs e)
     {
       string filterString = string.Empty;
       string sortExpression = string.Empty;

       GridView grdReturnClientCallback = (sender as GridView);
       filterString = grdReturnClientCallback.ToolTip;
       sortExpression = e.SortExpression + " " + GridViewSortDirection;
       sortData(grdReturnClientCallback, "tblClientCallback", filterString, sortExpression);
       ChangeSortDirection();
     }

    /// <summary>
    /// Handles the Sorting event of the grdLead control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdLead_Sorting(object sender, GridViewSortEventArgs e)
     {
       string filterString = string.Empty;
       string sortExpression = string.Empty;

       GridView grdLead = (sender as GridView);
       filterString = grdLead.ToolTip;
       sortExpression = e.SortExpression + " " + GridViewSortDirection;
       sortData(grdLead, "tblClientCallbackLead", filterString, sortExpression);
       ChangeSortDirection();
     }


    /// <summary>
    /// Handles the Sorting event of the grdFCC control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdFCC_Sorting(object sender, GridViewSortEventArgs e)
     {
       string filterString = string.Empty;
       string sortExpression = string.Empty;

       GridView grdFCC = (sender as GridView);
       filterString = grdFCC.ToolTip;
       sortExpression = e.SortExpression + " " + GridViewSortDirection;
       sortData(grdFCC, "tblFCC", filterString, sortExpression);
       ChangeSortDirection();
     }

    /// <summary>
    /// Handles the Sorting event of the grdResvHistory control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdInqAndWaitList_Sorting(object sender, GridViewSortEventArgs e)
     {
       string filterString = string.Empty;
       string sortExpression = string.Empty;

       GridView grdInqAndWaitList = (sender as GridView);
       filterString = grdInqAndWaitList.ToolTip;
       sortExpression = e.SortExpression + " " + GridViewSortDirection;
       sortData(grdInqAndWaitList, "tblReservation", filterString, sortExpression);
       ChangeSortDirection();
     }

    /// <summary>
    /// Handles the Sorting event of the grdReservation control.
    /// sorting the grid if a column heading is clicked.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdReservation_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;

      GridView grdReservation = (sender as GridView);
      filterString = grdReservation.ToolTip;
      sortExpression = e.SortExpression + " " + GridViewSortDirection;
      sortData(grdReservation, "tblReservation", filterString, sortExpression);
      ChangeSortDirection();
    }


    protected void lnkbtnCurrentFinalPayment_DataBinding(object sender, EventArgs e)
    {
      //LinkButton temp = sender as LinkButton;
      //currentFinalPayment += Convert.ToInt32(temp.Text); // This will add to running total of all.
    }

    /// <summary>
    /// Handles the Click event of the lnkbtnPrintFCC control.
    /// Print a FCC by opening another Browser tab and printer dialog box
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void lnkbtnPrintFCC_Click(object sender, EventArgs e)
    {
      int callbackID = Convert.ToInt32((sender as LinkButton).CommandArgument);
      Session["CallbackID"] = callbackID;
      //dashboard.PrintFCC(callbackID);
      string URL = "PrintFCC.aspx";
      ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + URL + "','_blank');", true);
    }

  }
}