﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using WimcoBaseLogic;
using WimcoBaseLogic.BusinessLogic;
namespace GWCustom
{

  public partial class EmailForm : System.Web.UI.UserControl
  {
    public string EmailTo { get; set; }
    public string EmailFrom { get; set; }
    public string EmailBody { get; set; }
    public string EmailSubject { get; set; }
    public string EmailCC { get; set; }
    public string EmailName { get; set; }
    public Attachment EmailAttach;
    public DataTable dtTemplates;
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        try
        {
          Session.Remove("EmailSent");
        }
        catch
        {
        }
      }
      DataSet dsUsers = WimcoBaseLogic.BaseLogic.AdHoc("SELECT DISTINCT mu.EmailAddress FROM  MagUser AS mu INNER JOIN UserGroup AS ug ON ug.UserID = mu.UserID WHERE  (mu.OrganizationID = 1) AND (NOT (mu.EmailAddress IS NULL)) AND (mu.IsEnabled = 'Y') AND (mu.EmailAddress <> 'none')");
      if (!hfMU.Value.Contains("@"))
      {
        foreach (DataRow drMU in dsUsers.Tables[0].Rows)
        {
          hfMU.Value += drMU[0].ToString() + ",";

        }
      }
      // HttpContext.Current.Session["FCCDateInitial"] = "4/02/2017";
      //  HttpContext.Current.Session["TemplateKindID"] = "1";
      if (HttpContext.Current.Session["TemplateKindID"] == null)
      {
        HttpContext.Current.Session["TemplateKindID"] = "3";
      }
      if (HttpContext.Current.Session["TemplateKindID"] != null)
      {
        Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
        String UserID = ud.UserID.ToString();
        // String UserID = "28";
        String TKID = HttpContext.Current.Session["TemplateKindID"].ToString();
        DataSet dsTemplates = Dashboard.BaseLogic.AdHoc("Exec spGetUserEmailTemplatesByKind @TemplateKindID = " + TKID + ", @UserID = " + UserID);
        dtTemplates = dsTemplates.Tables[0];


        DataTable dtTemplate = new DataTable(); // dsTemplate.Tables[0];
        if (ddlTemplates.Items.Count == 0)
        {
          for (int i = 0; i < dtTemplates.Rows.Count; i++)
          {
            ListItem li2 = new ListItem();
            li2.Value = dtTemplates.Rows[i][0].ToString();
            if (dtTemplates.Rows[i][4] == null || dtTemplates.Rows[i][4].ToString() == string.Empty)
            {
              li2.Text = dtTemplates.Rows[i][6].ToString().Trim();
            }
            else { li2.Text = dtTemplates.Rows[i][6].ToString().Trim(); }

            ddlTemplates.Items.Add(li2);
          }
          ListItem li = new ListItem();
          li.Value = "0";
          li.Text = "Select a Template";
          ddlTemplates.Items.Insert(0, li);
          ddlTemplates.SelectedIndex = 0;
        }

        if (HttpContext.Current.Session["DID"] != null)
        {
          wimAddFCC.FCCDID = HttpContext.Current.Session["DID"].ToString();
          HttpContext.Current.Session.Remove("DID");
        }
        if (HttpContext.Current.Session["FCCContactSourceID"] != null)
        {
          wimAddFCC.ContactSourceID = HttpContext.Current.Session["FCCContactSourceID"].ToString();
          HttpContext.Current.Session.Remove("FCCContactSourceID");
        }
        //    String sql = "Select et.TemplateTextHTML, etur.FCCDateRule, et.Subject From EmailTemplate et Inner Join EmailTemplateUserRef etur on et.EmailTemplateID = etur.EmailTemplateID where et.TemplateKindID = " + TKID + " and etur.UserID = " + UserID + " and et.IsEnabled = 'True'";
        //  DataSet dsTemplate = Dashboard.BaseLogic.AdHoc(sql);


        if (dtTemplate.Rows.Count > 0)
        {
          if (dtTemplate.Rows[0][0] != null)
          {
            //This commented out section can be re-added if threading is desired below a template.
            //   if (HttpContext.Current.Session["EmailBody"] != null)
            //   {
            //        EmailBody = dtTemplate.Rows[0][1].ToString() + "<br><br><w99><!--INSERT SIG HERE--><hr><br></w99>" + HttpContext.Current.Session["EmailBody"].ToString();
            //   }
            //    else
            //    {
            EmailBody = dtTemplate.Rows[0][1].ToString();
            //   }
            Session["EmailOriginal"] = EmailBody;

            //    wimAddFCC.FCCMethod = "2";
            if (dtTemplate.Rows[0][2] != null)
            {
              wimAddFCC.FCCDateRule = dtTemplate.Rows[0][1].ToString();
            }
            if (dtTemplate.Rows[0][3] != null)
            {
              EmailSubject = dtTemplate.Rows[0][2].ToString();
            }
          }
          else
          {
            EmailBody = string.Empty;
          }
        }
        else
        {
          EmailBody = string.Empty;
        }
      }

      else
      {
        if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("new=true"))
        {
          EmailBody = string.Empty;
        }
      }
      if (!Request.Url.ToString().ToLower().Contains("showall=true"))
      {
        btnShowAllEmails.Visible = true;

      }

    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
      Boolean RetainFCC = false;
      try
      {
        if (Session["RetainFCC"].ToString().Equals("Y"))
        {
          RetainFCC = true;
        }
        else
        {
          RetainFCC = false;
        }
      }
      catch { RetainFCC = false; }

      if (pnlReply.Visible == true)
      {
        if (Session["EmailSent"] != null && Session["EmailBody"] == null & Session["EmailOriginal"] == null)
        {
          String es = string.Empty;
          es = Session["EmailSent"].ToString();
          txtEditor.Text = HttpUtility.HtmlDecode(es);
        }

        if (lblAction.Text != "Message Sent!")
        {
          try
          {
            wimAddFCC.WimcoClientID = Int32.Parse(Session["ClientID"].ToString());
            ((System.Web.UI.WebControls.Button)wimAddFCC.FindControl("Button2")).Visible = false;
          }
          catch { }
          if (txtEmailFromEmailAddress.Text == string.Empty)
          {
            txtEmailCCEmailAddress.Text = EmailCC;
            txtEmailFromName.Text = EmailName;
            txtEmailFromEmailAddress.Text = EmailFrom;
            txtEmailToAddress.Text = EmailTo;
            txtEmailSubject.Text = EmailSubject;
          }

          Session["EmailOriginal"] = EmailBody;
          String Signature = string.Empty;
          try
          {
            Dashboard.BaseLogic.UserData ud = (Dashboard.BaseLogic.UserData)Session["UserData"];
            DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = " + ud.UserID.ToString());
            //  DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = 23"); //Test data, Bethany's Signature
            Signature = dsSig.Tables[0].Rows[0][0].ToString();
          }
          catch
          {
            Signature = string.Empty;
          }

          txtEditSig.Text = Signature;

          String strt;
          try
          {
            strt = HttpContext.Current.Session["TemplateKindID"].ToString();
          }
          catch
          {
            strt = "3";
          }

          String TemplateID = string.Empty;
          try
          {
            TemplateID = HttpContext.Current.Session["TemplateID"].ToString();
          }
          catch { }
          if (strt != null && strt != "3" || strt == "3" && TemplateID != string.Empty)
          {
            if (HttpContext.Current.Session["TemplateKindID"].ToString() == "2" || chkTemplate.Checked)
            {
              btnSend.Enabled = false;
              if (!IsPostBack)
              {
                // loadingIcon3.Visible = true;

              }
              try
              {


                if (hfSigPostBack.Value != "Y")
                {
                  txtEditor.Text = "<br><br><w99></w99>" + Session["EmailBody"].ToString().Replace("&nbsp;", " ");
                }
                else
                {
                  txtEditor.Text = HttpUtility.HtmlDecode(txtEditor.Text).Replace("&nbsp;", " ");
                }

                //   if (Session["TemplateID"] == null)
                //   {
                Session["EmailOriginal"] = Session["EmailBody"].ToString();
                //    }
                EmailBody = Session["EmailBody"].ToString();
              }
              catch { }

            }

            if (strt != null && TemplateID != string.Empty) //&& !strt.Equals("3")) //Check for template
            {


              // String TemplateID = strt;
              for (int j = 0; j < dtTemplates.Rows.Count; j++)
              {
                if (dtTemplates.Rows[j][0].ToString().Equals(TemplateID))
                {
                  if (dtTemplates.Rows[j][1] != null)
                  {
                    if (hfSigPostBack.Value != "Y")
                    {
                      if (chkTemplate.Checked)
                      {
                        txtEditor.Text = dtTemplates.Rows[j][1].ToString().Replace("&nbsp;", " ") + "<br/>" + txtEditor.Text;
                      }
                      else
                      {
                        txtEditor.Text = dtTemplates.Rows[j][1].ToString().Replace("&nbsp;", " ");
                      }
                    }
                    else
                    {
                      txtEditor.Text = HttpUtility.HtmlDecode(txtEditor.Text).Replace("&nbsp;", " ");
                    }
                    //if (chkSig.Checked)
                    //{
                    //    txtEditor.Text = dtTemplates.Rows[j][1].ToString() + "<br />" + htmlEdit.Decode(Signature);
                    //}
                    //else {
                    //    txtEditor.Text = dtTemplates.Rows[j][1].ToString();
                    //}
                  }
                  if (dtTemplates.Rows[j][2] != null)
                  {
                    if (!RetainFCC)
                    {
                      wimAddFCC.FCCDateRule = dtTemplates.Rows[j][2].ToString();
                    }
                  }

                  if (dtTemplates.Rows[j][3] != null)
                  {
                    EmailSubject = dtTemplates.Rows[j][3].ToString().Trim();
                    if (EmailSubject != string.Empty)
                    {
                      txtEmailSubject.Text = EmailSubject;
                      if (!RetainFCC && pnlFCCTemplate.Visible == false || wimAddFCC.FCCNote == null)
                      {
                        wimAddFCC.FCCNote = EmailSubject;
                        ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = txtEmailSubject.Text;
                      }
                      else
                      {
                        try
                        {
                          Array a = ((Array)HttpContext.Current.Session["fccPastValues"]);
                          wimAddFCC.FCCNote = a.GetValue(2).ToString();
                          ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = a.GetValue(2).ToString();

                        }
                        catch { ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = txtEmailSubject.Text; }
                        //((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = wimAddFCC.FCCNote;
                      }
                    }
                  }
                  if (dtTemplates.Rows[j][4] != null && dtTemplates.Rows[j][4].ToString() != string.Empty)
                  {
                    btnEditTemplate.Visible = true;
                    btnDisableTemplate.Visible = true;
                  }
                  else
                  {
                    btnEditTemplate.Visible = false;
                    btnDisableTemplate.Visible = false;
                  }
                  pnlFCC.Update();


                  break;
                }
              }
            }
            else
            {
              if (wimAddFCC.FCCNote == null && Session["FCCValuesInitial"] == null)
              {
                wimAddFCC.FCCNote = EmailSubject;
                pnlFCC.Update();
                ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = txtEmailSubject.Text;
              }
            }
            hfSigPostBack.Value = string.Empty;

            // txtEditor.Text = Session["EmailOriginal"].ToString() + htmlEdit.Decode(Signature);

          }
          else
          {
            pnlTemplates.Visible = false;
            btnSaveAsTemplate.Visible = false;

            if (wimAddFCC.FCCNote == null && ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text == string.Empty)
            {
              wimAddFCC.FCCNote = EmailSubject;
              pnlFCC.Update();
              ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = txtEmailSubject.Text;
            }
            //if (chkSig.Checked)
            //{
            //    txtEditor.Text = htmlEdit.Decode(Signature) + Session["EmailOriginal"].ToString();
            //}
            //else {
            try
            {
              if (hfSigPostBack.Value != "Y")
              {
                txtEditor.Text = Session["EmailOriginal"].ToString().Replace("&nbsp;", " ");
              }
              else
              {
                txtEditor.Text = HttpUtility.HtmlDecode(txtEditor.Text).Replace("&nbsp;", " ");
              }

            }
            catch
            {
              txtEditor.Text = string.Empty;
            }
            //   }
          }
          //}
          //  }
          if (((System.Web.UI.WebControls.Label)wimAddFCC.FindControl("lblInfo")).Text.Length == 0)
          {
            wimAddFCC.Attributes["purpose"] = "addFCC";
            if (!RetainFCC && pnlFCCTemplate.Visible == false)
            {
              ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = txtEmailSubject.Text;
            }
            else
            {
              try
              {
                Array a = ((Array)HttpContext.Current.Session["fccPastValues"]);
                wimAddFCC.FCCNote = a.GetValue(2).ToString();
                ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = a.GetValue(2).ToString();

              }
              catch { ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = txtEmailSubject.Text; }
              //((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = wimAddFCC.FCCNote;
            }
          }
        }
      }
      else
      {
        try
        {
          txtEditor.Text = HttpUtility.HtmlDecode(txtEditor.Text).Replace("&nbsp;", " ");
        }
        catch { }
      }
      hfSigPostBack.Value = string.Empty;
      //Session.Remove("RetainFCC");
    }
    protected void fuAttach_Load(object sender, EventArgs e) //Depreciated
    {
      if (!fuAttach.HasFile)
      {
        // fuAttach.
      }

    }
    protected void btnFCCTemplateSubmit_Click(object sender, EventArgs e)
    {
      switch (rblFCCTemplate.SelectedValue.ToString())
      {
        case "0":
          HttpContext.Current.Session["TemplateID"] = hfNewTemplateID.Value; //Change Template, but revert back to saved values
          try
          {
            Array aFCCStored = ((Array)(HttpContext.Current.Session["fccPastValues"]));
            // ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDate")).Text = aFCCStored.GetValue(0).ToString();
            wimAddFCC.FCCDateRule = null;
            //  wimAddFCC.FCC
            ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDate")).Text = aFCCStored.GetValue(0).ToString();
            ((DropDownList)wimAddFCC.FindControl("ddlNewSource")).SelectedIndex = Int32.Parse(aFCCStored.GetValue(1).ToString());
            ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = aFCCStored.GetValue(2).ToString();
            wimAddFCC.FCCNote = aFCCStored.GetValue(2).ToString();
            //  wimAddFCC.FCCNote = null;//aFCCStored.GetValue(2).ToString();
            Session["RetainFCC"] = "Y";
          }
          catch { }




          break;
        case "1":
          HttpContext.Current.Session["TemplateID"] = hfNewTemplateID.Value;
          Session["RetainFCC"] = "N";

          //Just change template
          break;
        default: //Keep old values, do not update template in session
          try
          {
            ddlTemplates.SelectedValue = HttpContext.Current.Session["TemplateID"].ToString();
          }
          catch
          {
            ddlTemplates.SelectedValue = "0";
          }
          //HttpContext.Current.Session["TemplateID"] = hfNewTemplateID.Value; //Change Template, but revert back to saved values
          try
          {
            Array aFCCStored = ((Array)(HttpContext.Current.Session["fccPastValues"]));
            // ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDate")).Text = aFCCStored.GetValue(0).ToString();
            wimAddFCC.FCCDateRule = null;
            ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDate")).Text = aFCCStored.GetValue(0).ToString();
            ((DropDownList)wimAddFCC.FindControl("ddlNewSource")).SelectedIndex = Int32.Parse(aFCCStored.GetValue(1).ToString());
            ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = aFCCStored.GetValue(2).ToString();
            wimAddFCC.FCCNote = aFCCStored.GetValue(2).ToString();
            Session["RetainFCC"] = "Y";
          }
          catch { }



          break;
      }
      pnlFCCTemplate.Visible = false;
      pnlFCC.Update();
    }
    protected void btnTemplate_Click(object sender, EventArgs e)
    {

      if (ddlTemplates.SelectedIndex > 0)
      {

        Array aFCCInitial = ((Array)(HttpContext.Current.Session["FCCValuesInitial"]));
        //Set in AddFCC usercontrol.  Stores

        Session["EmailBody"] = Session["EmailOriginal"];

        String[] fccPastValues = new String[3];
        try
        {
          fccPastValues[0] = ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDate")).Text;
        }
        catch { fccPastValues[0] = string.Empty; }
        try
        {
          fccPastValues[1] = ((DropDownList)wimAddFCC.FindControl("ddlNewSource")).SelectedIndex.ToString();
        }
        catch { fccPastValues[1] = "0"; }
        try
        {
          fccPastValues[2] = ((System.Web.UI.WebControls.TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text;
        }
        catch { fccPastValues[2] = string.Empty; }
        try
        {
          Session.Remove("fccPastValues");

        }
        catch { }
        Session.Add("fccPastValues", fccPastValues);
        Boolean FccChanged = false;
        for (int i = 0; i < 3; i++) //Compare Current FCC Vales with Placeholders
        {
          try
          {
            if (fccPastValues[i] != aFCCInitial.GetValue(i).ToString())
            {
              FccChanged = true; //User changed default values
              break;
            }

          }
          catch { }
          if (FccChanged) //Open dialog window asking user how they want to handle FCC content, since the template change will overwrite. 
          {
            hfNewTemplateID.Value = ddlTemplates.SelectedValue;
            pnlFCCTemplate.Visible = true;
          }
          else
          { //If there weren't any changes, just update the template
            HttpContext.Current.Session["TemplateID"] = ddlTemplates.SelectedValue.ToString();
          }

        }
      }//If there weren't any changes, just update the template
      else { HttpContext.Current.Session["TemplateID"] = ddlTemplates.SelectedValue.ToString(); }




    }
    protected void chkSig_OnCheckedChanged(object sender, EventArgs e)
    {
      if (chkSig.Checked)
      {
        String Signature = string.Empty;
        try
        {
          Dashboard.BaseLogic.UserData ud = (Dashboard.BaseLogic.UserData)Session["UserData"];
          DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = " + ud.UserID.ToString());
          //  DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = 23"); //Test data, Bethany's Signature
          Signature = dsSig.Tables[0].Rows[0][0].ToString();
        }
        catch
        {
          Signature = string.Empty;
        }
        txtEditor.Text = string.Empty;
        if (HttpContext.Current.Session["TemplateKindID"] != null)
        {
          //    txtEditor.Text = Session["EmailOriginal"].ToString() + Signature;
        }
        else
        {
          //      txtEditor.Text = Signature + Session["EmailOriginal"].ToString();
        }
      }
      else
      {
        //    txtEditor.Text = Session["EmailOriginal"].ToString();
      }
      upReply.Update();
    }
    protected void btnSig_Click(object sender, EventArgs e)
    { //Updates signature
      String strSig = txtEditSig.Text;
      Dashboard.BaseLogic.UserData ud = (Dashboard.BaseLogic.UserData)Session["UserData"];
      int UserID = ud.UserID;
      sdsSig.UpdateParameters[0].DefaultValue = UserID.ToString();
      sdsSig.UpdateParameters[1].DefaultValue = txtEditSig.Text;
      sdsSig.Update();
      pnlEditSig.Visible = false;
      btnEditSig.Visible = true;
      hfSigPostBack.Value = "Y";
      //String sql = "Exec spSaveRAEmailSignature @UserID = " + UserID.ToString() + " @EmailSignature = '" + strSig + "'";
      //DataSet ds = BaseLogic.AdHoc(sql);
    }
    protected void btnEditSig_Click(object sender, EventArgs e)
    {
      pnlEditSig.Visible = true;
      String Signature = string.Empty;
      try
      {
        Dashboard.BaseLogic.UserData ud = (Dashboard.BaseLogic.UserData)Session["UserData"];
        DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = " + ud.UserID.ToString());
        //  DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = 23"); //Test data, Bethany's Signature
        Signature = dsSig.Tables[0].Rows[0][0].ToString();
      }
      catch
      {
        Signature = string.Empty;
      }
      txtEditSig.Text = Signature;
      btnEditSig.Visible = false;
      hfSigPostBack.Value = "Y";
      //upReply.Update();
    }

    protected void btnAttach_Click(object sender, EventArgs e)
    { //Depreciated.  This got moved to a separate file

      HttpContext.Current.Session["EmailOriginal"] = txtEditor.Text;

      int totalFileSize = 0;
      try
      {
        totalFileSize = Int32.Parse(Session["totalFileSize"].ToString());
      }
      catch
      {
        totalFileSize = 0;
      }


      if (fuAttach.HasFile)
      {

        System.IO.Stream thisFile = fuAttach.PostedFile.InputStream;
        string FileName = fuAttach.PostedFile.FileName;
        int fLen = Convert.ToInt32(fuAttach.PostedFile.InputStream.Length);
        totalFileSize += fLen;
        if (totalFileSize > 26214400) // bigger that 25mb? CHECK THIS ON UI SIDE.
        {
          litFileSize.Text = "<span style='color:red'>25mb File Size Limit Exceeded.  File was not attached.</span><br/><br/>";

          return;
          // this will fail.
        }

        byte[] bFile = new byte[fLen];
        thisFile.Read(bFile, 0, fLen);

        // file ia now in bFile
        // upload and get a ticket.
        WimcoBaseLogic.BusinessLogic.UploadTicket uploadTicket = WimcoBaseLogic.BaseLogic.Groupwise_UploadAttachement(bFile, FileName);
        if (uploadTicket.Success)
        {

          List<WimcoBaseLogic.BusinessLogic.UploadTicket> AttachmentList = new List<WimcoBaseLogic.BusinessLogic.UploadTicket>();
          AttachmentList.Add(uploadTicket);
          if (Session["EmailAttach"] == null)
          {

            Session["EmailAttach"] = AttachmentList;
            litAttachments.Text = uploadTicket.Filename;
          }
          else
          {
            AttachmentList = ((List<WimcoBaseLogic.BusinessLogic.UploadTicket>)Session["EmailAttach"]);

            AttachmentList.Add(uploadTicket);
            Session["EmailAttach"] = AttachmentList;
            String fileList = string.Empty;
            for (int i = 0; i < AttachmentList.Count; i++)
            {
              fileList = fileList + AttachmentList[i].Filename + ",<br />";

            }

            // you can examine here.. you will need this Struct to pass back an ARRAY of these. 1 per file.
            string ticket = uploadTicket.Ticket;
            Session["EmailAttach"] = uploadTicket;
          }
          Session["totalFileSize"] = totalFileSize;
          litFileSize.Text = string.Empty;
        }


        //if (fuAttach.HasFile) //Depreciated
        //{
        //    HttpPostedFile hpf = fuAttach.PostedFile;

        //    List<Attachment> AttachmentList = new List<Attachment>();
        //    if (Session["EmailAttach"] == null)
        //    {
        //        Attachment fuAttachment = new Attachment(hpf.InputStream, hpf.FileName);
        //        AttachmentList.Add(fuAttachment);
        //        Session["EmailAttach"] = AttachmentList;
        //        litAttachments.Text = hpf.FileName;
        //    }
        //    else
        //    {
        //        AttachmentList = ((List<Attachment>)Session["EmailAttach"]);
        //        Attachment fuAttachment = new Attachment(hpf.InputStream, hpf.FileName);
        //        AttachmentList.Add(fuAttachment);
        //        Session["EmailAttach"] = AttachmentList;
        //        String fileList = "";
        //        for (int i = 0; i < AttachmentList.Count; i++)
        //        {
        //            fileList = fileList + ((Attachment)AttachmentList[i]).Name + ",<br />";

        //        }
        //        litAttachments.Text = fileList;

        //    }
      }
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
      System.Web.UI.WebControls.Button b = (System.Web.UI.WebControls.Button)sender;
      if (b.ClientID.ToLower().Contains("fcc"))
      {

        DropDownList ddlNewSource = ((DropDownList)wimAddFCC.FindControl("ddlNewSource"));
        if (ddlNewSource.SelectedIndex <= 0)
        {
          ddlNewSource.BorderColor = System.Drawing.Color.Red;
          ddlNewSource.BorderWidth = 2;

          try { }
          catch { }
          Session["EmailBody"] = HttpUtility.HtmlDecode(txtEditor.Text);

          return;
        }

      }


      bool isHtmlEmail = false;
      string html = txtEditor.Text;
      string original = string.Empty;
      if (EmailBody != null)
      {
        if (EmailBody != string.Empty)
        {
          original = EmailBody;
        }
        else
        {
          try
          {
            original = Session["EmailOriginal"].ToString();
          }
          catch
          {
            original = "<w99></w99>";
          }
        }
      }
      else
      {
        try
        {
          original = Session["EmailOriginal"].ToString();
        }
        catch
        {
          original = "<w99></w99>";
        }

      }
      string useradded = string.Empty;
      string theBody = string.Empty;
      string FinalMessage = string.Empty;

      RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
      Regex regx = new Regex("<body(?<theBody>.*)/body>", options);
      Match match = regx.Match(original);




      //if (match.Success)
      //{
      //    isHtmlEmail = true;
      //    int end = html.IndexOf("&lt;w99");
      //    if (end == -1)
      //    {
      //        end = html.Length;
      //    }
      //    useradded = html.Substring(0, end);
      //    theBody = match.Value;
      //    theBody = theBody.Replace("</BODY", "</body");

      //    int sbody = theBody.IndexOf(">");
      //    int ebody = theBody.IndexOf("</body>");

      //    string subbody = theBody.Substring(sbody, ebody - sbody);

      //    int StartInsertInt = theBody.IndexOf(">");
      //    StartInsertInt++;

      //    theBody = theBody.Insert(StartInsertInt, useradded);
      //    FinalMessage = "<html>" + theBody + "</html>";

      //}

      //if (!match.Success)
      //{
      isHtmlEmail = true;
      int end = html.IndexOf("&lt;w99");
      if (end == -1)
      {
        end = html.Length;
      }
      useradded = html.Substring(0, end);
      if (html.Contains(original) && original != string.Empty)
      {
        FinalMessage = "<html>" + useradded + original;
      }
      else
      {
        FinalMessage = "<html>" + HttpUtility.HtmlDecode(html);
      }
      //}
      //+ "<br/>" + htmlEdit.Decode(Signature)
      String Signature = string.Empty;
      String temp = string.Empty;
      if (chkSig.Checked)
      {
        String temp2 = string.Empty;

        int ArrowIndex = FinalMessage.IndexOf(">>>"); //Designate spot to insert signature.  Based on >>>s
        if (ArrowIndex > 0)
        {
          int FMLength = FinalMessage.Length;
          string firstHalf = FinalMessage.Substring(0, ArrowIndex);
          string secondHalf = FinalMessage.Substring(ArrowIndex, FMLength - ArrowIndex);
          temp2 = firstHalf + "<!--INSERT SIG HERE-->" + secondHalf;
          FinalMessage = temp2;
        }

        try
        {
          Dashboard.BaseLogic.UserData ud = (Dashboard.BaseLogic.UserData)Session["UserData"];
          DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = " + ud.UserID.ToString());
          //  DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = 23"); //Test data, Bethany's Signature
          Signature = HttpUtility.HtmlDecode(dsSig.Tables[0].Rows[0][0].ToString()) + "<br/><br/>";
        }
        catch
        {
          Signature = string.Empty;
        }
        if (FinalMessage.Contains("<!--INSERT SIG HERE-->"))
        {
          temp = FinalMessage.Replace("<!--INSERT SIG HERE-->", Signature);
          FinalMessage = temp;
        }
        else
        {
          temp = FinalMessage + "<br/>" + Signature;
          FinalMessage = temp;
        }
      }
      else
      {
        Signature = string.Empty;
        temp = FinalMessage.Replace("<!--INSERT SIG HERE-->", string.Empty);
        FinalMessage = temp;
      }

      FinalMessage = FinalMessage + "</html>";


      //WimcoBaseLogic.xsd.Groupwise_OutboundDS gwo = new WimcoBaseLogic.xsd.Groupwise_OutboundDS();
      //WimcoBaseLogic.xsd.Groupwise_OutboundDS.Groupwise_OutboundRow r = gwo.Groupwise_Outbound.NewGroupwise_OutboundRow();
      //r.MessageID = Guid.NewGuid().ToString();
      //r.RecipentEmail = txtEmailToAddress.Text;
      //r.CCField = txtEmailCCEmailAddress.Text;
      ////r.BCCField = "wimcoops@wimco.com"; 

      //r.DateAdded = DateTime.Now;
      //r.DisplayFromEmailAddress = txtEmailFromEmailAddress.Text;
      //r.DisplayFromName = txtEmailFromName.Text;
      //r.EmailContent = FinalMessage;
      //r.EmailSubject = txtEmailSubject.Text;
      //r.GroupwiseID = lblTempStorageGroupWiseID.Text;
      //r.HTMLMessage = isHtmlEmail;
      //r.Sent = false;

      //  gwo.Groupwise_Outbound.AddGroupwise_OutboundRow(r);
      //WimcoBaseLogic.BaseLogic.UpdateDataSet(gwo);

      //***************************** Depreciated 4/20/17 ************************************ //
      //if (Session["EmailAttach"] == null)
      //{
      //    Dashboard.BaseLogic.sendHTMLEmail(txtEmailFromEmailAddress.Text, txtEmailFromName.Text, txtEmailSubject.Text, txtEmailToAddress.Text, txtEmailCCEmailAddress.Text, FinalMessage);
      //}
      //else
      //{
      //    Attachment[] EmailAttachments = ((List<Attachment>)Session["EmailAttach"]).ToArray();
      //    Dashboard.BaseLogic.sendHTMLEmail(txtEmailFromEmailAddress.Text, txtEmailFromName.Text, txtEmailSubject.Text, txtEmailToAddress.Text, txtEmailCCEmailAddress.Text, FinalMessage, EmailAttachments);
      //}

      //***********************************************************************************//
      WimcoBaseLogic.BusinessLogic.UploadTicket[] EmailAttachments;

      if (Session["EmailAttach"] != null)
      {
        try
        {
          EmailAttachments = ((List<WimcoBaseLogic.BusinessLogic.UploadTicket>)Session["EmailAttach"]).ToArray();
        }
        catch
        {
          EmailAttachments = null;
        }

      }
      else
      {
        EmailAttachments = null;
      }

      // WimcoBaseLogic.BusinessLogic.GWEmailToSendReturn gwtr = 
      Dashboard.BaseLogic.sendGWEmail(txtEmailFromEmailAddress.Text, txtEmailFromName.Text, txtEmailSubject.Text, txtEmailToAddress.Text, txtEmailCCEmailAddress.Text, FinalMessage, EmailAttachments);
      //    if (gwtr.Success)
      //   {
      btnReload.Visible = false;
      txtEditor.Text = FinalMessage;
      lblAction.Text = "Message Sent!";
      lblAction.Visible = true;
      timer.Interval = 2000;
      timer.Enabled = true;
      try
      {
        Session.Remove("EmailAttach");
      }
      catch { }
      try
      {
        Session.Remove("TemplateID");
      }
      catch { }
      try
      {
        Session.Remove("totalFileSize");
      }
      catch { }
      try
      {
        Session.Remove("EmailOriginal");
      }
      catch { }
      try
      {
        Session.Remove("EmailBody");
      }
      catch { }

      Session["EmailSent"] = FinalMessage;


      //}
      //else {
      //    txtEditor.Text = FinalMessage;
      //    lblAction.Text = gwtr.Message;


      //}
      //                if (!b.ClientID.ToLower().Contains("fcc"))
      //                {

      //                    string close = @"<script type='text/javascript'>
      //                                window.returnValue = true;
      //                                window.close();
      //                                </script>";
      //                    base.Response.Write(close);
      //                }

    }

    protected void btnCancelSend_Click(object sender, EventArgs e)
    {
      try
      {
        Session.Remove("EmailOriginal");
      }
      catch { }
      try
      {
        Session.Remove("EmailAttach");
      }
      catch { }
      try
      {
        Session.Remove("TemplateID");
      }
      catch { }
      try
      {
        Session.Remove("EmailBody");
      }
      catch { }
    }
    protected void btnCancelTemplate_Click(object sender, EventArgs e)
    {
      pnlSaveTemplate.Visible = false;
      btnSaveAsTemplate.Visible = true;
    }
    protected void btnSaveAsTemplate_Click(object sender, EventArgs e)
    {
      pnlSaveTemplate.Visible = true;
      DataSet ds = BaseLogic.AdHoc("Select (Name + ' - ' + Description) as title, EmailTemplateKindID From EmailTemplateKind Order by EmailTemplateKindID");

      ddlTemplateKind.DataSource = ds;
      ddlTemplateKind.DataTextField = "title";
      ddlTemplateKind.DataValueField = "EmailTemplateKindID";
      ddlTemplateKind.DataBind();
      try
      {
        ddlTemplateKind.SelectedValue = Session["TemplateKindID"].ToString();
      }
      catch { }

      btnSaveAsTemplate.Visible = false;
    }
    protected void btnSaveTemplate_Click(object sender, EventArgs e)
    {
      String TemplateKindID;
      try { TemplateKindID = Session["TemplateKindID"].ToString(); }
      catch { return; }

      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      String UserID = ud.UserID.ToString();
      String EmailTemplateID = BaseLogic.GetNextKey("EmailTemplate").ToString();
      String EmailTemplateRefID = BaseLogic.GetNextKey("EmailTemplateUserRef").ToString();
      sdsTemplate.InsertParameters[0].DefaultValue = EmailTemplateID;
      sdsTemplate.InsertParameters[1].DefaultValue = TemplateKindID;
      sdsTemplate.InsertParameters[2].DefaultValue = htmlEdit.Decode(txtEditor.Text);
      sdsTemplate.InsertParameters[3].DefaultValue = "True";
      sdsTemplate.InsertParameters[4].DefaultValue = DateTime.Now.ToString();
      sdsTemplate.InsertParameters[5].DefaultValue = txtNameTemplate.Text;
      sdsTemplate.InsertParameters[6].DefaultValue = EmailTemplateRefID;
      sdsTemplate.InsertParameters[7].DefaultValue = "14";
      sdsTemplate.InsertParameters[8].DefaultValue = UserID;
      sdsTemplate.InsertParameters[9].DefaultValue = txtEmailSubject.Text;
      sdsTemplate.Insert();
      pnlSaveTemplate.Visible = false;
      btnSaveAsTemplate.Visible = true;
      DataSet dsTemplates = Dashboard.BaseLogic.AdHoc("Exec spGetUserEmailTemplatesByKind @TemplateKindID = " + TemplateKindID + ", @UserID = " + UserID);
      dtTemplates = dsTemplates.Tables[0];
      ddlTemplates.Items.Clear();

      DataTable dtTemplate = new DataTable(); // dsTemplate.Tables[0];
      if (ddlTemplates.Items.Count == 0)
      {
        for (int i = 0; i < dtTemplates.Rows.Count; i++)
        {
          ListItem li2 = new ListItem();
          li2.Value = dtTemplates.Rows[i][0].ToString();
          li2.Text = dtTemplates.Rows[i][3].ToString().Trim();
          ddlTemplates.Items.Add(li2);
        }
        ListItem li = new ListItem();
        li.Value = "0";
        li.Text = "Select a Template";
        ddlTemplates.Items.Insert(0, li);
        ddlTemplates.SelectedIndex = 0;
      }

      //            <asp:Parameter Name="TemplateKindID" />
      //<asp:Parameter Name="TemplateTextHTML" />
      //<asp:Parameter Name="IsEnabled" />
      //<asp:Parameter Name="DateCreated" />
      //<asp:Parameter Name="Name" />
      //<asp:Parameter Name="EmailTemplateUserRefID" />
      //<asp:Parameter Name="FCCDateRule" />
      //<asp:Parameter Name="UserID" />

    }
    protected void btnEditTemplate_Click(object sender, EventArgs e)
    {

      String TemplateID = string.Empty;
      try
      {
        TemplateID = HttpContext.Current.Session["TemplateID"].ToString();
      }
      catch
      {
        return;
      }
      sdsTemplate.UpdateParameters[0].DefaultValue = TemplateID;
      sdsTemplate.UpdateParameters[1].DefaultValue = htmlEdit.Decode(txtEditor.Text);
      sdsTemplate.UpdateParameters[2].DefaultValue = txtEmailSubject.Text;
      sdsTemplate.UpdateParameters[3].DefaultValue = ddlTemplates.SelectedItem.Text;
      sdsTemplate.UpdateParameters[4].DefaultValue = "True";

      sdsTemplate.Update();

    }
    protected void btnDisableTemplate_Click(object sender, EventArgs e)
    {
      String TemplateID = string.Empty;
      try
      {
        TemplateID = HttpContext.Current.Session["TemplateID"].ToString();
      }
      catch
      {
        return;
      }
      DataSet dsTemplate = Dashboard.BaseLogic.AdHoc("Exec spGetUserEmailTemplate @EmailTemplateID = " + TemplateID);
      DataTable dtTemplate = new DataTable();
      //Select et.EmailTemplateID, et.TemplateTextHTML, et.Subject, etur.UserID, et.[Name]
      String EmailText = string.Empty;
      String EmailSubj = string.Empty;
      String EmailName = string.Empty;

      dtTemplate = dsTemplate.Tables[0];
      if (dtTemplate.Rows.Count == 1)
      {
        if (dtTemplate.Rows[0]["UserID"] != null && dtTemplate.Rows[0]["UserID"].ToString() != string.Empty)
        { //Don't overwrite WIMCO templates. Pull in existing data, since the delete is just an update
          try
          {
            EmailText = dtTemplate.Rows[0][1].ToString();
          }
          catch
          {

          }
          try
          {
            EmailSubj = dtTemplate.Rows[0][2].ToString();
          }
          catch
          {

          }
          try
          {
            EmailName = dtTemplate.Rows[0][4].ToString();
          }
          catch
          {

          }

          sdsTemplate.UpdateParameters[0].DefaultValue = TemplateID.ToString();
          sdsTemplate.UpdateParameters[1].DefaultValue = EmailText;
          sdsTemplate.UpdateParameters[2].DefaultValue = EmailSubj;
          sdsTemplate.UpdateParameters[3].DefaultValue = EmailName;
          sdsTemplate.UpdateParameters[4].DefaultValue = "False";

          sdsTemplate.Update();
          btnDisableTemplate.Visible = false;
          Session.Remove("TemplateID");
          String TKID = HttpContext.Current.Session["TemplateKindID"].ToString();
          Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
          String UserID = ud.UserID.ToString();
          DataTable dtTemplates = new DataTable(); // dsTemplate.Tables[0];
          DataSet dsTemplates = Dashboard.BaseLogic.AdHoc("Exec spGetUserEmailTemplatesByKind @TemplateKindID = " + TKID + ", @UserID = " + UserID);
          dtTemplates = dsTemplates.Tables[0];

          // String UserID = "23";
          ddlTemplates.Items.Clear(); //rebuild dropdown list
          if (ddlTemplates.Items.Count == 0)
          {
            for (int i = 0; i < dtTemplates.Rows.Count; i++)
            {
              ListItem li2 = new ListItem();
              li2.Value = dtTemplates.Rows[i][0].ToString();
              if (dtTemplates.Rows[i][4] == null || dtTemplates.Rows[i][4].ToString() == string.Empty)
              {
                li2.Text = dtTemplates.Rows[i][6].ToString().Trim();
              }
              else { li2.Text = dtTemplates.Rows[i][6].ToString().Trim(); }

              ddlTemplates.Items.Add(li2);
            }
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "Select a Template";
            ddlTemplates.Items.Insert(0, li);
            ddlTemplates.SelectedIndex = 0;
          }
        }
      }
    }
    protected void btnCancelSig_Click(object sender, EventArgs e)
    {
      pnlEditSig.Visible = false;
      btnEditSig.Visible = true;
      hfSigPostBack.Value = "Y";
    }
    protected void btnShowAllEmails_Click(object sender, EventArgs e)
    {
      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
      btnShowAllEmails.Visible = false;
      WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();
      //  ObjectDataSource1.SelectParameters["Email"].DefaultValue = Session["EmailToSearch"].ToString();
      //    ObjectDataSource1.DataBind();
      // bizLogic.Timeout = 5000;
      WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS cel = new WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS();
      String[] emails = WimcoBaseLogic.BaseLogic.clientemail_GetEmailAddressArray(Int32.Parse(Session["ClientID"].ToString()));
      GW_IndexSearchDS GWDS = WimcoBaseLogic.BaseLogic.SearchByEmailAddresses(emails);
      if (GWDS.Tables.Count > 0)
      {
        String EmailText = string.Empty;

        DataTable dtEmails = GWDS.Tables[0];
        for (int i = 0; i < dtEmails.Rows.Count; i++)
        {
          GWMailObject mail = new GWMailObject();
          String GroupWiseID = dtEmails.Rows[i]["GroupwiseID"].ToString();
          String html = string.Empty;
          mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
          if (mail.StatusCode > -1)
          {
            if (!mail.MessageInHTML.Contains("mail has been deleted from Server") && !mail.MessageInHTML.Contains("Can't reach Groupwise to read this email"))
            {

              html = html + "<div class='email'><jk></jk>";

              if (html == "<div class='email'><jk></jk>")
              {
                html += "<br/> >>> " + mail.FromName + " (" + mail.FromEmail + "&gt; ";
                html = html + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + ">>> <br/>";
              }


              //html = html + "Date : " + mail.DateSent.ToString("yyyy-MM-dd HH:mm:ss") + "<br/>";
              //html = html + "From: " + mail.FromEmail + " To: " + mail.Distribution.to + "<br/>";
              //html = html + "Subject : " + mail.Subject + "<br/>";
              //html = html + "<br/><br/><br/>";
              html = html + "</div>";
              html = html + mail.MessageInHTML.Replace("!important", string.Empty).Replace("text/javascript", "text/noscript");
              EmailText += html;
            }
          }

        }
        Session["EmailBody"] = EmailText;
      }

    }


    protected void btnReload_Click(object sender, EventArgs e)
    {

      // loadingIcon3.Visible = false;
      if (!IsPostBack)
      {
        try
        {
          EmailBody = HttpContext.Current.Session["EmailBody"].ToString();
          btnReload.Visible = false;
        }
        catch { }
      }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    { //Wipe contents of main text box. Prevents extra code from getting stuck if removed otherwise
      try
      {
        Session.Remove("EmailOriginal");
      }
      catch { }
      try
      {
        Session.Remove("EmailBody");
      }
      catch { }
      try
      {
        Session.Remove("TemplateID");
      }
      catch { }
      try
      {
        ddlTemplates.SelectedIndex = 0;
      }
      catch { }
      txtEditor.Text = string.Empty;

    }
    protected void timer_Tick(object sender, EventArgs e)
    {
      // lblAction.Text = "";
      lblAction.Visible = false;
      timer.Enabled = false;

      string close = @"<script type='text/javascript'>
                                window.returnValue = true;
                                window.close();
                                </script>";
      base.Response.Write(close);


    }
  }
}