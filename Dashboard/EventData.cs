﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dashboard
{
  public class EventData
  {
    public string Application { get; set; }
    public string Event { get; set; }
    public string Message { get; set; }
    public string MessageID { get; set; }
    public DateTime MessageDate { get; set; }
  }
}