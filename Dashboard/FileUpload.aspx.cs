﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace VillaOwner
{
  public partial class FileUpload : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
      if (lbAttachments.Items.Count == 0)
      {
        lbAttachments.Visible = false;
        btnRemoveSelected.Visible = false;

      }
      else
      {
        lbAttachments.Visible = true;
        btnRemoveSelected.Visible = true;
      }
    }
    protected void btnRemoveSelected_Click(object sender, EventArgs e)
    {
      List<WimcoBaseLogic.BusinessLogic.UploadTicket> AttachmentList = new List<WimcoBaseLogic.BusinessLogic.UploadTicket>();
      try
      {

        AttachmentList = ((List<WimcoBaseLogic.BusinessLogic.UploadTicket>)Session["EmailAttach"]);
      }
      catch { return; }
      for (int i = 0; i < lbAttachments.Items.Count; i++)
      {
        if (lbAttachments.Items[i].Selected == true)
        {
          String ticket = lbAttachments.Items[i].Value.ToString();
          for (int j = 0; j < AttachmentList.Count; j++)
          {
            if (AttachmentList[j].Ticket == ticket)
            {
              AttachmentList.RemoveAt(j);
              break;
            }
          }

        }

      }
      Session["EmailAttach"] = AttachmentList;
      lbAttachments.Items.Clear();

      for (int i = 0; i < AttachmentList.Count; i++)
      {
        ListItem liAttach = new ListItem();
        liAttach.Text = AttachmentList[i].Filename;
        liAttach.Value = AttachmentList[i].Ticket;
        // fileList = fileList + ((WimcoBaseLogic.BusinessLogic.UploadTicket)AttachmentList[i]).Filename + ",<br />";
        lbAttachments.Items.Add(liAttach);
      }
    }
    protected void btnAttach_Click(object sender, EventArgs e)
    {



      int totalFileSize = 0;
      try
      {
        totalFileSize = Int32.Parse(Session["totalFileSize"].ToString());
      }
      catch
      {
        totalFileSize = 0;
      }


      if (fuAttach.HasFile)
      {

        System.IO.Stream thisFile = fuAttach.PostedFile.InputStream;
        string FileName = fuAttach.PostedFile.FileName;
        int fLen = Convert.ToInt32(fuAttach.PostedFile.InputStream.Length);
        totalFileSize += fLen;
        if (totalFileSize > 26214400) // bigger that 25mb? CHECK THIS ON UI SIDE.
        {
          litFileSize.Text = "<span style='color:red'>25mb File Size Limit Exceeded.  File was not attached.</span><br/><br/>";

          return;
          // this will fail.
        }

        byte[] bFile = new byte[fLen];
        thisFile.Read(bFile, 0, fLen);

        // file ia now in bFile
        // upload and get a ticket.
        WimcoBaseLogic.BusinessLogic.UploadTicket uploadTicket = WimcoBaseLogic.BaseLogic.Groupwise_UploadAttachement(bFile, FileName);
        if (uploadTicket.Success)
        {

          List<WimcoBaseLogic.BusinessLogic.UploadTicket> AttachmentList = new List<WimcoBaseLogic.BusinessLogic.UploadTicket>();
          AttachmentList.Add(uploadTicket);
          if (Session["EmailAttach"] == null)
          {
            ListItem li = new ListItem();
            li.Text = uploadTicket.Filename;
            li.Value = uploadTicket.Ticket;
            Session["EmailAttach"] = AttachmentList;
            // litAttachments.Text = uploadTicket.Filename;
            lbAttachments.Items.Clear();
            lbAttachments.Items.Add(li);
          }
          else
          {
            AttachmentList = ((List<WimcoBaseLogic.BusinessLogic.UploadTicket>)Session["EmailAttach"]);
            lbAttachments.Items.Clear();

            AttachmentList.Add(uploadTicket);
            Session["EmailAttach"] = AttachmentList;
            String fileList = string.Empty;
            for (int i = 0; i < AttachmentList.Count; i++)
            {
              ListItem liAttach = new ListItem();
              liAttach.Text = AttachmentList[i].Filename;
              liAttach.Value = AttachmentList[i].Ticket;
              // fileList = fileList + ((WimcoBaseLogic.BusinessLogic.UploadTicket)AttachmentList[i]).Filename + ",<br />";
              lbAttachments.Items.Add(liAttach);
            }

            // you can examine here.. you will need this Struct to pass back an ARRAY of these. 1 per file.
            string ticket = uploadTicket.Ticket;
            //Session["EmailAttach"] = uploadTicket;
          }
          Session["totalFileSize"] = totalFileSize;
          // litFileSize.Text = "";
        }
      }

    }
  }
}

