﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailForm.ascx.cs" Inherits="GWCustom.EmailForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="AddFCC.ascx" TagName="wimAddFCC" TagPrefix="wimcoCustom" %>
<style type="text/css">
    .style1
    {
        width: 67px;
    }
    #wimMail_EmailForm_upTemplateButton
    {
        position: absolute;
        left: 10px;
    }
    #wimMail_EmailForm_btnSig, #wimMail_EmailForm_btnCancelSig
    {
        margin-top: 10px;
    }
    #wimMail_EmailForm_pnlEditSig
    {
        padding-top: 15px;
        height: 250px;
        width: 370px;
        border: 1px black solid;
        padding-left: 15px;
        position: absolute;
        z-index: 999;
        background-color: white;
        top: 0;
    }
    #wimMail_EmailForm_pnlSaveTemplate
    {
        height: 100px;
        width: 400px;
        border: 1px black solid;
        padding-left: 15px;
        position: absolute;
        padding-top: 50px;
        z-index: 999;
        background-color: white;
        top: 0px;
    }
    #wimMail_EmailForm_pnlFCCTemplate
    {
        height: 200px;
        width: 400px;
        border: 1px black solid;
        padding: 15px;
        position: absolute;
        z-index: 999;
         top:300px;
    /*  top: -8px; */
    right: -384px;
        background-color: #F6F7F9;
    }
    #wimMail_EmailForm_upSig
    {
    }
  /*   #wimMail_EmailForm_pnlTemplates
    {
        width: 350px;
        top: 179px;
        position: absolute;
    }*/
    #attachments
    {
        top: 300px;
        position: absolute;
        left: 600px;
    }
    #attachments iframe
    {
        border: none;
        overflow: hidden;
    }
    #buttonrow
    {
        position: absolute;
        top: -50px;
        width: 727px;
        left: -10px;
        padding: 7px 123px 5px 7px;
        text-align: right;
        background-color: #66CCFF;
        border: solid 1px gray;
    }
</style>
<asp:HiddenField ID="hfMU" runat="server" Value="" />
<asp:Panel ID="pnlReply" runat="server" Visible="true" Width="330">
  <asp:Panel runat="server" ID="pnlTemplates">
                            <asp:DropDownList runat="server" Width="260px" CausesValidation="false" ID="ddlTemplates">
                            </asp:DropDownList>
                            &nbsp;&nbsp;
                            <asp:Button ID="btnTemplate" runat="server" OnClick="btnTemplate_Click" Text="Select" />
                           
                            <br />
                            <asp:CheckBox ID="chkTemplate" Checked="true" runat="server" AutoPostBack="false"
                                Text="Include past emails below template?" />
                        </asp:Panel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="upReply">
        <ProgressTemplate>
            <img alt="loading" id="loading-icon2" style="position: absolute; z-index: 999999;
                top: 210px; left: 135px;" src="http://nomad.wimco.com/images/ajax-loader.gif" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true"
        ID="upReply">
        <ContentTemplate>
<asp:Panel ID="pnlFCCTemplate" runat="server" Visible="false">
                    <p>
                        Attention! Pieces of your FCC form will be overwritten by changing the template.
                        Would you like to:</p>
                    <asp:RadioButtonList AutoPostBack="false" runat="server" ID="rblFCCTemplate">
                        <asp:ListItem Selected="True" Text="Continue Template Selection, Keep Current FCC Values"
                            Value="0"></asp:ListItem>
                        <asp:ListItem Text="Continue Template Selection, Overwrite FCC Values" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Cancel Template Selection, Keep Current FCC Values" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                    <p style="text-align: center">
                        <asp:Button OnClick="btnFCCTemplateSubmit_Click" runat="server" ID="btnFCCTemplateSubmit"
                            Text="Submit" />
                    </p>
                    <asp:HiddenField ID="hfNewTemplateID" runat="server" />
                </asp:Panel>
            <script type="text/javascript">
     function extractLast( term ) {
      return split( term + ",").pop();
    }
     function split( val ) {
    // return val.split( /,\s*/ );
  return val.split(new RegExp("[,\;]", "i" ) );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
    
    function checkSource(){ //Stop agents from sending an FCC without a source
     console.log($('#wimMail_EmailForm_wimAddFCC_ddlNewSource').val());
     try{ if($('#wimMail_EmailForm_wimAddFCC_ddlNewSource').val() =="SELECT A SOURCE"){
       $('#wimMail_EmailForm_btnSendEmailFCC').prop('disabled',true);
        $('wimMail_EmailForm_wimAddFCC_btnNewFCC').prop('disabled',true);
       }else{
         $('#wimMail_EmailForm_btnSendEmailFCC').prop('disabled',false);
        $('wimMail_EmailForm_wimAddFCC_btnNewFCC').prop('disabled',false);
       }
       }catch(Error){}
    }
       function pageLoad(){  
          
     checkSource();
       
       $('#wimMail_EmailForm_wimAddFCC_ddlNewSource').change(function(){checkSource()});
       
           // openAttach();
       var emails = $('#wimMail_EmailForm_hfMU').attr("value").split(",");
     console.log(emails);
     //   $( "#wimMail_EmailForm_txtEmailToAddress" ).autocomplete({
     //     multiple:true,
     //      source: function( request, response ) {
     //    var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
         
    //     response( $.grep( emails, function( item ){
    //         return matcher.test( item );
    //     }) );
  // }
  //  });
    
    $( "#wimMail_EmailForm_txtEmailToAddress" ).on( "keydown", function( event ) {
            if (event.keyCode === 186){
    
      event.preventDefault();
    
      $(this).val($(this).val() + ",");
        }
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      }).autocomplete({
      multiple:true,
        minLength: 0,
      
        source: function( request, response ) {
         var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
        var matches = $.map(emails, function (acItem) {
      
        if(request.term.indexOf(",") > -1){
          
        var s; 
        var comma = request.term.lastIndexOf(",") + 1;
        var semi = request.term.indexOf(";");
        console.log(semi);
        if (comma > -1)
        {
         s = request.term.substring(comma).trim();
        }else{
         s = request.term.substring(semi).trim();
         console.log(s + "asdfadf");
        }
        //alert(s); 
         if (acItem.toString().toUpperCase().indexOf(s.toUpperCase()) === 0) {
         //  alert(request.term.toUpperCase());  //alert(s);
               console.log(acItem);
                return acItem;
              
            }
        }else{
          if (acItem.toString().toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
           // alert(request.term.toUpperCase());
                return acItem;
            }
        }
        });
        
          
      //  console.log($.ui.autocomplete.filter(matches, extractLast( request.term )),request.term);
         response( $.ui.autocomplete.filter(matches, extractLast( request.term ) ) );
        
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( "," );
          return false;
        }
      });
    
      //  $( "#wimMail_EmailForm_txtEmailCCEmailAddress" ).autocomplete({
        //  multiple:true,
        //  source: function( request, response ) {
       //  var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
      //   response( $.grep( emails, function( item ){
          //   return matcher.test( item );
        // }) );
 //  }
  //  });
   $( "#wimMail_EmailForm_txtEmailCCEmailAddress" ).on( "keydown", function( event ) {
        if (event.keyCode === 186){
    
      event.preventDefault();
    
      $(this).val($(this).val() + ",");
        }
        
        if ( event.keyCode === $.ui.keyCode.TAB && $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      }).autocomplete({
      multiple:true,
        minLength: 0,
      
        source: function( request, response ) {
         var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
        var matches = $.map(emails, function (acItem) {
      
        if(request.term.indexOf(",") > -1 || request.term.indexOf(";") > -1){
         var s; 
        var comma = request.term.lastIndexOf(",") + 1;
        var semi = request.term.indexOf(";");
        if (comma > -1)
        {
         s = request.term.substring(comma).trim();
        }else{
         s = request.term.substring(semi).trim();
        }
        //alert(s); 
         if (acItem.toString().toUpperCase().indexOf(s.toUpperCase()) === 0) {
         //  alert(request.term.toUpperCase());  //alert(s);
                return acItem;
              
            }
        }else{
          if (acItem.toString().toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
           // alert(request.term.toUpperCase());
                return acItem;
            }
        }
        });
        
          
        //console.log($.ui.autocomplete.filter(matches, extractLast( request.term )),request.term);
         response( $.ui.autocomplete.filter(matches, extractLast( request.term ) ) );
        
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
         // terms = split( ";");
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
       //   alert(ui.item.value);
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( "," );
          return false;
        }
      });
  
//alert($("#wimMail_wimAddFCC_txtNewFCCDisplay").value);
  
    //if($("#wimMail_wimAddFCC_txtNewFCCDisplay").html==""){
//$("#wimMail_EmailForm_wimAddFCC_txtNewFCCDisplay").html($("#wimMail_EmailForm_txtEmailSubject").val());
//}
   
 }
    function sendFCC(){
    var timeout = setTimeout(function() {
   try{
    $("#wimMail_EmailForm_wimAddFCC_btnNewFCC").click();
    
// closePopup();
    }
    catch(e)
    {
        console.log(e);
    }
    },2000);
    }
    function closePopup(){
   
    var timeout = setTimeout(function() {
        window.close();
      }, 10000);
     
          
    }
            </script>

            <asp:Label ID="lblAction" runat="server" BackColor="Yellow" Font-Bold="true" ForeColor="Black"
                Visible="false"></asp:Label>
            <asp:Timer ID="timer" runat="server" Enabled="false" OnTick="timer_Tick">
            </asp:Timer>
            <table style="margin-top: 11px;" border="0" class="emailHead">
                <tr>
                    <td class="style1">
                        To:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmailToAddress" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        From:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmailFromName" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        Email:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmailFromEmailAddress" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        CC:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmailCCEmailAddress" C runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        Subject:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmailSubject" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <div id="buttonrow">
                <asp:UpdatePanel ChildrenAsTriggers="true" runat="server" ID="upTemplateButton">
                    <ContentTemplate>
                        <asp:Button ID="btnSaveAsTemplate" Text="Save As New Template" runat="server" OnClick="btnSaveAsTemplate_Click" />
                        <asp:Button ID="btnEditTemplate" Visible="false" Text="Save Template" runat="server"
                            ToolTip="This updates the subject and text of your template with the current email's subject and body"
                            OnClick="btnEditTemplate_Click" />
                        <asp:Button ID="btnDisableTemplate" Visible="false" Text="Delete Template" runat="server"
                            OnClick="btnDisableTemplate_Click" />
                         <asp:Panel runat="server" ID="pnlSaveTemplate" Visible="false">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;
                                color: blue;">Name this Template: </span>
                            <asp:TextBox Style="margin-bottom: 15px" Text="" runat="server" MaxLength="50" ID="txtNameTemplate"
                                Width="250"></asp:TextBox>
                            <br />
                            <asp:DropDownList runat="server" ID="ddlTemplateKind">
                            </asp:DropDownList>
                            <asp:Button runat="server" Visible="true" Text="Save Template" ID="btnSaveTemplate"
                                OnClick="btnSaveTemplate_Click" />
                            <asp:Button runat="server" Visible="true" Text="Cancel" ID="btnCancelTemplate" OnClick="btnCancelTemplate_Click" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Button ID="btnShowAllEmails" runat="server" OnClick="btnShowAllEmails_Click"
                    Text="Show All" />
                &nbsp;
                <asp:Button ID="btnSend" runat="server" OnClick="btnSend_Click" Text="Send" />
                &nbsp;
                <asp:Button ID="btnSendEmailFCC" OnClientClick="sendFCC();" runat="server" OnClick="btnSend_Click"
                    Text="Send + FCC" />
                &nbsp;
                <asp:Button ID="btnCancelSend" runat="server" OnClick="btnCancelSend_Click" OnClientClick="javascript:window.close();"
                    Text="Cancel" /></div>
            <br />
            <br />
    <asp:UpdatePanel ID="upSig" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlEditSig" Visible="false">
                <span style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;
                    color: blue;">Signature:</span>
                <asp:HtmlEditorExtender ID="HtmlEditorExtender1" runat="server" EnableSanitization="False"
                    TargetControlID="txtEditSig" DisplaySourceTab="true">
                    <Toolbar>
                    </Toolbar>
                </asp:HtmlEditorExtender>
                <asp:TextBox CausesValidation="true" Height="150px" Rows="4" runat="server" ID="txtEditSig"
                    Width="350"></asp:TextBox><br />
                <br />
                <asp:Button runat="server" Visible="true" Text="Save Signature" ID="btnSig" OnClick="btnSig_Click" />
                <asp:Button runat="server" Visible="true" Text="Cancel" ID="btnCancelSig" OnClick="btnCancelSig_Click" />
            </asp:Panel>
            <asp:HiddenField runat="server" ID="hfSigPostBack" Value="" />
            <asp:Button ID="btnEditSig" Visible="true" OnClick="btnEditSig_Click" CausesValidation="false"
                Text="Edit Signature" runat="server" />
            <div style="display: inline; width: 250px; margin-right: 20px; margin-top: 15px;
                text-align: right">
                &nbsp;
                <asp:CheckBox ID="chkSig" Checked="true" CausesValidation="false" Visible="true"
                    AutoPostBack="false" ToolTip="Added on Email Send" runat="server" Text="Include Signature" />
            </div>
            <asp:SqlDataSource runat="server" ID="sdsSig" ConnectionString="<%$ ConnectionStrings:MagnumConnectionString %>"
                UpdateCommand="spSaveRAEmailSignature" UpdateCommandType="StoredProcedure">
                <UpdateParameters>
                    <asp:Parameter Name="prmUserID" />
                    <asp:Parameter Name="prmEmailSignature" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
         
            <asp:Button runat="server" OnClick="btnClear_Click" ID="btnClear" Text="Clear Email Text" />
            <br />
            <%--  Files Attached:--%>
            <br />
            <asp:HtmlEditorExtender ID="htmlEdit" DisplaySourceTab="true" runat="server" Enabled="true"
                EnableSanitization="False" TargetControlID="txtEditor">
                <Toolbar>
                    <asp:Undo />
                    <asp:Redo />
                    <asp:Bold />
                    <asp:Italic />
                    <asp:Underline />
                    <asp:StrikeThrough />
                    <asp:JustifyLeft />
                    <asp:JustifyCenter />
                    <asp:JustifyRight />
                    <asp:JustifyFull />
                    <asp:InsertOrderedList />
                    <asp:InsertUnorderedList />
                    <asp:RemoveFormat />
                    <asp:SelectAll />
                    <asp:UnSelect />
                    <asp:Delete />
                    <asp:Cut />
                    <asp:Copy />
                    <asp:Paste />
                    <asp:BackgroundColorSelector />
                    <asp:ForeColorSelector />
                    <asp:FontNameSelector />
                    <asp:FontSizeSelector />
                    <asp:Indent />
                    <asp:Outdent />
                    <asp:InsertHorizontalRule />
                    <asp:HorizontalSeparator />
                    <asp:CreateLink />
                    <asp:UnLink />
                </Toolbar>
            </asp:HtmlEditorExtender>
            <asp:TextBox ID="txtEditor" CausesValidation="false" runat="server" Columns="80"
                Rows="20" Text="" TextMode="MultiLine" />
            <br />
           
            <asp:Button runat="server" OnClick="btnReload_Click" ID="btnReload" Text="" CssClass="hidden" />
            
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="attachments">
        <iframe height="250px" src="FileUpload.aspx"></iframe>
        <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
            ID="upAttachments">
            <ContentTemplate>
                <asp:FileUpload ID="fuAttach" Visible="false" runat="server" /><br />
                <br />
                <%--<button id="btnFileAttach" onclick="openFileLoad()"></button>--%>
                <asp:Button ID="btnAttach" runat="server" Visible="false" OnClick="btnAttach_Click"
                    Text="Attach" />
                <br />
                <asp:Literal runat="server" ID="litFileSize"></asp:Literal>
                <asp:Literal runat="server" ID="litAttachments" />
                
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAttach" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Panel>
<asp:UpdatePanel ChildrenAsTriggers="true" UpdateMode="Conditional" ID="pnlFCC" runat="server"
    Visible="true">
    <ContentTemplate>
        <wimcoCustom:wimAddFCC ID="wimAddFCC" runat="server" purpose="addFCC" Visible="true" />
    </ContentTemplate>
</asp:UpdatePanel>
<asp:SqlDataSource SelectCommand="spGetUserEmailTemplatesByKind" ConnectionString="<%$ ConnectionStrings:MagnumConnectionString %>"
    SelectCommandType="StoredProcedure" ID="sdsTemplate" InsertCommand="spAddEmailTemplate"
    UpdateCommand="spUpdateEmailTemplate" UpdateCommandType="StoredProcedure" InsertCommandType="StoredProcedure"
    runat="server">
    <SelectParameters>
        <asp:Parameter Name="TemplateKindID" />
        <asp:Parameter Name="UserID" />
    </SelectParameters>
    <InsertParameters>
        <asp:Parameter Name="EmailTemplateID" />
        <asp:Parameter Name="TemplateKindID" />
        <asp:Parameter Name="TemplateTextHTML" />
        <asp:Parameter Name="IsEnabled" />
        <asp:Parameter Name="DateCreated" />
        <asp:Parameter Name="Name" />
        <asp:Parameter Name="EmailTemplateUserRefID" />
        <asp:Parameter Name="FCCDateRule" />
        <asp:Parameter Name="UserID" />
        <asp:Parameter Name="Subject" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="EmailTemplateID" />
        <asp:Parameter Name="TemplateTextHTML" />
        <asp:Parameter Name="Subject" />
        <asp:Parameter Name="Name" />
        <asp:Parameter Name="IsEnabled" />
    </UpdateParameters>
</asp:SqlDataSource>
