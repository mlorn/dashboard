﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResvAgentAssist.aspx.cs" Inherits="Dashboard.ResvAgentAssist" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
          <br />
          Please select a Reservation Agent.<asp:GridView ID="grdResvAgentForAssist" runat="server" AutoGenerateColumns="False" Width="375px">
            <Columns>
                <asp:TemplateField HeaderText="" >
                  <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnSelectResvAgent" runat="server" style="text-decoration:none"
                      Text='Select RA' CommandArgument='<%# Eval("UserID") %>' OnClick="lnkbtnSelectResvAgent_Click"></asp:LinkButton>
                  </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UserName" HeaderText="User Name" />
                <asp:BoundField DataField="FullName" HeaderText="Resv Agent" />
            </Columns>
          </asp:GridView>
          <br />
          <br />
        </div>
    </form>
</body>
</html>
