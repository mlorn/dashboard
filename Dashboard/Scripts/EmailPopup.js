﻿/*
This is the code required to handle the email hover and creating the email popup.
Need to include  <script src='http://static.wimco.com/js/jquery.js' ></script>  on the page first to include jquery
This does not go in the popup itself, just the page that would open the popup.

You also need to include a div to the top of the page with an emailModal class to add the hover feature on the button.

Here is the CSS for that

    .emailModal
    {
    	position:absolute;
    	z-index:30;
    	background-color:#FFF;
    	width:500px;
    	height:700px;
    	border:gray solid 1px;
    	padding:5px;
    	left:200px;
    	top:0px;
    	overflow:scroll;
    	
    	}

If the popup is not in the correct spot, add the following code to your page. to reposition it on scroll
Update the number inside the as necessary
<script type="text/javascript">
$("body").on("scroll",function(){
repositionDiv(0);
});
</script>

*/
var url1 = window.location.toString().substring(0, window.location.toString().indexOf(window.location.pathname));


function popOpen(gwID) { window.open(url1 + '/viewmail.aspx?m=' + gwID, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=50, left=50, width=860, height=660', ''); }
//ShowAllEmails runs off of session ClientID.  Would need to set this on the button click and call this on client click
function showAllEmails() {
    setTimeout(function () {
        window.open(url1 + '/viewmail.aspx?showall=true', ' View All Emails', 'toolbar=no, scrollbars=yes, resizable=yes, top=50, left=50, width=860, height=660', '');

    }, 500); //500 is milliseconds.  This needs to fire after the session is set.  Can be increased as needed.
}
function openNewEmail() {
    setTimeout(function () {

        window.open(url1 + '/sendemail.aspx?new=true', 'Send an Email', 'toolbar=no, scrollbars=yes, resizable=yes, top=50, left=50, width=860, height=660', '');

    }, 500); //500 is milliseconds.  This needs to fire after the session is set.  Can be increased as needed.
}
function openNewEmailRight() { //Open emails to left of Nomad popup. Adjust left value as needed
    setTimeout(function () {

        window.open(url1 + '/sendemail.aspx?new=true', 'Send an Email', 'toolbar=no, scrollbars=yes, resizable=yes, top=50, left=870, width=860, height=660', '');

    }, 500); //500 is milliseconds.  This needs to fire after the session is set.  Can be increased as needed.
}
function hoverEmailDash(clientID) { //on hover, open a modal with the email contents. Call onmouseover.  This is for the dashboard which will use ClientID
    var d = $('.emailModal');
    d.removeClass("hidden");
    d.find("#theEmail").html("");
    $.ajax({
        url: url1 + '/viewmail.aspx?hid=' + clientID,
        success: function (data) {

            var header = $(data).find("#wimMail_lblEmail").html();
            var email = $(data).find("#wimMail_lblHTML").html().replace(".true", "");

            d.find("#theEmail").append(header + "<br/>" + email);
            repositionDiv(50);
        },

        error: function (ex) {
            console.log(ex);
        }
    });
}
function hoverEmail(gwID) { //on hover, open a modal with the email contents. Call onmouseover
    var d = $('.emailModal');
    d.removeClass("hidden");
    d.find("#theEmail").html("");
    $.ajax({
        url: url1 + '/viewmail.aspx?m=' + gwID,
        success: function (data) {

            var header = $(data).find("#wimMail_lblEmail").html() + $(data).find("#attachTable").html();
            var email = $(data).find("#wimMail_lblHTML").html().replace(".true", "");

            d.find("#theEmail").append(header + "<br/>" + email);
            repositionDiv(50);
        },

        error: function (ex) {
            console.log(ex);
        }
    });
}
function hoverOff() {
    var d = $('.emailModal');
    d.find("#theEmail").html("");
    d.addClass("hidden");
}
function unHoverEmail() { //Call onmouseout
    //When user leaves the button, give them 2 seconds to hover over the modal. At which point start timer again

    var d = $('.emailModal');
    var timeout = setTimeout(function () {

        hoverOff();


    }, 5000);
    d.on("mouseover", function () {
        clearTimeout(timeout);

    });



}
function repositionDiv(top) {
    //take in a variable to adjust position on email preview popup if it doesn't appear correctly on page.
    var d = $('.emailModal');
    d.css("top", ($("body").scrollTop() + top));

}
function closePopup() {


    var timeout = setTimeout(function () {  // console.log($("body").html());
        if ($("body").html().indexOf("Your FCC has been Created") > -1) {
            //console.log($("body").html());
            //alert("this is firing" + " " + $("body").html().indexOf("Your FCC has been Created"));

            window.close();
        }
    }, 2000);


}
function previewEmail(d, gwID, x) { //display Email in body

    $.ajax({
        url: window.location.protocol + "//" + window.location.host + '/viewmail.aspx?m=' + gwID,
        success: function (data) {

            var email = $(data).find("#wimMail_lblHTML").text();
            if (email.length < 400) {
                x = email.length - 1;
            }

            d.append( email.substring(0, x) + "...");
            var e = d;

            $.ajax({
                type: "POST", //Send Email to Session
                url: window.location.protocol + "//" + window.location.host + "/viewmail.aspx/EmailString",

                data: JSON.stringify({ email: e.html() }),
                dataType: 'text',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    console.log("success");


                },
                error: function (ex) {
                    console.log(ex);

                }

            });

        },
        error: function (ex) {
            console.log(ex);
        }
    });
    return "done";
}
function switchEmailView(btn) {
    var epr = $(".emailPreviewRow").first();
    console.log(epr);
    var switchto = "";
    if ($(epr).hasClass("hidden")) {
        switchto = "preview";

    } else {
        switchto = "default";

    }
    $.ajax({ // call ashx file to update session.
        url: window.location.protocol + "//" + window.location.host + "/emailgridmode.ashx?mode=" + switchto,
        async: true,
        dataType: 'json',
        type: "GET",
        data: {},
        complete: function (data) {
            if ($(epr).hasClass("hidden")) {
             
                fillGridRows();
            
                
            } else {
                $(".emailPreviewRow").addClass("hidden");
            }
        }


    });


}

function fillGridRows() {
    $(".emailSpinner").removeClass("hidden");
    $(".emailPreviewRow").removeClass("hidden");
  
    for (var i = 0; i < $(".emailPreviewRow").length; i++) {
        var d = $(".emailPreviewRow").eq(i).find(".theEmail");

        if ($(d).innerHTML === "" || $(d).innerHTML == null) {

            var gw = $(d).attr("gwID");
            previewEmail(d, gw, 400);
        }

    }
    $(document).ajaxStop(function () { $(".emailSpinner").addClass("hidden"); });
}


