﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace VillaOwner
{
  public partial class EmailSnippets : System.Web.UI.Page
  {
    protected void Page_PreRender(object sender, EventArgs e)
    {
      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      if (ud.Loggedin == false)
      {
        // Response.Redirect("/login.aspx");

      }
      String UserID = ud.UserID.ToString(); // "11199"; //ud.UserID.ToString();
      DataSet dsTemplates = Dashboard.BaseLogic.AdHoc("Exec spGetUserEmailTemplatesByKind @TemplateKindID = 4, @UserID= " + UserID);
      DataTable dtTemplates = dsTemplates.Tables[0];
      ddlSnippets.Items.Clear();
      foreach (DataRow dr in dtTemplates.Rows)
      {
        ListItem li = new ListItem();
        if (dr["Bias"].ToString() == "1")
        {
          if (dr["TemplateTextHTML"] != null)
          {
            String QT = dr["TemplateTextHTML"].ToString();
            if (QT.Length > 30)
            {
              li.Text = QT.Substring(0, 30) + "...";

            }
            else
            {
              li.Text = QT;
            }
            li.Value = QT;
            if (hfLastText.Value != string.Empty && hfLastText.Value == li.Value && ddlSnippets.SelectedIndex <= 0)
            {
              ddlSnippets.SelectedItem.Selected = false;
              li.Selected = true;
              txtDump.Text = string.Empty;
            }
            ddlSnippets.Items.Add(li);

          }
        }
      }

    }
    protected void btnQT_Click(object sender, EventArgs e)
    {
      if (txtQT.Text.Length == 0)
      {
        txtQT.BorderColor = System.Drawing.Color.Red;
        txtQT.BorderWidth = 2;
        return;
      }
      else
      {
        txtQT.BorderColor = System.Drawing.Color.Gray;
        txtQT.BorderWidth = 1;
      }
      String fullText = txtQT.Text;
      String shorterText = txtQT.Text;
      if (fullText.Length > 50)
      {
        shorterText = fullText.Substring(0, 49);
      }

      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      String UserID = ud.UserID.ToString();  //"11199";
      String EmailTemplateID = Dashboard.BaseLogic.GetNextKey("EmailTemplate").ToString();
      String EmailTemplateRefID = Dashboard.BaseLogic.GetNextKey("EmailTemplateUserRef").ToString();
      sdsQT.InsertParameters[0].DefaultValue = EmailTemplateID;
      sdsQT.InsertParameters[1].DefaultValue = "4";
      sdsQT.InsertParameters[2].DefaultValue = fullText;
      sdsQT.InsertParameters[3].DefaultValue = "True";
      sdsQT.InsertParameters[4].DefaultValue = DateTime.Now.ToString();
      sdsQT.InsertParameters[5].DefaultValue = shorterText;
      sdsQT.InsertParameters[6].DefaultValue = EmailTemplateRefID;
      sdsQT.InsertParameters[7].DefaultValue = "14";
      sdsQT.InsertParameters[8].DefaultValue = UserID;
      sdsQT.InsertParameters[9].DefaultValue = shorterText;
      sdsQT.Insert();
      txtQT.Text = string.Empty;
      hfLastText.Value = fullText;
    }

  }
}

