﻿using DataAccessLayer;
using System;
using System.Data;
using System.Linq;

namespace Dashboard
{
  public partial class PrintFCC : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      DataSet ds;
      try
      {
        ds = Session["DataSet"] as dsDashboard;
        grdPrintFCC.DataSource = ds.Tables["tblFCC"];
        grdPrintFCC.DataBind();
      }
      catch
      {
        // TODO: Better handle exception
      }
    }
  }
}