﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailSnippets.aspx.cs" Inherits="VillaOwner.EmailSnippets" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link type="text/css" href="/css/common.css" rel="Stylesheet" />
    <link type="text/css" href="/css/screen.css" rel="Stylesheet" />
    <link type="text/css" href="/css/styles.css" rel="Stylesheet" />
    <link type="text/css" href="/css/Portal.css" rel="Stylesheet" />

    <script src='http://static.wimco.com/js/jquery.js'></script>
</head>
<body>
<style type="text/css">
div
{
	margin:20px;
	width:400px;
	}
	#ddlSnippets
	{
		    padding-top: 5px;
    padding-bottom: 5px;
		}
</style>
<script type="text/javascript">
function copyDDL(){
//var ddl = $("#<%= ddlSnippets.ClientID %>");
//console.log(ddl.val());
// var copyText = ddl.val();
//  copyText.select();
//  document.execCommand("copy");
 var ddl = $("#<%= ddlSnippets.ClientID %>");
  
  var tb = document.getElementById("<%= txtDump.ClientID %>");
  tb.value = ddl.val();
  var copyText = document.getElementById("<%= txtDump.ClientID %>").select();
  document.execCommand("copy");
      alert("copied");
}
</script>
    <form id="form1" runat="server">
     <asp:ToolkitScriptManager runat="server" ID="scrpMgr">
  </asp:ToolkitScriptManager>
    <div style="margin:auto">
    <asp:DropDownList Width="132px" ID="ddlSnippets" runat="server" AutoPostBack="false">
   
    </asp:DropDownList>
    <input type="button" onclick="copyDDL();" value="Copy to Clipboard"/>
    </div> 
   
    <div style="margin:auto">
    <br />    
    <asp:TextBox Visible="true" runat="server" ID="txtQT">
    </asp:TextBox>
    <asp:Button runat="server" ID="btnQT" Text="Add Quick Text" OnClick="btnQT_Click" />
    </div> <div style="margin:auto">
        <br /> Preview Clipboard:   
     <asp:TextBox Width="300" Height="120" TextMode="MultiLine" Wrap="true" runat="server" ID="txtDump">
    </asp:TextBox>
    
    </div>
   <asp:HiddenField ID="hfLastText" runat="server" />
    <asp:SqlDataSource SelectCommand="spGetUserEmailTemplatesByKind" ConnectionString="<%$ ConnectionStrings:MagnumConnectionString %>"
    SelectCommandType="StoredProcedure" ID="sdsQT" InsertCommand="spAddEmailTemplate"
    UpdateCommand="spUpdateEmailTemplate" UpdateCommandType="StoredProcedure" InsertCommandType="StoredProcedure"
    runat="server">
    <SelectParameters>
        <asp:Parameter Name="TemplateKindID" />
        <asp:Parameter Name="UserID" />
    </SelectParameters>
    <InsertParameters>
        <asp:Parameter Name="EmailTemplateID" />
        <asp:Parameter Name="TemplateKindID" />
        <asp:Parameter Name="TemplateTextHTML" />
        <asp:Parameter Name="IsEnabled" />
        <asp:Parameter Name="DateCreated" />
        <asp:Parameter Name="Name" />
        <asp:Parameter Name="EmailTemplateUserRefID" />
        <asp:Parameter Name="FCCDateRule" />
        <asp:Parameter Name="UserID" />
        <asp:Parameter Name="Subject" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="EmailTemplateID" />
        <asp:Parameter Name="TemplateTextHTML" />
        <asp:Parameter Name="Subject" />
        <asp:Parameter Name="Name" />
        <asp:Parameter Name="IsEnabled" />
    </UpdateParameters>
</asp:SqlDataSource>
    </form>
</body>
</html>
