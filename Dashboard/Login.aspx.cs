﻿using DataAccessLayer;
using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Dashboard
{

  public partial class Login : System.Web.UI.Page
  {
    DashboardData dashboard = null;
    string logInAs = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
      dashboard = new DashboardData();

      if (!IsPostBack)
      {
        ddlLoggingInAs.DataTextField = "FullName";
        ddlLoggingInAs.DataValueField = "UserName";
        ddlLoggingInAs.DataSource = dashboard.GetUser(string.Empty);
        ddlLoggingInAs.DataBind();
        ddlLoggingInAs.Items.Insert(0, new ListItem("All", "All"));

      }
      //set focus on user name when the page is loaded
      SetFocus(LoginControl.FindControl("UserName"));

    }
    private bool AuthenticationUser(string userName, string password)
    {

      bool result = false;
      LoginControl.FailureText = string.Empty;


      // Insert code that implements a site-specific custom 
      // authentication method here.
      //
      // This example implementation always returns false.
      bool rememberMe;
      rememberMe = LoginControl.RememberMeSet;

      LoginControl.RememberMeSet = rememberMe;

      result = dashboard.Login(userName, password, logInAs);
      //if a user found, store userID and GroupID where a user belongs to
      if (result == true)
      {
        Session["UserID"] = dashboard.UserID;
        Session["GroupID"] = dashboard.GroupID;
        Session["UserName"] = dashboard.UserName;
        Session["FullName"] = dashboard.FullName;
        Session["AdminUserName"] = dashboard.AdminUserName;
      }

      BaseLogic.UserData ud = new BaseLogic.UserData();
      ud = BaseLogic.CheckLogin(userName, password);
      if (ud.Loggedin)
      {
        // create ud cookie
        string uds = BaseLogic.SerializeAnObject(ud);
        string b64 = BaseLogic.EncodeTo64(uds);
        HttpCookie c = new HttpCookie("udv");
        c.Expires = DateTime.Now.AddHours(12);
        c.Value = b64;
        c.Path = "/";
        Response.Cookies.Add(c);
      }

      return result;

    }

    protected void LoginControl_Authenticate(object sender, AuthenticateEventArgs e)
    {
      //ASPxLoadingPanel1.Visible = true;
      bool Authenticated = false;

      logInAs = ddlLoggingInAs.SelectedValue;
      logInAs = ddlLoggingInAs.SelectedItem.Value;

      Authenticated = AuthenticationUser(LoginControl.UserName, LoginControl.Password);

      e.Authenticated = Authenticated;
      LoginControl.FailureText = "Your login attempt was not successful. Please try again.";
      //ASPxLoadingPanel1.Visible = false;
    }
  }
}