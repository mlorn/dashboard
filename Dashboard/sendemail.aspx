﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" ValidateRequest="false" CodeBehind="sendemail.aspx.cs" Inherits="GWCustom.sendemail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="AddFCC.ascx" TagName="wimAddFCC" TagPrefix="wimcoCustom" %>
<%@ Register Src="EmailControl.ascx" TagName="wimMail" TagPrefix="wimcoCustom" %>
<%@ Register Src="~/EditFCC.ascx" TagPrefix="wimcoCustom" TagName="EditFCC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta charset="utf-8">
</head>
<body>
 <style type="text/css">
     input[type="date"] {
     font-family:sans-serif;
     }
      #buttonrow
    {
        position: absolute;
        top: -50px;
        width: 727px;
        left: -10px;
        padding: 7px 123px 5px 7px;
        text-align: right;
        background-color: #66CCFF;
        border: solid 1px gray;
    }
  .emailHead
    {
    	font-family: Arial, Helvetica, sans-serif;
    	font-size:14px;
    	font-weight:bold;
    	color:blue;
    	}
    	.emailHead >input
    {
    	
    	color:black;
    	}
    textarea
    {
	    font:13.3333px Arial;
	
	}
	#pnlNewFCC
	{
		border: 1px solid black;
		font-family:Arial, Helvetica, sans-serif;
		padding-left:5px;
		
		padding-bottom:5px;
	}
	#pnlNewFCC 
	{
		font-size:14px;
	}
	#pnlNewFCC > textarea
    {
		height:2em;
		 display:inline;
    }
	#pnlFCC
	{
	width: 400px;
    top: 50px;
    position: absolute;
    left: 340px;
	}
	  #wimMail_pnlReply
    {
        top: 50px;
    position: absolute;
    }
	.hidden
	{
		display:none!important;
		}
		#attachments
    {
        top: 500px;
        position: absolute;
        left: 600px;
    }
    #attachments iframe
    {
        border: none;
        overflow: hidden;
    }
    #snippets
    {
    	
    top: 200px;
    position: absolute;
    left: 600px;
    border-bottom:1px solid black;
    	}
</style>

    <form id="form1" runat="server">
    <asp:ToolkitScriptManager runat="server" ID="scrpMgr"></asp:ToolkitScriptManager> 
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src='http://static.wimco.com/js/jquery.js' ></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script> 
    <script src="js/EmailPopup.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function () 
        //overrides default button function of edit sig
            {
            try{
                $("#wimMail$EmailForm$btnEditSig").keydown(function (e)          
                {
                    if (e.keyCode == 13) // 27=esc
                    {
                    e.stopPropagation();
                    e.preventDefault();
                       
                    }
                });      
                }catch(Error){};
            });
      
    </script>
    <script type="text/javascript">
 function clientOrdersUpdate(){
//Does nothing, here to keep the FCC piece from breaking.
}

    </script>
    <!-- Begin Inspectlet Embed Code -->
<script type="text/javascript" id="inspectletjs">
window._insp = window._insp || [];
_insp.push(['wid', 955821305]);
(function() {
function ldinsp(){if(typeof window._inspld != "undefined") return; window._inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
setTimeout(ldinsp, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
})();
</script>
<!-- End Inspectlet Embed Code -->
<wimcoCustom:wimMail ID="wimMail" runat="server" />
<asp:UpdatePanel  UpdateMode="Always" ID="pnlFCC" runat="server"
    Visible="true">
    <ContentTemplate>
        <wimcoCustom:EditFCC runat="server" ID="EditFCC" />
        <wimcoCustom:wimAddFCC ID="wimAddFCC" runat="server" purpose="addFCC"/>
    </ContentTemplate>
</asp:UpdatePanel>

    </form>
</body>
</html>
