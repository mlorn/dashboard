﻿using System;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

public class GlobalSetting
{
  //for ExtraPax
  public const string extraPaxResvNo = "ExtraPaxResvNo";
  public const string primaryClientID = "PrimaryClientID";
  public const string primaryRliID = "PrimaryRliID";
  public const string extraPaxClientID = "ExtraPaxClientID";



  public enum LetterGroup
  {
    Revision,
    NonDeposit,
    FinalPayment
  }

  public static void RemoveSession()
  {
    string[] aSessions = { "EmailToSearch", "CurrentItinerary", "CurrentReservation", "ReservationNumber", "LastName", "FirstName", "EmailAddress", "ClientName" };
    foreach (string sMe in aSessions)
    {
      try
      {
        HttpContext.Current.Session.Remove(sMe);
      }
      catch
      {
        //TODO: Better handle exception
      }
    }
  }
  public static void RemoveSession(string[] sessions)
  {
    foreach (string sMe in sessions)
    {
      try
      {
        HttpContext.Current.Session.Remove(sMe);
      }
      catch
      {
        //TODO: Better handle exception
      }
    }
  }


  [Serializable]
  [XmlRoot("LetterObj")]
  public class LetterObj
  {
    public LetterObj()
    {

    }

    [XmlElement("ProcessLetterKind")]
    public LetterGroup ProcessLetterKind
    {
      get;
      set;
    }

    [XmlElement("ReservationNumber")]
    public string ReservationNumber
    {
      get;
      set;
    }

    [XmlElement("LetterID")]
    public int LetterID
    {
      get;
      set;
    }


    [XmlElement("LetterName")]
    public string LetterName
    {
      get;
      set;
    }

    [XmlElement("JumpPage")]
    public string JumpPage
    {
      get;
      set;
    }

    [XmlElement("FinalURL")]
    public string FinalURL
    {
      get;
      set;
    }

    [XmlElement("MagUserID")]
    public int MagUserID
    {
      get;
      set;
    }

    [XmlElement("MagUserName")]
    public string MagUserName
    {
      get;
      set;
    }

    [XmlElement("ResvNumberList")]
    public string ResvNumberList
    {
      get;
      set;
    }

    [XmlElement("FullName")]
    public string FullName
    {
      get;
      set;
    }
    /*
    [XmlElement("LoggedIn")]
    public bool LoggedIn
    {
      get;
      set;
    }
    [XmlElement("Password")]
    public string Password
    {
      get;
      set;
    }
    [XmlElement("EmailAddress")]
    public string EmailAddress
    {
      get;
      set;
    }
    [XmlElement("FullName")]
    public string FullName
    {
      get;
      set;
    }
    */
  }
}

