﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WimcoFCC.ascx.cs" Inherits="fccCustom.WimcoFCC" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="AddFCC.ascx" TagName="wimAddFCC" TagPrefix="wimcoCustom" %>
<%@ Register Src="~/EditFCC.ascx" TagPrefix="wimcoCustom" TagName="EditFCC" %>

<style type="text/css">
    .FCCControl-Selected-Row
    {
        background-color: #FFFF99 !important; 
    }
     .fccHistory > tbody > tr > td, .fccHistory > tbody > tr > th
    {
       padding-top:2.5px;
       padding-bottom:2.5px;
    }
    .EU_dataTable > tr
    {
    	min-height:55px;
    	padding-top:10px;
    	padding-bottom:10px;
    	}
</style>    
   <script type="text/javascript">
 function clientOrdersUpdate(){
 
 try{  // alert("ok");
var clientOrders = $("id*='gvClientOrders'");
var dateButton = clientOrders.find("th:eq(3)").find("a");
alert(dateButton.text);
dateButton.click();
dateButton.click();
}
catch(err){console.log(err);}

      }
      </script>

  
<div id="fccControl">
<div id="fccMainGridDisplay" >
    <asp:Panel runat="server" ID="pnlMainFCC">
<asp:Timer ID="timer" runat="server" Enabled="false" ontick="timer_Tick"></asp:Timer>
<asp:Label ID="lblInformation" runat="server" Visible="false" BackColor="Yellow" Font-Bold="true" ForeColor="Black" />
<asp:Button ID="btnAddNewFCC" OnClientClick="focusFCC(800);" runat="server" Text="Add New FCC" 
        onclick="btnAddNewFCC_Click"  ForeColor="Red" />
<asp:Button ID="btnAddNewAdminProfile" OnClientClick="focusFCC(800);" runat="server" Text="Add New Admin/Profile Note" 
          onclick="btnAddNewAdminProfile_Click" />


<asp:ObjectDataSource ID="odsFCC" runat="server" 
        SelectMethod="GetCallbackByClientID"  OnSelecting="odsFCCSelecting" 
    TypeName="WimcoBaseLogic.BaseLogic" 
        DataObjectTypeName="WimcoBaseLogic.BusinessLogic.FCCobj" 
        DeleteMethod="DeleteFCC" InsertMethod="InsertFCC" UpdateMethod="UpdateFCC">
    <SelectParameters>
        <asp:Parameter Name="ClientID" Type="Int32" DefaultValue="406613" />
    </SelectParameters>
</asp:ObjectDataSource>
   
<div style="overflow-y: scroll; max-height: 400px;">
<asp:GridView ID="grdFCC" OnRowDataBound="grdFCC_OnRowDataBound" runat="server" AllowSorting="True" Caption="FCC"
    CssClass="EU_DataTable"
    AutoGenerateColumns="False" 
    DataKeyNames="CallbackID,ContactKindID,BusinessID,CallbackDate,CreateDate,MocID,LeadScoreID,LastUpdate,IsEnabled,Description,AgentID,FullName,MocDesc,ClientID,FCCType" 
    DataSourceID="odsFCC" onselectedindexchanged="grdFCC_SelectedIndexChanged"  
    >  
    <Columns>
        <asp:CommandField ButtonType="Button" HeaderText="Modify" SelectText="Modify" 
                    ShowSelectButton="True" />
        <asp:BoundField DataField="OrderNumber" HeaderText="Order#" 
            SortExpression="OrderNumber" />
        <asp:BoundField DataField="CallbackDate" HeaderText="Callback" 
            SortExpression="CallbackDate" DataFormatString="{0:d}" />
        
        <asp:BoundField DataField="CreateDate" HeaderText="Created" 
            SortExpression="CreateDate" DataFormatString="{0:d}" />
       
        <asp:BoundField DataField="LeadHeat" HeaderText="Lead" 
            SortExpression="LeadHeat" />
        
        <asp:BoundField DataField="Description" HeaderText="Notes" 
            SortExpression="Description" />
        <asp:BoundField DataField="FullName" HeaderText="Assigned" 
            SortExpression="FullName" />
        <asp:BoundField DataField="MocDesc" HeaderText="Method" 
            SortExpression="MocDesc" />
        <asp:BoundField DataField="ContactSourceName" HeaderText="Source" 
            SortExpression="ContactSourceName" />
        <asp:BoundField DataField="DestinationName" HeaderText="Dest" 
            SortExpression="DestinationName" />
             <asp:TemplateField HeaderText ="FCC Type">
            
            <ItemTemplate>
              <asp:Literal runat="server" ID="litFCCType" Text='<%# GetFCCType(Eval("FCCType")) %>' />
            </ItemTemplate>
        </asp:TemplateField>    
        <asp:TemplateField>
            <HeaderTemplate>
               <h3>History</h3>
            </HeaderTemplate>
            <ItemTemplate>
                      <asp:LinkButton ID="btnShowFCCHistory" runat="server" CommandArgument='<%#Eval("CallbackID")%>' OnClientClick="focusFCC(800);" OnCommand="cmdFCCHistory" Text="History">
                      </asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>    
    </Columns>
    <EmptyDataTemplate>
    <div>
    <br />
    <strong>&nbsp;&nbsp;No FCC's for this client. Make One Now! :)&nbsp;&nbsp;</strong>
    <br />
    </div>
    </EmptyDataTemplate> 
    <SelectedRowStyle CssClass="FCCControl-Selected-Row"  />
</asp:GridView>
</div>
</asp:Panel>
</div>
<div id="fccEditDiv" class="">
    <asp:Panel ID="pnlFCCEdit" runat="server" Visible="false">           

    <wimcoCustom:EditFCC runat="server" id="EditFCC" />
     
           <asp:Label ID="lblTempData" runat="server" Visible="false" />

</asp:Panel>
</div>
<div id="fccNewDiv" class="">
 <wimcoCustom:wimAddFCC ID="wimAddFCC" purpose="default" runat="server" Visible="true" />
<asp:Panel ID="pnlEditNote" runat="server" Visible="false">
  
<asp:Label ID="lblCreate" runat="server" Visible ="false"></asp:Label>
    <asp:RadioButtonList ID="rdoNoteEdit" runat="server" 
        RepeatDirection="Horizontal" TextAlign="Left">
        <asp:ListItem Value="5">Admin</asp:ListItem>
        <asp:ListItem Value="9">Profile</asp:ListItem>
    </asp:RadioButtonList>



        <h3 style="width: 400px; height: 28px">Assigned&nbsp&nbsp<asp:DropDownList 
                ID="ddlNoteEditAssigned" runat="server">
            </asp:DropDownList>
        </h3>

                <h3 style="width: 400px; height: 28px">Source&nbsp&nbsp<asp:DropDownList 
                ID="ddlSourceNote" runat="server">
                </asp:DropDownList>
        </h3>
    
    
                <h3 style="width: 400px; height: 28px">Destination&nbsp&nbsp<asp:DropDownList 
                ID="ddlNoteDest" runat="server">
                </asp:DropDownList>
        </h3>    
      <asp:TextBox ID="txtNoteEdit" runat="server" TextMode="MultiLine" 
        Width="500px" Rows="6"></asp:TextBox>
        <br />
        <asp:Button ID="btnNoteModify" OnClientClick="clientOrdersUpdate();" runat="server" Text="Submit"  onclick="btnNoteModify_Click" 
         />&nbsp&nbsp<asp:Button ID="btnNoteCancel" runat="server" 
        Text="Cancel"  onclick="btnNoteCancel_Click"  />&nbsp&nbsp<asp:Button ID="btnNoteDelete" 
        runat="server" Text="Delete"  Font-Bold="True" 
        Font-Underline="False" ForeColor="#FF3300" Visible="false"
        ToolTip="This will Delete the Admin/Profile . Use Caution" 
        onclick="btnNoteDelete_Click" />
</asp:Panel>
<asp:Panel ID="pnlFCCHistory" runat="server" Visible="false">
<asp:GridView ID="grdFCCHistory" CssClass="fccHistory"  runat="server"  >
<EmptyDataTemplate>
    No History To Display.
</EmptyDataTemplate>
<Columns>
<asp:TemplateField >
<FooterTemplate >

</FooterTemplate>
</asp:TemplateField>

</Columns>
</asp:GridView>
<asp:Button ID="btnFCCHistoryClose"  runat="server" 
        onclick="btnFCCHistoryClose_Click" Font-Bold="True" 
        Font-Underline="False" ForeColor="#FF3300"    Text="Close" />


</asp:Panel>
</div>
</div>


