﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using WimcoBaseLogic.BusinessLogic;


namespace GWCustom
{
  public partial class GWWimco : System.Web.UI.UserControl
  {
    public bool ReplyButtonVisible { get; set; }
    public bool SearchByEmail { get; set; }
    public bool SeachByClientID { get; set; }

    public string EmailFromDisplayName { get; set; }
    public string EmailFromEmailAddress { get; set; }


    public string EmailToSearch { get; set; }
    public int WimcoClientID { get; set; }

    public string GWGroupwiseID { get; set; }
    public string GWContent { get; set; }

    public string ReservationNumber { get; set; }
    public string ItineraryNumber { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

      Page.MaintainScrollPositionOnPostBack = true;

      if (ReplyButtonVisible)
      {
        btnReply.Visible = true;
      }
      if (!ReplyButtonVisible)
      {
        btnReply.Visible = false;
      }


      ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
      scriptManager.RegisterPostBackControl(this.lnkBtnAtt1);
      scriptManager.RegisterPostBackControl(this.lnkBtnAtt2);
      scriptManager.RegisterPostBackControl(this.lnkBtnAtt3);
      scriptManager.RegisterPostBackControl(this.lnkBtnAtt4);
      scriptManager.RegisterPostBackControl(this.lnkBtnAtt5);


    }



    protected void wEmailGrid_SelectedIndexChanged(object sender, EventArgs e)
    {
      txtEmailMsg.Text = string.Empty;
      lblHTML.Text = string.Empty;
      pnlEmail.Visible = true;
      pnlSearch.Visible = false;
      GridView gv = (GridView)sender;
      DataKey k = gv.SelectedDataKey;
      Session["ee2_^_nnW"] = gv.SelectedIndex;
      string GroupWiseID = gv.SelectedDataKey[0].ToString();
      lblTempStorageGroupWiseID.Text = GroupWiseID;
      WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();
      GWMailObject mail = new GWMailObject();
      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
      bizLogic.Timeout = 5000;
      try
      {
        mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
      }
      catch
      {
        mail.StatusCode = -1144;
      }
      if (mail.StatusCode == 0)
      {
        string m = string.Empty;
        foreach (Recipient dd in mail.Distribution.recipients)
        {
          if (dd.distType.ToString() == "To")
          {
            m = m + dd.distType.ToString() + " : " + dd.email + "<br/>";
          }
        }

        foreach (Recipient dd in mail.Distribution.recipients)
        {
          if (dd.distType.ToString() != "To")
          {
            m = m + dd.distType.ToString() + " : " + dd.email + "<br/>";
          }
        }

        m = m + "From            : " + mail.FromName + " <" + mail.FromEmail + "><br/>";
        m = m + "Subj            : " + mail.Subject + "<br/>";
        m = m + "Date            : " + mail.DateSent + "<br/>";

        lblTempStorageSubject.Text = mail.Subject;

        lblEmail.Text = m;
        lblTempStorageEmailTo.Text = mail.FromEmail;


        // attachments start


        Session.Remove("s1");
        Session.Remove("s2");
        Session.Remove("s3");
        Session.Remove("s4");
        Session.Remove("s5");
        lblAttachment.Visible = false;
        lnkBtnAtt1.Text = string.Empty;
        lnkBtnAtt1.Visible = false;
        lnkBtnAtt2.Text = string.Empty;
        lnkBtnAtt2.Visible = false;
        lnkBtnAtt3.Text = string.Empty;
        lnkBtnAtt3.Visible = false;
        lnkBtnAtt4.Text = string.Empty;
        lnkBtnAtt4.Visible = false;
        lnkBtnAtt5.Text = string.Empty;
        lnkBtnAtt5.Visible = false;

        try
        {
          int i = 1;
          foreach (AttachmentItemInfo ao in mail.Attachments)
          {

            if (ao.size == 0)
            {
              goto jmp;
            }


            if (ao.name.Contains(".htm"))
            {
              goto jmp;
            }

            if (ao.name.Contains("Mime"))
            {
              goto jmp;
            }

            if (i == 1)
            {
              lnkBtnAtt1.Text = ao.name;
              lnkBtnAtt1.CommandArgument = "s1";
              lnkBtnAtt1.Visible = true;
              Session.Add("s1", ao);
              Session.Add("s1gwid", GroupWiseID);
              lblAttachment.Visible = true;
            }

            if (i == 2)
            {
              lnkBtnAtt2.Text = ao.name;
              lnkBtnAtt2.CommandArgument = "s2";
              lnkBtnAtt2.Visible = true;
              Session.Add("s2", ao);
              Session.Add("s2gwid", GroupWiseID);
              lblAttachment.Visible = true;
            }

            if (i == 3)
            {
              lnkBtnAtt3.Text = ao.name;
              lnkBtnAtt3.CommandArgument = "s3";
              lnkBtnAtt3.Visible = true;
              Session.Add("s3", ao);
              Session.Add("s3gwid", GroupWiseID);
              lblAttachment.Visible = true;
            }

            if (i == 4)
            {
              lnkBtnAtt4.Text = ao.name;
              lnkBtnAtt4.CommandArgument = "s4";
              lnkBtnAtt4.Visible = true;
              Session.Add("s4", ao);
              Session.Add("s4gwid", GroupWiseID);
              lblAttachment.Visible = true;
            }

            if (i == 5)
            {
              lnkBtnAtt5.Text = ao.name;
              lnkBtnAtt5.CommandArgument = "s5";
              lnkBtnAtt5.Visible = true;
              Session.Add("s5", ao);
              Session.Add("s5gwid", GroupWiseID);
              lblAttachment.Visible = true;
            }



            i++;

          jmp:;

          }
        }
        catch (Exception exx)
        {
          string err = exx.Message;
        }

        // attachments end


        if (mail.MessageContainsHTML)
        {

          if (mail.MessageInHTML.Contains("<img src=\"cid:"))
          {
            mail.MessageInHTML = mail.MessageContent;
          }


          lblHTML.Text = mail.MessageInHTML;
          lblTempStorage.Text = mail.MessageInHTML;
          txtEmailMsg.Text = string.Empty;
          txtEmailMsg.Visible = false;
        }

        if (!mail.MessageContainsHTML)
        {
          lblHTML.Text = string.Empty;
          txtEmailMsg.Text = mail.MessageContent;
          lblTempStorage.Text = mail.MessageContent;
          txtEmailMsg.Visible = true;
        }

      }

      if (mail.StatusCode != 0)
      {
        txtEmailMsg.Visible = false;
        lblEmail.Text = "<br/><strong> **** Email has been deleted from Server.. Sorry :(   </strong><br/>";
      }

      if (mail.StatusCode == -1144)
      {
        txtEmailMsg.Visible = false;
        lblEmail.Text = "<br/><strong> **** I Can't reach Groupwise to read this email.. Perhaps the server is down. Please try again later. Sorry :(   </strong><br/>";

      }
    }


    protected void btnClose_Click(object sender, EventArgs e)
    {
      pnlEmail.Visible = false;
      pnlSearch.Visible = true;
    }


    public void SearchEmail()
    {
      if (SearchByEmail)
      {
        wEmailGrid.DataBind();
      }

      if (SeachByClientID)
      {
        SearchByEmail = false;
      }

    }

    protected void btnReply_Click(object sender, EventArgs e)
    {

      txtEmailToAddress.Text = lblTempStorageEmailTo.Text.ToLower();
      txtEmailFromName.Text = EmailFromDisplayName;
      txtEmailFromEmailAddress.Text = EmailFromEmailAddress;
      txtEmailSubject.Text = "Re: " + lblTempStorageSubject.Text;

      string replydata = "<br><br><br><hr><br><w99></w99>" + lblTempStorage.Text;

      txtEditor.Text = replydata;
      pnlEmail.Visible = false;
      pnlSearch.Visible = false;
      pnlReply.Visible = true;

    }

    protected void btnSend_Click(object sender, EventArgs e)
    {


      bool isHtmlEmail = false;
      string html = txtEditor.Text;
      string original = lblTempStorage.Text;
      string useradded = string.Empty;
      string theBody = string.Empty;
      string FinalMessage = string.Empty;

      RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
      Regex regx = new Regex("<body(?<theBody>.*)/body>", options);
      Match match = regx.Match(original);




      if (match.Success)
      {
        isHtmlEmail = true;
        int end = html.IndexOf("&lt;w99");
        useradded = html.Substring(0, end);
        theBody = match.Value;
        theBody = theBody.Replace("</BODY", "</body");

        int sbody = theBody.IndexOf(">");
        int ebody = theBody.IndexOf("</body>");

        string subbody = theBody.Substring(sbody, ebody - sbody);

        int StartInsertInt = theBody.IndexOf(">");
        StartInsertInt++;

        theBody = theBody.Insert(StartInsertInt, useradded);
        FinalMessage = "<html>" + theBody + "</html>";

      }

      if (!match.Success)
      {
        isHtmlEmail = true;
        int end = html.IndexOf("&lt;w99");
        useradded = html.Substring(0, end);
        FinalMessage = "<html>" + useradded + original + "</html>";
      }

      WimcoBaseLogic.xsd.Groupwise_OutboundDS gwo = new WimcoBaseLogic.xsd.Groupwise_OutboundDS();
      WimcoBaseLogic.xsd.Groupwise_OutboundDS.Groupwise_OutboundRow r = gwo.Groupwise_Outbound.NewGroupwise_OutboundRow();
      r.MessageID = Guid.NewGuid().ToString();
      r.RecipentEmail = txtEmailToAddress.Text;
      r.CCField = txtEmailCCEmailAddress.Text;
      //r.BCCField = "wimcoops@wimco.com"; 
      r.DateAdded = DateTime.Now;
      r.DisplayFromEmailAddress = txtEmailFromEmailAddress.Text;
      r.DisplayFromName = txtEmailFromName.Text;
      r.EmailContent = FinalMessage;
      r.EmailSubject = txtEmailSubject.Text;
      r.GroupwiseID = lblTempStorageGroupWiseID.Text;
      r.HTMLMessage = isHtmlEmail;
      r.Sent = false;
      gwo.Groupwise_Outbound.AddGroupwise_OutboundRow(r);
      WimcoBaseLogic.BaseLogic.UpdateDataSet(gwo);

      lblAction.Text = "Email in Que to be Sent!";
      lblAction.Visible = true;
      timer.Interval = 2000;
      timer.Enabled = true;


      pnlEmail.Visible = false;
      pnlSearch.Visible = true;
      pnlReply.Visible = false;
    }

    protected void btnCancelSend_Click(object sender, EventArgs e)
    {
      lblAction.Text = "Send Canceled";
      lblAction.Visible = true;
      timer.Interval = 2000;
      timer.Enabled = true;

      pnlEmail.Visible = false;
      pnlSearch.Visible = true;
      pnlReply.Visible = false;
    }



    protected void ODSSelecting(object source, ObjectDataSourceMethodEventArgs e)
    {
      if (EmailToSearch == null)
      {
        try
        {
          EmailToSearch = Session["gwxSr_^_991.w"].ToString();
        }
        catch
        {

        }
      }

      if (EmailToSearch != null)
      {
        Session["gwxSr_^_991.w"] = EmailToSearch;
        e.InputParameters["Email"] = EmailToSearch;
      }

    }

    protected void timer_Tick(object sender, EventArgs e)
    {
      lblAction.Text = string.Empty;
      lblAction.Visible = false;
      timer.Enabled = false;
    }

    protected void lnkBtnAtt1_Click(object sender, EventArgs e)
    {

      LinkButton li = (LinkButton)sender;
      string op = li.CommandArgument;
      string op2 = op + "gwid";


      AttachmentItemInfo ao = (AttachmentItemInfo)Session[op];
      string GroupWiseID = (string)Session[op2];


      WimcoBusinessLogic bizLogic = new WimcoBusinessLogic();

      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
      string UserName = WimcoBaseLogic.BaseLogic.AdHocSeek("SELECT Groupwise_User.UserName FROM  Groupwise_Recipients INNER JOIN Groupwise_User ON Groupwise_Recipients.MsgOwner = Groupwise_User.GroupwiseUserID WHERE (Groupwise_Recipients.GroupwiseID = N'" + GroupWiseID + "')", "UserName");
      string Password = WimcoBaseLogic.BaseLogic.AdHocSeek("SELECT Groupwise_User.Password FROM  Groupwise_Recipients INNER JOIN Groupwise_User ON Groupwise_Recipients.MsgOwner = Groupwise_User.GroupwiseUserID WHERE (Groupwise_Recipients.GroupwiseID = N'" + GroupWiseID + "')", "Password");
      GWAttachmentObject att = new GWAttachmentObject();
      att = bizLogic.Groupwise_GetAttachment(AuthKey, UserName, Password, ao);


      if (att.Content.Length == 0) // got an empty attachment.
      {
        lblAction.Text = "Cant Access this attachment on Groupwise. Sorry :(";
        lblAction.Visible = true;
        timer.Interval = 5000;
        timer.Enabled = true;
      }

      if (att.Content.Length > 0)
      {
        Response.ClearContent();
        Response.ClearHeaders();
        Response.BufferOutput = true;
        Response.AddHeader("Content-Disposition", "filename=" + att.AttachmentName.Replace(" ", "_"));
        Response.AddHeader("Content-Length", att.Lenght.ToString());
        Response.ContentType = "application/unknown";
        Response.BinaryWrite(att.Content);
        Response.Flush();
        Response.Close();
      }
    }


  }
}