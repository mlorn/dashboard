﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditFCC.ascx.cs" Inherits="Dashboard.EditFCC" %>
<asp:Label ID="lblTempData" runat="server" Visible="false" />



<asp:RangeValidator BackColor="White" BorderColor="Black" CssClass="FCCRange" ID="RangeValidator1" runat="server" ErrorMessage="Dates must be within 18 months of today"
  ControlToValidate="txtCalEdit"></asp:RangeValidator>
<table border="0" class="emailHead">
  <tr>
    <td>Callback</td>
    <td style="padding-right: 4px; width: 130px">
      <asp:TextBox TextMode="Date" Max='<%# DateTime.Now.AddMonths(18) %>' ID="txtCalEdit" runat="server" />


    </td>

    <td align="right">Type:
    </td>
    <td style="padding-right: 4px;">
      <asp:DropDownList Enabled="true"
        ID="ddlFCCType" runat="server">
        <asp:ListItem Value="">FCC Type</asp:ListItem>
        <asp:ListItem Value="390">Client Prospecting</asp:ListItem>
        <asp:ListItem Value="983">Opportunity</asp:ListItem>
        <asp:ListItem Value="982">Service</asp:ListItem>

      </asp:DropDownList>
    </td>
  </tr>
  <tr>
    <td>Assigned:
    </td>
    <td colspan="3">
      <asp:DropDownList
        ID="ddlAssigned" Width="100%" runat="server">
      </asp:DropDownList>
    </td>

  </tr>
  <tr>
    <td>Lead:
    </td>
    <td>
      <asp:DropDownList
        ID="ddlLeadScore" Width="100%" runat="server">
        <asp:ListItem Value="1">Hot</asp:ListItem>
        <asp:ListItem Value="2">Warm</asp:ListItem>
        <asp:ListItem Value="3">Cold</asp:ListItem>
      </asp:DropDownList>
    </td>
    <td align="right">Method:
    </td>
    <td>
      <asp:DropDownList
        ID="ddlMocEdit" Width="100%" runat="server">
        <asp:ListItem Value="6">Phone</asp:ListItem>
        <asp:ListItem Value="2">Email</asp:ListItem>
        <asp:ListItem Value="1">Mail</asp:ListItem>
        <asp:ListItem Value="3">Fax</asp:ListItem>
        <asp:ListItem Value="4">Hardcopy</asp:ListItem>
        <asp:ListItem Value="7">Telex</asp:ListItem>
        <asp:ListItem Value="8">Other</asp:ListItem>
        <asp:ListItem Value="10">Close FCC</asp:ListItem>
      </asp:DropDownList>
    </td>

    <tr>
      <td>Source:
      </td>
      <td colspan="3">
        <asp:DropDownList
          ID="ddlSourceName" Width="100%" runat="server">
        </asp:DropDownList>
      </td>
    </tr>
  <tr>
    <td>Destination:
    </td>
    <td colspan="3">
      <asp:DropDownList
        ID="ddlDest" Width="100%" runat="server">
      </asp:DropDownList>
  </tr>

  <tr>
    <td style="vertical-align: top">FCC Notes:
    </td>
    <td colspan="3" style="padding-right: 5px;">
      <asp:TextBox ID="txtFccDisplay" runat="server" TextMode="MultiLine"
        Width="350px" Rows="5"></asp:TextBox>
    </td>
  </tr>
</table>
<br />
<%-- OnClientClick="clientOrdersUpdate();" --%>
<asp:Button CssClass="FCCButton" ID="btnModify" ForeColor="Red" runat="server" Text="Submit"
  OnClick="btnModify_Click" />&nbsp&nbsp<asp:Button ID="btnCancel" runat="server"
    Text="Cancel" OnClick="btnCancel_Click" />&nbsp&nbsp<asp:Button ID="btnAddNewFCC" runat="server"
      Text="Add New FCC" OnClick="btnAddNewFCC_Click" />&nbsp&nbsp<asp:Button ID="btnDelete" Visible="false"
        runat="server" Text="Delete" OnClientClick="focusFCCpreBuild();" OnClick="btnDelete_Click" Font-Bold="True"
        Font-Underline="False" ForeColor="#FF3300"
        ToolTip="This will Delete the FCC. Use Caution" />
<asp:Label ID="lblInformation" runat="server" Visible="true" BackColor="Yellow" Font-Bold="true" ForeColor="Black" />

