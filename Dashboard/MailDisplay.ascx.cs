﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WimcoBaseLogic.BusinessLogic;

namespace GWCustom
{
  public partial class MailDisplay : System.Web.UI.UserControl
  {

    protected void Page_Load(object sender, EventArgs e)
    {
      if (HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().Contains("new=true"))
      {

        if (!IsPostBack)
        {
          EmailForm.Visible = true;
          Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
          EmailForm.Visible = true;
          try
          {
            EmailForm.EmailTo = HttpContext.Current.Session["EmailToSearch"].ToString();
            EmailForm.EmailSubject = HttpContext.Current.Request.QueryString["subject"].ToString();
          }
          catch
          {
          }
          try
          {
            EmailForm.EmailTo = HttpContext.Current.Session["EmailToAddress"].ToString();

            //This is an override for the client email grid because an email could go to the 
            //non-primary email address or a different one from what appears in search results.  
            //Added 4/17/2017 DA
          }
          catch
          {
          }
          if (HttpContext.Current.Session["TemplateKindID"] != null)
          {
            if (HttpContext.Current.Session["TemplateKindID"].ToString() == "2")
            {
              WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS celFCC = new WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS();
              String[] emails2 = WimcoBaseLogic.BaseLogic.clientemail_GetEmailAddressArray(Int32.Parse(Session["ClientID"].ToString()));

              GW_IndexSearchDS GWDS = WimcoBaseLogic.BaseLogic.SearchByEmailAddresses(emails2);
              if (GWDS.Tables.Count > 0)
              {
                String EmailText = string.Empty;
                string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
                String html = string.Empty;
                WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();
                //  ObjectDataSource1.SelectParameters["Email"].DefaultValue = Session["EmailToSearch"].ToString();
                //    ObjectDataSource1.DataBind();
                bizLogic.Timeout = 5000;
                DataTable dtEmails = GWDS.Tables[0];
                for (int i = 0; i < dtEmails.Rows.Count; i++)
                {
                  if (i >= 2)
                  {
                    break;
                  }
                  GWMailObject mail = new GWMailObject();
                  String GroupWiseID = dtEmails.Rows[i]["GroupwiseID"].ToString();

                  String html2 = string.Empty;
                  mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
                  if (mail.StatusCode > -1)
                  {
                    if (i == 0)
                    {
                      EmailForm.EmailSubject = "Re: " + mail.Subject;
                    }
                    if (mail.MessageContainsHTML)
                    {
                      if (!mail.MessageInHTML.Contains("mail has been deleted from Server") && !mail.MessageInHTML.Contains("Can't reach Groupwise to read this email"))
                      {
                        //if (html2 == "")
                        //{
                        try
                        {
                          html2 += "<br/> >>> " + mail.FromName + " &lt;" + mail.FromEmail + "&gt; ";
                          html2 = html2 + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + " >>> <br/>";
                        }
                        catch { }

                        //}




                        html2 = html2 + "<div class='email'><jk></jk>";


                        //html2 = html2 + "Date : " + mail.DateSent.ToString("yyyy-MM-dd HH:mm:ss") + "<br/>";
                        //html2 = html2 + "From: " + mail.FromEmail + " To: " + mail.Distribution.to + "<br/>";
                        //html2 = html2 + "Subject : " + mail.Subject + "<br/>";
                        //html2 = html2 + "<br/><br/><br/>";
                        html2 = html2 + "</div>";
                        html2 = html2 + mail.MessageInHTML.Replace("!important", string.Empty).Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href");
                        EmailText += html2;
                      }
                    }
                    else
                    {
                      if (html2 == string.Empty)
                      {
                        try
                        {
                          html2 += "<br/> >>> " + mail.FromName + " &lt;" + mail.FromEmail + "&gt; ";
                          html2 = html2 + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + " >>> <br/>";
                        }
                        catch { }

                      }
                      html2 = html2 + "<div class='email'><jk></jk>";


                      //html2 = html2 + "Date : " + mail.DateSent.ToString("yyyy-MM-dd HH:mm:ss") + "<br/>";
                      //html2 = html2 + "From: " + mail.FromEmail + " To: " + mail.Distribution.to + "<br/>";
                      //html2 = html2 + "Subject : " + mail.Subject + "<br/>";
                      //html2 = html2 + "<br/><br/><br/>";
                      html2 = html2 + "</div>";
                      html2 = html2 + "<pre>" + mail.MessageContent.Replace("!important", string.Empty).Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href") + "</pre>";
                      EmailText += html2;
                    }
                  }


                }
                Session["EmailBody"] = EmailText;
              }
              //DataSet dsEmails2 = WimcoBaseLogic.BaseLogic.SearchByEmailAddresses(emails2);
              //DataTable dtEmails2 = new DataTable();
              //dtEmails2.Columns.Add("GroupwiseID");

              //for (int em = 0; em < dsEmails2.Tables[0].Rows.Count; em++) {
              //     if (em >= 2) {
              //        break;
              //    }
              //    DataRow drGW = dtEmails2.NewRow();
              //    drGW["GroupwiseID"] =dsEmails2.Tables[0].Rows[em]["GroupwiseID"].ToString();
              //    dtEmails2.Rows.Add(drGW);

              //}
              // RptEmails.DataSource = dtEmails2;
              // RptEmails.DataBind();
              // RptEmails.Visible = false;
              pnlEmail.Visible = false;
              // EmailForm.EmailBody = Session["EmailBody"].ToString();
            }


          }

          EmailForm.EmailName = ud.FullName; //lblTempStorageEmailFromName.Text;
          EmailForm.EmailFrom = ud.EmailAddress;//lblTempStorageEmailFrom.Text;
                                                //EmailForm.EmailSubject = "";
                                                // EmailForm.EmailCC = ud.EmailAddress;

          pnlEmailButtons.Visible = false;
          pnlEmail.Visible = false;
          //For Debugging
          // HttpContext.Current.Session["FCCContactSource"] = "700";
          //HttpContext.Current.Session["TemplateKindID"] = "1";
          //

        }
      }
      else
      {
        if (HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().Contains("showall") && EmailForm.Visible == false)
        {


          //try
          //{
          //    lblDebug.Text = Session["EmailDebug"].ToString();
          //}
          //catch { }
          try
          {
            //  ObjectDataSource1.SelectParameters["Email"].DefaultValue = Session["EmailToSearch"].ToString();
            //    ObjectDataSource1.DataBind();
            WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS cel = new WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS();
            String[] emails = WimcoBaseLogic.BaseLogic.clientemail_GetEmailAddressArray(Int32.Parse(Session["ClientID"].ToString()));
            DataSet dsShowAll = ((DataSet)WimcoBaseLogic.BaseLogic.SearchByEmailAddresses(emails));
            //   dsShowAll.Tables[0].DefaultView.Sort = "Date DESC";
            DataView dvShowAll = dsShowAll.Tables[0].DefaultView;
            dvShowAll.Sort = "Date DESC";

            RptEmails.DataSource = dvShowAll;
            //  RptEmails.DataSource = WimcoBaseLogic.BaseLogic.SearchByEmailAddresses(emails);
            RptEmails.DataBind();


          }
          catch { }
          try
          {
            lblTempStorageEmailTo.Text = HttpContext.Current.Session["EmailToSearch"].ToString();

          }
          catch
          {
          }

          upEmailbody.Visible = true;
          pnlEmail.Visible = false;
          btnShowAll.Visible = false;
          btnReplyAll.Visible = false;
          btnReply.Visible = true;// false;
        }
        else
        {

          upEmailbody.Visible = false;
          if (!IsPostBack)
          {

            string GroupWiseID = string.Empty;

            // clean URL.
            /*
            string GroupWiseID = "";
            string gotid = "";
            try
            {
                gotid = Session["msg-gotID"].ToString();
            }
            catch
            { }

            if (gotid == "")
            {
                Session.Add("GroupWiseID", Request.QueryString["m"].ToString());
                Session.Add("msg-gotID", "y");
                Response.Redirect("viewmail.aspx");
            }


            if (gotid == "y")
            {
                GroupWiseID = Session["GroupWiseID"].ToString();
                Session.Remove("msg-gotID");
            }
            */


            //Session.Add("mgs-gotID",(string)GroupWiseID);
            if (Request.Url.AbsoluteUri.ToLower().Contains("hid="))
            { //This will handle the dashboard hover piece using ClientID
              Session["ClientID"] = Request.QueryString["hid"].ToString();
              String[] emails = WimcoBaseLogic.BaseLogic.clientemail_GetEmailAddressArray(Int32.Parse(Session["ClientID"].ToString()));
              for (int i = 0; i < emails.Length; i++)
              {
                WimcoBaseLogic.BaseLogic.GWSearch gws = new WimcoBaseLogic.BaseLogic.GWSearch();
                gws = WimcoBaseLogic.BaseLogic.Groupwise_SearchByEmail(Int32.Parse(Session["ClientID"].ToString()), emails[i].ToString());


              }

              DataRow[] drgw = WimcoBaseLogic.BaseLogic.SearchByEmailAddresses(emails).Tables[0].Select(string.Empty, "Date DESC", DataViewRowState.CurrentRows);
              if (drgw.Length > 0)
              {
                GroupWiseID = drgw[0]["GroupwiseID"].ToString();
              }
              else
              {
                GroupWiseID = Request.QueryString["m"];
              }
            }
            else
            {
              GroupWiseID = Request.QueryString["m"];
            }


            WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();
            GWMailObject mail = new GWMailObject();
            string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
            bizLogic.Timeout = 5000;
            try
            {
              mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
            }
            catch
            {
              mail.StatusCode = -1144;
            }
            if (mail.StatusCode == 0)
            {
              Page.Title = mail.Subject;
              string m = string.Empty;


              foreach (Recipient dd in mail.Distribution.recipients)
              {
                if (dd.distType.ToString() == "To")
                {
                  m = m + dd.distType.ToString() + " : " + dd.email + "<br/>";

                }
              }

              foreach (Recipient dd in mail.Distribution.recipients)
              {
                if (dd.distType.ToString() != "To" && dd.distType != DistributionType.BC)
                {
                  m = m + dd.distType.ToString() + " : " + dd.email + "<br/>";
                  lblTempStorageEmailFrom.Text = dd.email;
                }
              }

              m = m + "From            : " + mail.FromName + " <" + mail.FromEmail + "><br/>";
              m = m + "Subj            : " + mail.Subject + "<br/>";
              m = m + "Date            : " + mail.DateSent + "<br/>";

              lblTempStorageSubject.Text = mail.Subject;
              lblTempStorageEmailFromName.Text = mail.Distribution.to;
              lblEmail.Text = m;
              lblTempStorageEmailTo.Text = mail.FromEmail;
              //lblTempStorageEmailFrom.Text = 

              // attachments start


              Session.Remove("s1");
              Session.Remove("s2");
              Session.Remove("s3");
              Session.Remove("s4");
              Session.Remove("s5");
              lblAttachment.Visible = false;
              lnkBtnAtt1.Text = string.Empty;
              lnkBtnAtt1.Visible = false;
              lnkBtnAtt2.Text = string.Empty;
              lnkBtnAtt2.Visible = false;
              lnkBtnAtt3.Text = string.Empty;
              lnkBtnAtt3.Visible = false;
              lnkBtnAtt4.Text = string.Empty;
              lnkBtnAtt4.Visible = false;
              lnkBtnAtt5.Text = string.Empty;
              lnkBtnAtt5.Visible = false;

              try
              {
                int i = 1;
                foreach (AttachmentItemInfo ao in mail.Attachments)
                {

                  if (ao.size == 0)
                  {
                    goto jmp;
                  }


                  if (ao.name.Contains(".htm"))
                  {
                    goto jmp;
                  }

                  if (ao.name.Contains("Mime"))
                  {
                    goto jmp;
                  }

                  if (i == 1)
                  {
                    lnkBtnAtt1.Text = ao.name;
                    lnkBtnAtt1.CommandArgument = "s1";
                    lnkBtnAtt1.Visible = true;
                    Session.Add("s1", ao);
                    Session.Add("s1gwid", GroupWiseID);
                    lblAttachment.Visible = true;
                  }

                  if (i == 2)
                  {
                    lnkBtnAtt2.Text = ao.name;
                    lnkBtnAtt2.CommandArgument = "s2";
                    lnkBtnAtt2.Visible = true;
                    Session.Add("s2", ao);
                    Session.Add("s2gwid", GroupWiseID);
                    lblAttachment.Visible = true;
                  }

                  if (i == 3)
                  {
                    lnkBtnAtt3.Text = ao.name;
                    lnkBtnAtt3.CommandArgument = "s3";
                    lnkBtnAtt3.Visible = true;
                    Session.Add("s3", ao);
                    Session.Add("s3gwid", GroupWiseID);
                    lblAttachment.Visible = true;
                  }

                  if (i == 4)
                  {
                    lnkBtnAtt4.Text = ao.name;
                    lnkBtnAtt4.CommandArgument = "s4";
                    lnkBtnAtt4.Visible = true;
                    Session.Add("s4", ao);
                    Session.Add("s4gwid", GroupWiseID);
                    lblAttachment.Visible = true;
                  }

                  if (i == 5)
                  {
                    lnkBtnAtt5.Text = ao.name;
                    lnkBtnAtt5.CommandArgument = "s5";
                    lnkBtnAtt5.Visible = true;
                    Session.Add("s5", ao);
                    Session.Add("s5gwid", GroupWiseID);
                    lblAttachment.Visible = true;
                  }



                  i++;

                jmp:;

                }
              }
              catch (Exception exx)
              {
                string err = exx.Message;
              }

              // attachments end


              if (mail.MessageContainsHTML)
              {

                if (mail.MessageInHTML.Contains("<img src=\"cid:"))
                {
                  mail.MessageInHTML = mail.MessageInHTML.Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href");
                }


                lblHTML.Text = mail.MessageInHTML.Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href");
                lblTempStorage.Text = mail.MessageInHTML.Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href");
                txtEmailMsg.Text = string.Empty;
                txtEmailMsg.Visible = false;
              }

              if (!mail.MessageContainsHTML)
              {
                lblHTML.Text = txtEmailMsg.Text = "<pre>" + HttpUtility.HtmlDecode(mail.MessageContent.Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href")) + "</pre>";
                //txtEmailMsg.Text = mail.MessageContent.Replace("text/javascript", "text/noscript"); 
                lblTempStorage.Text = "<pre>" + mail.MessageContent.Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href") + "</pre>";
                txtEmailMsg.Visible = false;
                txtEmailMsg.Text = string.Empty;
              }

            }

            if (mail.StatusCode != 0)
            {
              txtEmailMsg.Visible = false;
              lblEmail.Text = "<br/><strong> **** Email has been deleted from Server.. Sorry :(   </strong><br/>";
            }

            if (mail.StatusCode == -1144)
            {
              txtEmailMsg.Visible = false;
              lblEmail.Text = "<br/><strong> **** I Can't reach Groupwise to read this email.. Perhaps the server is down. Please try again later. Sorry :(   </strong><br/>";

            }


          }
        }
      }
    }


    protected void lnkBtnAtt1_Click(object sender, EventArgs e)
    {

      LinkButton li = (LinkButton)sender;
      string op = li.CommandArgument;
      string op2 = op + "gwid";


      AttachmentItemInfo ao = (AttachmentItemInfo)Session[op];
      string GroupWiseID = (string)Session[op2];


      WimcoBusinessLogic bizLogic = new WimcoBusinessLogic();

      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
      string UserName = WimcoBaseLogic.BaseLogic.AdHocSeek("SELECT Groupwise_User.UserName FROM  Groupwise_Recipients INNER JOIN Groupwise_User ON Groupwise_Recipients.MsgOwner = Groupwise_User.GroupwiseUserID WHERE (Groupwise_Recipients.GroupwiseID = N'" + GroupWiseID + "')", "UserName");
      string Password = WimcoBaseLogic.BaseLogic.AdHocSeek("SELECT Groupwise_User.Password FROM  Groupwise_Recipients INNER JOIN Groupwise_User ON Groupwise_Recipients.MsgOwner = Groupwise_User.GroupwiseUserID WHERE (Groupwise_Recipients.GroupwiseID = N'" + GroupWiseID + "')", "Password");
      GWAttachmentObject att = new GWAttachmentObject();
      att = bizLogic.Groupwise_GetAttachment(AuthKey, UserName, Password, ao);


      if (att.Content.Length == 0) // got an empty attachment.
      {
        /*
        lblAction.Text = "Cant Access this attachment on Groupwise. Sorry :(";
        lblAction.Visible = true;
        timer.Interval = 5000;
        timer.Enabled = true;
        */
      }

      if (att.Content.Length > 0)
      {
        Response.ClearContent();
        Response.ClearHeaders();

        Response.Buffer = true;
        //Response.BufferOutput = true;


        Response.Cache.SetCacheability(HttpCacheability.Public);
        Response.Cache.SetMaxAge(new TimeSpan(1, 0, 0));

        Response.AddHeader("Content-Disposition", "filename=" + att.AttachmentName.Replace(" ", "_"));
        Response.AddHeader("Content-Length", att.Lenght.ToString());


        Response.ContentType = "application/unknown";
        Response.BinaryWrite(att.Content);

        Response.Flush();

        Response.End();
        //Response.Close();

      }
    }
    protected void btnShowAll_Click(object sender, EventArgs e)
    {
      Response.Redirect("viewmail.aspx?showall=true");

    }
    protected void btnReply_Click(object sender, EventArgs e)
    {
      String m = string.Empty;
      WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();

      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
      GWMailObject mail = new GWMailObject();
      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      if (HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().Contains("showall"))
      {
        HiddenField hfGWID = ((HiddenField)RptEmails.Items[0].FindControl("hfEmailID"));
        mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, hfGWID.Value.ToString());
        try
        {
          lblTempStorage.Text = Session["emailBody"].ToString();
        }
        catch { }
      }
      else
      {
        String GroupWiseID = string.Empty;
        try
        {
          GroupWiseID = Request.QueryString["m"];

          //     bizLogic.Timeout = 5000;
          try
          {
            mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
          }
          catch
          {
            mail.StatusCode = -1144;
          }
          if (mail.StatusCode == 0)
          {

          }

        }
        catch
        {

        }
      }

      try
      {
        m += ">>> " + mail.FromName + " &lt;" + mail.FromEmail + "&gt; ";
        m = m + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + " >>> <br/>";
      }
      catch { }
      EmailForm.Visible = true;
      EmailForm.EmailTo = lblTempStorageEmailTo.Text.ToLower();
      EmailForm.EmailName = ud.FullName; //lblTempStorageEmailFromName.Text;
      EmailForm.EmailFrom = ud.EmailAddress;//lblTempStorageEmailFrom.Text;
      EmailForm.EmailSubject = "Re: " + lblTempStorageSubject.Text;
      //  EmailForm.EmailCC = ud.EmailAddress;
      string replydata = string.Empty;
      // EmailForm.EmailBody = replydata;
      if (lblTempStorage.Text.Contains("INSERT SIG HERE"))
      {
        replydata = lblTempStorage.Text;
      }
      else
      {
        replydata = "<br><br><w99><br></w99>" + m + lblTempStorage.Text;
      }
      if (lblTempStorage.Text != string.Empty)
      {
        EmailForm.EmailBody = replydata;
      }
      else
      {
        EmailForm.EmailBody = string.Empty;
      }
      pnlEmail.Visible = false;
      upEmailbody.Visible = false;
      //pnlSearch.Visible = false;
      pnlEmailButtons.Visible = false;
      //  pnlReply.Visible = true;
      //   pnlFCC.Visible = true;
      //   wimAddFCC.Attributes["purpose"] = "addFCC";
      //    ((TextBox)wimAddFCC.FindControl("txtNewFCCDisplay")).Text = lblTempStorageSubject.Text;

    }

    protected void btnReplyAll_Click(object sender, EventArgs e)
    {
      string m = string.Empty;
      string strEmails = string.Empty;
      String EmailToSearch = string.Empty;
      DataTable dtEmails;

      try
      {
        EmailToSearch = Session["EmailToSearch"].ToString();
      }
      catch
      {
        EmailToSearch = lblTempStorageEmailTo.Text.ToLower();
      }


      ObjectDataSource1.SelectParameters["Email"].DefaultValue = EmailToSearch;
      ObjectDataSource1.DataBind();


      dtEmails = (((DataView)ObjectDataSource1.Select()).ToTable());

      WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();
      GWMailObject mail = new GWMailObject();
      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();


      if (HttpContext.Current.Request.Url.AbsoluteUri.ToString().ToLower().Contains("showall"))
      {
        HiddenField hfGWID = ((HiddenField)RptEmails.Items[0].FindControl("hfEmailID"));
        mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, hfGWID.Value.ToString());
        try
        {
          lblTempStorage.Text = Session["emailBody"].ToString();

        }
        catch { }
        foreach (DataRow dr in dtEmails.Rows)
        {
          if (strEmails.IndexOf(dr["FromEmail"].ToString().ToLower()) == -1)
          {
            strEmails += dr["FromEmail"].ToString().ToLower() + "; ";

          }
          if (strEmails.IndexOf(dr["Recipient"].ToString().ToLower()) == -1)
          {
            strEmails += dr["Recipient"].ToString().ToLower() + "; ";

          }

        }
        if (strEmails.Length == 0)
        {

          strEmails = EmailToSearch;
        }

      }
      else
      {
        String GroupWiseID = string.Empty;
        try
        {
          GroupWiseID = Request.QueryString["m"];

          //     bizLogic.Timeout = 5000;
          try
          {
            mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
          }
          catch
          {
            mail.StatusCode = -1144;
          }
          if (mail.StatusCode == 0)
          {
            strEmails = mail.FromEmail + ";" + lblTempStorageEmailFrom.Text;
          }

        }
        catch
        {
          strEmails = lblTempStorageEmailFrom.Text;
        }
      }

      m += "<br/> >>> " + mail.FromName + " &lt;" + mail.FromEmail + "&gt; ";
      m = m + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + " >>> <br/>";



      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      EmailForm.Visible = true;
      EmailForm.EmailTo = strEmails;
      EmailForm.EmailName = ud.FullName; //lblTempStorageEmailFromName.Text;
      EmailForm.EmailFrom = ud.EmailAddress;//lblTempStorageEmailFrom.Text;
      EmailForm.EmailSubject = "Re: " + lblTempStorageSubject.Text;
      EmailForm.EmailCC = ud.EmailAddress;
      // string replydata = "<br><br><br><hr><br><w99></w99>" + lblTempStorage.Text;
      string replydata = string.Empty;
      // EmailForm.EmailBody = replydata;
      if (lblTempStorage.Text.Contains("INSERT SIG HERE"))
      {
        replydata = "<br><hr><br><w99></w99>" + lblTempStorage.Text;
      }
      else
      {
        replydata = "<br><br><w99></w99>" + m + lblTempStorage.Text;
      }

      if (lblTempStorage.Text != string.Empty)
      {
        EmailForm.EmailBody = replydata;
      }
      else
      {
        EmailForm.EmailBody = string.Empty;
      }

      //  txtEmailToAddress.Text = strEmails; //lblTempStorageEmailTo.Text.ToLower();
      //  txtEmailFromName.Text = ud.FullName; //lblTempStorageEmailFromName.Text;


      //   txtEmailFromEmailAddress.Text = ud.EmailAddress;//lblTempStorageEmailFrom.Text;


      //   txtEmailSubject.Text = "Re: " + lblTempStorageSubject.Text;
      //   txtEmailCCEmailAddress.Text = ud.EmailAddress;
      //  string replydata = "<br><br><br><hr><br><w99></w99>" + lblTempStorage.Text;

      //  txtEditor.Text = replydata;
      pnlEmail.Visible = false;
      //pnlSearch.Visible = false;
      //   pnlReply.Visible = true;
      //     pnlFCC.Visible = true;
      pnlEmailButtons.Visible = false;

    }







  }

}