﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using WimcoBaseLogic;
using WimcoBaseLogic.BusinessLogic;

using System.Net.Mail;
namespace GWCustom
{
  public partial class sendemail : System.Web.UI.Page
  {
    public string TemplateKind = "3"; // 3 is Generic
    public int DateRule = 1; // This gets handled by the templateID or TemplateKind combined with the user. 
    public String FWDUser = "";
    protected void Page_Load(object sender, EventArgs e)
    {
      HttpContext.Current.Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
      HttpContext.Current.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
      HttpContext.Current.Response.AppendHeader("Expires", "0"); // Proxies.

      //Session["ClientID"] = clientID;
      //Session["EmailToSearch"] = email;

      if (!IsPostBack)
      {



        try
        {
          Session.Remove("EmailObjectOriginal");
        }
        catch { }
        MailMessage mm = new MailMessage();
        EmailControl ec = ((EmailControl)Page.FindControl("wimMail"));
        if (Session["TemplateKindID"] != null)
        {
          TemplateKind = Session["TemplateKindID"].ToString();

        }
        else { TemplateKind = "3"; }
        if (TemplateKind == "6")
        {
          String em = Session["EmailSendToAddress"].ToString();
          try
          {

            FWDUser = WimcoBaseLogic.BaseLogic.AdHocSeek(String.Format("Select UserID From MagUser Where LOWER(EmailAddress) = '{0}' and MagUser.IsEnabled = 'Y'", em), "UserID");

          }
          catch (Exception err) { }

        }

        if (Request.Url.AbsoluteUri.ToLower().Contains("new") || Session["EmailObject"] == null)
        { // empty email body




          if (Session["LeadID"] == null)
          {
            mm = BuildEmptyEmail();
          }
          else
          {
            mm = BuildLeadEmail();

          }

          Session["EmailObject"] = mm;
        }

        if (Session["EmailObject"] != null)
        {
          mm = ((MailMessage)Session["EmailObject"]);

        }
        Session["EmailObjectOriginal"] = mm;


        string strTo = "";
        string FromAddress = "";
        string FromName = "";
        string strCC = "";

        foreach (MailAddress ma in mm.To)
        {
          strTo += ma.Address + ",";

        }
        foreach (MailAddress ma in mm.CC)
        {
          strCC += ma.Address + ",";

        }
        wimMail.EmailTo = strTo;
        wimMail.EmailFrom = mm.From.Address;
        wimMail.EmailName = mm.From.DisplayName;
        wimMail.EmailSubject = mm.Subject;
        wimMail.EmailCC = strCC;
        wimMail.EmailBody = HttpUtility.HtmlEncode(mm.Body);
        wimMail.EmailOriginal = HttpUtility.HtmlEncode(mm.Body);
        wimMail.TemplateKindID = TemplateKind;
        ConstructFCC();//Load the FCC Control
      }

    }
    private String GetNextFCC(DateTime FCCDate, bool HasLead)
    {

      String sClientID = Session["ClientID"].ToString();
      String s = "";
      if (HasLead)
      {
        s = BaseLogic.AdHocSeek(String.Format("Select Top 1 CallbackID from Callback cb Join MagOrder mo on cb.BusinessID = mo.OrderID Where mo.ClientID = {0} and CAST(cb.CreateDate As DATE) >= '{1}' AS DATE) and mo.IsEnabled = 'Y' and cb.IsEnabled ='Y'  order by cb.CallbackDate", sClientID, FCCDate), "CallbackID");

      }
      else
      {
        s = BaseLogic.AdHocSeek(String.Format("Select Top 1 CallbackID from Callback cb Join MagOrder mo on cb.BusinessID = mo.OrderID Where mo.ClientID = {0} and CAST(cb.CallbackDate As DATE) >= '{1}' AS DATE) and mo.IsEnabled = 'Y' and cb.IsEnabled ='Y'  order by cb.CallbackDate", sClientID, FCCDate), "CallbackID");
      }
      if (s == null)
      {
        s = "";
      }



      return s;

    }
    private void ConstructFCC() //Fill in the FCC Form if coming off the Dashboard
    {
      //Session["DID"] = destinationID;
      //Session["TemplateKindID"] = RCCTemplate;
      //Session["FCCContactSourceID"] = 700;
      //Session["FCCDateInitial"] = dateTo;

      String DID;
      int FCCCSID;
      String sCallBackNumber = "";
      DateTime FCCDate = DateTime.Today;
      bool HasLead = false;
      if (Session["LeadID"] != null) //Lead Requires the FCC to have been created after the Lead
      {
        if (Session["LeadID"].ToString() != "")
        {
          int LeadID = Int32.Parse(Session["LeadID"].ToString());
          String AuthKey = BaseLogic.GetAuthKey();
          WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic biz = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();
          WimcoBaseLogic.BusinessLogic.Leads l = biz.leads_GetLead(AuthKey, LeadID);
          if (l.RequestDate != null)
          {
            FCCDate = (DateTime)l.RequestDate;
            HasLead = true;
          }
        }
      }
      sCallBackNumber = GetNextFCC(FCCDate, HasLead);
      if (sCallBackNumber != "")
      {
        EditFCC.WimcoClientID = Int32.Parse(Session["ClientID"].ToString());
        EditFCC.CallBackID = Int32.Parse(sCallBackNumber);
        EditFCC.DataBind();
        EditFCC.loadFCC();

        EditFCC.Visible = true;
        wimAddFCC.Visible = false;
        EditFCC.FindControl("btnCancel").Visible = false;
      }
      else
      {
        EditFCC.Visible = false;
        ((Button)wimMail.FindControl("btnSend")).Enabled = false;

      }


      if (Session["DID"] != null)
      {
        DID = Session["DID"].ToString();
        wimAddFCC.FCCDID = DID;
      }
      if (Session["FCCContactSourceID"] != null)
      {
        wimAddFCC.ContactSourceID = Session["FCCContactSourceID"].ToString();

      }

      try
      {
        if (TemplateKind == "6")
        {

          wimAddFCC.WimcoAgentID = Int32.Parse(FWDUser);
          wimAddFCC.ContactSourceID = "1122";
        }


        wimAddFCC.purpose = "addFCC";

        wimAddFCC.DataBind();
        if (HasLead)
        {
          ((DropDownList)wimAddFCC.FindControl("ddlNewFCCType")).SelectedValue = "983";
        }
        wimAddFCC.WimcoClientID = Int32.Parse(Session["ClientID"].ToString());

        ((System.Web.UI.WebControls.Button)wimAddFCC.FindControl("Button2")).Visible = false;
      }
      catch (Exception err) { }

    }
    private MailMessage BuildLeadEmail() //If the user has an open lead to the user they are contacting, this will prepopulate the email body with the lead message
    {
      String LeadID = Session["LeadID"].ToString();
      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      MailMessage m = new MailMessage();
      string em = "";

      String AuthKey = BaseLogic.GetAuthKey();
      Dashboard.BusinessLogic.WimcoBusinessLogic biz = new Dashboard.BusinessLogic.WimcoBusinessLogic();
      Dashboard.BusinessLogic.Leads l = biz.leads_GetLead(AuthKey, Int32.Parse(LeadID));
      //VillaOwner.BusinessLogic.Leads l = VillaOwner.BaseLogic.businessLogic.leads_GetLead(AuthKey, Int32.Parse(LeadID));
      if (l != null)
      {
        if (l.MagUserID == ud.UserID || ud.Manager) //Only open for the correct user or a manager
        {
          m.To.Add(new MailAddress(l.ClientEmailAddress, l.ClientEmailAddress));
          m.From = new MailAddress(ud.EmailAddress, ud.FullName);
          StringBuilder strbEmail = null;
          string filterString = string.Empty;

          DateTime dateFrom;
          DateTime dateTo;
          TimeSpan span;
          int numberOfDay;
          string travelDate;
          string destination = "";
          string villaName = "";
          string contactSource;
          string comment;
          string clientName = string.Empty;
          string clientEmail = string.Empty;
          string phoneNumber = string.Empty;
          string requestDate = string.Empty;
          string firstDestination = string.Empty;
          string secondDestination = string.Empty;
          string numberOfBedroom = string.Empty;
          string webInqContactSource = string.Empty;
          string webInqContactSourceCode = string.Empty;
          decimal minimumPrice = 0.0m;
          decimal maximumPrice = 0.0m;
          //string travelWithChildren = string.Empty;
          //string requirePool = string.Empty;
          bool travelWithChildren = false;
          bool requirePool = false;

          strbEmail = new StringBuilder();



          //userEmail = r.Field<string>("ClientEMailAddress");
          if (l.DateFrom != null || l.DateTo != null)
          {
            dateFrom = (DateTime)l.DateFrom;
            dateTo = (DateTime)l.DateTo;
            span = dateTo - dateFrom;
            numberOfDay = span.Days;
            if (numberOfDay == 0)
            {
              numberOfDay = 1;
            }
            travelDate = String.Format("{0:MMM dd, yyyy} to {1:MMM dd, yyyy} ({2})", dateFrom, dateTo, numberOfDay);
          }
          else
          {
            travelDate = string.Empty;
          }
          if (l.RequestDate == null)
          {
            requestDate = string.Empty;
          }
          else
          {
            requestDate = l.RequestDate.ToString();
          }

          destination = BaseLogic.AdHocSeek("Select LocaleName from Locale Where LocaleID = " + l.DestinationID.ToString(), "LocaleName");//
          if (l.ProductBaseID != null)
          {
            villaName = Dashboard.BaseLogic.GetVillaNameFromProductBaseID((int)l.ProductBaseID);
          }
          contactSource = BaseLogic.AdHocSeek("Select Name from ContactSource Where ContactSourceID = " + l.ClientContactSourceID.ToString(), "Name"); ;
          comment = l.Comment;// r.Field<string>("Comment");
          clientName = l.FirstName + " " + l.LastName; // $"{r.Field<string>("FirstName")} {r.Field<string>("LastName")}";
          clientEmail = l.ClientEmailAddress; // r.Field<string>("ClientEMailAddress");
          phoneNumber = l.PhoneNumber;// r.Field<string>("PhoneNumber");
          webInqContactSource = BaseLogic.AdHocSeek("Select Name from ContactSource Where ContactSourceID = " + l.WebInqContactSourceID.ToString(), "Name");  //r.Field<string>("WebInqContactSourceCodeName");
          webInqContactSourceCode = l.WebInqContactSourceID.ToString(); // r.Field<string>("webInqContactSourceCode");
                                                                        // if a request is a Web Resv, change background color to the salmon color.
          if (webInqContactSourceCode == "REZ2_ED")
          {
            strbEmail.Append("<table align='center' width='400' style='border: 1px solid #d9d9d9; background-color: #FEA07A;'><tr><td style='padding: 20px'>");
          }
          else
          {
            strbEmail.Append("<table align='center' width='400' style='border: 1px solid #d9d9d9;'><tr><td style='padding: 20px'>");
          }
          strbEmail.Append("");
          strbEmail.Append("<p style='font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #4071CF'><strong>Request Information</strong></p>");
          strbEmail.Append("<p style='font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #505050; line-height: 18px'>");
          strbEmail.Append(String.Format("Name:<strong> {0}</strong><br />", clientName));
          if (!string.IsNullOrEmpty(clientEmail))
          {
            strbEmail.Append(String.Format("Email:<strong> {0}</strong><br />", clientEmail));
          }
          if (!string.IsNullOrEmpty(phoneNumber))
          {
            strbEmail.Append(String.Format("Phone:<strong> {0}</strong><br />", phoneNumber));
          }
          if (!string.IsNullOrEmpty(requestDate))
          {
            strbEmail.Append(String.Format("Request Date:<strong> {0}</strong><br />", requestDate));
          }
          if (!string.IsNullOrEmpty(villaName))
          {
            strbEmail.Append(String.Format("Villa Name:<strong> {0}</strong><br />", villaName));
          }
          if (!string.IsNullOrEmpty(destination))
          {
            strbEmail.Append(String.Format("Destination:<strong> {0}</strong><br />", destination));
          }
          //if (!string.IsNullOrEmpty(secondDestination))
          //{
          //  strbEmail.Append(String.Format("Second Choice:<strong> {secondDestination}</strong><br />"));
          //}

          if (!string.IsNullOrEmpty(travelDate))
          {
            strbEmail.Append(String.Format("Dates:<strong> {0}</strong><br />", travelDate));
          }

          if (!string.IsNullOrEmpty(numberOfBedroom))
          {
            strbEmail.Append(String.Format("Number Of Bedrooms:<strong> {0}</strong><br />", numberOfBedroom));
          }
          if (minimumPrice > 0 || maximumPrice > 0)
          {
            strbEmail.Append(String.Format("Price Range:<strong> {0:C2} to {1:C2}</strong><br />", minimumPrice, maximumPrice));
          }
          if (travelWithChildren == true)
          {
            strbEmail.Append("Traveling with Children:<strong> Yes</strong><br />");
          }
          if (requirePool == true)
          {
            strbEmail.Append("Must Have a Pool:<strong> Yes</strong><br />");
          }
          if (!string.IsNullOrEmpty(comment))
          {
            strbEmail.Append(String.Format("Comments:<strong> {0}</strong><br />", comment));
          }
          if (!string.IsNullOrEmpty(contactSource))
          {
            strbEmail.Append(String.Format("How did you hear of WIMCO:<strong> {0}</strong><br />", contactSource));
          }
          // ML - Requested by Ann Marie Caye not to include
          /*
          if (!string.IsNullOrEmpty(webInqContactSource))
          {
            strbEmail.Append(string.Format("Inquiry Source:<strong> {0}</strong></p>", webInqContactSource));
          }
          */

          strbEmail.Append("</p></td></tr></table>");
          String DateString = "";
          if (l.DateFrom != null)
          {
            DateString = String.Format("{0:MMMM yyyy}", ((DateTime)l.DateFrom));
          }
          m.Subject = "Re: " + DateString + " " + destination + " Requested by " + clientName;
          m.Body = "<br><br><w99><br></w99>" + strbEmail.ToString();

          wimAddFCC.ContactSourceID = l.WebInqContactSourceID.ToString();

          return m;
        }
        else
        {
          return BuildEmptyEmail();
        }

      }
      else
      {

        return BuildEmptyEmail();
      }





    }
    private MailMessage BuildEmptyEmail()
    {
      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      MailMessage m = new MailMessage();
      string em = "";
      int clientID = 0;

      if (Session["ClientID"] != null)
      {
        try { clientID = Int32.Parse(Session["ClientID"].ToString()); }
        catch { }
      }
      m.From = new MailAddress(ud.EmailAddress, ud.FullName);
      if (Session["EmailSendToAddress"] != null)
      {
        em = Session["EmailSendToAddress"].ToString();

      }
      else
      {
        if (clientID != 0)
        {
          em = BaseLogic.clientemail_GetPrimaryEmail(clientID);

        }
      }
      m.To.Add(new MailAddress(em, em));
      if (Session["TemplateKindID"] != null)
      {
        TemplateKind = Session["TemplateKindID"].ToString();
      }
      switch (TemplateKind)
      {
        case "2":
        case "6":
        case "7":    //FCC, Client Recovery or FWD
          string[] FccContent = GetFCCDefault(clientID); //pull last 2 emails in
          m.Body = FccContent[0];
          m.Subject = FccContent[1];
          break;

        default: //rccb or default
          m.Body = "";
          m.Subject = "";
          break;


      }
      return m;
    }
    public string[] GetFCCDefault(int ClientID)
    {
      string body = "";
      string subject = "";
      WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS celFCC = new WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS();
      String[] emails = WimcoBaseLogic.BaseLogic.clientemail_GetEmailAddressArray(ClientID); //Get all client email addresses

      GW_IndexSearchDS GWDS = WimcoBaseLogic.BaseLogic.SearchByEmailAddresses(emails); //Get dataset of emails
      if (GWDS.Tables.Count > 0) //If emails exist
      {
        String EmailText = "";
        string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
        String html = "";
        WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();

        bizLogic.Timeout = 5000;
        DataTable dtEmails = GWDS.Tables[0];
        for (int i = 0; i < dtEmails.Rows.Count; i++) //Loop through the emails, but stop at 2.
        {
          if (i >= 2)
          {
            break;
          }
          GWMailObject mail = new GWMailObject();
          String GroupWiseID = dtEmails.Rows[i]["GroupwiseID"].ToString();

          String html2 = "";
          mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID); //Get the email from the list of emails
          if (mail.StatusCode > -1) //If the email can be found
          {
            if (i == 0)
            {
              subject = "Re: " + mail.Subject; //Set subject for email to be a reply to the first email
            }
            if (mail.MessageContainsHTML) //Get an HTML email if it exists
            {
              if (!mail.MessageInHTML.Contains("mail has been deleted from Server") && !mail.MessageInHTML.Contains("Can't reach Groupwise to read this email"))
              {
                //if (html2 == "")
                //{
                try //Add a separator above the email
                {
                  html2 += "<br/> >>> " + mail.FromName + " &lt;" + mail.FromEmail + "&gt; ";
                  html2 = html2 + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + " >>> <br/>";
                }
                catch { }

                //}




                html2 = html2 + "<div class='email'><jk></jk>"; //I don't really remember what this does, but I would guess it handles some kind of meta data.



                html2 = html2 + "</div>";
                html2 = html2 + mail.MessageInHTML.Replace("!important", "").Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href"); //Clean out tracking scripts that break the form.
                EmailText += html2;
              }
            }
            else
            {
              if (html2 == "") //Non-HTML emails
              {
                try
                {
                  html2 += "<br/> >>> " + mail.FromName + " &lt;" + mail.FromEmail + "&gt; "; //Build separator line
                  html2 = html2 + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + " >>> <br/>";
                }
                catch { }

              }
              html2 = html2 + "<div class='email'><jk></jk>"; //Still not sure.  


              html2 = html2 + "</div>";
              //Wrap in a pretag so it isn't unreadable.  Remove tracking scripts that break content.
              html2 = html2 + "<pre>" + mail.MessageContent.Replace("!important", "").Replace("text/javascript", "text/noscript").Replace("<base href", "<xbase href") + "</pre>";
              EmailText += html2;
            }
          }


        }
        body = EmailText;
      }

      string[] array = new string[2]; //Send the body and subject back to the main method in a string array
      array[0] = body;
      array[1] = subject;
      return array;
    }
  }
}
