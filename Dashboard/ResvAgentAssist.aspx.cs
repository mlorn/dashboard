﻿using DataAccessLayer;
using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace Dashboard
{
  public partial class ResvAgentAssist : System.Web.UI.Page
  {
    const int resvAgentGroupID = 186;
    DashboardData dashboard;
    DataSet ds;
    int salesAssistID;

    protected void Page_Load(object sender, EventArgs e)
    {

      dashboard = Session["DashboardData"] as DashboardData;
      if (dashboard == null)
      {
        dashboard = new DashboardData();
        using (MagnumDataContext magnumDb = new MagnumDataContext())
        {
          try
          {
            salesAssistID = Convert.ToInt32(Session["UserID"]);
          }
          catch
          {
            salesAssistID = 0;
          }

          if (salesAssistID > 0)
          {
            Session["SalesAssistID"] = salesAssistID;
            dashboard.FillResvAgentAssist(magnumDb, salesAssistID);
            ds = dashboard.DashboardDataset;
            Session["DataSet"] = ds;
          }
        }
      }

      grdResvAgentForAssist.DataSource = dashboard.DashboardDataset.tblRAForAssistant;
      grdResvAgentForAssist.DataBind();
    }

    protected void lnkbtnSelectResvAgent_Click(object sender, EventArgs e)
    {
      int userID;
      string userName;
      string FullResvAgentName;
      int grdRowIndex;

      LinkButton lnkbtnResvAgent = (sender as LinkButton);
      GridViewRow grdRow = (GridViewRow)lnkbtnResvAgent.NamingContainer;
      grdRowIndex = Convert.ToInt32(grdRow.RowIndex);

      userID = Convert.ToInt32(lnkbtnResvAgent.CommandArgument);
      userName = grdResvAgentForAssist.Rows[grdRowIndex].Cells[1].Text;
      FullResvAgentName = grdResvAgentForAssist.Rows[grdRowIndex].Cells[2].Text;

      Session["UserID"] = userID;
      Session["GroupID"] = resvAgentGroupID;
      Session["UserName"] = userName;
      Session["FullName"] = FullResvAgentName;
      Response.Redirect("Default.aspx");
    }
  }

}