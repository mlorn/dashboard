﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Default.aspx.cs" Inherits="Dashboard.Default" %>
<%@ Register src="GWWimcoEmailGrid.ascx" tagname="GWWimcoEmailGrid" tagprefix="wimcoCustom" %><%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head runat="server">
    <title>Dashboard</title>

  <link href="CSS/main.css" rel="stylesheet" />
  <link rel="stylesheet" href="CSS/PrintStyle.css" type="text/css" media="print" />

  <!-- <script src="Scripts/jquery-1.8.2.min.js"></script> -->



  <link rel="stylesheet" href="js/css/jquery-ui-1.8.21.css" />
  <script src="js/jquery-1.8.2.js"></script>
	<script src="js/ui/jquery.ui.core.js"></script>
	<script src="js/ui/jquery-ui-1.8.24.min.js"></script>
  <script src="js/ui/jquery.ui.button.js"></script>
	<script src="js/ui/jquery.ui.widget.js"></script>
	<script src="js/ui/jquery.ui.datepicker.js"></script>

  <script src="Scripts/default.js"></script>

  <script async="async" src="Scripts/EmailPopup.js"></script>


  <script>
    function pageLoad() {
      $(function () {
        $("#dtStartDate").datepicker({
          dateFormat: "M d, yy",
          showOn: "button",
          buttonImage: "../images/cal.png",
          buttonImageOnly: true
        });
      });
      $(function () {
        $("#dtEndDate").datepicker({
          dateFormat: "M d, yy",
          showOn: "button",
          buttonImage: "../images/cal.png",
          buttonImageOnly: true
        });
      });

      $(function () {
        $("#dtStartFCCDate").datepicker({
          dateFormat: "M d, yy",
          showOn: "button",
            buttonImage: "../images/cal.png",
          buttonImageOnly: true
        });
      });
      $(function () {
        $("#dtEndFCCDate").datepicker({
          dateFormat: "M d, yy",
          showOn: "button",
            buttonImage: "../images/cal.png",
          buttonImageOnly: true
        });
      });

      $(function () {
        $("#dtStartActivityDate").datepicker({
          dateFormat: "M d, yy",
          showOn: "button",
            buttonImage: "../images/cal.png",
          buttonImageOnly: true
        });
      });
      $(function () {
        $("#dtEndActivityDate").datepicker({
          dateFormat: "M d, yy",
          showOn: "button",
            buttonImage: "../images/cal.png",
          buttonImageOnly: true
        });
      });

      $(function () {
        var RowID = $('#hdnClientID').val();

        if (RowID != "0") {

          $('#<%=grdFCCRA.ClientID%> tr[id=' + RowID + ']').css({ "background-color": "Red", "color": "White"});

        }

        $('#<%=grdFCCRA.ClientID%> tr[id]').click(function () {

          $('#<%=grdFCCRA.ClientID%> tr[id]').css({ "background-color": "White", "color": "Black"});

          $(this).css({ "background-color": "Red", "color": "White"});
          $('#hdnClientID').val($(this).attr("id"));
        });

        $('#<%=grdFCCRA.ClientID%> tr[id]').mouseover(function () {

            $(this).css({ cursor: "hand", cursor: "pointer" });

          });
      });

      setInterval("KeepSessionAlive()", 60000);
    }

    function KeepSessionAlive() {

      url = "/keepalive.ashx?";
      var xmlHttp = new XMLHttpRequest();
      xmlHttp.open("GET", url, true);
      xmlHttp.send();
    }

	</script>
    <style type="text/css">
      .auto-style1 {
        margin-left: 0px;
      }

      .auto-style2 {
        width: 275px;
      }

      .auto-style3 {
        width: 98px;
      }

      .auto-style5 {
        width: 60px;
      }

      .auto-style6 {
        width: 150px;
      }

      .auto-style7 {
        width: 157px;
      }
      .emailModal
    {
    	position:fixed;
    	z-index:30;
    	background-color:#FFF;
    	width:500px;
    	height:500px;
    	border:gray solid 1px;
    	padding:5px;
    	left:500px;
    	top:0px;
     	
    	}
    	.emailModal > #theEmail
    	{
    		width:500px;
    		height:470px;
    		overflow:scroll;
    		}
      .hidden {
      
      display:none;
      }
        .auto-style9 {
            width: 275px;
            height: 30px;
        }
        .auto-style10 {
            width: 98px;
            height: 30px;
        }
        .auto-style11 {
            width: 157px;
            height: 30px;
        }
        .auto-style12 {
            width: 60px;
            height: 30px;
        }
        .auto-style13 {
            width: 150px;
            height: 30px;
        }
        .auto-style14 {
            height: 30px;
        }
    </style>
    </head>
  <body>
      <form id="form1" runat="server">  
          <%-- Admin User --%>            
          <div>
            <asp:ToolkitScriptManager runat="server" ID="scrpMgr"></asp:ToolkitScriptManager>   
   
            <%-- Reservation Agent --%>
            <asp:hiddenfield id="hfScrollPosition" runat="server"/>

            <table style="width:50%;">
              <tr>
                <td>
                  <asp:Label ID="lblLoggedInAs" runat="server" Text="Logging in as:"></asp:Label>
                </td>

                <td>
                  <asp:Label ID="lblSelectResvAgent" runat="server" Text="Select Resv Agent:"></asp:Label>
                </td>
                <td>
                  <asp:DropDownList ID="ddlSelectResvAgent" runat="server" AutoPostBack="True" DataTextField="FullName" DataValueField="UserName" OnSelectedIndexChanged="ddlSelectResvAgent_SelectedIndexChanged">
                  </asp:DropDownList>
                </td>
              </tr>
       
            </table>

            

            <asp:GridView ID="grdRA" runat="server" AutoGenerateColumns="False" CssClass="Grid" DataKeyNames="UserID" Caption="Reservation Agent"
                OnRowDataBound="grdRA_RowDataBound" Width="100%" EmptyDataText="No Data Available" ShowFooter="True" EnableModelValidation="True" OnSelectedIndexChanged="grdRA_SelectedIndexChanged">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnAllGrids" runat="server" ImageUrl="~/images/plus.gif"
                                CommandArgument="Show" OnClick="showHideAllGrids" />
                            <asp:Panel ID="pnlAllGrids" runat="server" Visible="false" Style="position: relative">
                <%-- FinalPayment --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdFinalAndNonDepositAdmin" runat="server" AutoGenerateColumns="False"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFinalAndNonDeposit_PageIndexChanging" OnSorting="grdFinalAndNonDeposit_Sorting"
                    CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Non-Deposits/Final Payments" Width="100%">
                    <Columns>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnFinalAndNonDepositAdminName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                            Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="lnkbtnFinalAndNonDepositAdminEmail" runat="server" NavigateUrl='<%# $"mailto:{Eval("EMailAddress")}" %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnFinalAndNonDepositAdminResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                            Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
                      <asp:BoundField DataField="TotalCharge" SortExpression="TotalCharge" HeaderText="Total Charge" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      <asp:BoundField DataField="TotalReceived" SortExpression="TotalReceived" HeaderText="Received" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      <asp:BoundField DataField="BalanceDue" SortExpression="BalanceDue" HeaderText="Balance Due" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      <asp:BoundField DataField="PaymentDueDate" SortExpression="PaymentDueDate" HeaderText="Payment Due" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="NumberOfDayDue" SortExpression="NumberOfDayDue" HeaderText="Days Due" />
                    </Columns>
                  </asp:GridView>
                </div>
                <%-- Returning Client Callback --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdReturnClientCallbackAdmin" runat="server" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdReturnClientCallback_PageIndexChanging" OnSorting="grdReturnClientCallback_Sorting"
                    CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Return Client Callback" Width="100%">
                    <Columns>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnReturnClientCallbackAdminName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                            Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="hlReturnClientCallbackAdminEmail" runat="server" NavigateUrl='<%# $"mailto:{Eval("EMailAddress")}" %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnReturnClientCallbackAdminResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                            Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
                    </Columns>
                  </asp:GridView>
                </div>
                <%-- Future Client Callback --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdFCCAdmin" runat="server" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFCC_PageIndexChanging" OnSorting="grdFCC_Sorting" OnRowDataBound="grdFCCRA_RowDataBound"
                    CssClass="AltRow TableHeader" DataKeyNames="ClientID" Caption="Future Client Callback" Width="100%">
                    <Columns>
                      <%--
              <asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton ID="imgbtnResvHistory2" runat="server" OnClick="showHideGrdResvHistory2" ImageUrl="~/images/plus.gif"
                    CommandArgument="Show" />
                  <asp:Panel ID="pnlResvHistory2" runat="server" Visible="false" Style="position: relative">
                    <asp:GridView ID="grdResvHistory2" runat="server" AutoGenerateColumns="false" PageSize="10"
                      AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdResvHistory2_PageIndexChanging" OnSorting="grdResvHistory_Sorting"
                      CssClass="Nested_ChildGrid" Caption="Reservation">
                      <Columns>
                        <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                          <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnResvHistory2Itinerary" runat="server" OnClick="lnkbtnItinerary_Click"
                              Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                          </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                          <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnResvHistory2ResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                              Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                          </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                        <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                        <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                          <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FullName" SortExpression="FullName" HeaderText="Resv Agent" />
                      </Columns>
                    </asp:GridView>
                  </asp:Panel>
                </ItemTemplate>
              </asp:TemplateField>
                      --%>
                      <%--
                      <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="Order No." />
                      --%>
                      <asp:TemplateField HeaderText="Print" SortExpression="Print">
                        <ItemTemplate>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnFCCAdminName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                            Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="hlFCCAdminmail" runat="server" NavigateUrl='<%# $"mailto:{Eval("EMailAddress")}" %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="Date Entered" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="CallbackDate" SortExpression="CallbackDate" HeaderText="Callback Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
                      <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
                      <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="Remarks" />
                      <asp:BoundField DataField="Color" SortExpression="Color" HeaderText="Color" Visible="false" />
                    </Columns>
                  </asp:GridView>
                </div>
                <%-- Inq and Waitlist --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdInqAndWaitListAdmin" runat="server" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdInqAndWaitList_PageIndexChanging" OnSorting="grdInqAndWaitList_Sorting"
                    CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Inquiries and Waitlists" Width="100%">
                    <Columns>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnInqAndWaitListAdminName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                            Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="lnkbtnInqAndWaitListAdminEmail" runat="server" NavigateUrl='<%# $"mailto:{Eval("EMailAddress")}" %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <%--
                      <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                            Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      --%>
                      <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnInqAndWaitListAdminResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                            Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
                      <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                      <%--
                      <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      --%>
                    </Columns>
                  </asp:GridView>
                </div>
                <%-- In House and Arrival (next 10 days) --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdInHouseAndArrivalAdmin" runat="server" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdInHouseAndArrival_PageIndexChanging" OnSorting="grdInHouseAndArrival_Sorting"
                    CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="In House and Arrivals" Width="100%">
                    <Columns>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnInHouseAndArrivalAdminName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                            Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="hlInHouseAndArrivalAdminEmail" runat="server" NavigateUrl='<%# $"mailto:{Eval("EMailAddress")}" %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <%--
                      <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                            Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      --%>
                      <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnInHouseAndArrivalAdminResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                            Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'>
                          </asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
                      <%-- 
                      <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                      <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      --%>
                      <asp:BoundField DataField="Car" SortExpression="Car" HeaderText="Car" />
                      <asp:BoundField DataField="Arrival" SortExpression="Arrival" HeaderText="Arr" />
                      <asp:BoundField DataField="AdditionalPassenger" SortExpression="AdditionalPassenger" HeaderText="Add Pax" />
                      <asp:BoundField DataField="CellularPhoneNumber" SortExpression="CellularPhoneNumber" HeaderText="Cell #" />
                    </Columns>
                  </asp:GridView>
                </div>
                <%-- Reservation (BKD) --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdReservationAdmin" runat="server" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdReservation_PageIndexChanging" OnSorting="grdReservation_Sorting"
                    CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Reservations" Width="100%">
                    <Columns>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnReservationAdminName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                            Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="hlReservationAdminEmail" runat="server" NavigateUrl='<%# $"mailto:{Eval("EMailAddress")}" %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <%--
                      <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                            Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      --%>
                      <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnReservationResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                            Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
                      <%--
                      <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                      <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      --%>
                    </Columns>
                  </asp:GridView>
                </div>
              </asp:Panel>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Clients belong to RA --%>
                    <asp:TemplateField HeaderText="Res Agent">
                        <FooterTemplate>
                          <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Total"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnRAClient" runat="server" OnClick="showHideRAClient"
                                CommandArgument="Show" Text='<%#Eval("FullName") %>'></asp:LinkButton>
                            <asp:Panel ID="pnlRAClient" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdRAClient" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdClient_PageIndexChanging" OnSorting="grdClient_Sorting"
                                    CssClass="ChildGrid" DataKeyNames="ClientID,EMailAddress" Caption="Client" Width="100%">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgRAClientGrids" runat="server" OnClick="showHideRAClientGrids" ImageUrl="~/images/plus.gif"
                                                    CommandArgument="Show" />
                                                <asp:Panel ID="pnlRAClientGrids" runat="server" Visible="false" Style="position: relative; top: 0px; left: 0px;">
                                                    <%-- Final Payment --%>
                                                    <div style="overflow-y: auto; height: auto; min-height: 1px;"> 
                                                    <asp:GridView ID="grdRAClntFinalPayment2" runat="server" AutoGenerateColumns="false" PageSize="10"
                                                        AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFinalPayment2_PageIndexChanging" OnSorting="grdFinalPayment_Sorting"
                                                        CssClass="Nested_ChildGrid" Caption="Final Payment" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber[" >
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkbtnRAClntFinalPaymentResvNo" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                                        Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                                            <asp:BoundField DataField="TotalCharge" SortExpression="TotalCharge" HeaderText="Total Charge" DataFormatString="{0:n}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FinalDueAmt" SortExpression="FinalDueAmt" HeaderText="Balance Due" DataFormatString="{0:n}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FinalDueDay" SortExpression="FinalDueDay" HeaderText="Days Late">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FinalDueDate" SortExpression="FinalDueDate" HeaderText="Payment Due" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="Resv Agent" />
                                                        </Columns>
                                                    </asp:GridView>
                                                    </div>
                                                    <%-- Non Deposit --%>
                                                    <div style="overflow-y: auto; height: auto; min-height: 1px;"> 
                                                    <asp:GridView ID="grdRAClntNonDeposit2" runat="server" AutoGenerateColumns="false" PageSize="10"
                                                        AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdNonDeposit2_PageIndexChanging" OnSorting="grdNonDeposit_Sorting"
                                                        CssClass="Nested_ChildGrid" DataKeyNames="ClientID" Caption="Non-Deposit" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkbtnRAClntNonDeposit2ResvNo" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                                        Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                                            <asp:BoundField DataField="DepositAmt" SortExpression="DepositAmt" HeaderText="Deposit Amt" DataFormatString="{0:n}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DepositDueAmt" SortExpression="DepositDueAmt" HeaderText="Dep. Due Amt" DataFormatString="{0:n}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="NumberOfDayLate" SortExpression="NumberOfDayLate" HeaderText="Days Late">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DepositDueDate" SortExpression="DepositDueDate" HeaderText="Deposit Due Date" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="Resv Agent" />
                                                        </Columns>
                                                    </asp:GridView>
                                                    </div>
                                                    <%-- Returning Client Callback --%>
                                                    <div style="overflow-y: auto; height: auto; min-height: 1px;"> 
                                                    <asp:GridView ID="grdRAClntReturnClientCallback2" runat="server" AutoGenerateColumns="false" PageSize="10"
                                                        AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdReturnClientCallback2_PageIndexChanging" OnSorting="grdReturnClientCallback_Sorting"
                                                        CssClass="Nested_ChildGrid" DataKeyNames="ClientID" Caption="Return Client Callback" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber" >
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkbtnRAClntClientCallback2ResvNo" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                                        Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                                            <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="Resv Agent" DataFormatString="{0:n}" />
                                                            <asp:BoundField DataField="TotalReceived" SortExpression="TotalReceived" HeaderText="Total Received" DataFormatString="{0:n}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    </div>
                                                    <%-- Client Lead --%>
                                                    <div style="overflow-y: auto; height: auto; min-height: 1px;"> 
                                                    <asp:GridView ID="grdRAClntLead2" runat="server" AutoGenerateColumns="false" PageSize="10"
                                                        AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdLead2_PageIndexChanging" OnSorting="grdLead_Sorting"
                                                        CssClass="Nested_ChildGrid" DataKeyNames="ClientID" Caption="Lead" Width="100%">
                                                        <Columns>
                                                            <asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="Date Entered" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="Order No." />
                                                            <asp:BoundField DataField="OrderContactSource" SortExpression="OrderContactSource" HeaderText="Lead Source" />
                                                            <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
                                                            <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
                                                            <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="Remarks" />
                                                        </Columns>
                                                    </asp:GridView>
                                                    </div>
                                                    <%-- Future Client Callback --%>
                                                    <div style="overflow-y: auto; height: auto; min-height: 1px;"> 
                                                    <asp:GridView ID="grdRAClntFCC2" runat="server" AutoGenerateColumns="false" PageSize="10"
                                                        AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFCC2_PageIndexChanging" OnSorting="grdFCC_Sorting"
                                                        CssClass="Nested_ChildGrid" DataKeyNames="ClientID" Caption="Future Client Callback" Width="100%">
                                                        <Columns>
                                                            <asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="Date Entered" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="CallbackDate" SortExpression="CallbackDate" HeaderText="Callback Date" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
                                                            <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
                                                            <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="Remarks" />
                                                            <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="Resv Agent" />
                                                        </Columns>
                                                    </asp:GridView>
                                                    </div>
                                                    <%-- Client Bookings history --%>
                                                    <div style="overflow-y: auto; height: auto; min-height: 1px;"> 
                                                    <asp:GridView ID="grdRAClntResvHistory2" runat="server" AutoGenerateColumns="false" PageSize="10"
                                                        AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdResvHistory2_PageIndexChanging" OnSorting="grdResvHistory_Sorting"
                                                        CssClass="Nested_ChildGrid" DataKeyNames="ClientID" Caption="Reservation" Width="100%">
                                                        <Columns>
                                                           <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkbtnRAClntResvHistory2Itinerary" runat="server" OnClick="lnkbtnItinerary_Click" 
                                                                        Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                               <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber" >
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkbtnRAClntResvHistory2ResvNo" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                                        Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                                            <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                                            <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                                                            <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="Resv Agent" />

                                                        </Columns>
                                                    </asp:GridView>
                                                    </div>
                                                    <%-- GroupWise Custom Control --%>
                                                    <asp:UpdateProgress ID="UpdateProgressGW" runat="Server" AssociatedUpdatePanelID="pnlUpdateGW">
                                                    <ProgressTemplate >
                                                        Please wait ...
                                                    </ProgressTemplate>
                                                    </asp:UpdateProgress>        
                                                    <asp:UpdatePanel ID="pnlUpdateGW" runat="server">        
                                                        <ContentTemplate>        
                                                            <wimcoCustom:GWWimcoEmailGrid ID="gwGrid" runat="server" CssClass="Nested_ChildGrid" Width="100%"> 
                                                            </wimcoCustom:GWWimcoEmailGrid>                      
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel> 
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnRAClntResvHistory2ClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress" >
                                            <ItemTemplate>
                                                <asp:HyperLink ID="ClientEmailAddress1" runat="server" NavigateUrl='<%# $"mailto:{Eval("EMailAddress")}?body={Eval("DetailInfo")}" %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FinalDueAmt" SortExpression="FinalDueAmt" HeaderText="Final due Amt" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DepositDueAmt" SortExpression="DepositDueAmt" HeaderText="Deposit Due Amt" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LastContact" SortExpression="LastContact" HeaderText="Last Contact" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="AllKind" SortExpression="AllKind" HeaderText="Type" />

                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>

                    <%-- Current FinalPayment --%>
                    <asp:TemplateField HeaderText="Final (P)">
                        <FooterTemplate>
                          <asp:Label ID="lblFinalPayment" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnCurrentFinalPayment" runat="server" OnClick="showHideCurrentFinalPayment"
                                CommandArgument="Show" Text='<%#Eval("FinalPayment") %>' OnDataBinding="lnkbtnCurrentFinalPayment_DataBinding"></asp:LinkButton>
                            <asp:Panel ID="pnlCurrentFinalPayment" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height:  auto;"> 
                                <asp:GridView ID="grdCurrentFinalPayment" runat="server" AutoGenerateColumns="false" PageSize="10"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFinalPayment_PageIndexChanging" OnSorting="grdFinalPayment_Sorting"
                                    CssClass="ChildGrid" Caption="Current Final Payment" Width="100%">
                                    <Columns>
                                         <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnCurrentFinalPaymentClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress" >
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnCurrentEmailAddress" runat="server" NavigateUrl='<%# $"mailto:{Eval("EMailAddress")}" %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnCurrentFinalPaymentResvNo" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                        <asp:BoundField DataField="TotalCharge" SortExpression="TotalCharge" HeaderText="Total Charge" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FinalDueAmt" SortExpression="FinalDueAmt" HeaderText="Balance Due" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FinalDueDate" SortExpression="FinalDueDate" HeaderText="Payment Due" DataFormatString="{0:d}" />
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Past FinalPayment --%>
                    <asp:TemplateField HeaderText="Past">
                        <FooterTemplate>
                          <asp:Label ID="lblFinalPaymentPast" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnPastFinalPayment" runat="server" OnClick="showHidePastFinalPayment"
                                CommandArgument="Show" Text='<%#Eval("FinalPaymentPast") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlPastFinalPayment" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdPastFinalPayment" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFinalPayment_PageIndexChanging" OnSorting="grdFinalPayment_Sorting"
                                    CssClass="ChildGrid" Caption="Past Final Payment" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnFinalPaymentPastClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress" >
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnPastEmailAddress" runat="server" NavigateUrl='<%# $"mailto:{Eval("EMailAddress")}" %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnPastReservationNumber" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>  
                                        <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                        <asp:BoundField DataField="TotalCharge" SortExpression="TotalCharge" HeaderText="Total Charge" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FinalDueAmt" SortExpression="FinalDueAmt" HeaderText="Balance Due" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FinalDueDay" SortExpression="FinalDueDay" HeaderText="Days Late">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FinalDueDate" SortExpression="FinalDueDate" HeaderText="Payment Due" DataFormatString="{0:d}" />
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Deposit Due --%>
                    <asp:TemplateField HeaderText="Dep. Due(D)">
                        <FooterTemplate>
                          <asp:Label ID="lblNonDeposit" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnCurrentPastNonDeposit" runat="server" OnClick="showHideCurrentPastNonDeposit"
                                CommandArgument="Show" Text='<%#Eval("NonDeposit") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlCurrentPastNonDeposit" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdCurrentPastNonDeposit" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdNonDeposit_PageIndexChanging" OnSorting="grdNonDeposit_Sorting"
                                    CssClass="ChildGrid" Caption="Current/Past Non-Deposit" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnCurrentPastNonDepositClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress" >
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnCurrentPastNonDepositClientEmailAddress" runat="server" NavigateUrl='<%# $"mailto:{Eval("EMailAddress")}" %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnCurrentPastNonDepositResvNo" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                        <asp:BoundField DataField="DepositAmt" SortExpression="DepositAmt" HeaderText="Deposit Amt" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DepositDueAmt" SortExpression="DepositDueAmt" HeaderText="Dep. Due Amt" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NumberOfDayLate" SortExpression="NumberOfDayLate" HeaderText="Days Late">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DepositDueDate" SortExpression="DepositDueDate" HeaderText="Deposit Due Date" DataFormatString="{0:d}" />
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Current Returning Client Callback --%>
                    <asp:TemplateField HeaderText="RCCB(R)">
                        <FooterTemplate>
                          <asp:Label ID="lblReturnClientCallback" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnCurrentReturnClientCallback" runat="server" OnClick="showHideCurrentReturnClientCallback"
                                CommandArgument="Show" Text='<%#Eval("ReturnClientCallback") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlCurrentReturnClientCallback" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdCurrentReturnClientCallback" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdReturnClientCallback_PageIndexChanging" OnSorting="grdReturnClientCallback_Sorting"
                                    CssClass="ChildGrid" Caption="Current Return Client Callback" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnCurrentReturnClientCallbackClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress" >
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnCurrentReturnClientCallbackClientEmailAddress" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnCurrentReturnClientCallbackResvNo" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                        <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="Resv Agent" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="TotalReceived" SortExpression="TotalReceived" HeaderText="Total Received" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Past Returning Client Callback --%>
                    <asp:TemplateField HeaderText="Past">
                        <FooterTemplate>
                          <asp:Label ID="lblReturnClientCallbackPast" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnPastReturnClientCallback" runat="server" OnClick="showHidePastReturnClientCallback"
                                CommandArgument="Show" Text='<%#Eval("ReturnClientCallbackPast") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlPastReturnClientCallback" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdPastReturnClientCallback" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdReturnClientCallback_PageIndexChanging" OnSorting="grdReturnClientCallback_Sorting"
                                    CssClass="ChildGrid" Caption="Past Return Client Callback" Width="100%" EmptyDataText="No Data Available">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnPastReturnClientCallbackClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress" >
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnPastReturnClientCallbackClientEmailAddress" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnPastReturnClientCallbackResvNo" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                        <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="Resv Agent" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="TotalReceived" SortExpression="TotalReceived" HeaderText="Total Received" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <div>
                                            <br />
                                            <strong>&nbsp;&nbsp;No Data Available&nbsp;&nbsp;</strong>
                                            <br />
                                        </div>
                                    </EmptyDataTemplate> 
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Current Future Client Callback --%>
                    <asp:TemplateField HeaderText="Current FCC(F)">
                        <FooterTemplate>
                          <asp:Label ID="lblFCCCurrent" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnCurrentFCC" runat="server" OnClick="showHideCurrentFCC"
                                CommandArgument="Show" Text='<%#Eval("FCCCurrent") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlCurrentFCC" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdCurrentFCC" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFCC_PageIndexChanging" OnSorting="grdFCC_Sorting"
                                    CssClass="ChildGrid" Caption="Current FCC" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="Order No." />
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnCurrentFCCClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnCurrentFCCClientEmailAddress" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="Date Entered" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="CallbackDate" SortExpression="CallbackDate" HeaderText="Callback Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
                                        <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
                                        <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Past Future Client Callback --%>
                    <asp:TemplateField HeaderText="Past">
                        <FooterTemplate>
                          <asp:Label ID="lblFCCPast" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnPastFCC" runat="server" OnClick="showHidePastFCC"
                                CommandArgument="Show" Text='<%#Eval("FCCPast") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlPastFCC" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdPastFCC" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFCC_PageIndexChanging" OnSorting="grdFCC_Sorting"
                                    CssClass="ChildGrid" Caption="Past FCC" Width="100%">
                                    <Columns>
                                         <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="Order No." />
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnPastFCCFCCClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnPastFCCClientEmailAddress" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="Date Entered" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="CallbackDate" SortExpression="CallbackDate" HeaderText="Callback Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
                                        <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
                                        <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Current Lead --%>
                    <asp:TemplateField HeaderText="Current Lead(L)">
                        <FooterTemplate>
                          <asp:Label ID="lblCurrentLead" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnCurrentLead" runat="server" OnClick="showHideCurrentLead"
                                CommandArgument="Show" Text='<%#Eval("CurrentLead") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlCurrentLead" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdCurrentLead" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdLead_PageIndexChanging" OnSorting="grdLead_Sorting"
                                    CssClass="ChildGrid" Caption="Current Lead" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnCurrentLeadClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnCurrentLeadClientEmailAddress" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="Order No." />
                                        <asp:BoundField DataField="OrderContactSource" SortExpression="OrderContactSource" HeaderText="Lead Source" />
                                        <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
                                        <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
                                        <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Past Lead --%>
                    <asp:TemplateField HeaderText="Past">
                        <FooterTemplate>
                          <asp:Label ID="lblPastLead" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnPastLead" runat="server" OnClick="showHidePastLead"
                                CommandArgument="Show" Text='<%#Eval("PastLead") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlPastLead" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdPastLead" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdLead_PageIndexChanging" OnSorting="grdLead_Sorting"
                                    CssClass="ChildGrid" Caption="Past Lead" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnPastLeadClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                         <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnPastLeadClientEmailAddress" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="Order No." />
                                        <asp:BoundField DataField="OrderContactSource" SortExpression="OrderContactSource" HeaderText="Lead Source" />
                                        <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
                                        <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
                                        <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Reservation INQ status --%>
                    <asp:TemplateField HeaderText="Resv INQ(I)">
                        <FooterTemplate>
                          <asp:Label ID="lblInqRLI" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnInqRLI" runat="server" OnClick="showHideInqRLI"
                                CommandArgument="Show" Text='<%#Eval("InqRLI") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlInqRLI" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdInqRLI" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdResvHistory_PageIndexChanging" OnSorting="grdResvHistory_Sorting"
                                    CssClass="ChildGrid" Caption="New Reservation" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnInqRLIClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnInqRLIClientEmailAddress" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnInqRLIItineraryID" runat="server" OnClick="lnkbtnItinerary_Click" 
                                                    Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnInqRLIReservationNumber" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                        <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                                        <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Reservation Booking Status --%>
                    <asp:TemplateField HeaderText="Resv BKD(B)">
                        <FooterTemplate>
                          <asp:Label ID="lblBkdRLI" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnBkdRLI" runat="server" OnClick="showHideBkdRLI"
                                CommandArgument="Show" Text='<%#Eval("BkdRLI") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlBkdRLI" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdBkdRLI" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdResvHistory_PageIndexChanging" OnSorting="grdResvHistory_Sorting"
                                    CssClass="ChildGrid" Caption="Booked Reservation" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnBkdRLIClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnBkdRLIClientEmailAddress" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnBkdRLIItineraryID" runat="server" OnClick="lnkbtnItinerary_Click" 
                                                    Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnBkdRLIReservationNumber" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                        <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                                        <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <%-- Reservation Waitinglist Status --%>
                    <asp:TemplateField HeaderText="Resv Waitlist(W)">
                        <FooterTemplate>
                          <asp:Label ID="lblWtlRLI" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnWtlRLI" runat="server" OnClick="showHideWtlRLI"
                                CommandArgument="Show" Text='<%#Eval("WtlRLI") %>'>
                            </asp:LinkButton>
                            <asp:Panel ID="pnlWtlRLI" runat="server" Visible="false" Style="position: relative">
                                <div style="overflow-y: auto; height: auto;"> 
                                <asp:GridView ID="grdWtlRLI" runat="server" AutoGenerateColumns="false" PageSize="10" DataKeyNames="UserID"
                                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdResvHistory_PageIndexChanging" OnSorting="grdResvHistory_Sorting"
                                    CssClass="ChildGrid" Caption="Waiting List Reservation" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnWtlRLIClientID" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                                                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkbtnWtlRLIClientEmailAddress" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>' 
                                                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnWtlRLIItineraryID" runat="server" OnClick="lnkbtnItinerary_Click" 
                                                    Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber" >
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnWtlRLIReservationNumber" runat="server" OnClick="lnkbtnResvNo_Click" 
                                                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                                        <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                                        <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total">
                      <FooterTemplate>
                        <asp:Label ID="lblTotalRow" runat="server"></asp:Label>
                      </FooterTemplate>
                      <ItemTemplate>
                        <asp:Label ID="lblRowTotal" runat="server" Text='<%# Eval("RowTotal") %>'></asp:Label>
                      </ItemTemplate>
                      <FooterStyle Font-Bold="True" HorizontalAlign="Center" />
                      <ItemStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <asp:Label ID="lblRANodata" runat="server" Text="No data to display."></asp:Label>
                </EmptyDataTemplate>
            </asp:GridView>
              <!-- Activity Counter -->
          <asp:UpdatePanel ID="uplActivityCounterAdmin" runat="server" UpdateMode="Conditional">
              <ContentTemplate> 
              <asp:Panel ID="plnActivityCounter" runat="server">
                <table style="width:100%;">
                  <tr>
                    <td class="auto-style9">
                      <asp:Label ID="Label7" runat="server" width="320px" Text="Date ranges for Activity Counter:"></asp:Label>
                    </td>
                    <td class="auto-style10">
                      <asp:Label ID="Label8" runat="server" width="130px" Text="Start Date:" CssClass="auto-style1"></asp:Label>
                    </td>
                    <td class="auto-style11">
                      <asp:TextBox ID="dtStartActivityDate" runat="server" width="100px"></asp:TextBox>
                    </td>
                    <td class="auto-style12">
                      <asp:Label ID="Label9" runat="server" width="120px" Text="End Date:"></asp:Label>
                    </td>
                    <td class="auto-style13">
                      <asp:TextBox ID="dtEndActivityDate" runat="server" width="100px"></asp:TextBox>
                    </td>
                    <td class="auto-style12">
                        <asp:Button ID="btnSubmitActivity" runat="server" OnClick="btnSubmitActivity_Click" Text="Submit" width="76px" />
                    </td>
                    <td class="auto-style13">
                        &nbsp;</td>
                    <td class="auto-style14">
                        &nbsp;</td>
                    <td class="auto-style14">
                       </td>
                  </tr>
              </table>
            </asp:Panel>
           <asp:UpdateProgress ID="upActivityCounter" runat="server" AssociatedUpdatePanelID="uplActivityCounterAdmin" DisplayAfter="2000" DynamicLayout ="true">
            <ProgressTemplate>
              <center>Processing...<br /> <img src="images/loader.gif" runat="server" /></center>
            </ProgressTemplate>
            </asp:UpdateProgress>
              <asp:GridView ID="grdActivityCounter" runat="server"  CssClass="AltRow TableHeader" AutoGenerateColumns="False" DataKeyNames="UserID" Caption="Activity Counter" AllowSorting="True" OnPageIndexChanging="grdActivityCounter_PageIndexChanging" OnSorting="grdActivityCounter_Sorting">
                  <Columns>
                    <asp:BoundField DataField="FullName" SortExpression="FullName" HeaderText="Full Name"/>
                    <asp:BoundField DataField="PendingFCC" SortExpression="PendingFCC"  HtmlEncode="false" HeaderText="Pending<br/> FCC<br/> OPP/CLP/SVC">
                       <ItemStyle HorizontalAlign="Right" />
                     </asp:BoundField>
                    <asp:BoundField DataField="CompletedFCC" SortExpression="CompletedFCC" HtmlEncode="false" HeaderText="FCC<br/> Entries<br/> OPP/CLP/SVC">
                       <ItemStyle HorizontalAlign="Right" />
                     </asp:BoundField>
                    <asp:BoundField DataField="ClaimedLead" SortExpression="ClaimedLead" HtmlEncode="false" HeaderText="Claimed<br/> Leads">
                       <ItemStyle HorizontalAlign="Right" />
                     </asp:BoundField>
                    <asp:BoundField DataField="ConversionLead" SortExpression="ConversionLead" HtmlEncode="false" HeaderText="Leads<br/> Conversion<br/> Rate" DataFormatString="{0:p2}">
                       <ItemStyle HorizontalAlign="Right" />
                     </asp:BoundField>
                    <asp:BoundField DataField="PendingResv" SortExpression="PendingResv" HtmlEncode="false" HeaderText="Bookings by<br/> Status<br/> INQ/OKD/BKD">
                       <ItemStyle HorizontalAlign="Right" />
                     </asp:BoundField>
                    <asp:BoundField DataField="TotalSales" SortExpression="TotalSales" HtmlEncode="false" HeaderText="Total<br/> Sales<br/> $" DataFormatString="{0:n0}">
                         <ItemStyle Width="100px"></ItemStyle>
                         <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TotalBud" SortExpression="TotalBud" HtmlEncode="false" HeaderText="Total<br/> BUDs<br/> $" DataFormatString="{0:n0}">
                        <ItemStyle Width="100px"></ItemStyle>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                  </Columns>
              </asp:GridView>
              </ContentTemplate>
              <Triggers>
                  <asp:AsyncPostBackTrigger ControlID="btnSubmitActivity" EventName="Click" />
              </Triggers>
            </asp:UpdatePanel>
        </div>
    <div>
      <asp:Panel ID="pnlAllGrids" runat="server" Visible="true" Style="position: relative">

        <%-- Hurricane Related Reservation --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:GridView ID="grdHurricaneReservation" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="grdHurricaneReservation_PageIndexChanging" OnSorting="grdHurricaneReservation_Sorting"
            CssClass="TableHeader" DataKeyNames="UserID" Caption="Hurricane Related Reservations" Width="100%">
            <AlternatingRowStyle BackColor="Silver" />
            <Columns>
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
              <asp:BoundField DataField="DateHurricaneEnd" SortExpression="DateHurricaneEnd" HeaderText="Date Hurricane End" DataFormatString="{0:d}" />
              <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnHurricaneReservationItinerary" runat="server" OnClick="lnkbtnItinerary_Click"
                    Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
      
              <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnHurricaneReservationResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />


              <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Resv Status" />
              <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtHurricaneReservationClientName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtHurricaneReservationEmail" runat="server" OnClick="Email_Click" CommandArgument='<%# Eval("ClientID") %>'
                    Text='<%# Eval("EMailAddress") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>

            </Columns>
          </asp:GridView>
        </div>

        <%-- Letter RA --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:UpdatePanel ID="upnlGrdLetterRA" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
          <ContentTemplate> 
          <asp:GridView ID="grdLetterRA" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="grdLetterRA_PageIndexChanging" OnSorting="grdLetterRA_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Letter" Width="100%" EnableModelValidation="True">
            <Columns>
              <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                  <asp:LinkButton ID="btnPrintLetterRA" runat="server" CausesValidation="false" CommandName="" OnClick="btnPrintLetterRA_Click" Text="Letter"
                    CommandArgument='<%# $"{Eval("ReservationNumber")}:{Eval("Kind")}:{Eval("TotalReceived")}" %>' EnableViewState="True"></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                  <asp:LinkButton ID="btnRemoveLetterRA" runat="server" CausesValidation="false" CommandName="" OnClick="btnRemoveLetterRA_Click" Text="Remove Letter"
                    CommandArgument='<%# $"{Eval("ReservationNumber")}:{Eval("Kind")}" %>' EnableViewState="True"></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="MessageDateTime" SortExpression="MessageDateTime" HeaderText="Last Letter Printed" DataFormatString="{0:MM/dd/yyyy HH:mm}" >
                <ItemStyle Width="80px" />
              </asp:BoundField>
              <asp:BoundField DataField="LetterType" SortExpression="LetterType" HeaderText="Letter Type" />
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnFinalAndNonDepositRaName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                    Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>' Font-Underline="True"></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnFinalAndNonDepositRaEmail" runat="server" OnClick="Email_Click"
                    Text='<%# Eval("EMailAddress") %>' CommandArgument='<%# Eval("ClientID") %>' EnableViewState="True"> </asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnFinalAndNonDepositRaResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>' EnableViewState="True"></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
              <asp:BoundField DataField="TotalCharge" SortExpression="TotalCharge" HeaderText="Total Charge" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              <asp:BoundField DataField="TotalReceived" SortExpression="TotalReceived" HeaderText="Received" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              <asp:BoundField DataField="BalanceDue" SortExpression="BalanceDue" HeaderText="Balance Due" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              <asp:BoundField DataField="PaymentDueDate" SortExpression="PaymentDueDate" HeaderText="Payment Due" DataFormatString="{0:d}" />
              <asp:BoundField DataField="NumberOfDayDue" SortExpression="NumberOfDayDue" HeaderText="Days Due" />
            </Columns>
              <EmptyDataTemplate>
                  This is no letter.
              </EmptyDataTemplate>
          </asp:GridView>
          </ContentTemplate>
              <Triggers>
                  <asp:AsyncPostBackTrigger ControlID="btnRefressLetter" EventName="Click" />
              </Triggers>
        </asp:UpdatePanel>
            <div>
                <asp:Button ID="btnRefressLetter" runat="server" Text="Refresh Letter Grid" OnClick="btnRefressLetter_Click" />
            </div>
        </div>

        <%-- FinalPayment RA --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:GridView ID="grdFinalAndNonDepositRA" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="grdFinalAndNonDeposit_PageIndexChanging" OnSorting="grdFinalAndNonDeposit_Sorting"
            CssClass="AltRow TableHeader Deposits" DataKeyNames="UserID" Caption="Non-Deposits/Final Payments" Width="100%" EnableModelValidation="True">
            <Columns>
              <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                  <asp:LinkButton ID="btnPrintLetterFinalAndNonDepositRA" runat="server" CausesValidation="false" CommandName="" OnClick="btnPrintLetterFinalAndNonDepositRA_Click" Text="Letter"
                    CommandArgument='<%# $"{Eval("ReservationNumber")}:{Eval("Kind")}:{Eval("TotalReceived")}" %>' EnableViewState="True"></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="MessageDateTime" SortExpression="MessageDateTime" HeaderText="Last Letter Printed" DataFormatString="{0:MM/dd/yy HH:mm}" >
                <ItemStyle Width="80px" />
              </asp:BoundField>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnFinalAndNonDepositRaName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                    Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>' Font-Underline="True"></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnFinalAndNonDepositRaEmail" runat="server" OnClick="Email_Click"
                    Text='<%# Eval("EMailAddress") %>' CommandArgument='<%# Eval("ClientID") %>' EnableViewState="True"> </asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnFinalAndNonDepositRaResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>' EnableViewState="True"></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
              <asp:BoundField DataField="TotalCharge" SortExpression="TotalCharge" HeaderText="Total Charge" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              <asp:BoundField DataField="TotalReceived" SortExpression="TotalReceived" HeaderText="Received" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              <asp:BoundField DataField="BalanceDue" SortExpression="BalanceDue" HeaderText="Balance Due" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              <asp:BoundField DataField="PaymentDueDate" SortExpression="PaymentDueDate" HeaderText="Payment Due" DataFormatString="{0:d}" />
              <asp:BoundField DataField="NumberOfDayDue" SortExpression="NumberOfDayDue" HeaderText="Days Due" />
            </Columns>
          </asp:GridView>
        </div>

        <%-- Sales Management Board --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
       <asp:UpdatePanel ID="updPnlLead" runat="server" UpdateMode="Conditional">
          <ContentTemplate> 
            <asp:Panel ID="pnlSMB" runat="server">
                <table style="width:100%;">
                  <tr>
                    <td class="auto-style2">
                      <asp:Label ID="Label2" runat="server" width="270px" Text="Date ranges for Sales Management:"></asp:Label>
                    </td>
                    <td class="auto-style3">
                      <asp:Label ID="lblStartDate" runat="server" width="80px" Text="Start Date:" CssClass="auto-style1"></asp:Label>
                    </td>
                    <td class="auto-style7">
                      <asp:TextBox ID="dtStartDate" runat="server" width="100px" OnTextChanged="dtStartDate_TextChanged"></asp:TextBox>
                    </td>
                    <td class="auto-style5">
                      <asp:Label ID="lblEndDate" runat="server" width="80px" Text="End Date:"></asp:Label>
                    </td>
                    <td class="auto-style6">
                      <asp:TextBox ID="dtEndDate" runat="server" width="100px"></asp:TextBox>
                    </td>
                    <td>
                      <asp:Button ID="btnSubmitSMB" runat="server" width="76px" OnClick="btnSubmitSMB_Click" Text="Submit"></asp:Button>
                    </td>
                  </tr>
              </table>   
            </asp:Panel>
           <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updPnlLead" DisplayAfter="2000" DynamicLayout ="true">
            <ProgressTemplate>
              <center>Processing...<br /> <img src="images/loader.gif" runat="server" /></center>
            </ProgressTemplate>
            </asp:UpdateProgress>                                   
            <asp:GridView ID="grdSalesManagementBoard" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="grdSalesManagementBoard_PageIndexChanging" OnSorting="grdSalesManagementBoard_Sorting"
              CssClass="AltRow TableHeader" DataKeyNames="UserID" Width="100%" EnableModelValidation="True" Caption="Sales Management" OnRowDataBound="grdSalesManagementBoard_RowDataBound">
              <Columns>
                <asp:TemplateField HeaderText="">
                  <ItemTemplate>
                    <asp:LinkButton ID="lnkbtnSalesManagementBoardDetail" runat="server" OnClick="Detail_Click" 
                      Text="Email" CommandArgument='<%# Eval("ClientID") %>' OnClientClick="showAllEmails();" EnableViewState="True"> </asp:LinkButton>
                  </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="HQIDate" SortExpression="HQIDate" HeaderText="HQI Date" DataFormatString="{0:MM/dd/yy HH:mm}" >
                  <ItemStyle Width="80px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Client Name" HeaderStyle-Wrap="False"  SortExpression="ClientName">
                    <ItemTemplate>
                        <asp:LinkButton ID="hlSalesManagementBoardClientName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                            Text='<%# Eval("ClientName") %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" />
                    <ItemStyle Width="162px" />
                </asp:TemplateField>

                <asp:BoundField DataField="VillaCode" SortExpression="VillaCode" HeaderStyle-Wrap="False" HeaderText="Villa Code" >
                    <HeaderStyle Wrap="False" />
                </asp:BoundField>
                <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" > 
                  <ItemStyle Width="132px" />
                </asp:BoundField>
                <asp:BoundField DataField="TravelDate" SortExpression="TravelDate" HeaderText="Travel Date" >
                  <ItemStyle Width="180px" />
                </asp:BoundField>
                <asp:BoundField DataField="OP" SortExpression="OP" HeaderText="OP" />
                <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                <asp:BoundField DataField="FCCCallbackDate" SortExpression="FCCCallbackDate" HeaderText="FCC Callback" HeaderStyle-Wrap="False" DataFormatString="{0:d}" />
                <asp:BoundField DataField="FCCType" SortExpression="FCCType" HeaderText="FCC Type" >
                  <ItemStyle Width="132px" />
                </asp:BoundField>
              <asp:BoundField DataField="LastEmailInDate" DataFormatString="{0:MM/dd/yy HH:mm}"
                   HeaderStyle-Wrap="False" HeaderText="Last Email In"
                  SortExpression="LastEmailInDate" >
                  <HeaderStyle Wrap="False" />
                  <ItemStyle Width="80px" />
               </asp:BoundField>
              <asp:BoundField DataField="LastEmailOutDate" DataFormatString="{0:MM/dd/yy HH:mm}"
                   HeaderStyle-Wrap="False"  HeaderText="Last Email Out"
                  SortExpression="LastEmailOutDate" >
                  <HeaderStyle Wrap="False" />
                  <ItemStyle Width="80px" />
               </asp:BoundField>
                <asp:BoundField DataField="FCCNote" SortExpression="FCCNote" HeaderText="FCC Note" />
              </Columns>
              <EmptyDataTemplate>
                No data for the date ranges.
              </EmptyDataTemplate>
            </asp:GridView>
          </ContentTemplate>
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSubmitSMB" EventName="Click" />
          </Triggers>
        </asp:UpdatePanel>
        </div>
   

        <%-- Returning Client Callback --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:UpdatePanel ID="upPanelReturnClientCallbackRA" runat="server" UpdateMode="Conditional">
              <ContentTemplate> 
          <asp:GridView ID="grdReturnClientCallbackRA" runat="server" AutoGenerateColumns="false" PageSize="10"
            AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdReturnClientCallback_PageIndexChanging" OnSorting="grdReturnClientCallback_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Return Client Callback" Width="100%">
            <Columns>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnReturnClientCallbackRaName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                    Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:LinkButton ID="hlReturnClientCallbackRaEmail" runat="server" OnClick="ReturnClientCallbackEmail_Click"  
                    CommandArgument='<%# $"{Eval("ClientID")};{Eval("DestinationID")};{Eval("DateTo")}" %>'
                    Text='<%# Eval("EMailAddress") %>' OnClientClick="openNewEmail();"></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnReturnClientCallbackRaResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
              <asp:BoundField DataField="CallbackDate" SortExpression="CallbackDate" HeaderText="Callback Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="CallbackStatus" SortExpression="CallbackStatus" HeaderText="Callback Status" />
            </Columns>
            <EmptyDataTemplate>
              No data for the date ranges.
            </EmptyDataTemplate>
          </asp:GridView>
          </ContentTemplate>
        </asp:UpdatePanel>
        </div>
        <%-- Future Client Callback RA --%>
          <INPUT id="hdnClientID" type="hidden" value="0" runat="server" >
          <asp:UpdatePanel ID="UpdatePanelFCCRA" runat="server" UpdateMode="Conditional">
              <ContentTemplate> 
              <asp:Panel ID="pnlFCC" runat="server">
                <table style="width:100%;">
                  <tr>
                    <td class="auto-style9">
                      <asp:Label ID="Label3" runat="server" width="320px" Text="Date ranges for Future Client Callback:"></asp:Label>
                    </td>
                    <td class="auto-style10">
                      <asp:Label ID="Label4" runat="server" width="130px" Text="Start FCC Date:" CssClass="auto-style1"></asp:Label>
                    </td>
                    <td class="auto-style11">
                      <asp:TextBox ID="dtStartFCCDate" runat="server" width="100px" OnTextChanged="dtStartDate_TextChanged"></asp:TextBox>
                    </td>
                    <td class="auto-style12">
                      <asp:Label ID="Label5" runat="server" width="120px" Text="End FCC Date:"></asp:Label>
                    </td>
                    <td class="auto-style13">
                      <asp:TextBox ID="dtEndFCCDate" runat="server" width="100px"></asp:TextBox>
                    </td>
                    <td class="auto-style12">
                      <asp:Label ID="Label6" runat="server" width="120px" Text="FCC Type:"></asp:Label>
                    </td>
                    <td class="auto-style13">
                      <asp:DropDownList ID="dlFCCType" runat="server" width="132px" AutoPostBack="True" DataTextField="ContactSourceName" DataValueField="ContactSourceID">
                      </asp:DropDownList>
                    </td>
                    <td class="auto-style14">
                      <asp:Button ID="btnSubmitFCC" runat="server" width="76px" OnClick="btnSubmitFCC_Click" Text="Submit"></asp:Button>
                    </td>
                    <td class="auto-style14">
                       </td>
                  </tr>
              </table>
            </asp:Panel>
           <asp:UpdateProgress ID="UpdateProgressFCC" runat="server" AssociatedUpdatePanelID="UpdatePanelFCCRA" DisplayAfter="2000" DynamicLayout ="true">
            <ProgressTemplate>
              <center>Processing...<br /> <img src="images/loader.gif" runat="server" /></center>
            </ProgressTemplate>
            </asp:UpdateProgress>   
          

           <asp:GridView ID="grdFCCRA" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="grdFCC_PageIndexChanging" OnSorting="grdFCC_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="ClientID,CallbackID,PreviousState" Caption="Future Client Callback" Width="100%" OnRowDataBound="grdFCCRA_RowDataBound" OnRowCreated="grdFCCRA_RowCreated">
            <Columns>
              <asp:TemplateField HeaderText="MOK" SortExpression="DisplayCheckBox">
                <HeaderTemplate>
                  <asp:Label Text="MOK" runat="server"></asp:Label>
                      <asp:Button ID="btnSubmitChange" runat="server" OnClick="btnSubmitChange_Click" Text="Submit" />
                </HeaderTemplate>
                <ItemTemplate>
                  <asp:RadioButtonList ID="rdbOKNotOK" runat="server" AutoPostBack="True" Font-Bold="False" RepeatDirection="Horizontal" Visible="False">
                    <asp:ListItem Value="ROK">OK</asp:ListItem>
                    <asp:ListItem Value="RNK">Not OK</asp:ListItem>
                    <asp:ListItem Value="NC">Neither</asp:ListItem>
                  </asp:RadioButtonList>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress" ShowHeader="False">
                <ItemTemplate>

                <%--  <asp:LinkButton ID="hlFCCRaEmail" runat="server" Onmouseover='<%#String.Format("hoverEmailDash({0});",Eval("ClientID"))%>' OnClick="grdFCCRAEmail_Click" CommandArgument='<%# $"{Eval("ClientID")}:{Eval("EMailAddress")}" %>'
                    Text="Email" onmouseout="unHoverEmail();" OnClientClick="openNewEmail();unHoverEmail();" ></asp:LinkButton>--%>
                      <asp:LinkButton ID="hlFCCRaEmail" runat="server"  OnClick="grdFCCRAEmail_Click" CommandArgument='<%# $"{Eval("ClientID")}:{Eval("EMailAddress")}" %>'
                    Text="Email"  OnClientClick="openNewEmailRight();" ></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="CallbackID" SortExpression="CallbackID" HeaderText="Callback ID" />

              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnFCCRaName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                    Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="Date Entered" DataFormatString="{0:d}" />                                        
              <asp:BoundField DataField="CallbackDate" SortExpression="CallbackDate" HeaderText="FCC Callback" DataFormatString="{0:d}" />
              <asp:BoundField DataField="FCCType" SortExpression="FCCType,CallbackDate" HeaderText="FCC Type" />
              <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
              <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
              <asp:BoundField DataField="LastTravelDate" SortExpression="LastTravelDate" HeaderText="Last Travel Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="FCC Note" />
              <asp:BoundField DataField="Color" SortExpression="Color" HeaderText="Color" Visible="false" />
            </Columns>
             <EmptyDataRowStyle BackColor="#99CCFF" />
             <EmptyDataTemplate>
               No data for the date ranges.
             </EmptyDataTemplate>
          </asp:GridView>
           </ContentTemplate>
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSubmitFCC" EventName="Click" />
          </Triggers>
        </asp:UpdatePanel>
        
        <%-- Inquiry and Waitlist Reservations--%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:GridView ID="grdInqAndWaitListRA" runat="server" AutoGenerateColumns="false" PageSize="10"
            AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdInqAndWaitList_PageIndexChanging" OnSorting="grdInqAndWaitList_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Inquiries and Waitlists" Width="100%" OnRowDataBound="grdInqAndWaitListRA_RowDataBound">
            <Columns>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnInqAndWaitListRaName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                    Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnInqAndWaitListRaEmail" runat="server" OnClick="Email_Click" CommandArgument='<%# Eval("ClientID") %>'
                    Text='<%# Eval("EMailAddress") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <%--
              <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                    Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              --%>
              <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnInqAndWaitListRaResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
              <asp:BoundField DataField="DateEntered" SortExpression="DateEntered" HeaderText="Date Entered" DataFormatString="{0:d}"/>
              <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
              <asp:TemplateField HeaderText="Status" SortExpression="Status">
                <ItemTemplate>
                  <asp:Label ID="grdInqAndWaitListRAStatus" runat="server"
                    Text='<%# Eval("Status") %>'></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <%--
              <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              --%>
            </Columns>
          </asp:GridView>
        </div>
        <%-- Reservation (BKD) --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:GridView ID="grdReservationRA" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="grdReservation_PageIndexChanging" OnSorting="grdReservation_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="In-House/Arrivals (Pink) and Reservations" Width="100%" EnableModelValidation="True" OnRowDataBound="grdReservationRA_RowDataBound">
            <Columns>
              <asp:TemplateField ShowHeader="False" SortExpression="ReservationNumber" HeaderText="Letter">
                <ItemTemplate>
                  <asp:LinkButton ID="btnPrintLetterReservationRA" runat="server" CausesValidation="false" CommandName="" OnClick="btnPrintLetterReservationRA_Click"
                    EnableViewState ="True" Text="Letter" CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="MessageDateTime" SortExpression="MessageDateTime" HeaderText="Last Letter Printed" DataFormatString="{0:g}" />
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName,FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnReservationRaName" runat="server" OnClick="lnkbtnClientID_Click" style="text-decoration:none"
                    Text='<%# $"{Eval("LastName")}, {Eval("FirstName")}" %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:LinkButton ID="hlReservationRaEmail" runat="server" OnClick="Email_Click" CommandArgument='<%# Eval("ClientID") %>'
                    Text='<%# Eval("EMailAddress") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <%--
              <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                    Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              --%>
              <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnReservationRaResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
              <asp:BoundField DataField="DateBooked" SortExpression="DateBooked" HeaderText="Date Booked" DataFormatString="{0:d}" />
              <%--
              <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
              <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              --%>
              <asp:BoundField DataField="Car" SortExpression="Car" HeaderText="Car" />
              <asp:BoundField DataField="Arrival" SortExpression="Arrival" HeaderText="Arr" />
              <asp:BoundField DataField="AdditionalPassenger" SortExpression="AdditionalPassenger" HeaderText="Add Pax" />
              <asp:BoundField DataField="CellularPhoneNumber" SortExpression="CellularPhoneNumber" HeaderText="Cell #" />
            </Columns>
          </asp:GridView>
        </div>
        </asp:Panel>
    </div>
     <div id="EmailModal" onmouseout="unHoverEmail();" class="emailModal hidden">
            <div style="text-align:right;padding-right:28px"><span style="color:red;font-size:24px;cursor:pointer" onclick="hoverOff();">Xan>       <div id="theEmail">
            </div>
    </div>
    </form>
</body>
</html>
