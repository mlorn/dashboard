﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GWWimcoEmailGrid.ascx.cs" Inherits="GWCustom.GWWimco"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .GWControl-Selected-Row
    {
        background-color: #FFFF99 !important; 
    }
</style>


<!--
03.11.14. -jk   grid was driven by command field :

                <asp:CommandField ButtonType="Button" HeaderText="Read" SelectText="Read" 
                    ShowSelectButton="True"  />                    
Changed to JS button.                    
-->

        <asp:Panel ID="pnlMain" runat="server" CssClass="gwmi">
        <asp:Label id="lblAction" runat="server" Visible="false" BackColor="Yellow" Font-Bold="true" ForeColor="Black"></asp:Label>
        <asp:Panel ID="pnlSearch" runat="server" Visible="true">        
        <div style="overflow-y: scroll; height: 200px;">
        <asp:GridView ID="wEmailGrid" runat="server" PageSize="16" CssClass="wEmailGridCSS" Caption="Groupwise Email"                        
                OnSelectedIndexChanged="wEmailGrid_SelectedIndexChanged" 
                AutoGenerateColumns="False" DataKeyNames="GroupwiseID" 
                AllowPaging="False"  AllowSorting="True" 
                EnableSortingAndPagingCallbacks="True" DataSourceID="ObjectDataSource1" 
                Width="100%" >
            <Columns>                
                <asp:TemplateField>
                        <HeaderTemplate>
                        <label>Read</label>
                        </HeaderTemplate>
                        <ItemTemplate>                                                                                   
                            <button causesvalidation="false" onclick="javascript:window.open('viewmail.aspx?m=<%# Eval("GroupwiseID")%>' , '_blank','toolbar=no, scrollbars=yes, resizable=yes, top=50, left=50, width=860, height=400','')" >Read</button>                         
                        </ItemTemplate>
                </asp:TemplateField>  
                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" ControlStyle-CssClass="wEmailGridDateCSS"
                    DataFormatString="{0:MMM dd, yyyy HH:mm}"  >
                    <ControlStyle CssClass="wEmailGridDateCSS" />
                </asp:BoundField>
                <asp:BoundField DataField="FromEmail" HeaderText="From"  
                    SortExpression="FromEmail" />
                <asp:BoundField DataField="Recipient" HeaderText="Recipient"  
                    SortExpression="Recipient" />
                <asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject" ControlStyle-CssClass="wGEmailGridSubjectCSS"  >
                    <ControlStyle CssClass="wGEmailGridSubjectCSS" />
                </asp:BoundField>                
                            

                
            
            </Columns>
        <EmptyDataTemplate>
        <div>
        <br />
        <strong>&nbsp;&nbsp;No Data Available&nbsp;&nbsp;</strong>
        <br />
        </div>
        </EmptyDataTemplate> 
            <SelectedRowStyle CssClass="GWControl-Selected-Row"  />
        </asp:GridView>
        </div>
        <br />
        <asp:Label ID="wEmailLabelInfo" runat="server" CssClass="wEmailLabelInfo" ></asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlEmail" runat="server" Visible="false">
        <br />
        <hr />
        <br />
        <asp:Button ID="btnClose" runat="server" Text="Close Email" OnClick="btnClose_Click" />
        <asp:Button ID="btnReply" runat="server" Text="Reply" onclick="btnReply_Click" />
        <br /><br />
        <asp:Label ID="lblEmail" runat="server"></asp:Label>        
        <table border="0">
        <tr><td><asp:Label ID="lblAttachment" runat="server" Text="Attachments" Visible="false" /></td></tr>
        <tr><td><asp:LinkButton ID="lnkBtnAtt1" runat="server" Visible="false" onclick="lnkBtnAtt1_Click" /></td></tr>
        <tr><td><asp:LinkButton ID="lnkBtnAtt2" runat="server" Visible="false" onclick="lnkBtnAtt1_Click" /></td></tr>
        <tr><td><asp:LinkButton ID="lnkBtnAtt3" runat="server" Visible="false" onclick="lnkBtnAtt1_Click" /></td><td></td></tr>
        <tr><td><asp:LinkButton ID="lnkBtnAtt4" runat="server" Visible="false" onclick="lnkBtnAtt1_Click" /></td><td></td></tr>
        <tr><td><asp:LinkButton ID="lnkBtnAtt5" runat="server" Visible="false" onclick="lnkBtnAtt1_Click" /></td><td></td></tr>
        </table>
        <br />        
        <asp:Label ID="lblHTML" runat="server"></asp:Label>
        <asp:TextBox ID="txtEmailMsg" runat="server" Rows="30" TextMode="MultiLine" Width="800px" Visible="false"></asp:TextBox>
        <br />
        <hr />
        <br />
        </asp:Panel>
        <asp:Panel ID="pnlReply" runat="server" Visible="false">
        
        <table border="0">
        <tr><td>To:</td><td><asp:TextBox ID="txtEmailToAddress" runat="server" Width="250px"></asp:TextBox></td></tr>
        <tr><td>From:</td><td><asp:TextBox ID="txtEmailFromName" runat="server" Width="250px"></asp:TextBox></td></tr>
        <tr><td>Email:</td><td><asp:TextBox ID="txtEmailFromEmailAddress" runat="server" Width="250px"></asp:TextBox></td></tr>
        <tr><td>CC:</td><td><asp:TextBox ID="txtEmailCCEmailAddress" runat="server" Width="250px"></asp:TextBox></td></tr>
        <tr><td><strong>Subject:</strong></td><td><asp:TextBox ID="txtEmailSubject" runat="server" Width="250px"></asp:TextBox></td></tr>
        </table>
        <hr />

        
        <asp:Button ID="btnSend" runat="server" Text="Send" onclick="btnSend_Click"   />&nbsp
        <asp:Button ID="btnCancelSend" runat="server" Text="Cancel" 
                onclick="btnCancelSend_Click" />
        <hr />                
        <br />
        <asp:HtmlEditorExtender runat="server" ID="htmlEdit" TargetControlID="txtEditor" EnableSanitization="False" Enabled="true" >
                    <Toolbar>                 
                    <asp:Undo />
                    <asp:Redo />
                    <asp:Bold />
                    <asp:Italic />
                    <asp:Underline />
                    <asp:StrikeThrough />
                    <asp:JustifyLeft />
                    <asp:JustifyCenter />
                    <asp:JustifyRight />
                    <asp:JustifyFull />
                    <asp:InsertOrderedList />
                    <asp:InsertUnorderedList />
                    <asp:RemoveFormat />
                    <asp:SelectAll />
                    <asp:UnSelect />
                    <asp:Delete />
                    <asp:Cut />
                    <asp:Copy />
                    <asp:Paste />
                    <asp:BackgroundColorSelector />
                    <asp:ForeColorSelector />
                    <asp:FontNameSelector />
                    <asp:FontSizeSelector />
                    <asp:Indent />
                    <asp:Outdent />
                    <asp:InsertHorizontalRule />
                    <asp:HorizontalSeparator />
                </Toolbar>
        </asp:HtmlEditorExtender>
        <asp:TextBox runat="server"
            ID="txtEditor" 
            TextMode="MultiLine" 
            Columns="80" 
            Rows="20" 
            Text="" />
        <br />                                
        </asp:Panel>
        
        <asp:Label ID="lblTempStorage" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblTempStorageEmailTo" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblTempStorageSubject" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblTempStorageGroupWiseID" runat="server" Visible="false"></asp:Label>
        
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="SearchByEmail" OnSelecting="ODSSelecting"
            TypeName="WimcoBaseLogic.BaseLogic" >
            <SelectParameters>
                <asp:Parameter DefaultValue="" Name="Email" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>             
        <asp:Timer ID="timer" runat="server" Enabled="false" ontick="timer_Tick"></asp:Timer>
        </asp:Panel>