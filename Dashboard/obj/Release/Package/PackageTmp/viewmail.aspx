﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" ValidateRequest="false" CodeBehind="viewmail.aspx.cs" Inherits="GWCustom.viewmail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="AddFCC.ascx" TagName="wimAddFCC" TagPrefix="wimcoCustom" %>
<%@ Register Src="MailDisplay.ascx" TagName="wimMail" TagPrefix="wimcoCustom" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta charset="utf-8">
</head>
<body>
 <style type="text/css">
  .emailHead
    {
    	font-family: Arial, Helvetica, sans-serif;
    	font-size:14px;
    	font-weight:bold;
    	color:blue;
    	}
    	.emailHead >input
    {
    	
    	color:black;
    	}
    textarea
    {
	    font:13.3333px Arial;
	
	}
	#wimMail_EmailForm_wimAddFCC_pnlNewFCC
	{
		border: 1px solid black;
		font-family:Arial, Helvetica, sans-serif;
		padding-left:5px;
		
		padding-bottom:5px;
	}
	#wimMail_EmailForm_wimAddFCC_pnlNewFCC 
	{
		font-size:14px;
	}
	#wimMail_EmailForm_wimAddFCC_pnlNewFCC > textarea
    {
		height:2em;
		 display:inline;
    }
	#wimMail_EmailForm_pnlFCC
	{
	width: 400px;
    top: 50px;
    position: absolute;
    left: 340px;
	}
	  #wimMail_EmailForm_pnlReply
    {
        top: 50px;
    position: absolute;
    }
	.hidden
	{
		display:none!important;
		}
</style>
 <script type="text/javascript">
    window.onload = function () {
            setInterval("KeepSessionAlive()", 60000)
    }

     function KeepSessionAlive() {
     url = "/keepalive.ashx?";
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("GET", url, true);
            xmlHttp.send();
            }
    </script>
<script type="text/javascript">
  $(document).ready(function () 
        //overrides default button function of edit sig
            {
                $("#wimMail$EmailForm$btnEditSig").keydown(function (e)          
                {
                    if (e.keyCode == 13) // 27=esc
                    {
                    e.stopPropagation();
                    e.preventDefault();
                       
                    }
                });
            });
            
    </script>
    <script type="text/javascript">
 function clientOrdersUpdate(){
//Does nothing, here to keep the FCC piece from breaking.
}

    </script>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager runat="server" ID="scrpMgr"></asp:ToolkitScriptManager> 
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src='http://static.wimco.com/js/jquery.js' ></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <!-- Begin Inspectlet Embed Code -->
<script type="text/javascript" id="inspectletjs">
window.__insp = window.__insp || [];
__insp.push(['wid', 955821305]);
(function() {
function ldinsp(){if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
setTimeout(ldinsp, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
})();
</script>
<!-- End Inspectlet Embed Code -->
<wimcoCustom:wimMail ID="wimMail" runat="server"  Visible="true" />

    </form>
</body>
</html>
