<%@ Page Language="VB" AutoEventWireup="false" CodeFile="test.aspx.vb" Inherits="villas_test" Trace="true" %>

<html>
<head runat="server">
    <title></title>
    <link type="text/css" href="/css/common.css" rel="Stylesheet" />
    <link type="text/css" href="/css/screen.css" rel="Stylesheet" />
    <link type="text/css" href="/css/styles.css" rel="Stylesheet" />
    <link type="text/css" href="/css/Portal.css" rel="Stylesheet" />
<style type="text/css">
    .Search{font-size:small;}
    .alternate{ background-color:white; }
    .ActiveRow {
    
    }
    #ClientTable
    {
        border-top: 1px solid black; 
        border-bottom: 1px solid black;
        border-left: 1px solid black;
        border-right: 1px solid black; 
    }
    #MessageGrid
    {
        visibility:hidden;
    }
    #ClientLabel{}
    #ClientField{font-weight:bold;}
    .width-18 { width: 18% !important }
    .width-25 { width: 25% !important }
    .width-33 { width: 33% !important }
    .width-50 { width: 50% !important }
    .width-65 { width: 65% !important }
    .width-100 { width: 100% !important }
    
    /* FORM CLASSES */
    ul.form {
        margin: 10px 0 0 0;
        padding: 0;
        list-style-type: none !important;
    }
    ul.form li {
        width: 33%;
        float: left;
	    padding: 10px 0;
        border-bottom: 0px solid #D4D4D4;
    }
    ul.form li.margin-right { margin-right: 3% !important }
    body{margin-left: 10px}
    </style>
     
    <style type="text/css" >
        round
        {
            -moz-border-radius: 35px;
            border-radius: 35px;
         }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <ASP:SCRIPTMANAGER ID="ScriptManager1" runat="server">
    </ASP:SCRIPTMANAGER>
    <div>
    Session:<br />
    <ASP:LABEL ID="lblSession" runat="server" /><br />
    User-Agent: <br />
    <ASP:LABEL ID="lblUserAgent" runat="server" />
    <hr />
    Session: <ASP:TEXTBOX ID="tbSessionName" runat="server" CssClass="round" />&nbsp;
    Value: <ASP:TEXTBOX ID="tbSessionValue" runat="server" CssClass="round" />&nbsp;
        <ASP:BUTTON ID="btnAddSession" runat="server" Text="Add To Session" OnClick="btnAddSession_Click" /><br />   
    Kill One Session: 
    <ASP:TEXTBOX id="tbKillSessionName" runat="server" Text="WimcoStopWatch" /> 
    <ASP:BUTTON id="btnKillOneSession" runat="server" Text="Kill One Session" OnClick="btnKillOneSession_Click" />
        <ASP:BUTTON ID="btnKillSwitch" runat="server" Text="Kill Session" OnClick="btnKillSwitch_Click" /><br /> 
        <ASP:BUTTON ID="btnMakeMessage" runat="server" Text="Generate Message" OnClick="btnMakeMessage_Click" />
<%--        <asp:Button ID="btnKillCookies" runat="server" Text="Kill All Cookies" OnClick="btnKillCookies_Click" />--%><br />
    Cookie to Kill 
    <ASP:TEXTBOX id="tbCookieName" runat="server" CssClass="round" /> 
    <ASP:BUTTON ID="btnKillOneCookie" runat="server" Text="Kill this cookie" OnClick="btnKillOneCookie_Click" />  <br />
    Cookie text: <ASP:LITERAL id="litCookie" runat="server" />      
        
    </div>
    
    <hr />
    Form values: <br />
    <ASP:LITERAL ID="litFormValues" runat="server" />
    <br />
    Headers: <br />
    <ASP:LITERAL ID="litHeaderValues" runat="server" />
    </form>
</body>
</html>