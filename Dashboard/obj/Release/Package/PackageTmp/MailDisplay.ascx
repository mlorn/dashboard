﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MailDisplay.ascx.cs" Inherits="GWCustom.MailDisplay" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="EmailForm.ascx" TagName="EmailForm" TagPrefix="wimcoCustom" %>



    <div> 
    <img alt="loading" id="loading-icon" class="hidden" src="http://nomad.wimco.com/images/ajax-loader.gif" />
        <asp:Panel ID="pnlEmailButtons" runat="server">
        <asp:Label ID="lblDebug" runat="server"></asp:Label>
         <asp:Button ID="btnShowAll" runat="server" OnClick="btnShowAll_Click" 
                Text="Show All" />
            &nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnReply" runat="server" onclick="btnReply_Click" Text="Reply" 
                Visible="true" />
               
            &nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnReplyAll" runat="server" onclick="btnReplyAll_Click" 
                Text="Reply All" Visible="true" />
            <br />
            <asp:Label ID="lblEmail" runat="server"></asp:Label>
            <table border="0">
                <tr>
                    <td>
                        <asp:Label ID="lblAttachment" runat="server" Text="Attachments" 
                            Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkBtnAtt1" runat="server" onclick="lnkBtnAtt1_Click" 
                            Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkBtnAtt2" runat="server" onclick="lnkBtnAtt1_Click" 
                            Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkBtnAtt3" runat="server" onclick="lnkBtnAtt1_Click" 
                            Visible="false" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkBtnAtt4" runat="server" onclick="lnkBtnAtt1_Click" 
                            Visible="false" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkBtnAtt5" runat="server" onclick="lnkBtnAtt1_Click" 
                            Visible="false" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <br />
        </asp:Panel>
        
        
           <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" ID="upEmailbody">
     <ContentTemplate>
   
     <script type="text/javascript">

$(document).ready(function(){

for(var i=0;i< $('.showAllEmail').length;i++){
var d = $('.showAllEmail').eq(i);

var gwID = d.parent(".emailParent").find('input[type="hidden"]').attr("value");
displayEmail(d,gwID);

}






});
$(document)
  .ajaxStart(function () {
   $("#wimMail_btnReply").addClass("hidden");
   $("#loading-icon").removeClass("hidden");
  })
  .ajaxStop(function () {
   $("#wimMail_btnReply").removeClass("hidden");
   $("#loading-icon").addClass("hidden");
  });



function forLoop(){
for(var i=0;i< $('.showAllEmail').length;i++){
var d = $('.showAllEmail').eq(i);

var gwID = d.parent(".emailParent").find('input[type="hidden"]').attr("value");
displayEmail(d,gwID);

}

}

function displayEmail(d,gwID){ //display Email in body

$.ajax({
            url: 'viewmail.aspx?m=' + gwID,
            success: function (data) {
               var header = $(data).find("#wimMail_lblEmail").html();
                var email = $(data).find("#wimMail_lblHTML").html();
         
           
                d.append(header + "<br/>" + email + "<hr style='border:red solid 5px;height:0;width:100%'/>");
                var e = d;
               
                   $.ajax({
                type: "POST", //Send Email to Session
                url:"/viewmail.aspx/EmailString",
               
                data: JSON.stringify({email:e.html()}),
                dataType:'text', 
                contentType: "application/json; charset=utf-8",
               success: function(data){
               console.log("success");
           
            
               },
               error: function (ex) {
                    console.log(ex);
                 
                }
               
                });
            
             },
            error: function (ex) {
                console.log(ex);
            }
   });
return "done";
}
</script>
 
 <asp:Repeater ID="RptEmails" runat="server">
 <ItemTemplate>
 <div class="emailParent">
 <asp:Literal runat="server" ID="litEmailBody"><div id="emailContent" class="showAllEmail">
 </div></asp:Literal>
 <asp:HiddenField runat="server" ID="hfEmailID" Value='<%#Eval("GroupwiseID").ToString()%>' />
 </div>
 </ItemTemplate> 
 
 </asp:Repeater>
     </ContentTemplate>
     </asp:UpdatePanel>
        <asp:Panel ID="pnlEmail" runat="server" Visible="true">
        <!--
        <asp:Button ID="btnClose" runat="server" Text="Close Email"  />
        --> 
        
           
            <asp:Label ID="lblHTML" runat="server"></asp:Label>
            <asp:TextBox ID="txtEmailMsg" runat="server" Rows="30" TextMode="MultiLine" 
                Visible="false" Width="800px"></asp:TextBox>
            <br />
            <hr />
            <br />
        </asp:Panel>
  <wimcoCustom:EmailForm ID="EmailForm" runat="server" Visible="false" />
        <asp:HiddenField runat="server" ID ="hfThread" />
        <asp:Label ID="lblTempStorage" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblTempStorageEmailTo" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblTempStorageEmailFrom" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblTempStorageEmailFromName" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblTempStorageSubject" runat="server" Visible="false"></asp:Label>
        <asp:Label ID="lblTempStorageGroupWiseID" runat="server" Visible="false"></asp:Label>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="SearchByEmail" TypeName="WimcoBaseLogic.BaseLogic">
            <SelectParameters>
                <asp:Parameter DefaultValue="" Name="Email" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <%-- SelectCommand="Select et.TemplateTextHTML From EmailTemplate et Inner Join EmailTemplateUserRef etur on et.EmailTemplateID = etur.EmailTemplateID where et.TemplateKindID = @TemplateKindID and etur.UserID = @UserID and et.IsEnabled = 'True'" SelectCommandType="Text" --%>
    
<%--        <asp:Timer ID="timer1" runat="server" Enabled="false" ontick="timer_Tick">
        </asp:Timer>--%>
</div>

