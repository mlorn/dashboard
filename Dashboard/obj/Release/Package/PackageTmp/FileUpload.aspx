﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileUpload.aspx.cs" Inherits="VillaOwner.FileUpload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>File Uploader</title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:ToolkitScriptManager runat="server" ID="scrpMgr"></asp:ToolkitScriptManager> 
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src='http://static.wimco.com/js/jquery.js' ></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
         <div style="width:150px" id="upload">
              <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" ID="upAttachments"><ContentTemplate>
                <asp:FileUpload ID="fuAttach" Visible="true" runat="server" /><br />
                <br />
                <asp:Button ID="btnAttach" runat="server" Visible="true" OnClick="btnAttach_Click"
                    Text="Attach" /> <br />
                    <asp:Literal runat="server" ID="litFileSize"></asp:Literal>
                    <asp:Literal runat="server" ID="litAttachments" />
                    <br /><asp:ListBox runat="server" ID="lbAttachments" SelectionMode="Multiple" AutoPostBack="false">
                    </asp:ListBox><br /><br />
                    <asp:Button ID="btnRemoveSelected" text="Remove Selected" runat="server" OnClick="btnRemoveSelected_Click" />
                    </ContentTemplate><Triggers>
        <asp:PostBackTrigger ControlID="btnAttach" />
        <asp:PostBackTrigger ControlID="btnRemoveSelected" />
</Triggers>

</asp:UpdatePanel>
                    </div>
    </form>
</body>
</html>
