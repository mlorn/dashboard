﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using VillaOwner;
namespace fccCustom
{
    public partial class WimcoFCC : System.Web.UI.UserControl
    {

        public int WimcoClientID { get; set; }
        public int WimcoAgentID { get; set; }
        public bool ShowFCCEntry { get; set; }
        public bool ShowNoteEntry { get; set; }


        public void RebindGrid()
        {
           // pnlFCCEdit.Visible = false;
            grdFCC.DataBind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ShowFCCEntry)
                {
                    Object o = new Object();
                    EventArgs ee = new EventArgs();
                    btnAddNewFCC_Click(o, ee);
                }

                if (ShowNoteEntry)
                {
                    Object o = new Object();
                    EventArgs ee = new EventArgs();
                    btnAddNewAdminProfile_Click(o, ee);
                }
            }
            try
            {
                WimcoClientID = Int32.Parse(Session["ClientID"].ToString());
                odsFCC.DataBind();
                grdFCC.DataBind();
            }
            catch { }

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (wimAddFCC.Attributes["purpose"] == "none" || wimAddFCC.Attributes["purpose"] == "submitted" || wimAddFCC.Attributes["purpose"] == "cancel")
            {
                if (pnlEditNote.Visible == false && pnlFCCEdit.Visible == false)
                {
                    pnlMainFCC.Visible = true;
                }
            }
            if (wimAddFCC.Attributes["purpose"] == "redo") {

                pnlMainFCC.Visible = false;
            }

        }

        protected void odsFCCSelecting(object source, ObjectDataSourceMethodEventArgs e)
        {
            e.InputParameters["ClientID"] = WimcoClientID;
            //string Name = WimcoBaseLogic.BaseLogic.AdHocSeek("SELECT FirstName + ' ' + LastName AS Name FROM  Client WHERE (ClientID = " + WimcoClientID + ")", "Name");
            //btnAddNewFCC.Text = "Add New FCC for " + Name;
            btnAddNewFCC.Text = "Add New FCC";
        }
        protected void grdFCC_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    ((Button)e.Row.Cells[0].Controls[0]).OnClientClick = "focusFCC(1200);";
                }
                catch { }
            }
           
        }
        //protected void grdFCC_OnRowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Footer)
        //    {
        //        if (e.Row.Cells.Count > 1){
        //            for (int i = 1; i < e.Row.Cells.Count; i++)
        //            {
        //                e.Row.Cells.RemoveAt(i);
        //            }
        //        }
        //    }

        //}



        protected void grdFCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridView gv = (GridView)sender;
                DataKey k = gv.SelectedDataKey;
                int CallBackID = (Int32)k[0];
                lblTempData.Text = CallBackID.ToString();
                pnlMainFCC.Visible = false;

                              
        
                WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
                fcc = WimcoBaseLogic.BaseLogic.GetFCC(CallBackID);
                ListItem li639 = new ListItem("NTI-EML New Inquiry-E-MAIL", "639");
                ListItem li648 = new ListItem("NTI-ENL New Inquiry-ENewsletter", "648");
                ListItem li640 = new ListItem("NTI-PHO New Inquiry-PHONE", "640");
                ListItem li646 = new ListItem("NTI-CUS New Inquiry-Contact Us", "646");
                ListItem li647 = new ListItem("NTI-CHA New Inquiry-CHAT", "647");
                ListItem li701 = new ListItem("NTI-RFA New Inquiry-Req For Avail", "701");
                ListItem li826 = new ListItem("NTI-HOME New Inquiry-Homeaway", "826");
                ListItem li904 = new ListItem("NTI-CAN New Inquiry-canadastays", "904");
                ListItem li728 = new ListItem("NTI_FLP New Inquiry-Flipkey", "728");
                ListItem li915 = new ListItem("NTI-ABNB New Inquiry-AirBnB", "915");
                ListItem li722 = new ListItem("NTI-TA New Inquiry-TA", "722");
                ListItem li390 = new ListItem("CLP Client Prospecting", "390");
                ListItem li803 = new ListItem("NTI-HTL New Inquiry-Hotel RFA", "803");
                ListItem li673 = new ListItem("NL New Lead Worked by Agent", "673"); 	
                //Load Preferred Destinations:
                //ListItem li487 = new ListItem("St Barthelemy - SBH", "487");
                //ListItem li503 = new ListItem("Anguilla - AXA", "503");
                //ListItem li506 = new ListItem("Barbados - BGI", "506");
                //ListItem li518 = new ListItem("France - FRA", "518");
                //ListItem li519 = new ListItem("Grand - Cayman GCM", "519");
                //ListItem li522 = new ListItem("Greece - GRC", "522");
                //ListItem li527 = new ListItem("Italy - ITA", "527");
                //ListItem li529 = new ListItem("Jamaica - MBJ", "529");
                //ListItem li1158 = new ListItem("Mexico - MEX", "1158");
                //ListItem li561 = new ListItem("Mustique - MQS", "561");
                //ListItem li1177 = new ListItem("Nantucket - ACK", "1177");
                //ListItem li535 = new ListItem("Necker - NEC", "535");
                //ListItem li536 = new ListItem("Nevis - NEV", "536");
                //ListItem li1234 = new ListItem("New York - NYS", "1234");
                //ListItem li1033 = new ListItem("Peter Island - PET", "1033");
                //ListItem li540 = new ListItem("Saba - SAB", "540");
                //ListItem li542 = new ListItem("St John - SJF", "542");
                //ListItem li553 = new ListItem("St Martin - SXM", "553");
                //ListItem li549 = new ListItem("St Thomas - STT", "549");
                //ListItem li514 = new ListItem("Tortola - EIS", "514");
                //ListItem li537 = new ListItem("Turks & Caicos - PLS", "537");
                //ListItem li1260 = new ListItem("Uruguay - URU", "1260");
                //ListItem li558 = new ListItem("Virgin Gorda - VIJ", "558");
                //ListItem li1108 = new ListItem("Caribbean - CCC", "1108");
                //ListItem li1109 = new ListItem("Europe - EEE", "1109"); 


                bool icFCC = true;
                // is this a FCC or Note?
                if (fcc.MocID == 5 | fcc.MocID == 9)
                {
                    icFCC = false;
                }

                if (!icFCC) // pop Note editor.
                {

                    txtNoteEdit.Text = fcc.Description;


                    rdoNoteEdit.ClearSelection();
                    foreach (ListItem li in rdoNoteEdit.Items)
                    {
                        if (li.Value == fcc.MocID.ToString())
                        {
                            li.Selected = true;
                        }
                    }



                    ddlNoteEditAssigned.Items.Clear();
                    DataSet ds = WimcoBaseLogic.BaseLogic.AdHoc("SELECT MagUser.UserID, MagUser.FullName FROM  MagUser INNER JOIN UserGroup ON MagUser.UserID = UserGroup.UserID INNER JOIN MagGroup ON UserGroup.GroupID = MagGroup.GroupID WHERE (MagUser.IsEnabled = 'Y') AND (MagUser.OrganizationID = 1) AND (UserGroup.GroupID = 186) ORDER BY MagUser.FullName");
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ListItem li = new ListItem(dr[1].ToString(), dr[0].ToString());

                        if (fcc.AgentID.ToString() == li.Value)
                        {
                            li.Selected = true;
                        }
                        ddlNoteEditAssigned.Items.Add(li);
                    }

                    ddlSourceNote.ClearSelection();

                    DataSet dscsn = WimcoBaseLogic.BaseLogic.AdHoc("SELECT Code + ' ' + Name, ContactSourceID FROM    ContactSource WHERE IsEnabled = 'Y' AND ( (NOT (Name IS NULL)) OR (NOT (Name = ''))) ORDER BY Code");
                    foreach (DataRow dr in dscsn.Tables[0].Rows)
                    {
                        ListItem li = new ListItem(dr[0].ToString(), dr[1].ToString());
                        if (dr[0].ToString().Length > 0)
                        {
                            if (fcc.ContactSourceID.ToString() == li.Value)
                            {
                                li.Selected = true;
                            }

                            ddlSourceNote.Items.Add(li);
                        }
                    }
                    //Add Preferred Sources:
                    //Load preferred Sources:
     
                    ddlSourceNote.Items.Insert(0, "=======================");
                    ddlSourceNote.Items.Insert(0, li673);
                    ddlSourceNote.Items.Insert(0, li803);
                    ddlSourceNote.Items.Insert(0, li390);
                    ddlSourceNote.Items.Insert(0, li722);
                    ddlSourceNote.Items.Insert(0, li915);
                    ddlSourceNote.Items.Insert(0, li728);
                    ddlSourceNote.Items.Insert(0, li904);
                    ddlSourceNote.Items.Insert(0, li826);
                    ddlSourceNote.Items.Insert(0, li640);
                    ddlSourceNote.Items.Insert(0, li647);
                    ddlSourceNote.Items.Insert(0, li646);
                    ddlSourceNote.Items.Insert(0, li701);
                    ddlSourceNote.Items.Insert(0, li648);
                    ddlSourceNote.Items.Insert(0, li639);
                    ddlSourceNote.Items.Insert(0, "SELECT A SOURCE");

                    ddlNoteDest.Items.Clear();
                   // DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("SELECT Name + ' - ' + DestinationCode AS Name, DestinationID FROM  Destination ORDER BY DestinationCode");
                    //DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("SELECT DestinationCode, DestinationID FROM Destination ORDER BY DestinationCode");
                    DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("Exec spGetApprovedDestinationList @External = 0");
                    
                    foreach (DataRow dr in dsDest.Tables[0].Rows)
                    {
                        ListItem li = new ListItem(dr[2].ToString(), dr[1].ToString());

                        if (fcc.DestinationID.ToString() == li.Value)
                        {
                            li.Selected = true;

                        }

                        ddlNoteDest.Items.Add(li);

                    }
                    //ddlNoteDest.Items.Insert(0, li1109);
                    //ddlNoteDest.Items.Insert(0, li1108);
                    //ddlNoteDest.Items.Insert(0, li558);
                    //ddlNoteDest.Items.Insert(0, li1260);
                    //ddlNoteDest.Items.Insert(0, li537);
                    //ddlNoteDest.Items.Insert(0, li514);
                    //ddlNoteDest.Items.Insert(0, li549);
                    //ddlNoteDest.Items.Insert(0, li553);
                    //ddlNoteDest.Items.Insert(0, li542);
                    //ddlNoteDest.Items.Insert(0, li540);
                    //ddlNoteDest.Items.Insert(0, li1033);
                    //ddlNoteDest.Items.Insert(0, li1234);
                    //ddlNoteDest.Items.Insert(0, li536);
                    //ddlNoteDest.Items.Insert(0, li535);
                    //ddlNoteDest.Items.Insert(0, li1177);
                    //ddlNoteDest.Items.Insert(0, li561);
                    //ddlNoteDest.Items.Insert(0, li1158);
                    //ddlNoteDest.Items.Insert(0, li529);
                    //ddlNoteDest.Items.Insert(0, li527);
                    //ddlNoteDest.Items.Insert(0, li522);
                    //ddlNoteDest.Items.Insert(0, li519);
                    //ddlNoteDest.Items.Insert(0, li518);
                    //ddlNoteDest.Items.Insert(0, li506);
                    //ddlNoteDest.Items.Insert(0, li503);
                    //ddlNoteDest.Items.Insert(0, li487);
                    pnlEditNote.Visible = true;
                }

                if (icFCC) // pop FCC editor.
                {

                  

                    lblCreate.Text = gv.SelectedRow.Cells[3].Text; //((GridView)sender).SelectedRow.ToString("{0:d}");

                    pnlMainFCC.Visible = false;
                    EditFCC.CallBackID = CallBackID;
                    EditFCC.loadFCC();
                    pnlFCCEdit.Visible = true;
                }

            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
        }
    
      
 //       protected void btnModify_Click(object sender, EventArgs e)

 //       {
 //           VillaOwner.BaseLogic.UserData udo = VillaOwner.BaseLogic.GetUserData();
 //           //if (ddlMocEdit.SelectedValue == "10")
 //           //{
 //           //   goto icCloseJmp;
 //           // }
          
 //        //   else
 //          // {
             


 //               if (Convert.ToDateTime(txtCalEdit.Text) < DateTime.Today)
 //               {
 //                   lblInformation.Text = "Your FCC has <bold>NOT</bold> been Modified. Choose a date in the Future!<br/>";
 //                   lblInformation.Visible = true;
 //                   timer.Interval = 8000;
 //                   timer.Enabled = true;
 //                   goto jmp;

 //               }

 //           icCloseJmp: ;

 //               Int32 CallBackID = Convert.ToInt32(lblTempData.Text);
 //               WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
 //               fcc = WimcoBaseLogic.BaseLogic.GetFCC(CallBackID);
 //               fcc.AgentID = Convert.ToInt32(ddlAssigned.SelectedValue);
 //               fcc.MocID = Convert.ToInt32(ddlMocEdit.SelectedValue);
 //               fcc.ContactSourceID = Convert.ToInt32(ddlSourceName.SelectedValue);
 //               fcc.CallbackDate = Convert.ToDateTime(txtCalEdit.Text);
 //               fcc.LeadScoreID = Convert.ToInt32(ddlLeadScore.SelectedValue);
 //               fcc.DestinationID = Convert.ToInt32(ddlDest.SelectedValue);
 //               fcc.Description = txtFccDisplay.Text;
 //               if (ddlFCCType.SelectedValue != "")
 //               {
 //                   fcc.CallbackContactSourceID = Convert.ToInt32(ddlFCCType.SelectedValue);
 //               }
 //               else {
 //                   ddlFCCType.BorderColor = System.Drawing.Color.Red;
 //                   ddlFCCType.BorderWidth = 2;
 //                   return;
 //               }
 //               pnlMainFCC.Visible = true;
 //               pnlFCCEdit.Visible = false;
 //             WimcoBaseLogic.BusinessLogic.FCCDataReturn fdr = WimcoBaseLogic.BaseLogic.UpdateFCC(fcc);
 //               UpdateFCCHistory(fcc);
                
 //               RebindGrid();
 //if (ddlMocEdit.SelectedValue.ToString() == "10" && !udo.Manager) //if Closing FCC and if user isn't a manager, contact Sales Manager
 //           {

 //               string strSMEmail = "ACaye@wimco.com";
 //              // strSMEmail = "damaral@wimco.com"; //for testing
 //               VillaOwner.BusinessLogic.flatClient fc = VillaOwner.BaseLogic.flatclient_GetClient(WimcoClientID);
 //               string clientinfo = "Client: " + fc.LastName + ", " + fc.FirstName + " (" + WimcoClientID.ToString() + ")<br />";
 //               string fccdetails = "Order Number: " + fcc.CallbackID + "<br />";
 //               fccdetails += "Callback: " + fcc.CallbackDate.ToString("{0:d}") + "<br />";
               
 //    fccdetails += "Created: " + lblCreate.Text + "<br />";
 //               fccdetails += "Lead: " + ddlLeadScore.SelectedItem.Text + "<br />";
 //               fccdetails += "Notes: " + fcc.Description + "<br />";
 //               fccdetails += "Assigned: " + ddlAssigned.SelectedItem.Text + "<br />";
 //               fccdetails += "Source: " + ddlSourceName.SelectedItem.Text + "<br />";
 //               fccdetails += "Destination: " + ddlDest.SelectedItem.Text + "<br />";
               
 //       //        VillaOwner.BaseLogic.sendHTMLEmail(udo.EmailAddress, udo.FullName, "Nomad: Closing FCC #" + lblTempData.Text, strSMEmail, udo.EmailAddress, clientinfo + fccdetails);
             
            
 //           }
 //               lblInformation.Text = "Your FCC has been Modified.<br/>";
 //               lblInformation.Visible = true;
 //               timer.Interval = 2000;
 //               timer.Enabled = true;

 //           jmp: ;
 //         //  }
 //       }
 //       protected void btnCancel_Click(object sender, EventArgs e)
 //       {
 //           pnlMainFCC.Visible = true;
 //           pnlFCCEdit.Visible = false;
 //         //  pnlNewFCC.Visible = false;
 //           RebindGrid();

 //           lblInformation.Text = "Cancel. No Changes Made.<br/>";
 //           lblInformation.Visible = true;
 //           timer.Interval = 2000;
 //           timer.Enabled = true;
 //       }

        protected void btnAddNewFCC_Click(object sender, EventArgs e)
        {

           wimAddFCC.Attributes["purpose"] =  "addFCC";
           wimAddFCC.WimcoClientID = WimcoClientID;
            pnlMainFCC.Visible = false;
        }

        //protected void btnNewFCC_Click(object sender, EventArgs e)
        //{
        //    // adding new FCC here.

        //    if (txtNewFCCDisplay.Text == "")
        //    {
        //        lblInformation.Text = "Your FCC has <bold>NOT</bold> been Created. Add a Description!<br/>";
        //        lblInformation.Visible = true;
        //        timer.Interval = 8000;
        //        timer.Enabled = true;
        //        goto jmp;
        //    }

        //    if (txtNewFCCDisplay.Text.Length > 199)
        //    {
        //        lblInformation.Text = "Your FCC has <bold>NOT</bold> been Created. Description to long. (200max)<br/>";
        //        lblInformation.Visible = true;
        //        timer.Interval = 8000;
        //        timer.Enabled = true;
        //        goto jmp;

        //    }


        //    if (Convert.ToDateTime(txtNewFCCDate.Text) < DateTime.Today)
        //    {
        //        lblInformation.Text = "Your FCC has <bold>NOT</bold> been Created. Choose a date in the Future!<br/>";
        //        lblInformation.Visible = true;
        //        timer.Interval = 8000;
        //        timer.Enabled = true;
        //        goto jmp;

        //    }

        //    WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
        //    //fcc.AgentID = WimcoAgentID;




        //    fcc.CallbackDate = Convert.ToDateTime(txtNewFCCDate.Text);
        //    //fcc.CallbackDate = fccCalendarNew.SelectedDate;
        //    //fcc.CallbackID;            

        //    fcc.AgentID = Convert.ToInt32(ddlAssignNew.SelectedValue);

        //    fcc.ClientID = WimcoClientID;
        //    fcc.ContactSourceID = Convert.ToInt32(ddlNewSource.SelectedValue);
        //    fcc.Description = txtNewFCCDisplay.Text;
        //    fcc.DestinationID = Convert.ToInt32(ddlNewDest.SelectedValue);
        //    fcc.MocID = Convert.ToInt32(ddlMoc.SelectedValue);
        //    fcc.IsEnabled = "Y";
        //    fcc.LeadScoreID = Convert.ToInt32(ddlHeat.SelectedValue);
        //    fcc.ProductBaseID = 600492;
        //    WimcoBaseLogic.BusinessLogic.FCCDataReturn fccd = new WimcoBaseLogic.BusinessLogic.FCCDataReturn();
        //    fccd = WimcoBaseLogic.BaseLogic.InsertFCC(fcc);
        //    fcc.CallbackID = fccd.CallbackID;
        //    UpdateFCCHistory(fcc);



        //    RebindGrid();


        //    lblInformation.Text = "Your FCC has been Created.<br/>";
        //    lblInformation.Visible = true;
        //    timer.Interval = 2000;
        //    timer.Enabled = true;

        //jmp: ;

        //    pnlMainFCC.Visible = true;
        //    pnlFCCEdit.Visible = false;
        //    pnlNewFCC.Visible = false;


        //}

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            pnlMainFCC.Visible = true;
            pnlFCCEdit.Visible = false;

            Int32 CallBackID = Convert.ToInt32(lblTempData.Text);
            WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
            fcc = WimcoBaseLogic.BaseLogic.GetFCC(CallBackID);
            fcc.MocID = 6;
            fcc.IsEnabled = "N";
            WimcoBaseLogic.BusinessLogic.FCCDataReturn fccd = new WimcoBaseLogic.BusinessLogic.FCCDataReturn();
            fccd = WimcoBaseLogic.BaseLogic.DeleteFCC(fcc);
            UpdateFCCHistory(fcc);
            RebindGrid();

            lblInformation.Text = "Your FCC has been Deleted.<br/>";
            lblInformation.Visible = true;
            timer.Interval = 2000;
            timer.Enabled = true;
        }


        protected void timer_Tick(object sender, EventArgs e)
        {

            lblInformation.Text = "";
            lblInformation.Visible = false;
            timer.Enabled = false;
        }


        public void UpdateFCCHistory(WimcoBaseLogic.BusinessLogic.FCCobj fcc)
        {
            WimcoBaseLogic.BusinessLogic.FCC_HistoryDS hist = new WimcoBaseLogic.BusinessLogic.FCC_HistoryDS();
            WimcoBaseLogic.BusinessLogic.FCC_HistoryDS.FCC_HistoryRow hr = (WimcoBaseLogic.BusinessLogic.FCC_HistoryDS.FCC_HistoryRow)hist.FCC_History.NewFCC_HistoryRow();
            try
            {
                hr.CallbackDate = fcc.CallbackDate;
            }
            catch { }

            hr.CallbackID = fcc.CallbackID;
            hr.ChangeUserID = WimcoAgentID;
            hr.DateModified = DateTime.Now;
            hr.Description = fcc.Description;
            hr.FCC_HistoryID = Guid.NewGuid().ToString();
            hr.MagUserID = fcc.AgentID;
            hr.LeadScore = fcc.LeadScoreID;
            hr.MocID = fcc.MocID;

            if (hr.MocID == 5 | hr.MocID == 9)
            {
                hr.SetCallbackDateNull();
            }


            hist.FCC_History.AddFCC_HistoryRow(hr);

            WimcoBaseLogic.BaseLogic.UpdateDataSet(hist);

        }


        protected void btnAddNewAdminProfile_Click(object sender, EventArgs e)
        {
            pnlMainFCC.Visible = false;
            wimAddFCC.Attributes["purpose"] = "addNote";
            wimAddFCC.WimcoClientID = WimcoClientID;
            //pnlNewNote.Visible = true;
            //// pnlFCCEdit.Visible = false;
            //// pnlNewFCC.Visible = false;

            //txtNewNote.Text = "";



            //ddlNewNoteAssigned.Items.Clear();
            //DataSet ds = WimcoBaseLogic.BaseLogic.AdHoc("SELECT MagUser.UserID, MagUser.FullName FROM  MagUser INNER JOIN UserGroup ON MagUser.UserID = UserGroup.UserID INNER JOIN MagGroup ON UserGroup.GroupID = MagGroup.GroupID WHERE (MagUser.IsEnabled = 'Y') AND (MagUser.OrganizationID = 1) AND (UserGroup.GroupID = 186) ORDER BY MagUser.FullName");
            //foreach (DataRow dr in ds.Tables[0].Rows)
            //{
            //    ListItem li = new ListItem(dr[1].ToString(), dr[0].ToString());

            //    if (WimcoAgentID.ToString() == li.Value)
            //    {
            //        li.Selected = true;
            //    }
            //    ddlNewNoteAssigned.Items.Add(li);
            //}


            //ddlNewSourceNote.Items.Clear();
            //DataSet dscsn = WimcoBaseLogic.BaseLogic.AdHoc("SELECT Name, ContactSourceID, IsEnabled FROM  ContactSource WHERE (NOT (Name IS NULL)) AND (IsEnabled = 'Y') OR  (NOT (Name = '')) ORDER BY Name");
            //foreach (DataRow dr in dscsn.Tables[0].Rows)
            //{
            //    ListItem li = new ListItem(dr[0].ToString(), dr[1].ToString());
            //    if (dr[0].ToString().Length > 0)
            //    {
            //        if (li.Value == "889")
            //        {
            //            li.Selected = true;
            //        }

            //        ddlNewSourceNote.Items.Add(li);
            //    }
            //}


            //ddlNewNoteDest.Items.Clear();
            //DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("SELECT Name + ' - ' + DestinationCode AS Name, DestinationID FROM  Destination ORDER BY DestinationCode");
            ////DataSet dsDest = WimcoBaseLogic.BaseLogic.AdHoc("SELECT DestinationCode, DestinationID FROM Destination ORDER BY DestinationCode");
            //foreach (DataRow dr in dsDest.Tables[0].Rows)
            //{
            //    ListItem li = new ListItem(dr[0].ToString(), dr[1].ToString());

            //    if ("487" == li.Value)
            //    {
            //        li.Selected = true;

            //    }

            //    ddlNewNoteDest.Items.Add(li);

            //}


            //rdoNewNote.Items[0].Selected = true;

        }
      
        protected void btnNoteModify_Click(object sender, EventArgs e)
        {

            pnlEditNote.Visible = false;
            pnlMainFCC.Visible = true;
        
         
            Int32 CallBackID = Convert.ToInt32(lblTempData.Text);
            WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
            fcc = WimcoBaseLogic.BaseLogic.GetFCC(CallBackID);
            fcc.Description = txtNoteEdit.Text;
            fcc.DestinationID = Convert.ToInt32(ddlNoteDest.SelectedValue);
            fcc.AgentID = Convert.ToInt32(ddlNoteEditAssigned.SelectedValue);
            fcc.MocID = Convert.ToInt32(rdoNoteEdit.SelectedValue);
            fcc.ContactSourceID = Convert.ToInt32(ddlSourceNote.SelectedValue);
           
                WimcoBaseLogic.BaseLogic.UpdateFCC(fcc);
                UpdateFCCHistory(fcc);

                RebindGrid();

                string fType = "";
                if (fcc.MocID == 5) { fType = "Admin Record"; }
                if (fcc.MocID == 9) { fType = "Profile Record"; }

                lblInformation.Text = "Your " + fType + " has been Modified.<br/>";
                lblInformation.Visible = true;
                timer.Interval = 2000;
                timer.Enabled = true;
            
        }

        protected void btnNoteCancel_Click(object sender, EventArgs e)
        {
            pnlMainFCC.Visible = true;
            pnlFCCEdit.Visible = false;
          //  pnlNewFCC.Visible = false;
            pnlEditNote.Visible = false;
     //       pnlNewNote.Visible = false;
            RebindGrid();

            lblInformation.Text = "Cancel. No Changes Made.<br/>";
            lblInformation.Visible = true;
            timer.Interval = 2000;
            timer.Enabled = true;
        }

        protected void btnNoteDelete_Click(object sender, EventArgs e)
        {
            pnlMainFCC.Visible = true;
            pnlFCCEdit.Visible = false;
          //  pnlNewFCC.Visible = false;
            pnlEditNote.Visible = false;
           // pnlNewNote.Visible = false;

            Int32 CallBackID = Convert.ToInt32(lblTempData.Text);
            WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
            fcc = WimcoBaseLogic.BaseLogic.GetFCC(CallBackID);
            WimcoBaseLogic.BusinessLogic.FCCDataReturn fccd = new WimcoBaseLogic.BusinessLogic.FCCDataReturn();
            fccd = WimcoBaseLogic.BaseLogic.DeleteFCC(fcc);
            UpdateFCCHistory(fcc);
            RebindGrid();

            string fType = "";
            if (fcc.MocID == 5) { fType = "Admin Record"; }
            if (fcc.MocID == 9) { fType = "Profile Record"; }

            lblInformation.Text = "Your " + fType + " has been Modified.<br/>";

            lblInformation.Visible = true;
            timer.Interval = 2000;
            timer.Enabled = true;
        }

        protected void chkLstNote_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

      //protected void btnNewNoteAdd_Click(object sender, EventArgs e)
      //  {

      //      if (txtNewNote.Text == "")
      //      {
      //          lblInformation.Text = "Your record has <bold>NOT</bold> been Created. Add a Description!<br/>";
      //          lblInformation.Visible = true;
      //          timer.Interval = 8000;
      //          timer.Enabled = true;
      //          goto jmp;
      //      }

      //      if (txtNewNote.Text.Length > 199)
      //      {
      //          lblInformation.Text = "Your record has <bold>NOT</bold> been Created. Description to long. (200max)<br/>";
      //          lblInformation.Visible = true;
      //          timer.Interval = 8000;
      //          timer.Enabled = true;
      //          goto jmp;

      //      }





      //      WimcoBaseLogic.BusinessLogic.FCCobj fcc = new WimcoBaseLogic.BusinessLogic.FCCobj();
      //      fcc.AgentID = Convert.ToInt32(ddlNewNoteAssigned.SelectedValue);
      //      fcc.ClientID = WimcoClientID;
      //      fcc.ContactSourceID = 6;
      //      fcc.Description = txtNewNote.Text;

      //      fcc.MocID = Convert.ToInt32(rdoNewNote.SelectedValue);
      //      fcc.IsEnabled = "Y";
      //      fcc.LeadScoreID = 3;
      //      fcc.ProductBaseID = 600492;
      //      fcc.DestinationID = Convert.ToInt32(ddlNewNoteDest.SelectedValue);
      //      fcc.ContactSourceID = Convert.ToInt32(ddlNewSourceNote.SelectedValue);
      //      WimcoBaseLogic.BusinessLogic.FCCDataReturn fccd = new WimcoBaseLogic.BusinessLogic.FCCDataReturn();
      //      fccd = WimcoBaseLogic.BaseLogic.InsertFCC(fcc);
      //      fcc.CallbackID = fccd.CallbackID;
      //      UpdateFCCHistory(fcc);

      //  jmp: ;

      //      RebindGrid();
      //      pnlNewNote.Visible = false;
      //      pnlMainFCC.Visible = true;

      //  }


        protected void cmdFCCHistory(Object sender, CommandEventArgs e)
        {
            int CallBackID = Convert.ToInt32(e.CommandArgument.ToString());
            DataSet ds = GetFCCHistory(CallBackID);
            grdFCCHistory.DataSource = ds.Tables[0];
            grdFCCHistory.DataBind();
            pnlMainFCC.Visible = false;
            pnlFCCHistory.Visible = true;
        }

        public string GetFCCType(object o) {
            String rString = "";
            if (o != null)
            {
                String s = o.ToString();
                switch (s){
                    case "983":

                       rString = "Opportunity";
                        break;
                    case "982":
                        rString = "Service";
                        break;
                    case "390":
                        rString = "Client Prospecting";
                        break;
                   
                                        
                }

            }
            return rString;
        
        }
        public DataTable GetFCCHistoryTable(int CallbackID)
        {
            DataSet ds = new DataSet();
            ds = WimcoBaseLogic.BaseLogic.AdHoc("SELECT FCC_History.CallbackDate, Callback.CreateDate, FCC_History.DateModified, FCC_History.Description AS Note, MagUser.FullName AS Assigned, MagUser_1.FullName AS ChangedBy, MethodOfCommunication.MocDesc AS MOC, LeadScore.Description AS Lead FROM FCC_History INNER JOIN MagUser ON FCC_History.MagUserID = MagUser.UserID INNER JOIN MagUser AS MagUser_1 ON FCC_History.ChangeUserID = MagUser_1.UserID INNER JOIN MethodOfCommunication ON FCC_History.MocID = MethodOfCommunication.MocID INNER JOIN LeadScore ON FCC_History.LeadScore = LeadScore.LeadScoreID INNER JOIN Callback ON FCC_History.CallbackID = Callback.CallbackID WHERE (FCC_History.CallbackID = " + CallbackID + ") ORDER BY FCC_History.DateModified DESC");
            ds.Tables[0].TableName = "FCCHistory";
            ds.DataSetName = "FCCHistoryDS";
            return ds.Tables[0];
        }

        public DataSet GetFCCHistory(int CallbackID)
        {
            DataSet ds = new DataSet();
            ds = WimcoBaseLogic.BaseLogic.AdHoc("SELECT FCC_History.CallbackDate, Callback.CreateDate, FCC_History.DateModified, FCC_History.Description AS Note, MagUser.FullName AS Assigned, MagUser_1.FullName AS ChangedBy, MethodOfCommunication.MocDesc AS MOC, LeadScore.Description AS Lead FROM FCC_History INNER JOIN MagUser ON FCC_History.MagUserID = MagUser.UserID INNER JOIN MagUser AS MagUser_1 ON FCC_History.ChangeUserID = MagUser_1.UserID INNER JOIN MethodOfCommunication ON FCC_History.MocID = MethodOfCommunication.MocID INNER JOIN LeadScore ON FCC_History.LeadScore = LeadScore.LeadScoreID INNER JOIN Callback ON FCC_History.CallbackID = Callback.CallbackID WHERE (FCC_History.CallbackID = " + CallbackID + ") ORDER BY FCC_History.DateModified DESC");
            ds.Tables[0].TableName = "FCCHistory";
            ds.DataSetName = "FCCHistoryDS";
            return ds;
        }

        protected void btnFCCHistoryClose_Click(object sender, EventArgs e)
        {
            pnlFCCHistory.Visible = false;
            pnlMainFCC.Visible = true;
        }

    }

}