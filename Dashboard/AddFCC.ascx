﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddFCC.ascx.cs" Inherits="fccCustom.AddFCC" %>
<asp:Panel ID="pnlNewFCC"  runat="server" Visible="false">
<style type="text/css">
.FCCRange{
	position:absolute;
	}
	#ctl00_cphBody_gvClientGrid_ctl02_fvClientDetails_wimcoFCC_wimAddFCC_ddlNewFCCType
	{
		padding-top:5px;padding-bottom:5px;
		}
</style>
    
    <table border="0"  class="emailHead">
      
      
      
        <tr>
            <td>
                Callback:
            </td>
            <td style="padding-right: 4px;width:130px">
                               
                <asp:TextBox ID="txtNewFCCDate" Min='<%# DateTime.Now %>' Max='<%# DateTime.Now.AddMonths(18) %>' TextMode="Date" runat="server" Width="130px" />
                 <asp:RangeValidator BackColor="White" BorderColor="Black" CssClass="FCCRange" ID="RangeValidator1" runat="server" ErrorMessage="Dates must be within 18 months of today" 
                 ControlToValidate="txtNewFCCDate" ></asp:RangeValidator>
               
            </td>
               <td  align="right">
                Type:
            </td>
            <td style="padding-right: 4px;">
               <asp:DropDownList ID="ddlNewFCCType" Width="100%" runat="server">
                 <asp:ListItem Value=""></asp:ListItem>
                 <asp:ListItem Value="390">Client Prospecting</asp:ListItem>
               <asp:ListItem Value="983">Opportunity</asp:ListItem>
                    <asp:ListItem Value="982">Service</asp:ListItem>
                   
                    </asp:DropDownList>
                               
            </td>
        </tr>
       
        <tr>
            <td >
                Assigned:
            </td>
            <td colspan="3">
                <asp:DropDownList style="padding-top:1px;padding-bottom:1px;" ID="ddlAssignNew" runat="server" Width="100%">
                </asp:DropDownList>
            </td>
            </tr><tr>
            <td>
                Lead:
            </td>
            <td>
                <asp:DropDownList style="padding-top:1px;padding-bottom:1px;" ID="ddlHeat" Width="100%" runat="server">
                    <asp:ListItem  Value=""> </asp:ListItem>
                    <asp:ListItem Value="1">Hot</asp:ListItem>
                    <asp:ListItem Value="2">Warm</asp:ListItem>
                    <asp:ListItem Value="3">Cold</asp:ListItem>
                </asp:DropDownList>
            </td>
           <%-- <tr>--%>
                <td align="right">
                    Method:
                </td>
                <td>
                    <asp:DropDownList style="padding-top:1px;padding-bottom:1px;" ID="ddlMoc" Width="100%" runat="server">
                        <asp:ListItem Value="6">Phone</asp:ListItem>
                        <asp:ListItem Value="2">Email</asp:ListItem>
                        <%--<asp:ListItem Value="1">Mail</asp:ListItem>
                        <asp:ListItem Value="3">Fax</asp:ListItem>
                        <asp:ListItem Value="4">Hardcopy</asp:ListItem>
                        <asp:ListItem Value="7">Telex</asp:ListItem>
                        <asp:ListItem Value="8">Other</asp:ListItem>--%>
                        <asp:ListItem Value="10">Close FCC</asp:ListItem>
                    </asp:DropDownList>
                </td>
         <%--   </tr>--%>
            <tr>
                <td>
                    Source:
                </td>
                <td colspan="3">
                    <asp:DropDownList style="padding-top:1px;padding-bottom:1px;" ID="ddlNewSource" Width="100%" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Destination:
                </td>
                <td colspan="3">
                    <asp:DropDownList ID="ddlNewDest" Width="100%" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
             <tr>
            <td>
               FCC Notes:
            </td>
            <td colspan="3" style="padding-right: 5px;">
                <asp:TextBox ID="txtNewFCCDisplay" runat="server" TextMode="MultiLine" Width="100%"
                    Rows="2" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="btnNewFCC" CssClass="FCCButton" OnClientClick="clientOrdersUpdate();" ForeColor="Red" runat="server" Text="Submit New FCC" OnClick="btnNewFCC_Click"
         />&nbsp&nbsp<asp:Button ID="Button2" runat="server"
            Text="Cancel" OnClick="btnCancel_Click"  />
            &nbsp&nbsp<asp:Button OnClientClick="clientOrdersUpdate();" ID="btnFCCLead" Visible="False" runat="server"
            Text="Close Lead and Add FCC" OnClick="btnFCCLead_Click"  />
            
</asp:Panel>
<asp:Panel ID="pnlNewNote" runat="server" Visible="false">
    
    <asp:RadioButtonList ID="rdoNewNote" runat="server" RepeatDirection="Horizontal"
        TextAlign="Left">
        <asp:ListItem Value="5">Admin</asp:ListItem>
        <asp:ListItem Value="9">Profile</asp:ListItem>
    </asp:RadioButtonList>
    <h3>
        Assigned&nbsp&nbsp<asp:DropDownList ID="ddlNewNoteAssigned" runat="server">
        </asp:DropDownList>
    </h3>
        <h3>
            Source&nbsp&nbsp<asp:DropDownList ID="ddlNewSourceNote" runat="server">
            </asp:DropDownList>
        </h3>
    <h3>
        Destination&nbsp&nbsp<asp:DropDownList ID="ddlNewNoteDest" runat="server">
        </asp:DropDownList>
    </h3>
    <br />
    <asp:TextBox ID="txtNewNote" runat="server" TextMode="MultiLine" Width="300px" Rows="6"></asp:TextBox>
    <br />
    <asp:Button ID="btnNewNoteAdd" OnClientClick="clientOrdersUpdate();" runat="server" Text="Add New"
        OnClick="btnNewNoteAdd_Click" />&nbsp&nbsp<asp:Button ID="Button3" runat="server"
            Text="Cancel"  OnClick="btnNoteCancel_Click" />&nbsp&nbsp
</asp:Panel>
<div>
    <asp:Label ID="lblInfo" runat="server" Visible="false" BackColor="Yellow" Font-Bold="true"
        ForeColor="Black" />
</div>
