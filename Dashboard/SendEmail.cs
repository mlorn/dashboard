﻿using System.Net.Mail;
using System;
using System.Text.RegularExpressions;



namespace Dashboard
{
  class SendEmail
  {
    /// <summary>
    /// Sends an e-mail message using the designated SMTP mail server.
    /// </summary>
    /// <param name="subject">The subject of the message being sent.</param>
    /// <param name="messageBody">The message body.</param>
    /// <param name="fromAddress">The sender's e-mail address.</param>
    /// <param name="toAddress">The recipient's e-mail address (separate multiple e-mail addresses
    /// with a semi-colon).</param>
    /// <param name="ccAddress">The address(es) to be CC'd (separate multiple e-mail addresses with
    /// a semi-colon).</param>
    /// <remarks>You must set the SMTP server within this method prior to calling.</remarks>
    /// <example>
    /// <code>
    ///   // Send a quick e-mail message
    ///   SendEmail.SendMessage("This is a Test", 
    ///                         "This is a test message...",
    ///                         "noboday@nowhere.com",
    ///                         "somebody@somewhere.com", 
    ///                         "ccme@somewhere.com");
    /// </code>
    /// </example>
    public static void SendMessage(string subject, string messageBody, string fromAddress, string toAddress, string ccAddress)
    {
      MailMessage message = new MailMessage();
      SmtpClient client = new SmtpClient();

      // Set the sender's address
      message.From = new MailAddress(fromAddress);

      // Allow multiple "To" addresses to be separated by a semi-colon
      if (toAddress.Trim().Length > 0)
      {
        foreach (string addr in toAddress.Split(';'))
        {
          message.To.Add(new MailAddress(addr));
        }
      }

      // Allow multiple "Cc" addresses to be separated by a semi-colon
      if (ccAddress.Trim().Length > 0)
      {
        foreach (string addr in ccAddress.Split(';'))
        {
          message.CC.Add(new MailAddress(addr));
        }
      }

      // Set the subject and message body text
      message.Subject = subject;
      message.Body = messageBody;

      client.Host = "mail.wimco.com";

      // Send the e-mail message
      try
      {
        client.Send(message);
      }
      catch { }


    }

    public static void SendHTMLEmail(string subject, string messageBody, string fromAddress, string toAddress, string ccAddress)
    {
      MailMessage message = new MailMessage();
      SmtpClient client = new SmtpClient();

      // Set the sender's address
      message.From = new MailAddress(fromAddress);

      // Allow multiple "To" addresses to be separated by a semi-colon
      if (toAddress.Trim().Length > 0)
      {
        foreach (string addr in toAddress.Split(';'))
        {
          message.To.Add(new MailAddress(addr));
        }
      }

      // Allow multiple "Cc" addresses to be separated by a semi-colon
      if (ccAddress.Trim().Length > 0)
      {
        foreach (string addr in ccAddress.Split(';'))
        {
          message.CC.Add(new MailAddress(addr));
        }
      }

      // Set the subject and message body text
      message.Subject = subject;
      message.Body = messageBody;
      message.IsBodyHtml = true;
      client.Host = "mail.wimco.com";

      // Send the e-mail message
      try
      {
        client.Send(message);
      }
      catch { }


    }


    public static bool ValidateEmailAddress(string emailAddress)
    {
      bool result = false;
      try
      {
        Regex expression = new Regex(@"\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}");
        result = expression.IsMatch(emailAddress);

      }
      catch (Exception)
      {
        throw;
      }
      return result;
    }

    public static void SendMessage(string subject, string messageBody, string fromAddress, string toAddress, string ccAddress, bool isHtml)
    {
      MailMessage message = new MailMessage();
      SmtpClient client = new SmtpClient();

      //set to HTML or ASCII
      message.IsBodyHtml = isHtml;

      // Set the sender's address
      message.From = new MailAddress(fromAddress);

      // Allow multiple "To" addresses to be separated by a semi-colon
      if (toAddress.Trim().Length > 0)
      {
        foreach (string addr in toAddress.Split(';'))
        {
          message.To.Add(new MailAddress(addr));
        }
      }

      // Allow multiple "Cc" addresses to be separated by a semi-colon
      if (ccAddress.Trim().Length > 0)
      {
        foreach (string addr in ccAddress.Split(';'))
        {
          message.CC.Add(new MailAddress(addr));
        }
      }

      // Set the subject and message body text
      message.Subject = subject;
      message.Body = messageBody;

      client.Host = "mail.wimco.com";

      // Send the e-mail message
      try
      {
        client.Send(message);
      }
      catch { }
    }
  }
}
