﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using DataAccessLayer;

namespace Dashboard
{
  public partial class DetailLead : System.Web.UI.Page
  {
    private LeadData leadData;
    private int userID = 0;
    // admin group id
    //private const int dashboardAdminGroup = 195;
    private const string errorMsg1 = "The lead could not be closed.  Please make sure that the order/resv number has been created after claiming the lead.";
    private const string errorMsg2 = "Please enter an order/resv number to close the lead.";
    private const string pendingStatus = "Pending";
    private const string openStatus = "Open";
    private const string msg = "The lead has been closed.";
    //private const string leadBeenAssigned = "Lead(s) has been assigned";

    protected void Page_Load(object sender, EventArgs e)
    {
      DataView dv = null;
      DataTable dt = null;

      string result = string.Empty;

      int userGroupID = 0;
      int leadID = 0;
      int recNo = 1;
      string raEmail = string.Empty;
      int clientID = 0;
      string clientEmail = string.Empty;
      string openNomad = "N";
      string AllowClaimLead = "Yes";

      try
      {
        userGroupID = Convert.ToInt32(Session["GroupID"]);
      }
      catch
      {
        //TODO: better handle exception
      }

      // get user from a session saved during a user logged in
      try
      {
        userID = Convert.ToInt32(Session["UserID"]);
      }
      catch
      {
        //TODO: better handle exception
      }
      #region Not IsPostBack
      if (!IsPostBack)
      {
        leadData = new LeadData();

        try
        {
          result = Session["SentMailResult"].ToString();
          if (result.Length > 0)
          {
            //lblInfo.Text = result;
            lblMsg.Text = result;

          }
        }
        catch
        {
          //TODO: better handle exception
        }

        // if a user is belonged to an admin group
        // create drop-down-list of RA, so that an admin can assign a lead to a RA.
        if (userGroupID == LeadData.dashboardAdminGroup)
        {
          lblFirstChoice.Visible = true;
          btnAssignDestination.Visible = true;

          btnAssignLead.Visible = true;
          lblAssignLead.Visible = true;

          btnClaimLead.Visible = false;

          // populate the drop-down-list for an admin user
          ddlResvAgent.DataTextField = "FullName";
          ddlResvAgent.DataValueField = "MagUserID";
          // get all active RAs when the paramenter is empty.
          ddlResvAgent.DataSource = leadData.GetUser(string.Empty);
          ddlResvAgent.DataBind();
          ddlResvAgent.Visible = true;
          ddlResvAgent.Focus();

          ddlFirstChoice.DataTextField = "LocaleName";
          ddlFirstChoice.DataValueField = "LocaleID";
          ddlFirstChoice.DataSource = leadData.GetDestination(0);
          ddlFirstChoice.DataBind();
          ddlFirstChoice.Visible = true;
        }
        else
        {
          ddlResvAgent.Visible = false;
          ddlFirstChoice.Visible = false;

          lblAssignLead.Visible = false;
          btnAssignLead.Visible = false;

          // make the Return Agent field invisible when a Reservation Agent is logging in
          dvLeadDetail.Fields[20].Visible = false;

          lblFirstChoice.Visible = false;
          btnAssignDestination.Visible = false;

          ddlResvAgent.Visible = false;

          //ML - 06/01/2021 : commented out
          //try
          //{
          //  AllowClaimLead = Session["AllowClaimLead"].ToString();
          //}
          //catch
          //{
          //  AllowClaimLead = "Yes";
          //}

          ////if allow claiming a lead, make the assign button and drop-down-list visible.
          //if (AllowClaimLead == "Yes")
          //{
          //  btnClaimLead.Visible = true;
          //}
          //else
          //{
          //  btnClaimLead.Visible = false;
          //}

          //ML - 06/01/2021 - Added
          btnClaimLead.Visible = false;

          txtFCCNumber.Focus();
        }

        dv = Session["DetailData"] as DataView;
        dvLeadDetail.DataSource = dv;
        dvLeadDetail.DataBind();
        dt = dv.Table;

        // reset the page number in the Detail grid
        ResetRec();
        // disable all record field
        EnableDisable(false);

        // set page info for displaying in the Detail grid
        foreach (DataRowView r in dt.DefaultView)
        {
          leadID = r.Row.Field<Int32>("LeadID");
          AssignRecNo(recNo, leadID);
          recNo++;
        }

        //if not an admin user, assign lead(s) to an agent.
        if (userGroupID != LeadData.dashboardAdminGroup)
        {
          AssignLead();
        }

        try
        {
          openNomad = Session["OpenNomad"].ToString();
        }
        catch
        {
          openNomad = "N";
        }

        if (openNomad == "Y")
        {
          try
          {
            clientEmail = Session["ClientEmail"].ToString();
          }
          catch
          {
            clientEmail = string.Empty;
          }

          try
          {
            clientID = Convert.ToInt32(Session["ClientID"]);
          }
          catch
          {
            clientID = 0;
          }

          //reset the session avariable
          Session["OpenNomad"] = "N";
          if (clientID > 0)
          {
            jumpToOtherSite("C", clientID.ToString(), "#divLead");
          }
          else
          {
            jumpToOtherSite("E", clientEmail, "#divLead");
          }

        }

      }
      #endregion
    }

    private void AssignRecNo(int recNo, int leadID)
    {
      // created navigation paging
      switch (recNo)
      {
        case 1:
          Rec1.Text = "1";
          Rec1.ToolTip = string.Format("LeadID = {0}", leadID);
          // save a lead id for the first record.
          Session["LeadID"] = leadID;
          // always disable the first record because it is on the first record.
          //Rec1.Enabled = true;
          break;
        case 2:
          Rec2.Text = "2";
          Rec2.ToolTip = string.Format("LeadID = {0}", leadID);
          Rec2.Enabled = true;
          break;
        case 3:
          Rec3.Text = "3";
          Rec3.ToolTip = string.Format("LeadID = {0}", leadID);
          Rec3.Enabled = true;
          break;
        case 4:
          Rec4.Text = "4";
          Rec4.ToolTip = string.Format("LeadID = {0}", leadID);
          Rec4.Enabled = true;
          break;
        case 5:
          Rec5.Text = "5";
          Rec5.ToolTip = string.Format("LeadID = {0}", leadID);
          Rec5.Enabled = true;
          break;
        case 6:
          Rec6.Text = "6";
          Rec6.ToolTip = string.Format("LeadID = {0}", leadID);
          Rec6.Enabled = true;
          break;
        case 7:
          Rec7.Text = "7";
          Rec7.ToolTip = string.Format("LeadID = {0}", leadID);
          Rec7.Enabled = true;
          break;
        case 8:
          Rec8.Text = "8";
          Rec8.ToolTip = string.Format("LeadID = {0}", leadID);
          Rec8.Enabled = true;
          break;
        case 9:
          Rec9.Text = "9";
          Rec9.ToolTip = string.Format("LeadID = {0}", leadID);
          Rec9.Enabled = true;
          break;
        case 10:
          Rec10.Text = "10";
          Rec10.ToolTip = string.Format("LeadID = {0}", leadID);
          Rec10.Enabled = true;
          break;
      }
    }

    /// <summary>
    /// Enables or disable record browsing labels
    /// </summary>
    /// <param name="flag">if set to <c>true</c> [flag].</param>
    private void EnableDisable(bool flag)
    {
      Rec1.Enabled = flag;
      Rec2.Enabled = flag;
      Rec3.Enabled = flag;
      Rec4.Enabled = flag;
      Rec5.Enabled = flag;
      Rec6.Enabled = flag;
      Rec7.Enabled = flag;
      Rec8.Enabled = flag;
      Rec9.Enabled = flag;
      Rec10.Enabled = flag;
    }

    /// <summary>
    /// Resets the labels
    /// </summary>
    private void ResetRec()
    {
      Rec1.Text = string.Empty;
      Rec2.Text = string.Empty;
      Rec3.Text = string.Empty;
      Rec4.Text = string.Empty;
      Rec5.Text = string.Empty;
      Rec6.Text = string.Empty;
      Rec7.Text = string.Empty;
      Rec8.Text = string.Empty;
      Rec9.Text = string.Empty;
      Rec10.Text = string.Empty;
    }

    /// <summary>
    /// Handles the Click event of the lnkbtnResvNo control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void lnkbtnEMailAddress_Click(object sender, EventArgs e)
    {
      int leadID;
      int clientID;
      string clientEmail;
      string[] cmdArg;

      LeadData leadData = new LeadData();
      // CommandArgument cotains both LeadID and ClientEmailAddress and seperated by :
      string temp = ((LinkButton)sender).CommandArgument;
      cmdArg = temp.Split(':');
      leadID = Convert.ToInt32(cmdArg[0]);
      clientEmail = cmdArg[1];

      if (leadID > 0)
      {
        try
        {
          clientID = leadData.GetClientID(leadID);
          if (clientID > 0)
          {
            jumpToOtherSite("C", clientID.ToString(), "#divLead");
          }
          else
          {
            jumpToOtherSite("E", clientEmail, "#divLead");
          }
        }
        catch
        {
          //TODO:
        }

      }
    }

    /// <summary>
    /// Filter data view according to a record label is clicked
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Paging(object sender, EventArgs e)
    {
      char[] delimiterChars = { '=' };
      int leadID;
      int pos;
      string temp;
      LinkButton lnkBtn;
      string filterString = string.Empty;
      lnkBtn = (sender as LinkButton);
      // enable all record fields
      EnableDisable(true);
      // disable the current clicked field
      lnkBtn.Enabled = false;
      filterString = lnkBtn.ToolTip;
      //get the Lead ID From lnkBtn.ToolTip in the format of  LeadID = 1334
      pos = filterString.IndexOf("=");
      // pos + 2 which to exclude a space after the = sign.
      temp = filterString.Substring(pos + 2);
      if (temp.Length > 0)
      {
        leadID = Convert.ToInt32(temp);
        Session["LeadID"] = leadID;
      }
      AssignData(filterString);
    }

    /// <summary>
    /// Assigns the data to the DetailView by using filter dataset
    /// </summary>
    /// <param name="filterString">The filter string.</param>
    private void AssignData(string filterString)
    {
      DataView dv = null;
      DataView temp = null;

      //string filterString = string.Empty;

      dv = Session["DetailData"] as DataView;

      //dt = ds.Tables[0];
      //filterString = string.Format("LeadID={0}", 502);
      dv.RowFilter = filterString;
      dvLeadDetail.DataSource = dv;
      dvLeadDetail.DataBind();
    }

    /// <summary>
    /// Jumps to other site by using parameters go to a specific page.
    /// </summary>
    /// <param name="refType">Type of the ref.</param>
    /// <param name="refNumber">The ref number.</param>
    private void jumpToOtherSite(string refType, string refNumber)
    {
      string userName = string.Empty;
      try
      {
        userName = Session["UserName"].ToString();
      }
      catch
      {
        //TODO: better handle exception
        userName = string.Empty;
      }
      string url = SecureJump.JumpToNomad.CreateJumpURL(userName, refType, refNumber);
      //Response.Write("<script>window.open('" + url + "','_blank');</script>");
      //string url = SecureJump.JumpToNomad.CreateJumpURL("MAO", "E", "mbowers@wimco.com");
      ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "#divLead','_blank');", true);
    }

    private void jumpToOtherSite(string refType, string refNumber, string divTag)
    {
      string userName = string.Empty;
      try
      {
        userName = Session["UserName"].ToString();
      }
      catch
      {
        //TODO: better handle exception
        userName = string.Empty;
      }
      string url = SecureJump.JumpToNomad.CreateJumpURL(userName, refType, refNumber);
      //Response.Write("<script>window.open('" + url + "','_blank');</script>");
      //string url = SecureJump.JumpToNomad.CreateJumpURL("MAO", "E", "mbowers@wimco.com");
      ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + divTag + "','_blank');", true);
    }

    /// <summary>
    /// Handles the Click event of the Save control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAssignLead_Click(object sender, EventArgs e)
    {
      int userID = 0;
      userID = Convert.ToInt32(ddlResvAgent.SelectedValue);
      Session["UserID"] = userID;
      AssignLead();
    }

    /// <summary>
    /// Assigns the lead.
    /// </summary>
    private void AssignLead()
    {
      DataView dv = null;
      DataTable dt = null;
      int leadID = 0;
      string leadStatusCode = string.Empty;

      string raEmail = string.Empty;
      string result = string.Empty;
      bool leadToBeAssigned = false;
      int userGroupID = 196;
      string clientEmail = string.Empty;

      Properties.Settings appSettings = new Properties.Settings();

      try
      {
        userID = Convert.ToInt32(Session["UserID"]);
      }
      catch
      {
        //TODO: better handle exception
        userID = 0;
      }

      try
      {
        userGroupID = Convert.ToInt32(Session["GroupID"]);
      }
      catch
      {
        //TODO: better handle exception
        userGroupID = 196;
      }

      try
      {
        clientEmail = Session["ClientEmail"].ToString();
      }
      catch
      {
        clientEmail = string.Empty;
      }


      leadData = new LeadData();

      dv = Session["DetailData"] as DataView;
      dt = dv.Table;
      // filter the default view with a client email
      dt.DefaultView.RowFilter = leadData.GetFilterString(userGroupID, clientEmail);
      dt.DefaultView.Sort = "RequestDate DESC";
      Session["DetailData"] = dt.DefaultView;

      foreach (DataRowView r in dt.DefaultView)
      {
        leadID = r.Row.Field<Int32>("LeadID");
        leadStatusCode = r.Row.Field<string>("LeadStatusCode");
        // assign only pending status (resign to a different user) or open status if an admin user
        if (userGroupID == LeadData.dashboardAdminGroup)
        {
          if (leadStatusCode == pendingStatus || leadStatusCode == openStatus)
          {
            result = leadData.AssignLead(leadID, userID, true);
            if (result == LeadData.leadBeenAssigned)
            {
              BaseLogic.InsertNomadLog(LeadData.pageLeadboard, LeadData.typeAssignLead, leadID.ToString());
              leadToBeAssigned = true;
              r["LeadStatusCode"] = "<span style='color:red'>The lead has been assigned.</span>";
            }
          }
        }
        else
        {
          if (leadStatusCode == openStatus)
          {
            result = leadData.AssignLead(leadID, userID, false);
            if (result == LeadData.leadBeenAssigned)
            {
              BaseLogic.InsertNomadLog(LeadData.pageLeadboard, LeadData.typeAssignLead, leadID.ToString());
              leadToBeAssigned = true;
              r["LeadStatusCode"] = "<span style='color:red'>The lead has been assigned.</span>";
            }
          }
        }
      }

      if (leadToBeAssigned)
      {
        //lblInfo.Text = result;
        lblMsg.Text = result;

        if (result == LeadData.leadBeenAssigned)
        {
          if (appSettings.IsTesting)
          {
            raEmail = appSettings.TestingEmail;
          }
          else
          {
            raEmail = leadData.GetRAEmail(userID);
          }
          //SendEmail(raEmail);
          //result = leadData.SmtpSendEmail(raEmail);
          result = leadData.GroupWiseSendEmail(raEmail);
          if (result.Length > 0)
          {
            //lblInfo.Text = result;
            lblMsg.Text = result;
          }
          dvLeadDetail.DataSource = dv;
          dvLeadDetail.DataBind();
        }
      }
      else
      {
        if (userGroupID == LeadData.dashboardAdminGroup)
        {
          //lblInfo.Text = "There is no open or pending lead to assign.";
          lblMsg.Text = "There is no open or pending lead to assign.";
        }
      }

    }


    protected void lnkbtnClientName_Click(object sender, EventArgs e)
    {
      int leadID;
      int clientID;
      string clientEmail;
      string[] cmdArg;

      clientEmail = Session["ClientEmail"].ToString();

      jumpToOtherSite("E", clientEmail, "#divLead");

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
      // hide detail grid when the Cancel/Exit button is press
      //this.dvLeadDetail.Visible = false;
      //this.mdlPopup.Hide();
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
      //Session["LeadID"] = this.grdLead.DataKeys[this.grdLead.SelectedIndex].Value;
      string URL = "PrintDetailLead.aspx";
      ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + URL + "','_blank');", true);
      //Response.Redirect("~/PrintDetailLead.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnCloseLead control.
    /// Close a lead if a RA entered an order number or resv number.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnCloseLead_Click(object sender, EventArgs e)
    {
      DataView dv = null;
      DataTable dt = null;
      LeadData leadData = null;
      int leadID;
      string leadStatusCode;
      int userGroupID;
      string clientEmail = string.Empty;

      string fccNumber = string.Empty;

      bool result = true;
      bool temp = true;
      bool leadToBeClosed = false;
      string errorMsg = string.Empty;

      try
      {
        userGroupID = Convert.ToInt32(Session["GroupID"]);
      }
      catch
      {
        //TODO: better handle exception
        userGroupID = 196;
      }

      try
      {
        clientEmail = Session["ClientEmail"].ToString();
      }
      catch
      {
        clientEmail = string.Empty;
      }

      leadData = new LeadData();

      dv = Session["DetailData"] as DataView;
      dt = dv.Table;
      // filter the default view with a client email
      dt.DefaultView.RowFilter = leadData.GetFilterString(userGroupID, clientEmail);
      dt.DefaultView.Sort = "RequestDate DESC";
      Session["DetailData"] = dt.DefaultView;
      if (txtFCCNumber.Text.Length > 0)
      {
        fccNumber = txtFCCNumber.Text.ToUpper();
        // if a client has multiple leads, assign all open leads to a RA
        // and set page info for displaying in the Detail grid
        foreach (DataRowView r in dt.DefaultView)
        {
          leadID = r.Row.Field<Int32>("LeadID");
          leadStatusCode = r.Row.Field<string>("LeadStatusCode");
          // close only pending status
          if (leadStatusCode == pendingStatus)
          {
            leadToBeClosed = true;
            temp = leadData.CloseLead(leadID, fccNumber);
            if (temp == false)
            {
              result = false;
              //cannot close a lead
              errorMsg = errorMsg1;
              txtFCCNumber.Focus();
              break;
            }
            BaseLogic.InsertNomadLog(LeadData.pageLeadboard, LeadData.typeCloseLead, leadID.ToString());
            r["LeadStatusCode"] = "<span style='color:red'>The lead has been closed.</span>";
          }
          else
          {
            //lblInfo.Text = string.Empty;
            lblMsg.Text = string.Empty;
          }
        }
      }
      else
      {
        result = false;
        // order/resv number must be entered.
        errorMsg = errorMsg2;
        txtFCCNumber.Focus();
      }

      if (result == true)  // the lead has been closed
      {
        if (leadToBeClosed)
        {
          //lblErrorMsg.Visible = true;
          //lblInfo.Text = msg;
          lblMsg.Text = msg;
        }
        else
        {
          //lblInfo.Text = "There is no open or pending lead to close.";
          lblMsg.Text = "There is no open or pending lead to close.";
        }
        dvLeadDetail.DataSource = dv;
        dvLeadDetail.DataBind();
      }
      else
      {
        // cannot close a lead
        //lblInfo.Text = errorMsg;
        lblMsg.Text = errorMsg;
        //lblErrorMsg.Visible = true;
      }

    }

    private void showMessage(string result)
    {
      Label lblMessage = new Label();
      lblMessage.Text = "<script language='javascript'>" + Environment.NewLine +
         "window.alert(" + "'" + result + "'" + ")</script>";

      // add the label to the page to display the alert
      Page.Controls.Add(lblMessage);
    }

    protected void btnGoBack_Click(object sender, EventArgs e)
    {
      Response.Redirect("~/default.aspx");
    }

    protected void btnAssignDestination_Click(object sender, EventArgs e)
    {
      int leadID;
      int destinationID;
      leadData = new LeadData();
      try
      {
        destinationID = Convert.ToInt32(ddlFirstChoice.SelectedValue);
        leadID = Convert.ToInt32(Session["LeadID"]);
        //lblInfo.Text = leadData.AssignDestination(leadID, destinationID);
        lblMsg.Text = leadData.AssignDestination(leadID, destinationID);
      }
      catch
      {
        //lblInfo.Text = "Could not assign the destination";
        lblMsg.Text = "Could not assign the destination";

      }
    }

    protected void btnGoToNomad_Click(object sender, EventArgs e)
    {
      int leadID;
      int clientID;
      leadID = Convert.ToInt32(Session["LeadID"]);
      leadData = new LeadData();
      clientID = leadData.GetClientID(leadID);
      Session["ClientID"] = clientID;
      Response.Redirect("~/TestDetailLeadControl.aspx");
    }

    protected void btnClaimLead_Click(object sender, EventArgs e)
    {
      AssignLead();
    }
  }
}