﻿using System;
using System.Data;

namespace Dashboard.App_Data
{


  public partial class CampaignLinksDS
  {
    public void GetCampainLinks(string EmailCampaignID)
    {

      EmailCampaignDS ec = new EmailCampaignDS();
      ec.GetCampaignByID(EmailCampaignID);
      EmailCampaignDS.EmailCampaignRow er = (EmailCampaignDS.EmailCampaignRow)ec.EmailCampaign.Rows[0];


      DataSet ds = new DataSet();
      string SQL = "SELECT EmailCampaignLink.EmailCampaignRowID, EmailCampaignLink.LinkNumber AS [Link Number], EmailCampaignLink.LinkURL AS [Target URL], EmailCampaignLink.Note, EmailCampaignLink.IsEnabled AS Enabled, EmailCampaignLink.CreateDate AS Created, EmailCampaignLink.ModifiedDate AS [Last Modified], MagUser.FullName AS [By User], EmailCampaignLink.Action, EmailCampaignLink.Value FROM    EmailCampaignLink INNER JOIN MagUser ON EmailCampaignLink.ModifiedBy = MagUser.UserID WHERE (EmailCampaignLink.EmailCampaignID = N'" + EmailCampaignID + "') ORDER BY EmailCampaignLink.LinkNumber";
      ds = BaseLogic.AdHoc(SQL);
      this.CampaignLinks.Merge(ds.Tables[0]);
      foreach (CampaignLinksRow r in this.CampaignLinks.Rows)
      {
        string tr = string.Empty;

        if (r.Action != string.Empty)
        {
          tr = "&" + r.Action + "=" + r.Value;
        }

        //r.PartnerLink = "http://www.wimco.com/lbredirect.aspx?stuff=1&morestuff=2&evenmorestuff=3&THISISAPLACEHOLDER=4";

        //r.PartnerLink = "http://wimco.com/lbredirect.aspx?ecrid=" + r.EmailCampaignRowID + "&" + "ecid=" + er.EmailCampaignID + "&" +  "csid=" + er.ContactSourceID  + "&" + "sid=" + er.SourceID + "&" + "UTM=" + er.UTM;
        r.PartnerLink = "http://www.wimco.com/lbredirect.aspx?ecrid=" + r.EmailCampaignRowID + tr;

        // who?

        string snd = string.Empty;
        if (er.Sender == 1) // mailchimp
        {
          snd = "&eml=*|EMAIL|*";
        }
        if (er.Sender == 2) //delivera
        {
          snd = "&eml=%%EmailAddr_%%";
        }

        r.PartnerLink = r.PartnerLink + snd;
        //r.PartnerLink = r.PartnerLink + "&eml=*|EMAIL|*"; // MailChimp. Hardcoded for now. 020414
        // fill in additional stuff. calc link et al.
      }

    }

    public void LoadByEmailCampaignRowID(string EmailCampaignRowID)
    {
      DataSet ds = new DataSet();
      ds = BaseLogic.AdHoc("SELECT * FROM  EmailCampaignLink WHERE (EmailCampaignRowID = '" + EmailCampaignRowID + "')");
      this.CampaignLinks.Merge(ds.Tables[0]);
    }

  }
}
