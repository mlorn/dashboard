﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace Dashboard
{
  /// <summary>
  /// Finds all controls of type T stores them in FoundControls
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class ControlFinder<T> where T : Control
  {
    private readonly List<T> _foundControls = new List<T>();
    public IEnumerable<T> FoundControls
    {
      get { return _foundControls; }
    }

    public void FindChildControlsRecursive(Control control)
    {
      foreach (Control childControl in control.Controls)
      {
        if (childControl.GetType() == typeof(T))
        {
          _foundControls.Add((T)childControl);
        }
        else
        {
          FindChildControlsRecursive(childControl);
        }
      }
    }
  }

  public static class ControlExtensions
  {
    /// <summary>
    /// recursively finds a child control of the specified parent.
    /// </summary>
    /// <param name="control"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static Control FindControlRecursive(this Control control, string id)
    {
      if (control == null) return null;
      //try to find the control at the current level
      Control ctrl = control.FindControl(id);

      if (ctrl == null)
      {
        //search the children
        foreach (Control child in control.Controls)
        {
          ctrl = FindControlRecursive(child, id);

          if (ctrl != null)
          {
            break;
          }
        }
      }
      return ctrl;
    }

    private static Control RecursiveFindControl(Control root, string id)
    {
      if (root.ID == id)
      {
        return root;
      }

      foreach (Control control in root.Controls)
      {
        Control found = RecursiveFindControl(control, id);
        if (found != null)
        {
          return found;
        }
      }

      return null;
    }

  }



}