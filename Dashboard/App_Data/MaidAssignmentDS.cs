﻿using System.Data;
namespace Dashboard.App_Data
{


  public partial class MaidAssignmentDS
  {
    public bool LoadDataByMaidID(int MaidID)
    {
      DataSet ds = new DataSet();
      //ds = BaseLogic.AdHoc("SELECT VillaMaidRef.AssetID, VendorBase.Code AS VendCode, ProductBase.Code, VillaMaidRef.IsEnabled, VillaMaidRef.VillaMaidID FROM VillaMaidRef INNER JOIN Villa ON VillaMaidRef.AssetID = Villa.AssetID INNER JOIN ProductBase ON Villa.ProductBaseID = ProductBase.ProductBaseID INNER JOIN VendorBase ON ProductBase.VendorBaseID = VendorBase.VendorBaseID WHERE (VillaMaidRef.MaidID = " + MaidID + ") AND (VillaMaidRef.IsEnabled = 'Y')");
      ds = BaseLogic.AdHoc("SELECT VillaMaidRef.AssetID, PropertyManager.VillaPMPrefix AS VendCode, ProductBase.Code, VillaMaidRef.IsEnabled, VillaMaidRef.VillaMaidID FROM VillaMaidRef INNER JOIN Villa ON VillaMaidRef.AssetID = Villa.AssetID INNER JOIN ProductBase ON Villa.ProductBaseID = ProductBase.ProductBaseID INNER JOIN VendorBase ON ProductBase.VendorBaseID = VendorBase.VendorBaseID INNER JOIN PropertyManager ON ProductBase.VendorBaseID = PropertyManager.VendorBaseID WHERE (VillaMaidRef.MaidID = " + MaidID + ") AND (VillaMaidRef.IsEnabled = 'Y') AND (ProductBase.IsEnabled = 'Y')");
      this.MaidAssignment.Merge(ds.Tables[0]);

      foreach (App_Data.MaidAssignmentDS.MaidAssignmentRow mr in this.MaidAssignment.Rows)
      {
        if (mr.IsEnabled == "Y")
        {
          mr.Assigned = true;
        }
        else
        {
          mr.Assigned = false;
        }
      }

      return true;

    }

  }
}
