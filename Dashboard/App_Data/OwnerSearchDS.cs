﻿using System.Data;

namespace Dashboard.App_Data
{

  public partial class OwnerSearchDS
  {

    public bool LoadDataByProductCode(string Code)
    {
      DataSet ds = BaseLogic.AdHoc("SELECT Villa.VillaOwnerID, Villa.ProductBaseID, VendorBase.Name, VendorBase.EmailAddress FROM Villa INNER JOIN ProductBase ON Villa.ProductBaseID = ProductBase.ProductBaseID LEFT OUTER JOIN VendorBase ON Villa.VillaOwnerID = VendorBase.VendorBaseID WHERE (ProductBase.Code = '" + Code + "') AND (ProductBase.AssetID IS NOT NULL) AND (ProductBase.IsEnabled = 'Y')");
      this.OwnerSearch.Merge(ds.Tables[0]);
      foreach (App_Data.OwnerSearchDS.OwnerSearchRow r in this.OwnerSearch.Rows)
      {
        r.VillaCode = BaseLogic.GetVillaNameFromProductBaseID(r.ProductBaseID);
      }
      this.OwnerSearch.AcceptChanges();
      return true;

    }


    public bool LoadDataByProductBaseID(int ProductBaseID)
    {
      DataSet ds = BaseLogic.AdHoc("SELECT Villa.VillaOwnerID, Villa.ProductBaseID, VendorBase.Name, VendorBase.EmailAddress FROM Villa LEFT OUTER JOIN VendorBase ON Villa.VillaOwnerID = VendorBase.VendorBaseID WHERE (Villa.ProductBaseID = " + ProductBaseID + ")");
      //DataSet ds = BaseLogic.AdHoc("SELECT Villa.VillaOwnerID, Villa.ProductBaseID, VendorBase.Name, VendorBase.EmailAddress FROM Villa INNER JOIN VendorBase ON Villa.VillaOwnerID = VendorBase.VendorBaseID WHERE (Villa.ProductBaseID = " + ProductBaseID + ")");
      this.OwnerSearch.Merge(ds.Tables[0]);
      foreach (App_Data.OwnerSearchDS.OwnerSearchRow r in this.OwnerSearch.Rows)
      {
        r.VillaCode = BaseLogic.GetVillaNameFromProductBaseID(r.ProductBaseID);
      }
      this.OwnerSearch.AcceptChanges();
      return true;
    }

    public bool LoadDataByEmailAddress(string EmailAddress)
    {
      //DataSet ds = BaseLogic.AdHoc("SELECT Villa.VillaOwnerID, Villa.ProductBaseID, VendorBase.Name, VendorBase.EmailAddress FROM Villa INNER JOIN VendorBase ON Villa.VillaOwnerID = VendorBase.VendorBaseID WHERE (Villa.VillaOwnerID IS NOT NULL) AND (VendorBase.EmailAddress = '"  + EmailAddress + "')");
      DataSet ds = BaseLogic.AdHoc("SELECT Villa.VillaOwnerID, Villa.ProductBaseID, VendorBase.Name, VendorBase.EmailAddress FROM Villa INNER JOIN VendorBase ON Villa.VillaOwnerID = VendorBase.VendorBaseID INNER JOIN ProductBase ON Villa.ProductBaseID = ProductBase.ProductBaseID WHERE (Villa.VillaOwnerID IS NOT NULL) AND (VendorBase.EmailAddress = '" + EmailAddress + "') AND (ProductBase.IsEnabled = 'Y')");
      this.OwnerSearch.Merge(ds.Tables[0]);
      foreach (App_Data.OwnerSearchDS.OwnerSearchRow r in this.OwnerSearch.Rows)
      {
        r.VillaCode = BaseLogic.GetVillaNameFromProductBaseID(r.ProductBaseID);
      }
      this.OwnerSearch.AcceptChanges();
      return true;
    }



  }
}
