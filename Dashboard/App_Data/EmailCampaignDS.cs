﻿using System;
using System.Data;

namespace Dashboard.App_Data
{


  public partial class EmailCampaignDS
  {

    public void GetAllCampaigns()
    {

      DataSet ds = BaseLogic.AdHoc("select * from EmailCampaign");
      this.EmailCampaign.Merge(ds.Tables[0]);
    }


    public void GetCampaignByID(string EmailCampaignID)
    {
      DataSet ds = BaseLogic.AdHoc("SELECT * FROM EmailCampaign WHERE (EmailCampaignID = '" + EmailCampaignID + "')");
      this.EmailCampaign.Merge(ds.Tables[0]);
    }

    public bool AddNewCampaign(string DisplayName, string SourceID, string UTM, string ContactSourceID, string Note, int Snd)
    {
      BaseLogic.UserData ud = BaseLogic.GetUserData();
      EmailCampaignDS ec = new EmailCampaignDS();
      EmailCampaignDS.EmailCampaignRow er = ec.EmailCampaign.NewEmailCampaignRow();
      er.EmailCampaignID = Guid.NewGuid().ToString();
      er.DisplayName = DisplayName;
      er.SourceID = SourceID;
      er.UTM = UTM;
      er.ContactSourceID = ContactSourceID;
      er.Note = Note;
      er.CreateDate = DateTime.Now;
      er.ModifiedDate = DateTime.Now;
      er.ModifiedBy = ud.UserID;
      er.IsEnabled = true;
      er.Sender = Snd;
      ec.EmailCampaign.AddEmailCampaignRow(er);
      BusinessLogic.UpdateDataSetResults udr = new BusinessLogic.UpdateDataSetResults();
      udr = BaseLogic.UpdateDataSet(ec);
      return udr.Success;
    }


  }
}
