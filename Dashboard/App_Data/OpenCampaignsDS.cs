﻿using System;
using System.Data;

namespace Dashboard.App_Data
{


  public partial class OpenCampaignsDS
  {
    public void LoadOpenCampaigns()
    {
      DataSet ds = new DataSet();
      string SQL = "SELECT EmailCampaign.EmailCampaignID, EmailCampaign.DisplayName AS Campaign, MagUser.FullName AS [Last User], EmailCampaign.CreateDate AS [Last Modified], EmailCampaign.IsEnabled AS Show FROM EmailCampaign INNER JOIN  MagUser ON EmailCampaign.ModifiedBy = MagUser.UserID WHERE (EmailCampaign.IsEnabled = 1)";



      // chnage to :
      //string SQL = "SELECT EmailCampaignID, CreateDate, DisplayName, Note, UTM, SourceID, IsEnabled FROM  EmailCampaign ORDER BY CreateDate DESC";
      ds = BaseLogic.AdHoc(SQL);
      this.OpenCampaigns.Merge(ds.Tables[0]);
    }

  }
}
