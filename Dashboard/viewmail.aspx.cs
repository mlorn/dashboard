﻿using System;
using System.Linq;
using System.Web;

namespace GWCustom
{
  public partial class viewmail : System.Web.UI.Page
  {
    [System.Web.Services.WebMethod]
    public static object EmailString(string email)
    {
      string temp = string.Empty;
      try
      {
        temp = HttpContext.Current.Session["emailBody"].ToString();
      }
      catch { }
      if (!email.Contains("mail has been deleted from Server") && !email.Contains("Can't reach Groupwise to read this email"))
      {

        HttpContext.Current.Session["emailBody"] = temp + email;
      }

      return email;
    }
  }
}
