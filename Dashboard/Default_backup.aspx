﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default_backup.aspx.cs" Inherits="Dashboard.Default_backup" %>

<%@ Register Src="GWWimcoEmailGrid.ascx" TagName="GWWimcoEmailGrid" TagPrefix="wimcoCustom" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Dashboard</title>
  <link href="CSS/main.css" rel="stylesheet" />
  <script src="Scripts/jquery-1.8.2.min.js"></script>
  <script src="Scripts/default.js"></script>
</head>
<body>
  <form id="form1" runat="server">

    <%-- Admin Group --%>
    <div>
      <asp:ToolkitScriptManager runat="server" ID="scrpMgr"></asp:ToolkitScriptManager>
      <asp:HiddenField ID="hfScrollPosition" runat="server" />
      <asp:GridView ID="grdRA" runat="server" AutoGenerateColumns="False" CssClass="AltRow TableHeader" DataKeyNames="UserID"
        OnRowDataBound="grdRA_RowDataBound" Width="100%" EmptyDataText="No Data Available" EnableModelValidation="True" ShowHeader="False" Visible="False">
        <Columns>
          <asp:TemplateField ShowHeader="False">
            <ItemTemplate>
              <asp:ImageButton ID="imgbtnAllGrids" runat="server" ImageUrl="~/images/plus.gif"
                CommandArgument="Show" OnClick="showHideAllGrids" />
              <asp:Panel ID="pnlAllGrids" runat="server" Visible="false" Style="position: relative">
                <%-- FinalPayment --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdFinalAndNonDepositAdmin" runat="server" AutoGenerateColumns="False"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFinalAndNonDeposit_PageIndexChanging" OnSorting="grdFinalAndNonDeposit_Sorting"
                    CssClass="AltRow caption" DataKeyNames="UserID" Caption="Non-Deposits/Final Payments" Width="100%">
                    <Columns>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnFinalAndNonDepositAdminName" runat="server" OnClick="lnkbtnClientID_Click"
                            Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="lnkbtnFinalAndNonDepositAdminEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnFinalAndNonDepositAdminResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                            Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
                      <asp:BoundField DataField="TotalCharge" SortExpression="TotalCharge" HeaderText="Total Charge" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      <asp:BoundField DataField="BalanceDue" SortExpression="BalanceDue" HeaderText="Balance Due" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      <asp:BoundField DataField="PaymentDueDate" SortExpression="PaymentDueDate" HeaderText="Payment Due" DataFormatString="{0:d}" />
                    </Columns>
                  </asp:GridView>
                </div>
                <%-- Returning Client Callback --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdReturnClientCallbackAdmin" runat="server" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdReturnClientCallback_PageIndexChanging" OnSorting="grdReturnClientCallback_Sorting"
                    CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Return Client Callback" Width="100%">
                    <Columns>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnReturnClientCallbackAdminName" runat="server" OnClick="lnkbtnClientID_Click"
                            Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="hlReturnClientCallbackAdminEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
                    </Columns>
                  </asp:GridView>
                </div>
                <%-- Future Client Callback --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdFCCAdmin" runat="server" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFCC_PageIndexChanging" OnSorting="grdFCC_Sorting"
                    CssClass="AltRow TableHeader" DataKeyNames="ClientID" Caption="Future Client Callback" Width="100%">
                    <Columns>
                      <%--
              <asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton ID="imgbtnResvHistory2" runat="server" OnClick="showHideGrdResvHistory2" ImageUrl="~/images/plus.gif"
                    CommandArgument="Show" />
                  <asp:Panel ID="pnlResvHistory2" runat="server" Visible="false" Style="position: relative">
                    <asp:GridView ID="grdResvHistory2" runat="server" AutoGenerateColumns="false" PageSize="10"
                      AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdResvHistory2_PageIndexChanging" OnSorting="grdResvHistory_Sorting"
                      CssClass="Nested_ChildGrid" Caption="Reservation">
                      <Columns>
                        <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                          <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnResvHistory2Itinerary" runat="server" OnClick="lnkbtnItinerary_Click"
                              Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                          </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                          <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnResvHistory2ResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                              Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                          </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Name" />
                        <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                        <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                          <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FullName" SortExpression="FullName" HeaderText="Resv Agent" />
                      </Columns>
                    </asp:GridView>
                  </asp:Panel>
                </ItemTemplate>
              </asp:TemplateField>
                      --%>
                      <%--
                      <asp:BoundField DataField="OrderNumber" SortExpression="OrderNumber" HeaderText="Order No." />
                      --%>
                      <asp:TemplateField HeaderText="Print">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnPrintFCCAdmin" runat="server" OnClick="lnkbtnPrintFCC_Click" CommandArgument='<%# Eval("CallbackID") %>'>Print</asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnFCCAdminName" runat="server" OnClick="lnkbtnClientID_Click"
                            Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="hlFCCAdminmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="CallbackDate" SortExpression="CallbackDate" HeaderText="Callback Date" DataFormatString="{0:d}" />
                      <%--
                      <asp:BoundField DataField="CreateDate" SortExpression="CreateDate" HeaderText="Create Date" DataFormatString="{0:d}" />
                      --%>
                      <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
                      <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
                      <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="Remarks" />
                    </Columns>
                  </asp:GridView>
                </div>
                <%-- Inq and Waitlist --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdInqAndWaitListAdmin" runat="server" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdInqAndWaitList_PageIndexChanging" OnSorting="grdInqAndWaitList_Sorting"
                    CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Inquiries and Waitlists" Width="100%">
                    <Columns>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnInqAndWaitListAdminName" runat="server" OnClick="lnkbtnClientID_Click"
                            Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="lnkbtnInqAndWaitListAdminEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <%--
                      <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                            Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      --%>
                      <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnInqAndWaitListAdminResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                            Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
                      <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                      <%--
                      <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      --%>
                    </Columns>
                  </asp:GridView>
                </div>
                <%-- In House and Arrival (next 10 days) --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdInHouseAndArrivalAdmin" runat="server" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdInHouseAndArrival_PageIndexChanging" OnSorting="grdInHouseAndArrival_Sorting"
                    CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="In House and Arrivals" Width="100%">
                    <Columns>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnInHouseAndArrivalAdminName" runat="server" OnClick="lnkbtnClientID_Click"
                            Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="hlInHouseAndArrivalAdminEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <%--
                      <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                            Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      --%>
                      <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnInHouseAndArrivalAdminResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                            Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'>
                          </asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
                      <%-- 
                      <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                      <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      --%>
                      <asp:BoundField DataField="Car" SortExpression="Car" HeaderText="Car" />
                      <asp:BoundField DataField="Arrival" SortExpression="Arrival" HeaderText="Arr" />
                      <asp:BoundField DataField="AdditionalPassenger" SortExpression="AdditionalPassenger" HeaderText="Add Pax" />
                      <asp:BoundField DataField="CellularPhoneNumber" SortExpression="CellularPhoneNumber" HeaderText="Cell #" />
                    </Columns>
                  </asp:GridView>
                </div>
                <%-- Reservation (BKD) --%>
                <div style="overflow-y: auto; height: auto; min-height: 1px;">
                  <asp:GridView ID="grdReservationAdmin" runat="server" AutoGenerateColumns="false" PageSize="10"
                    AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdReservation_PageIndexChanging" OnSorting="grdReservation_Sorting"
                    CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Reservations" Width="100%">
                    <Columns>
                      <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnReservationAdminName" runat="server" OnClick="lnkbtnClientID_Click"
                            Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                        <ItemTemplate>
                          <asp:HyperLink ID="hlReservationAdminEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                            Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <%--
                      <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                            Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      --%>
                      <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                        <ItemTemplate>
                          <asp:LinkButton ID="lnkbtnReservationResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                            Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
                      <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
                      <%--
                      <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
                      <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                        <ItemStyle HorizontalAlign="Right" />
                      </asp:BoundField>
                      --%>
                    </Columns>
                  </asp:GridView>
                </div>
              </asp:Panel>
            </ItemTemplate>
            <ItemStyle Width="20px" />
          </asp:TemplateField>
          <asp:BoundField DataField="FullName" ShowHeader="False" />
        </Columns>
        <EmptyDataTemplate>
          <asp:Label ID="lblRANodata" runat="server" Text="No data to display."></asp:Label>
        </EmptyDataTemplate>
      </asp:GridView>
    </div>
    <%-- Individaul Reservation Agent --%>
    <div>
      <asp:Panel ID="pnlAllGrids" runat="server" Visible="true" Style="position: relative">
        <%-- FinalPayment --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:GridView ID="grdFinalAndNonDepositRA" runat="server" AutoGenerateColumns="False"
            AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdFinalAndNonDeposit_PageIndexChanging" OnSorting="grdFinalAndNonDeposit_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Non-Deposits/Final Payments" Width="100%">
            <Columns>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnFinalAndNonDepositRaName" runat="server" OnClick="lnkbtnClientID_Click"
                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:HyperLink ID="lnkbtnFinalAndNonDepositRaEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnFinalAndNonDepositRaResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
              <asp:BoundField DataField="TotalCharge" SortExpression="TotalCharge" HeaderText="Total Charge" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              <asp:BoundField DataField="BalanceDue" SortExpression="BalanceDue" HeaderText="Balance Due" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              <asp:BoundField DataField="PaymentDueDate" SortExpression="PaymentDueDate" HeaderText="Payment Due" DataFormatString="{0:d}" />
            </Columns>
          </asp:GridView>
        </div>
        <%-- Returning Client Callback --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:GridView ID="grdReturnClientCallbackRA" runat="server" AutoGenerateColumns="false" PageSize="10"
            AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdReturnClientCallback_PageIndexChanging" OnSorting="grdReturnClientCallback_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Return Client Callback" Width="100%">
            <Columns>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnReturnClientCallbackRaName" runat="server" OnClick="lnkbtnClientID_Click"
                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:HyperLink ID="hlReturnClientCallbackRaEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
            </Columns>
          </asp:GridView>
        </div>
        <%-- Future Client Callback --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:GridView ID="grdFCCRA" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnPageIndexChanging="grdFCC_PageIndexChanging" OnSorting="grdFCC_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="ClientID" Caption="Future Client Callback" Width="100%" EnableModelValidation="True">
            <Columns>
              <asp:TemplateField>
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnPrintFCC" runat="server" OnClick="lnkbtnPrintFCC_Click" CommandArgument='<%# Eval("CallbackID") %>'>Print</asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnFCCRaName" runat="server" OnClick="lnkbtnClientID_Click"
                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:HyperLink ID="hlFCCRaEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="CallbackDate" SortExpression="CallbackDate" HeaderText="Callback Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="LeadScoreDescription" SortExpression="LeadScoreDescription" HeaderText="Lead Score" />
              <asp:BoundField DataField="Destination" SortExpression="Destination" HeaderText="Destination" />
              <asp:BoundField DataField="Remarks" SortExpression="Remarks" HeaderText="Remarks" />
            </Columns>
          </asp:GridView>
        </div>
        <%-- Inquiry and Waitlist Reservations--%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:GridView ID="grdInqAndWaitListRA" runat="server" AutoGenerateColumns="false" PageSize="10"
            AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdInqAndWaitList_PageIndexChanging" OnSorting="grdInqAndWaitList_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Inquiries and Waitlists" Width="100%">
            <Columns>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnInqAndWaitListRaName" runat="server" OnClick="lnkbtnClientID_Click"
                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:HyperLink ID="lnkbtnInqAndWaitListRaEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                </ItemTemplate>
              </asp:TemplateField>
              <%--
              <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                    Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              --%>
              <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnInqAndWaitListRaResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
              <%-- 
              <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
              <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              --%>
            </Columns>
          </asp:GridView>
        </div>
        <%-- In House and Arrival (next 10 days) --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:GridView ID="grdInHouseAndArrivalRA" runat="server" AutoGenerateColumns="false" PageSize="10"
            AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdInHouseAndArrival_PageIndexChanging" OnSorting="grdInHouseAndArrival_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="In House and Arrivals" Width="100%">
            <Columns>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnInHouseAndArrivalRaName" runat="server" OnClick="lnkbtnClientID_Click"
                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:HyperLink ID="hlInHouseAndArrivalRaEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                </ItemTemplate>
              </asp:TemplateField>
              <%--
              <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                    Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              --%>
              <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnInHouseAndArrivalRaResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'>
                  </asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
              <%-- 
              <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
              <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              --%>
              <asp:BoundField DataField="Car" SortExpression="Car" HeaderText="Car" />
              <asp:BoundField DataField="Arrival" SortExpression="Arrival" HeaderText="Arr" />
              <asp:BoundField DataField="AdditionalPassenger" SortExpression="AdditionalPassenger" HeaderText="Add Pax" />
              <asp:BoundField DataField="CellularPhoneNumber" SortExpression="CellularPhoneNumber" HeaderText="Cell #" />
            </Columns>
          </asp:GridView>
        </div>
        <%-- Reservation (BKD) --%>
        <div style="overflow-y: auto; height: auto; min-height: 1px;">
          <asp:GridView ID="grdReservationRA" runat="server" AutoGenerateColumns="false" PageSize="10"
            AllowPaging="false" AllowSorting="true" OnPageIndexChanging="grdReservation_PageIndexChanging" OnSorting="grdReservation_Sorting"
            CssClass="AltRow TableHeader" DataKeyNames="UserID" Caption="Reservations" Width="100%">
            <Columns>
              <asp:TemplateField HeaderText="Client Name" SortExpression="LastName, FirstName">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnReservationRaName" runat="server" OnClick="lnkbtnClientID_Click"
                    Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %>' CommandArgument='<%# Eval("ClientID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="EMail Address" SortExpression="EMailAddress">
                <ItemTemplate>
                  <asp:HyperLink ID="hlReservationRaEmail" runat="server" NavigateUrl='<%# string.Format("mailto:{0}", Eval("EMailAddress")) %>'
                    Text='<%# Eval("EMailAddress") %>'></asp:HyperLink>
                </ItemTemplate>
              </asp:TemplateField>
              <%--
              <asp:TemplateField HeaderText="Itinerary" SortExpression="ItineraryID">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnResvHistoryItineraryRA" runat="server" OnClick="lnkbtnItinerary_Click"
                    Text='<%# Eval("ItineraryID") %>' CommandArgument='<%# Eval("ItineraryID") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              --%>
              <asp:TemplateField HeaderText="Resv No." SortExpression="ReservationNumber">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkbtnReservationRaResvNo" runat="server" OnClick="lnkbtnResvNo_Click"
                    Text='<%# Eval("ReservationNumber") %>' CommandArgument='<%# Eval("ReservationNumber") %>'></asp:LinkButton>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="DateFrom" SortExpression="DateFrom" HeaderText="Begin Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="DateTo" SortExpression="DateTo" HeaderText="End Date" DataFormatString="{0:d}" />
              <asp:BoundField DataField="Villa" SortExpression="Villa" HeaderText="Villa Code" />
              <%--
              <asp:BoundField DataField="Status" SortExpression="Status" HeaderText="Status" />
              <asp:BoundField DataField="GrossPrice" SortExpression="GrossPrice" HeaderText="GrossPrice" DataFormatString="{0:n}">
                <ItemStyle HorizontalAlign="Right" />
              </asp:BoundField>
              --%>
            </Columns>
          </asp:GridView>
        </div>
      </asp:Panel>

    </div>
  </form>
</body>
</html>
