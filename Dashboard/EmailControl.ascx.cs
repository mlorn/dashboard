﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using WimcoBaseLogic;
using WimcoBaseLogic.BusinessLogic;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Windows.Forms;

namespace GWCustom
{
  public partial class EmailControl : System.Web.UI.UserControl
  {
    public string EmailTo { get; set; }
    public string EmailFrom { get; set; }
    public string EmailBody { get; set; }
    public string EmailSubject { get; set; }
    public string EmailCC { get; set; }
    public string EmailName { get; set; }
    public Attachment EmailAttach;
    public DataTable dtTemplates;
    public string Usage { get; set; }
    public string LeadID { get; set; }
    public string TemplateKindID { get; set; }
    public string EmailOriginal { get; set; }
    public string TemplateID;
    public Boolean IsSent { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        if (Session["EmailObject"] != null)
        {
          Session["EmailObjectOriginal"] = Session["EmailObject"];
        }
        try
        {
          Session.Remove("EmailSent");
          Session.Remove("TemplateID");
        }
        catch
        {
        }
        txtEmailToAddress.Text = EmailTo;
        txtEmailFromEmailAddress.Text = EmailFrom;
        txtEmailFromName.Text = EmailName;
        txtEmailSubject.Text = EmailSubject;
        txtEditor.Text = HttpUtility.HtmlDecode(EmailBody);
        txtEmailCCEmailAddress.Text = EmailCC;
        if (TemplateKindID == null)
        {
          TemplateKindID = "3";
        }
        PopulateTemplateDDL(TemplateKindID);
      }
      //Section below pre-populates the list of MagUsers that appear in the auto-fill on the email fields
      DataSet dsUsers = WimcoBaseLogic.BaseLogic.AdHoc("SELECT DISTINCT mu.EmailAddress FROM  MagUser AS mu INNER JOIN UserGroup AS ug ON ug.UserID = mu.UserID WHERE  (mu.OrganizationID = 1) AND (NOT (mu.EmailAddress IS NULL)) AND (mu.IsEnabled = 'Y') AND (mu.EmailAddress <> 'none')");
      if (!hfMU.Value.Contains("@"))
      {
        foreach (DataRow drMU in dsUsers.Tables[0].Rows)
        {
          hfMU.Value += drMU[0].ToString() + ",";

        }
      }
      //End pre-fill section
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
      if (IsPostBack)
      {
        if (Session["EmailSent"] != null)
        {
          txtEditor.Text = Session["EmailSent"].ToString();
          lblAction.Visible = true;
        }
        else
        {
          if (TemplateKindID == null)
          {
            TemplateKindID = "3";
          }
          PopulateTemplateDDL(TemplateKindID);
        }
      }



    }
    protected void PopulateTemplateDDL(string TKID)
    {

      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      DataSet dsTemplates = Dashboard.BaseLogic.AdHoc("Exec spGetUserEmailTemplatesByKind @TemplateKindID = " + TKID + ", @UserID = " + ud.UserID);
      DataTable dtTemplate = new DataTable();
      dtTemplates = dsTemplates.Tables[0];
      ddlTemplates.Items.Clear(); // Clean out DDL



      for (int i = 0; i < dtTemplates.Rows.Count; i++)
      {
        ListItem li2 = new ListItem();
        li2.Value = dtTemplates.Rows[i][0].ToString(); //Populate the DDL
        if (dtTemplates.Rows[i][4] == null || dtTemplates.Rows[i][4].ToString() == "")
        {
          li2.Text = dtTemplates.Rows[i][6].ToString().Trim();
        }
        else { li2.Text = dtTemplates.Rows[i][6].ToString().Trim(); }

        li2.Selected = true;

        ddlTemplates.Items.Add(li2);

      }
      ListItem li = new ListItem();
      li.Value = "0";
      li.Text = "Select a Template";
      ddlTemplates.Items.Insert(0, li);
      if (Session["TemplateID"] != null) //Set the Selected Index to match the TemplateID or 0 if it's not there
      {
        ddlTemplates.SelectedValue = Session["TemplateID"].ToString();
      }
      else
      {
        ddlTemplates.SelectedIndex = 0;
      }
      ddlTemplates.DataBind();
      //upReply.Update();

    }
    protected void repackageMailObject()
    {
      MailMessage emsg = new MailMessage();
      string strTos = txtEmailToAddress.Text.ToLower();
      if (strTos.Length > 0)
      {
        foreach (string s in strTos.Split(','))
        {
          if (s != null && s != "")
          {
            emsg.To.Add(new MailAddress(s, s));
          }
        }
      }

      emsg.From = new MailAddress(txtEmailFromEmailAddress.Text, txtEmailFromName.Text);
      emsg.Subject = txtEmailSubject.Text;
      string strCCs = txtEmailCCEmailAddress.Text;
      if (strCCs.Length > 0)
      {
        foreach (string s in strCCs.Split(','))
        {
          if (s != null && s != "")
          {
            emsg.CC.Add(new MailAddress(s, s));
          }
        }
      }
      emsg.Body = HttpUtility.HtmlEncode(txtEditor.Text);


      Session["EmailObject"] = emsg;


    } //Updates the mail object that drives the form fields.  Needs to happen because of html validation

    protected void btnSend_Click(object sender, EventArgs e)
    {
      MailMessage mm = new MailMessage();
      try
      {
        mm = (MailMessage)Session["EmailObjectOriginal"];
      }
      catch
      {

        mm.Body = "";
      }
      System.Web.UI.WebControls.Button b = (System.Web.UI.WebControls.Button)sender;
      if (b.ClientID.ToLower().Contains("fcc")) //Send + FCC
      {
        fccCustom.AddFCC addfcc = (fccCustom.AddFCC)b.NamingContainer.Parent.FindControl("wimAddFCC");
        if (addfcc.Visible)
        {

          //if (!addfcc.FCCIsValid())
          //{

          //    //Session["EmailBody"] = HttpUtility.HtmlDecode(txtEditor.Text);
          //    repackageMailObject();
          //    lblAction.Text = "Your email has NOT been sent due to missing FCC data. Please correct and try sending again.";
          //    return;
          //}
          //  else {
          if (!addfcc.AddNewFCC())
          {
            repackageMailObject();
            lblAction.Text = "Your email has NOT been sent due to missing FCC data. Please correct and try sending again.";
            return;
          }
        }

        //   }
        else
        {
          Dashboard.EditFCC editFCC = (Dashboard.EditFCC)b.NamingContainer.Parent.FindControl("editFCC");
          //if (!editFCC.FCCIsValid())
          //{
          //    repackageMailObject();
          //    lblAction.Text = "Your email has NOT been sent due to missing FCC data. Please correct and try sending again.";
          //    return;
          //}
          //else {
          if (!editFCC.ModifyFCC())
          {
            repackageMailObject();
            lblAction.Text = "Your email has NOT been sent due to missing FCC data. Please correct and try sending again.";
            return;
          }
          // }
        }
      }


      bool isHtmlEmail = false;
      string html = txtEditor.Text;
      string original = ""; // This section looks at the original email to determine Signature placement
      if (EmailBody != null)
      {
        if (EmailBody != "")
        {
          original = EmailBody;
        }
        else
        {
          try
          {
            original = mm.Body;
          }
          catch
          {
            original = "<w99></w99>";
          }
        }
      }
      else
      {
        try
        {
          original = mm.Body;
        }
        catch
        {
          original = "<w99></w99>";
        }

      }
      string useradded = "";
      string theBody = "";
      string FinalMessage = "";

      RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
      Regex regx = new Regex("<body(?<theBody>.*)/body>", options);
      Match match = regx.Match(original);
      isHtmlEmail = true;
      int end = html.IndexOf("&lt;w99");
      if (end == -1)
      {
        end = html.IndexOf("<w99");
        if (end == -1)
        {
          end = html.Length;
        }

      }
      useradded = html.Substring(0, end);
      if (html.Contains(original) && original != "")
      {
        FinalMessage = "<html>" + useradded + original;
      }
      else
      {
        FinalMessage = "<html>" + HttpUtility.HtmlDecode(html);
      }
      //}
      //+ "<br/>" + htmlEdit.Decode(Signature)
      String Signature = "";
      String temp = "";
      Dashboard.BaseLogic.UserData ud = (Dashboard.BaseLogic.UserData)Session["UserData"];
      if (chkSig.Checked)
      {
        String temp2 = "";

        int ArrowIndex = FinalMessage.IndexOf(">>>"); //Designate spot to insert signature.  Based on >>>s
        if (ArrowIndex > 0)
        {
          int FMLength = FinalMessage.Length;
          string firstHalf = FinalMessage.Substring(0, ArrowIndex);
          string secondHalf = FinalMessage.Substring(ArrowIndex, FMLength - ArrowIndex);
          temp2 = firstHalf + "<!--INSERT SIG HERE-->" + secondHalf;
          FinalMessage = temp2;
        }

        try
        {

          DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = " + ud.UserID.ToString());
          //  DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = 23"); //Test data, Bethany's Signature
          Signature = HttpUtility.HtmlDecode(dsSig.Tables[0].Rows[0][0].ToString()) + "<br/><br/>";
        }
        catch
        {
          Signature = "";
        }
        if (FinalMessage.Contains("<!--INSERT SIG HERE-->"))
        {
          temp = FinalMessage.Replace("<!--INSERT SIG HERE-->", Signature);
          FinalMessage = temp;
        }
        else
        {
          temp = FinalMessage + "<br/>" + Signature;
          FinalMessage = temp;
        }
      }
      else
      {
        Signature = "";
        temp = FinalMessage.Replace("<!--INSERT SIG HERE-->", "");
        FinalMessage = temp;
      }

      FinalMessage = FinalMessage + "</html>";
      WimcoBaseLogic.BusinessLogic.UploadTicket[] EmailAttachments;

      if (Session["EmailAttach"] != null)
      {
        try
        {
          EmailAttachments = ((List<WimcoBaseLogic.BusinessLogic.UploadTicket>)Session["EmailAttach"]).ToArray();
        }
        catch
        {
          EmailAttachments = null;
        }

      }
      else
      {
        EmailAttachments = null;
      }

      // WimcoBaseLogic.BusinessLogic.GWEmailToSendReturn gwtr = 
      WimcoBaseLogic.BusinessLogic.GWEmailToSendReturn gwtr = Dashboard.BaseLogic.sendGWEmail(txtEmailFromEmailAddress.Text, txtEmailFromName.Text, txtEmailSubject.Text, txtEmailToAddress.Text, txtEmailCCEmailAddress.Text, FinalMessage, EmailAttachments);

      if (gwtr.Success)
      {
        btnReload.Visible = false;
        txtEditor.Text = FinalMessage;
        lblAction.Text = "Message Sent!";
        lblAction.Visible = true;
        lblAction.BackColor = System.Drawing.Color.Yellow;
        timer.Interval = 2000;
        timer.Enabled = true;
        UpdateProspectingBoard(ud.UserID.ToString());
        try
        {
          Session.Remove("totalFileSize");
        }
        catch { }
        try
        {
          Session.Remove("EmailObject");
        }
        catch { }
        try
        {
          Session.Remove("EmailObjectOriginal");
        }
        catch { }
        try
        {
          Session.Remove("EmailOriginal");
        }
        catch { }
        try
        {
          Session.Remove("EmailAttach");
        }
        catch { }
        try
        {
          Session.Remove("TemplateID");
        }
        catch { }
        try
        {
          Session.Remove("TemplateKindID");
        }
        catch { }
        try
        {
          Session.Remove("EmailBody");
        }
        catch { }
        try
        {
          Session.Remove("EmailSendToAddress");
        }
        catch { }
        Session["EmailSent"] = HttpUtility.HtmlDecode(FinalMessage);

      }
      else
      {
        lblAction.Text = "Send Failed: " + gwtr.Message;
        lblAction.BackColor = System.Drawing.Color.Red;
        lblAction.Visible = true;
      }
    }

    protected void btnCancelSend_Click(object sender, EventArgs e)
    {
      try
      {
        Session.Remove("EmailObject");
      }
      catch { }
      try
      {
        Session.Remove("EmailObjectOriginal");
      }
      catch { }
      try
      {
        Session.Remove("EmailOriginal");
      }
      catch { }
      try
      {
        Session.Remove("EmailAttach");
      }
      catch { }
      try
      {
        Session.Remove("TemplateID");
      }
      catch { }
      try
      {
        Session.Remove("TemplateKindID");
      }
      catch { }
      try
      {
        Session.Remove("EmailBody");
      }
      catch { }
      try
      {
        Session.Remove("EmailSendToAddress");
      }
      catch { }
      try
      {
        Session.Remove("LeadID");
      }
      catch { }
    }
    protected void btnTemplate_Click(object sender, EventArgs e) //This one selects a template
    {
      MailMessage mmOriginal = new MailMessage();
      MailMessage mm = new MailMessage();

      if (Session["EmailObjectOriginal"] != null) //Check the original email.  may be adding it to the top in some cases
      {
        mmOriginal = (MailMessage)Session["EmailObjectOriginal"];
      }
      if (Session["EmailObject"] != null)
      {
        mm = (MailMessage)Session["EmailObject"];
      }



      String TemplateID = ddlTemplates.SelectedValue;
      if (TemplateID == "0")
      {
        if (mmOriginal.Body != null)
        {
          txtEditor.Text = HttpUtility.HtmlDecode(mmOriginal.Body); //Cleanout old template
        }
        else
        {
          txtEditor.Text = "";
        }
      }
      else
      {
        DataSet dsTemplate = BaseLogic.AdHoc("spGetUserEmailTemplate @EmailTemplateID = " + TemplateID);
        if (dsTemplate.Tables.Count > 0)
        {
          DataTable dtTemplate = dsTemplate.Tables[0];
          if (dtTemplate.Rows.Count > 0)
          {
            DataRow drTemplate = dtTemplate.Rows[0];
            string strOriginal = "";
            if (mmOriginal.Body != null) //Get original email text, add template above it
            {
              if (chkTemplate.Checked)
              {
                strOriginal = HttpUtility.HtmlDecode(mmOriginal.Body); //Keep Template if specified
              }

            }
            txtEditor.Text = drTemplate[1].ToString() + "<br  />" + strOriginal;
            txtEmailSubject.Text = drTemplate[2].ToString();
            txtNameTemplate.Text = drTemplate[4].ToString();
            if (drTemplate[3] != null)
            {
              if (drTemplate[3].ToString() != "")
              {
                btnDisableTemplate.Visible = true;
                btnEditTemplate.Visible = true;
              }
              else
              {
                btnDisableTemplate.Visible = false;
                btnEditTemplate.Visible = false;
              }
            }
            else
            {
              btnDisableTemplate.Visible = false;
              btnEditTemplate.Visible = false;
            }
          }

        }

      }
      Session["TemplateID"] = TemplateID;
      repackageMailObject();



    }
    protected void btnFCCTemplateSubmit_Click(object sender, EventArgs e)
    {
    }
    protected void btnCancelTemplate_Click(object sender, EventArgs e)
    {
      pnlSaveTemplate.Visible = false;
      btnSaveAsTemplate.Visible = true;
    }
    protected void btnSaveAsTemplate_Click(object sender, EventArgs e)
    {
      pnlSaveTemplate.Visible = true;
      DataSet ds = BaseLogic.AdHoc("Select (Name + ' - ' + Description) as title, EmailTemplateKindID From EmailTemplateKind Order by EmailTemplateKindID");

      ddlTemplateKind.DataSource = ds;
      ddlTemplateKind.DataTextField = "title";
      ddlTemplateKind.DataValueField = "EmailTemplateKindID";
      ddlTemplateKind.DataBind();
      try
      {
        ddlTemplateKind.SelectedValue = Session["TemplateKindID"].ToString();
      }
      catch { ddlTemplateKind.SelectedValue = "3"; } //3 is Generic
      Session["TemplateID"] = TemplateID;
      btnSaveAsTemplate.Visible = false;
      btnSaveNewTemplate.Visible = true;
      btnSaveTemplate.Visible = false;
      //     PopulateTemplateDDL(ddlTemplateKind.SelectedValue);
    }
    protected void btnSaveNewTemplate_Click(object sender, EventArgs e) //Save a new template
    {
      String TemplateKindID = "";
      try
      {
        TemplateKindID = ddlTemplateKind.SelectedValue;
      }
      catch { return; }

      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      String UserID = ud.UserID.ToString();
      String EmailTemplateID = BaseLogic.GetNextKey("EmailTemplate").ToString();
      String EmailTemplateRefID = BaseLogic.GetNextKey("EmailTemplateUserRef").ToString();
      sdsTemplate.InsertParameters[0].DefaultValue = EmailTemplateID;
      sdsTemplate.InsertParameters[1].DefaultValue = TemplateKindID;
      sdsTemplate.InsertParameters[2].DefaultValue = htmlEdit.Decode(txtEditor.Text);
      sdsTemplate.InsertParameters[3].DefaultValue = "True";
      sdsTemplate.InsertParameters[4].DefaultValue = DateTime.Now.ToString();
      sdsTemplate.InsertParameters[5].DefaultValue = txtNameTemplate.Text;
      sdsTemplate.InsertParameters[6].DefaultValue = EmailTemplateRefID;
      sdsTemplate.InsertParameters[7].DefaultValue = "14";
      sdsTemplate.InsertParameters[8].DefaultValue = UserID;
      sdsTemplate.InsertParameters[9].DefaultValue = txtEmailSubject.Text;
      sdsTemplate.Insert();
      pnlSaveTemplate.Visible = false;
      btnSaveAsTemplate.Visible = true;
      btnEditTemplate.Visible = true;
      btnDisableTemplate.Visible = true;
      Session["TemplateID"] = EmailTemplateID;
      //    PopulateTemplateDDL(TemplateKindID);
    }

    protected void btnSaveTemplate_Click(object sender, EventArgs e) //Save a new template
    {
      String TemplateKindID = "";
      try
      {
        TemplateKindID = ddlTemplateKind.SelectedValue;
      }
      catch { return; }

      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      String UserID = ud.UserID.ToString();
      String EmailTemplateID = Session["TemplateID"].ToString();

      //	@EmailTemplateID int,
      //@TemplateTextHTML nvarchar(MAX),
      //@Subject nchar(50),
      //@Name varchar(50),
      //@IsEnabled bit

      sdsTemplate.UpdateParameters[0].DefaultValue = EmailTemplateID;
      sdsTemplate.UpdateParameters[1].DefaultValue = TemplateKindID;
      sdsTemplate.UpdateParameters[2].DefaultValue = htmlEdit.Decode(txtEditor.Text);
      sdsTemplate.UpdateParameters[3].DefaultValue = txtEmailSubject.Text;
      sdsTemplate.UpdateParameters[4].DefaultValue = txtNameTemplate.Text;
      sdsTemplate.UpdateParameters[5].DefaultValue = "True";

      sdsTemplate.Update();
      pnlSaveTemplate.Visible = false;
      btnSaveAsTemplate.Visible = true;
      btnEditTemplate.Visible = true;
      btnDisableTemplate.Visible = true;
      Session["TemplateID"] = EmailTemplateID;
      //    PopulateTemplateDDL(TemplateKindID);
    }
    protected void btnEditTemplate_Click(object sender, EventArgs e)
    {
      pnlSaveTemplate.Visible = true;
      DataSet ds = BaseLogic.AdHoc("Select (Name + ' - ' + Description) as title, EmailTemplateKindID From EmailTemplateKind Order by EmailTemplateKindID");

      ddlTemplateKind.DataSource = ds;
      ddlTemplateKind.DataTextField = "title";
      ddlTemplateKind.DataValueField = "EmailTemplateKindID";
      ddlTemplateKind.DataBind();
      try
      {
        ddlTemplateKind.SelectedValue = Session["TemplateKindID"].ToString();
      }
      catch { ddlTemplateKind.SelectedValue = "3"; } //3 is Generic
                                                     //      Session["TemplateID"] = TemplateID;
      btnSaveAsTemplate.Visible = false;
      btnSaveTemplate.Visible = true;
      btnSaveNewTemplate.Visible = false;
      btnEditTemplate.Visible = false;
      btnDisableTemplate.Visible = false;
      ddlTemplateKind.DataBind();


      //    PopulateTemplateDDL(TemplateKindID);
    }
    protected void btnDisableTemplate_Click(object sender, EventArgs e)
    {
      pnlConfirmDelete.Visible = true;
    }

    protected void btnSig_Click(object sender, EventArgs e)
    { //Updates signature
      String strSig = txtEditSig.Text;
      Dashboard.BaseLogic.UserData ud = (Dashboard.BaseLogic.UserData)Session["UserData"];
      int UserID = ud.UserID;
      sdsSig.UpdateParameters[0].DefaultValue = UserID.ToString();
      sdsSig.UpdateParameters[1].DefaultValue = txtEditSig.Text;
      sdsSig.Update();
      pnlEditSig.Visible = false;
      btnEditSig.Visible = true;
      hfSigPostBack.Value = "Y"; //This may not be necessary. I think it was only for the FCC piece
      String sql = "Exec spSaveRAEmailSignature @UserID = " + UserID.ToString() + " @EmailSignature = '" + strSig + "'";
      DataSet ds = BaseLogic.AdHoc(sql);
    }
    protected void btnEditSig_Click(object sender, EventArgs e)
    {
      pnlEditSig.Visible = true;
      String Signature = "";
      try
      {
        Dashboard.BaseLogic.UserData ud = (Dashboard.BaseLogic.UserData)Session["UserData"];
        DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = " + ud.UserID.ToString());
        //  DataSet dsSig = BaseLogic.AdHoc("Select  Personnel.EmailSignature from Personnel Where Personnel.UserID = 23"); //Test data, Bethany's Signature
        Signature = dsSig.Tables[0].Rows[0][0].ToString();
      }
      catch
      {
        Signature = "";
      }
      txtEditSig.Text = Signature;
      btnEditSig.Visible = false;
      hfSigPostBack.Value = "Y";
    }

    protected void btnCancelSig_Click(object sender, EventArgs e)
    {
      pnlEditSig.Visible = false;
      btnEditSig.Visible = true;
      hfSigPostBack.Value = "Y";
    }
    protected void btnShowAllEmails_Click(object sender, EventArgs e)
    {
      string AuthKey = WimcoBaseLogic.BaseLogic.GetAuthKey();
      btnShowAllEmails.Visible = false;
      WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic bizLogic = new WimcoBaseLogic.BusinessLogic.WimcoBusinessLogic();
      //  ObjectDataSource1.SelectParameters["Email"].DefaultValue = Session["EmailToSearch"].ToString();
      //    ObjectDataSource1.DataBind();
      // bizLogic.Timeout = 5000;
      WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS cel = new WimcoBaseLogic.BusinessLogic.ClientEmailLookupDS();
      String[] emails = WimcoBaseLogic.BaseLogic.clientemail_GetEmailAddressArray(Int32.Parse(Session["ClientID"].ToString()));
      GW_IndexSearchDS GWDS = WimcoBaseLogic.BaseLogic.SearchByEmailAddresses(emails);
      if (GWDS.Tables.Count > 0)
      {
        String EmailText = "";

        DataTable dtEmails = GWDS.Tables[0];
        for (int i = 0; i < dtEmails.Rows.Count; i++)
        {
          GWMailObject mail = new GWMailObject();
          String GroupWiseID = dtEmails.Rows[i]["GroupwiseID"].ToString();
          String html = "";
          mail = bizLogic.Groupwise_GetGroupwiseMailObjectByGroupwiseID(AuthKey, GroupWiseID);
          if (mail.StatusCode > -1)
          {
            if (!mail.MessageInHTML.Contains("mail has been deleted from Server") && !mail.MessageInHTML.Contains("Can't reach Groupwise to read this email"))
            {

              html = html + "<div class='email'><jk></jk>";

              if (html == "<div class='email'><jk></jk>")
              {
                html += "<br/> >>> " + mail.FromName + " (" + mail.FromEmail + "&gt; ";
                html = html + mail.DateSent.ToString("MM/dd/yyyy HH:mm tt") + ">>> <br/>";
              }


              //html = html + "Date : " + mail.DateSent.ToString("yyyy-MM-dd HH:mm:ss") + "<br/>";
              //html = html + "From: " + mail.FromEmail + " To: " + mail.Distribution.to + "<br/>";
              //html = html + "Subject : " + mail.Subject + "<br/>";
              //html = html + "<br/><br/><br/>";
              html = html + "</div>";
              html = html + mail.MessageInHTML.Replace("!important", "").Replace("text/javascript", "text/noscript");
              EmailText += html;
            }
          }

        }
        txtEditor.Text = EmailText;
        repackageMailObject();
        Session["EmailObjectOriginal"] = Session["EmaliObject"]; //I think this has to persist in this case.  Retain the show all.
      }

    }

    protected void btnReload_Click(object sender, EventArgs e)
    {

    }
    protected void btnClear_Click(object sender, EventArgs e)
    { //Wipe contents of main text box. Prevents extra code from getting stuck if removed otherwise
      try
      {
        Session.Remove("EmailOriginal");
      }
      catch { }
      try
      {
        Session.Remove("EmailBody");
      }
      catch { }
      try
      {
        Session.Remove("TemplateID");
      }
      catch { }


      try
      {
        ddlTemplates.SelectedIndex = 0;
      }
      catch { }
      txtEditor.Text = "";
      btnShowAllEmails.Visible = true;

    }
    protected void timer_Tick(object sender, EventArgs e)
    {
      // lblAction.Text = "";


    }
    protected void UpdateProspectingBoard(String UserID)
    {
      if (Session["ClientID"] != null)
      {
        String ClientID = Session["ClientID"].ToString();
        DataSet ds = BaseLogic.AdHoc("Select * From ProspectingClient Where ClientID = " + ClientID);
        if (ds.Tables.Count > 0)
        {
          if (ds.Tables[0].Rows.Count > 0)
          {
            String sql = String.Format("EXEC [dbo].[spProspectingBoardUpdateLastEmailSent] @prmClientID = {0}, @prmUserID = {1}, @prmEmailSentDate = N'{2}'", ClientID, UserID, DateTime.Now.ToShortDateString());
            DataSet ds2 = BaseLogic.AdHoc(sql);
          }
        }

      }
    }

    protected void btnCancelDelete_Click(object sender, EventArgs e)
    {
      pnlConfirmDelete.Visible = false;
    }

    protected void btnConfirmDelete_Click(object sender, EventArgs e)
    {
      String TemplateKindID = "";
      try
      {
        TemplateKindID = ddlTemplateKind.SelectedValue;
      }
      catch { return; }

      Dashboard.BaseLogic.UserData ud = Dashboard.BaseLogic.GetUserData();
      String UserID = ud.UserID.ToString();
      String EmailTemplateID = Session["TemplateID"].ToString();


      sdsTemplate.UpdateParameters[0].DefaultValue = EmailTemplateID;
      sdsTemplate.UpdateParameters[1].DefaultValue = TemplateKindID;
      sdsTemplate.UpdateParameters[2].DefaultValue = htmlEdit.Decode(txtEditor.Text);
      sdsTemplate.UpdateParameters[3].DefaultValue = txtEmailSubject.Text;
      sdsTemplate.UpdateParameters[4].DefaultValue = txtNameTemplate.Text;
      sdsTemplate.UpdateParameters[5].DefaultValue = "False";

      sdsTemplate.Update();
      pnlConfirmDelete.Visible = false;
      btnSaveAsTemplate.Visible = true;
      btnEditTemplate.Visible = false;
      btnDisableTemplate.Visible = false;
      Session["TemplateID"] = null;
    }
  }
}