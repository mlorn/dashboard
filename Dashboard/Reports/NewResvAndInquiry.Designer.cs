﻿namespace Dashboard.Reports
{
  partial class NewResvAndInquiry
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      DevExpress.DataAccess.Sql.StoredProcQuery storedProcQuery1 = new DevExpress.DataAccess.Sql.StoredProcQuery();
      DevExpress.DataAccess.Sql.QueryParameter queryParameter1 = new DevExpress.DataAccess.Sql.QueryParameter();
      DevExpress.DataAccess.Sql.QueryParameter queryParameter2 = new DevExpress.DataAccess.Sql.QueryParameter();
      DevExpress.DataAccess.Sql.QueryParameter queryParameter3 = new DevExpress.DataAccess.Sql.QueryParameter();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewResvAndInquiry));
      DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary11 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary12 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary13 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary14 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary15 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary16 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary17 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary18 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary19 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary20 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary21 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary22 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary23 = new DevExpress.XtraReports.UI.XRSummary();
      DevExpress.XtraReports.UI.XRSummary xrSummary24 = new DevExpress.XtraReports.UI.XRSummary();
      this.sqlDataSource1 = new DevExpress.DataAccess.Sql.SqlDataSource(this.components);
      this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
      this.DetailCaption1 = new DevExpress.XtraReports.UI.XRControlStyle();
      this.DetailData1 = new DevExpress.XtraReports.UI.XRControlStyle();
      this.DetailData3_Odd = new DevExpress.XtraReports.UI.XRControlStyle();
      this.PageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
      this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
      this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
      this.pageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
      this.pageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
      this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
      this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
      this.label1 = new DevExpress.XtraReports.UI.XRLabel();
      this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.Detail = new DevExpress.XtraReports.UI.DetailBand();
      this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
      this.prmStartDateBooked = new DevExpress.XtraReports.Parameters.Parameter();
      this.prmEndDateBooked = new DevExpress.XtraReports.Parameters.Parameter();
      this.prmResvAgent = new DevExpress.XtraReports.Parameters.Parameter();
      this.calcDepositPercent = new DevExpress.XtraReports.UI.CalculatedField();
      this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
      this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
      this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
      this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
      this.celcWithMoney = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
      this.celGrossPrice = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
      this.calcNoOfNightWithMoney = new DevExpress.XtraReports.UI.CalculatedField();
      this.groupHeaderBand1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
      this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
      this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
      this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
      this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
      this.celcGrossPrice2 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
      this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
      this.calcTotalCharge = new DevExpress.XtraReports.UI.CalculatedField();
      this.calcPMTotalRecdClient = new DevExpress.XtraReports.UI.CalculatedField();
      this.calcDepositDue = new DevExpress.XtraReports.UI.CalculatedField();
      this.calcWimcoTotalRecdClient = new DevExpress.XtraReports.UI.CalculatedField();
      this.calcGrossPrice = new DevExpress.XtraReports.UI.CalculatedField();
      this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      // 
      // sqlDataSource1
      // 
      this.sqlDataSource1.ConnectionName = "MagnumConnectionString";
      this.sqlDataSource1.Name = "sqlDataSource1";
      storedProcQuery1.Name = "spDashboardNewAndInquiry";
      queryParameter1.Name = "@prmStartDateBooked";
      queryParameter1.Type = typeof(DevExpress.DataAccess.Expression);
      queryParameter1.Value = new DevExpress.DataAccess.Expression("?prmStartDateBooked", typeof(System.DateTime));
      queryParameter2.Name = "@prmEndDateBooked";
      queryParameter2.Type = typeof(DevExpress.DataAccess.Expression);
      queryParameter2.Value = new DevExpress.DataAccess.Expression("?prmEndDateBooked", typeof(System.DateTime));
      queryParameter3.Name = "@prmResvAgent";
      queryParameter3.Type = typeof(DevExpress.DataAccess.Expression);
      queryParameter3.Value = new DevExpress.DataAccess.Expression("?prmResvAgent", typeof(string));
      storedProcQuery1.Parameters.Add(queryParameter1);
      storedProcQuery1.Parameters.Add(queryParameter2);
      storedProcQuery1.Parameters.Add(queryParameter3);
      storedProcQuery1.StoredProcName = "spDashboardNewAndInquiry";
      this.sqlDataSource1.Queries.AddRange(new DevExpress.DataAccess.Sql.SqlQuery[] {
            storedProcQuery1});
      this.sqlDataSource1.ResultSchemaSerializable = resources.GetString("sqlDataSource1.ResultSchemaSerializable");
      // 
      // Title
      // 
      this.Title.BackColor = System.Drawing.Color.Transparent;
      this.Title.BorderColor = System.Drawing.Color.Black;
      this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.Title.BorderWidth = 1F;
      this.Title.Font = new System.Drawing.Font("Arial", 14.25F);
      this.Title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(70)))), ((int)(((byte)(80)))));
      this.Title.Name = "Title";
      // 
      // DetailCaption1
      // 
      this.DetailCaption1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.DetailCaption1.BorderColor = System.Drawing.Color.White;
      this.DetailCaption1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.DetailCaption1.BorderWidth = 2F;
      this.DetailCaption1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.DetailCaption1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(70)))), ((int)(((byte)(80)))));
      this.DetailCaption1.Name = "DetailCaption1";
      this.DetailCaption1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.DetailCaption1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      // 
      // DetailData1
      // 
      this.DetailData1.BorderColor = System.Drawing.Color.Transparent;
      this.DetailData1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.DetailData1.BorderWidth = 2F;
      this.DetailData1.Font = new System.Drawing.Font("Arial", 8.25F);
      this.DetailData1.ForeColor = System.Drawing.Color.Black;
      this.DetailData1.Name = "DetailData1";
      this.DetailData1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.DetailData1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      // 
      // DetailData3_Odd
      // 
      this.DetailData3_Odd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(231)))), ((int)(((byte)(231)))));
      this.DetailData3_Odd.BorderColor = System.Drawing.Color.Transparent;
      this.DetailData3_Odd.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.DetailData3_Odd.BorderWidth = 1F;
      this.DetailData3_Odd.Font = new System.Drawing.Font("Arial", 8.25F);
      this.DetailData3_Odd.ForeColor = System.Drawing.Color.Black;
      this.DetailData3_Odd.Name = "DetailData3_Odd";
      this.DetailData3_Odd.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.DetailData3_Odd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      // 
      // PageInfo
      // 
      this.PageInfo.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.PageInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(70)))), ((int)(((byte)(80)))));
      this.PageInfo.Name = "PageInfo";
      this.PageInfo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
      // 
      // TopMargin
      // 
      this.TopMargin.HeightF = 25F;
      this.TopMargin.Name = "TopMargin";
      // 
      // BottomMargin
      // 
      this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pageInfo1,
            this.pageInfo2});
      this.BottomMargin.HeightF = 28F;
      this.BottomMargin.Name = "BottomMargin";
      // 
      // pageInfo1
      // 
      this.pageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 5F);
      this.pageInfo1.Name = "pageInfo1";
      this.pageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
      this.pageInfo1.SizeF = new System.Drawing.SizeF(665F, 23F);
      this.pageInfo1.StyleName = "PageInfo";
      // 
      // pageInfo2
      // 
      this.pageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(680F, 4.999987F);
      this.pageInfo2.Name = "pageInfo2";
      this.pageInfo2.SizeF = new System.Drawing.SizeF(665F, 23F);
      this.pageInfo2.StyleName = "PageInfo";
      this.pageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.pageInfo2.TextFormatString = "Page {0} of {1}";
      // 
      // ReportHeader
      // 
      this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.label1});
      this.ReportHeader.HeightF = 96.66666F;
      this.ReportHeader.Name = "ReportHeader";
      // 
      // xrTable1
      // 
      this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 60.83333F);
      this.xrTable1.Name = "xrTable1";
      this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
      this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
      this.xrTable1.SizeF = new System.Drawing.SizeF(650.8333F, 25F);
      // 
      // xrTableRow1
      // 
      this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8});
      this.xrTableRow1.Name = "xrTableRow1";
      this.xrTableRow1.Weight = 1D;
      // 
      // xrTableCell1
      // 
      this.xrTableCell1.Multiline = true;
      this.xrTableCell1.Name = "xrTableCell1";
      this.xrTableCell1.Text = "Start Date:";
      this.xrTableCell1.Weight = 0.71666656494140624D;
      // 
      // xrTableCell2
      // 
      this.xrTableCell2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "?prmStartDateBooked")});
      this.xrTableCell2.Multiline = true;
      this.xrTableCell2.Name = "xrTableCell2";
      this.xrTableCell2.Text = "xrTableCell2";
      this.xrTableCell2.TextFormatString = "{0:MM/dd/yyyy}";
      this.xrTableCell2.Weight = 1.0000000762939454D;
      // 
      // xrTableCell3
      // 
      this.xrTableCell3.Multiline = true;
      this.xrTableCell3.Name = "xrTableCell3";
      this.xrTableCell3.Text = "End Date:";
      this.xrTableCell3.Weight = 0.716666488647461D;
      // 
      // xrTableCell4
      // 
      this.xrTableCell4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "?prmEndDateBooked")});
      this.xrTableCell4.Multiline = true;
      this.xrTableCell4.Name = "xrTableCell4";
      this.xrTableCell4.Text = "xrTableCell4";
      this.xrTableCell4.TextFormatString = "{0:MM/dd/yyyy}";
      this.xrTableCell4.Weight = 0.93017898559570311D;
      // 
      // xrTableCell5
      // 
      this.xrTableCell5.Multiline = true;
      this.xrTableCell5.Name = "xrTableCell5";
      this.xrTableCell5.Text = "Euro To Dollar:";
      this.xrTableCell5.Weight = 1.028154296875D;
      // 
      // xrTableCell6
      // 
      this.xrTableCell6.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[EuroToDollar]")});
      this.xrTableCell6.Multiline = true;
      this.xrTableCell6.Name = "xrTableCell6";
      this.xrTableCell6.Text = "xrTableCell6";
      this.xrTableCell6.Weight = 0.50833374023437494D;
      // 
      // xrTableCell7
      // 
      this.xrTableCell7.Multiline = true;
      this.xrTableCell7.Name = "xrTableCell7";
      this.xrTableCell7.Text = "Pound To Dollar:";
      this.xrTableCell7.Weight = 1.1166668701171876D;
      // 
      // xrTableCell8
      // 
      this.xrTableCell8.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PoundToDollar]")});
      this.xrTableCell8.Multiline = true;
      this.xrTableCell8.Name = "xrTableCell8";
      this.xrTableCell8.Text = "xrTableCell8";
      this.xrTableCell8.Weight = 0.4916661071777344D;
      // 
      // label1
      // 
      this.label1.LocationFloat = new DevExpress.Utils.PointFloat(5F, 5F);
      this.label1.Name = "label1";
      this.label1.SizeF = new System.Drawing.SizeF(1340F, 24.19433F);
      this.label1.StyleName = "Title";
      this.label1.StylePriority.UseTextAlignment = false;
      this.label1.Text = "New Reservations and Inquiries";
      this.label1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
      // 
      // GroupHeader1
      // 
      this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("ResvType", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
      this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
      this.GroupHeader1.HeightF = 0F;
      this.GroupHeader1.Name = "GroupHeader1";
      // 
      // Detail
      // 
      this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
      this.Detail.HeightF = 25F;
      this.Detail.Name = "Detail";
      this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("StatusDate", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
      // 
      // xrTable3
      // 
      this.xrTable3.BackColor = System.Drawing.Color.Transparent;
      this.xrTable3.EvenStyleName = "DetailData1";
      this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable3.Name = "xrTable3";
      this.xrTable3.OddStyleName = "DetailData3_Odd";
      this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
      this.xrTable3.SizeF = new System.Drawing.SizeF(1350F, 25F);
      this.xrTable3.StyleName = "DetailData3_Odd";
      // 
      // xrTableRow3
      // 
      this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42});
      this.xrTableRow3.Name = "xrTableRow3";
      this.xrTableRow3.Weight = 11.5D;
      // 
      // xrTableCell27
      // 
      this.xrTableCell27.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell27.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell27.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell27.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell27.BorderWidth = 2F;
      this.xrTableCell27.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StatusDate]")});
      this.xrTableCell27.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell27.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell27.Multiline = true;
      this.xrTableCell27.Name = "xrTableCell27";
      this.xrTableCell27.OddStyleName = "DetailData3_Odd";
      this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell27.Text = "xrTableCell7";
      this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell27.TextFormatString = "{0:MM/dd/yyyy}";
      this.xrTableCell27.Weight = 81.666656494140625D;
      // 
      // xrTableCell28
      // 
      this.xrTableCell28.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell28.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell28.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell28.Borders = DevExpress.XtraPrinting.BorderSide.None;
      this.xrTableCell28.BorderWidth = 2F;
      this.xrTableCell28.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReservationNumber]")});
      this.xrTableCell28.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell28.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell28.Multiline = true;
      this.xrTableCell28.Name = "xrTableCell28";
      this.xrTableCell28.OddStyleName = "DetailData3_Odd";
      this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell28.StylePriority.UseTextAlignment = false;
      this.xrTableCell28.Text = "xrTableCell1";
      this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell28.Weight = 62.405441284179688D;
      // 
      // xrTableCell29
      // 
      this.xrTableCell29.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell29.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell29.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell29.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell29.BorderWidth = 2F;
      this.xrTableCell29.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[DateFrom]")});
      this.xrTableCell29.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell29.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell29.Multiline = true;
      this.xrTableCell29.Name = "xrTableCell29";
      this.xrTableCell29.OddStyleName = "DetailData3_Odd";
      this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell29.StylePriority.UseTextAlignment = false;
      this.xrTableCell29.Text = "xrTableCell2";
      this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell29.TextFormatString = "{0:MM/dd/yyyy}";
      this.xrTableCell29.Weight = 80.796707153320312D;
      // 
      // xrTableCell30
      // 
      this.xrTableCell30.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell30.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell30.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell30.BorderWidth = 2F;
      this.xrTableCell30.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[DateTo]")});
      this.xrTableCell30.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell30.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell30.Multiline = true;
      this.xrTableCell30.Name = "xrTableCell30";
      this.xrTableCell30.OddStyleName = "DetailData3_Odd";
      this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell30.StylePriority.UseTextAlignment = false;
      this.xrTableCell30.Text = "xrTableCell3";
      this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell30.TextFormatString = "{0:MM/dd/yyyy}";
      this.xrTableCell30.Weight = 75.735595703125D;
      // 
      // xrTableCell31
      // 
      this.xrTableCell31.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell31.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell31.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell31.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell31.BorderWidth = 2F;
      this.xrTableCell31.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[DaysCharged]")});
      this.xrTableCell31.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell31.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell31.Multiline = true;
      this.xrTableCell31.Name = "xrTableCell31";
      this.xrTableCell31.OddStyleName = "DetailData3_Odd";
      this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell31.StyleName = "xrControlStyle1";
      this.xrTableCell31.StylePriority.UseTextAlignment = false;
      this.xrTableCell31.Text = "xrTableCell4";
      this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell31.Weight = 45.74688720703125D;
      // 
      // xrTableCell32
      // 
      this.xrTableCell32.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell32.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell32.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell32.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell32.BorderWidth = 2F;
      this.xrTableCell32.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Status]")});
      this.xrTableCell32.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell32.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell32.Multiline = true;
      this.xrTableCell32.Name = "xrTableCell32";
      this.xrTableCell32.OddStyleName = "DetailData3_Odd";
      this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell32.StylePriority.UseTextAlignment = false;
      this.xrTableCell32.Text = "xrTableCell6";
      this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell32.Weight = 56.07550048828125D;
      // 
      // xrTableCell33
      // 
      this.xrTableCell33.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell33.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell33.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell33.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell33.BorderWidth = 2F;
      this.xrTableCell33.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ClientName]")});
      this.xrTableCell33.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell33.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell33.Multiline = true;
      this.xrTableCell33.Name = "xrTableCell33";
      this.xrTableCell33.OddStyleName = "DetailData3_Odd";
      this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell33.StylePriority.UseTextAlignment = false;
      this.xrTableCell33.Text = "xrTableCell5";
      this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell33.Weight = 157.20120239257813D;
      // 
      // xrTableCell34
      // 
      this.xrTableCell34.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell34.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell34.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell34.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell34.BorderWidth = 2F;
      this.xrTableCell34.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[AgencyName]")});
      this.xrTableCell34.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell34.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell34.Multiline = true;
      this.xrTableCell34.Name = "xrTableCell34";
      this.xrTableCell34.OddStyleName = "DetailData3_Odd";
      this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell34.StylePriority.UseTextAlignment = false;
      this.xrTableCell34.Text = "xrTableCell8";
      this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell34.Weight = 209.53863525390625D;
      // 
      // xrTableCell35
      // 
      this.xrTableCell35.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell35.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[VillaCode]")});
      this.xrTableCell35.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell35.Multiline = true;
      this.xrTableCell35.Name = "xrTableCell35";
      this.xrTableCell35.OddStyleName = "DetailData3_Odd";
      this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell35.StylePriority.UseTextAlignment = false;
      this.xrTableCell35.Text = "xrLabel27";
      this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell35.Weight = 99.16650390625D;
      // 
      // xrTableCell36
      // 
      this.xrTableCell36.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell36.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell36.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell36.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell36.BorderWidth = 2F;
      this.xrTableCell36.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CurrencySymbol]")});
      this.xrTableCell36.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell36.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell36.Multiline = true;
      this.xrTableCell36.Name = "xrTableCell36";
      this.xrTableCell36.OddStyleName = "DetailData3_Odd";
      this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell36.StylePriority.UseTextAlignment = false;
      this.xrTableCell36.Text = "xrTableCell9";
      this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell36.Weight = 21.937255859375D;
      // 
      // xrTableCell37
      // 
      this.xrTableCell37.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell37.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell37.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell37.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell37.BorderWidth = 2F;
      this.xrTableCell37.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[GrossPrice]")});
      this.xrTableCell37.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell37.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell37.Multiline = true;
      this.xrTableCell37.Name = "xrTableCell37";
      this.xrTableCell37.OddStyleName = "DetailData3_Odd";
      this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell37.StylePriority.UseTextAlignment = false;
      this.xrTableCell37.Text = "xrTableCell10";
      this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell37.TextFormatString = "{0:n}";
      this.xrTableCell37.Weight = 82.050537109375D;
      // 
      // xrTableCell38
      // 
      this.xrTableCell38.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell38.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TotalCharge]")});
      this.xrTableCell38.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell38.Multiline = true;
      this.xrTableCell38.Name = "xrTableCell38";
      this.xrTableCell38.OddStyleName = "DetailData3_Odd";
      this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
      this.xrTableCell38.StylePriority.UseTextAlignment = false;
      this.xrTableCell38.Text = "xrLabel29";
      this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell38.TextFormatString = "{0:n}";
      this.xrTableCell38.Weight = 75.8331298828125D;
      // 
      // xrTableCell39
      // 
      this.xrTableCell39.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell39.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell39.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell39.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell39.BorderWidth = 2F;
      this.xrTableCell39.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[PMTotalRecdClient]")});
      this.xrTableCell39.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell39.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell39.Multiline = true;
      this.xrTableCell39.Name = "xrTableCell39";
      this.xrTableCell39.OddStyleName = "DetailData3_Odd";
      this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell39.StylePriority.UseTextAlignment = false;
      this.xrTableCell39.Text = "xrTableCell11";
      this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell39.TextFormatString = "{0:n}";
      this.xrTableCell39.Weight = 79.489990234375D;
      // 
      // xrTableCell40
      // 
      this.xrTableCell40.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell40.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell40.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell40.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell40.BorderWidth = 2F;
      this.xrTableCell40.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[WimcoTotalRecdClient]")});
      this.xrTableCell40.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell40.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell40.Multiline = true;
      this.xrTableCell40.Name = "xrTableCell40";
      this.xrTableCell40.OddStyleName = "DetailData3_Odd";
      this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell40.StylePriority.UseTextAlignment = false;
      this.xrTableCell40.Text = "xrTableCell12";
      this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell40.TextFormatString = "{0:n}";
      this.xrTableCell40.Weight = 86.7559814453125D;
      // 
      // xrTableCell41
      // 
      this.xrTableCell41.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell41.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell41.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell41.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell41.BorderWidth = 2F;
      this.xrTableCell41.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[DepositDue]")});
      this.xrTableCell41.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell41.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell41.Multiline = true;
      this.xrTableCell41.Name = "xrTableCell41";
      this.xrTableCell41.OddStyleName = "DetailData3_Odd";
      this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell41.StylePriority.UseTextAlignment = false;
      this.xrTableCell41.Text = "xrTableCell13";
      this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell41.TextFormatString = "{0:n}";
      this.xrTableCell41.Weight = 75.5999755859375D;
      // 
      // xrTableCell42
      // 
      this.xrTableCell42.BackColor = System.Drawing.Color.Transparent;
      this.xrTableCell42.BorderColor = System.Drawing.Color.Transparent;
      this.xrTableCell42.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell42.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell42.BorderWidth = 2F;
      this.xrTableCell42.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[calcDepositPercent]")});
      this.xrTableCell42.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell42.ForeColor = System.Drawing.Color.Black;
      this.xrTableCell42.Multiline = true;
      this.xrTableCell42.Name = "xrTableCell42";
      this.xrTableCell42.OddStyleName = "DetailData3_Odd";
      this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell42.StyleName = "DetailData1";
      this.xrTableCell42.StylePriority.UseTextAlignment = false;
      this.xrTableCell42.Text = "xrTableCell13";
      this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell42.TextFormatString = "{0:0.00%}";
      this.xrTableCell42.Weight = 60D;
      // 
      // prmStartDateBooked
      // 
      this.prmStartDateBooked.Name = "prmStartDateBooked";
      this.prmStartDateBooked.Type = typeof(System.DateTime);
      this.prmStartDateBooked.ValueInfo = "2020-04-01";
      // 
      // prmEndDateBooked
      // 
      this.prmEndDateBooked.Name = "prmEndDateBooked";
      this.prmEndDateBooked.Type = typeof(System.DateTime);
      this.prmEndDateBooked.ValueInfo = "2020-04-16";
      // 
      // prmResvAgent
      // 
      this.prmResvAgent.Name = "prmResvAgent";
      this.prmResvAgent.ValueInfo = "ANNIE";
      // 
      // calcDepositPercent
      // 
      this.calcDepositPercent.DataMember = "spDashboardNewAndInquiry";
      this.calcDepositPercent.Expression = "Iif([DepositDue] > 0, ([calcWimcoTotalRecdClient] / [calcDepositDue]), 0)";
      this.calcDepositPercent.Name = "calcDepositPercent";
      // 
      // PageHeader
      // 
      this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
      this.PageHeader.HeightF = 36.54168F;
      this.PageHeader.Name = "PageHeader";
      // 
      // xrTable5
      // 
      this.xrTable5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
      this.xrTable5.BorderWidth = 1F;
      this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable5.Name = "xrTable5";
      this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
      this.xrTable5.SizeF = new System.Drawing.SizeF(1350F, 28F);
      this.xrTable5.StylePriority.UseBorders = false;
      this.xrTable5.StylePriority.UseBorderWidth = false;
      // 
      // xrTableRow5
      // 
      this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76});
      this.xrTableRow5.Name = "xrTableRow5";
      this.xrTableRow5.Weight = 4.6D;
      // 
      // xrTableCell61
      // 
      this.xrTableCell61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell61.BorderColor = System.Drawing.Color.White;
      this.xrTableCell61.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell61.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell61.BorderWidth = 2F;
      this.xrTableCell61.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell61.ForeColor = System.Drawing.Color.White;
      this.xrTableCell61.Multiline = true;
      this.xrTableCell61.Name = "xrTableCell61";
      this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell61.StyleName = "DetailCaption1";
      this.xrTableCell61.StylePriority.UseBackColor = false;
      this.xrTableCell61.StylePriority.UseBorderColor = false;
      this.xrTableCell61.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell61.StylePriority.UseBorders = false;
      this.xrTableCell61.StylePriority.UseBorderWidth = false;
      this.xrTableCell61.StylePriority.UseFont = false;
      this.xrTableCell61.StylePriority.UseForeColor = false;
      this.xrTableCell61.StylePriority.UsePadding = false;
      this.xrTableCell61.StylePriority.UseTextAlignment = false;
      this.xrTableCell61.Text = "Resv\r\nDate";
      this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell61.Weight = 81.666656494140625D;
      // 
      // xrTableCell62
      // 
      this.xrTableCell62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell62.BorderColor = System.Drawing.Color.White;
      this.xrTableCell62.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell62.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell62.BorderWidth = 2F;
      this.xrTableCell62.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell62.ForeColor = System.Drawing.Color.White;
      this.xrTableCell62.Multiline = true;
      this.xrTableCell62.Name = "xrTableCell62";
      this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell62.StyleName = "DetailCaption1";
      this.xrTableCell62.StylePriority.UseBackColor = false;
      this.xrTableCell62.StylePriority.UseBorderColor = false;
      this.xrTableCell62.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell62.StylePriority.UseBorders = false;
      this.xrTableCell62.StylePriority.UseBorderWidth = false;
      this.xrTableCell62.StylePriority.UseFont = false;
      this.xrTableCell62.StylePriority.UseForeColor = false;
      this.xrTableCell62.StylePriority.UsePadding = false;
      this.xrTableCell62.StylePriority.UseTextAlignment = false;
      this.xrTableCell62.Text = "Resv #";
      this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell62.Weight = 62.405441284179688D;
      // 
      // xrTableCell63
      // 
      this.xrTableCell63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell63.BorderColor = System.Drawing.Color.White;
      this.xrTableCell63.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell63.BorderWidth = 2F;
      this.xrTableCell63.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell63.ForeColor = System.Drawing.Color.White;
      this.xrTableCell63.Multiline = true;
      this.xrTableCell63.Name = "xrTableCell63";
      this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell63.StyleName = "DetailCaption1";
      this.xrTableCell63.StylePriority.UseBackColor = false;
      this.xrTableCell63.StylePriority.UseBorderColor = false;
      this.xrTableCell63.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell63.StylePriority.UseBorders = false;
      this.xrTableCell63.StylePriority.UseBorderWidth = false;
      this.xrTableCell63.StylePriority.UseFont = false;
      this.xrTableCell63.StylePriority.UseForeColor = false;
      this.xrTableCell63.StylePriority.UsePadding = false;
      this.xrTableCell63.StylePriority.UseTextAlignment = false;
      this.xrTableCell63.Text = "Date From";
      this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell63.Weight = 80.796707153320312D;
      // 
      // xrTableCell64
      // 
      this.xrTableCell64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell64.BorderColor = System.Drawing.Color.White;
      this.xrTableCell64.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell64.BorderWidth = 2F;
      this.xrTableCell64.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell64.ForeColor = System.Drawing.Color.White;
      this.xrTableCell64.Multiline = true;
      this.xrTableCell64.Name = "xrTableCell64";
      this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell64.StyleName = "DetailCaption1";
      this.xrTableCell64.StylePriority.UseBackColor = false;
      this.xrTableCell64.StylePriority.UseBorderColor = false;
      this.xrTableCell64.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell64.StylePriority.UseBorders = false;
      this.xrTableCell64.StylePriority.UseBorderWidth = false;
      this.xrTableCell64.StylePriority.UseFont = false;
      this.xrTableCell64.StylePriority.UseForeColor = false;
      this.xrTableCell64.StylePriority.UsePadding = false;
      this.xrTableCell64.StylePriority.UseTextAlignment = false;
      this.xrTableCell64.Text = "Date To";
      this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell64.Weight = 75.735595703125D;
      // 
      // xrTableCell65
      // 
      this.xrTableCell65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell65.BorderColor = System.Drawing.Color.White;
      this.xrTableCell65.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell65.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell65.BorderWidth = 2F;
      this.xrTableCell65.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell65.ForeColor = System.Drawing.Color.White;
      this.xrTableCell65.Multiline = true;
      this.xrTableCell65.Name = "xrTableCell65";
      this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell65.StyleName = "DetailCaption1";
      this.xrTableCell65.StylePriority.UseBackColor = false;
      this.xrTableCell65.StylePriority.UseBorderColor = false;
      this.xrTableCell65.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell65.StylePriority.UseBorders = false;
      this.xrTableCell65.StylePriority.UseBorderWidth = false;
      this.xrTableCell65.StylePriority.UseFont = false;
      this.xrTableCell65.StylePriority.UseForeColor = false;
      this.xrTableCell65.StylePriority.UsePadding = false;
      this.xrTableCell65.StylePriority.UseTextAlignment = false;
      this.xrTableCell65.Text = "# of\r\nNight";
      this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell65.Weight = 45.746795654296875D;
      // 
      // xrTableCell66
      // 
      this.xrTableCell66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell66.BorderColor = System.Drawing.Color.White;
      this.xrTableCell66.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell66.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell66.BorderWidth = 2F;
      this.xrTableCell66.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell66.ForeColor = System.Drawing.Color.White;
      this.xrTableCell66.Multiline = true;
      this.xrTableCell66.Name = "xrTableCell66";
      this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell66.StyleName = "DetailCaption1";
      this.xrTableCell66.StylePriority.UseBackColor = false;
      this.xrTableCell66.StylePriority.UseBorderColor = false;
      this.xrTableCell66.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell66.StylePriority.UseBorders = false;
      this.xrTableCell66.StylePriority.UseBorderWidth = false;
      this.xrTableCell66.StylePriority.UseFont = false;
      this.xrTableCell66.StylePriority.UseForeColor = false;
      this.xrTableCell66.StylePriority.UsePadding = false;
      this.xrTableCell66.StylePriority.UseTextAlignment = false;
      this.xrTableCell66.Text = "Status";
      this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell66.Weight = 53.575592041015625D;
      // 
      // xrTableCell67
      // 
      this.xrTableCell67.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell67.BorderColor = System.Drawing.Color.White;
      this.xrTableCell67.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell67.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell67.BorderWidth = 2F;
      this.xrTableCell67.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell67.ForeColor = System.Drawing.Color.White;
      this.xrTableCell67.Multiline = true;
      this.xrTableCell67.Name = "xrTableCell67";
      this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell67.StyleName = "DetailCaption1";
      this.xrTableCell67.StylePriority.UseBackColor = false;
      this.xrTableCell67.StylePriority.UseBorderColor = false;
      this.xrTableCell67.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell67.StylePriority.UseBorders = false;
      this.xrTableCell67.StylePriority.UseBorderWidth = false;
      this.xrTableCell67.StylePriority.UseFont = false;
      this.xrTableCell67.StylePriority.UseForeColor = false;
      this.xrTableCell67.StylePriority.UsePadding = false;
      this.xrTableCell67.StylePriority.UseTextAlignment = false;
      this.xrTableCell67.Text = "Client\r\nName";
      this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell67.Weight = 159.70120239257813D;
      // 
      // xrTableCell68
      // 
      this.xrTableCell68.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell68.BorderColor = System.Drawing.Color.White;
      this.xrTableCell68.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell68.BorderWidth = 2F;
      this.xrTableCell68.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell68.ForeColor = System.Drawing.Color.White;
      this.xrTableCell68.Multiline = true;
      this.xrTableCell68.Name = "xrTableCell68";
      this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell68.StyleName = "DetailCaption1";
      this.xrTableCell68.StylePriority.UseBackColor = false;
      this.xrTableCell68.StylePriority.UseBorderColor = false;
      this.xrTableCell68.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell68.StylePriority.UseBorders = false;
      this.xrTableCell68.StylePriority.UseBorderWidth = false;
      this.xrTableCell68.StylePriority.UseFont = false;
      this.xrTableCell68.StylePriority.UseForeColor = false;
      this.xrTableCell68.StylePriority.UsePadding = false;
      this.xrTableCell68.StylePriority.UseTextAlignment = false;
      this.xrTableCell68.Text = "Agency\r\nName";
      this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell68.Weight = 209.5386962890625D;
      // 
      // xrTableCell69
      // 
      this.xrTableCell69.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell69.BorderColor = System.Drawing.Color.White;
      this.xrTableCell69.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell69.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell69.BorderWidth = 2F;
      this.xrTableCell69.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell69.ForeColor = System.Drawing.Color.White;
      this.xrTableCell69.Multiline = true;
      this.xrTableCell69.Name = "xrTableCell69";
      this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell69.StyleName = "DetailCaption1";
      this.xrTableCell69.StylePriority.UseBackColor = false;
      this.xrTableCell69.StylePriority.UseBorderColor = false;
      this.xrTableCell69.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell69.StylePriority.UseBorders = false;
      this.xrTableCell69.StylePriority.UseBorderWidth = false;
      this.xrTableCell69.StylePriority.UseFont = false;
      this.xrTableCell69.StylePriority.UseForeColor = false;
      this.xrTableCell69.StylePriority.UsePadding = false;
      this.xrTableCell69.StylePriority.UseTextAlignment = false;
      this.xrTableCell69.Text = "Villa\r\nCode";
      this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell69.Weight = 99.16644287109375D;
      // 
      // xrTableCell70
      // 
      this.xrTableCell70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell70.BorderColor = System.Drawing.Color.White;
      this.xrTableCell70.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell70.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell70.BorderWidth = 2F;
      this.xrTableCell70.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell70.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(70)))), ((int)(((byte)(80)))));
      this.xrTableCell70.Multiline = true;
      this.xrTableCell70.Name = "xrTableCell70";
      this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell70.StyleName = "DetailCaption1";
      this.xrTableCell70.StylePriority.UseBackColor = false;
      this.xrTableCell70.StylePriority.UseBorderColor = false;
      this.xrTableCell70.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell70.StylePriority.UseBorders = false;
      this.xrTableCell70.StylePriority.UseBorderWidth = false;
      this.xrTableCell70.StylePriority.UseFont = false;
      this.xrTableCell70.StylePriority.UseForeColor = false;
      this.xrTableCell70.StylePriority.UsePadding = false;
      this.xrTableCell70.StylePriority.UseTextAlignment = false;
      this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
      this.xrTableCell70.Weight = 21.937255859375D;
      // 
      // xrTableCell71
      // 
      this.xrTableCell71.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell71.BorderColor = System.Drawing.Color.White;
      this.xrTableCell71.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell71.BorderWidth = 2F;
      this.xrTableCell71.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell71.ForeColor = System.Drawing.Color.White;
      this.xrTableCell71.Multiline = true;
      this.xrTableCell71.Name = "xrTableCell71";
      this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell71.StyleName = "DetailCaption1";
      this.xrTableCell71.StylePriority.UseBackColor = false;
      this.xrTableCell71.StylePriority.UseBorderColor = false;
      this.xrTableCell71.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell71.StylePriority.UseBorders = false;
      this.xrTableCell71.StylePriority.UseBorderWidth = false;
      this.xrTableCell71.StylePriority.UseFont = false;
      this.xrTableCell71.StylePriority.UseForeColor = false;
      this.xrTableCell71.StylePriority.UsePadding = false;
      this.xrTableCell71.StylePriority.UseTextAlignment = false;
      this.xrTableCell71.Text = "Gross\r\nPrice";
      this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell71.Weight = 82.050537109375D;
      // 
      // xrTableCell72
      // 
      this.xrTableCell72.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell72.BorderColor = System.Drawing.Color.White;
      this.xrTableCell72.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell72.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell72.BorderWidth = 2F;
      this.xrTableCell72.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell72.ForeColor = System.Drawing.Color.White;
      this.xrTableCell72.Multiline = true;
      this.xrTableCell72.Name = "xrTableCell72";
      this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell72.StyleName = "DetailCaption1";
      this.xrTableCell72.StylePriority.UseBackColor = false;
      this.xrTableCell72.StylePriority.UseBorderColor = false;
      this.xrTableCell72.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell72.StylePriority.UseBorders = false;
      this.xrTableCell72.StylePriority.UseBorderWidth = false;
      this.xrTableCell72.StylePriority.UseFont = false;
      this.xrTableCell72.StylePriority.UseForeColor = false;
      this.xrTableCell72.StylePriority.UsePadding = false;
      this.xrTableCell72.StylePriority.UseTextAlignment = false;
      this.xrTableCell72.Text = "Total\r\nCharges";
      this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell72.Weight = 75.8331298828125D;
      // 
      // xrTableCell73
      // 
      this.xrTableCell73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell73.BorderColor = System.Drawing.Color.White;
      this.xrTableCell73.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell73.BorderWidth = 2F;
      this.xrTableCell73.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell73.ForeColor = System.Drawing.Color.White;
      this.xrTableCell73.Multiline = true;
      this.xrTableCell73.Name = "xrTableCell73";
      this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell73.StyleName = "DetailCaption1";
      this.xrTableCell73.StylePriority.UseBackColor = false;
      this.xrTableCell73.StylePriority.UseBorderColor = false;
      this.xrTableCell73.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell73.StylePriority.UseBorders = false;
      this.xrTableCell73.StylePriority.UseBorderWidth = false;
      this.xrTableCell73.StylePriority.UseFont = false;
      this.xrTableCell73.StylePriority.UseForeColor = false;
      this.xrTableCell73.StylePriority.UsePadding = false;
      this.xrTableCell73.StylePriority.UseTextAlignment = false;
      this.xrTableCell73.Text = "Total Rcvd\r\nBy PM";
      this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell73.Weight = 79.489990234375D;
      // 
      // xrTableCell74
      // 
      this.xrTableCell74.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell74.BorderColor = System.Drawing.Color.White;
      this.xrTableCell74.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell74.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell74.BorderWidth = 2F;
      this.xrTableCell74.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell74.ForeColor = System.Drawing.Color.White;
      this.xrTableCell74.Multiline = true;
      this.xrTableCell74.Name = "xrTableCell74";
      this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell74.StyleName = "DetailCaption1";
      this.xrTableCell74.StylePriority.UseBackColor = false;
      this.xrTableCell74.StylePriority.UseBorderColor = false;
      this.xrTableCell74.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell74.StylePriority.UseBorders = false;
      this.xrTableCell74.StylePriority.UseBorderWidth = false;
      this.xrTableCell74.StylePriority.UseFont = false;
      this.xrTableCell74.StylePriority.UseForeColor = false;
      this.xrTableCell74.StylePriority.UsePadding = false;
      this.xrTableCell74.StylePriority.UseTextAlignment = false;
      this.xrTableCell74.Text = "Total Rcvd\r\nBy WIMCO";
      this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell74.Weight = 86.7559814453125D;
      // 
      // xrTableCell75
      // 
      this.xrTableCell75.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell75.BorderColor = System.Drawing.Color.White;
      this.xrTableCell75.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell75.BorderWidth = 2F;
      this.xrTableCell75.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell75.ForeColor = System.Drawing.Color.White;
      this.xrTableCell75.Multiline = true;
      this.xrTableCell75.Name = "xrTableCell75";
      this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell75.StyleName = "DetailCaption1";
      this.xrTableCell75.StylePriority.UseBackColor = false;
      this.xrTableCell75.StylePriority.UseBorderColor = false;
      this.xrTableCell75.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell75.StylePriority.UseBorders = false;
      this.xrTableCell75.StylePriority.UseBorderWidth = false;
      this.xrTableCell75.StylePriority.UseFont = false;
      this.xrTableCell75.StylePriority.UseForeColor = false;
      this.xrTableCell75.StylePriority.UsePadding = false;
      this.xrTableCell75.StylePriority.UseTextAlignment = false;
      this.xrTableCell75.Text = "Deposit\r\nDue";
      this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell75.Weight = 75.5999755859375D;
      // 
      // xrTableCell76
      // 
      this.xrTableCell76.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
      this.xrTableCell76.BorderColor = System.Drawing.Color.White;
      this.xrTableCell76.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
      this.xrTableCell76.Borders = DevExpress.XtraPrinting.BorderSide.Left;
      this.xrTableCell76.BorderWidth = 2F;
      this.xrTableCell76.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell76.ForeColor = System.Drawing.Color.White;
      this.xrTableCell76.Multiline = true;
      this.xrTableCell76.Name = "xrTableCell76";
      this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
      this.xrTableCell76.StyleName = "DetailCaption1";
      this.xrTableCell76.StylePriority.UseBackColor = false;
      this.xrTableCell76.StylePriority.UseBorderColor = false;
      this.xrTableCell76.StylePriority.UseBorderDashStyle = false;
      this.xrTableCell76.StylePriority.UseBorders = false;
      this.xrTableCell76.StylePriority.UseBorderWidth = false;
      this.xrTableCell76.StylePriority.UseFont = false;
      this.xrTableCell76.StylePriority.UseForeColor = false;
      this.xrTableCell76.StylePriority.UsePadding = false;
      this.xrTableCell76.StylePriority.UseTextAlignment = false;
      this.xrTableCell76.Text = "Deposit\r\nPercent";
      this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
      this.xrTableCell76.Weight = 60D;
      // 
      // GroupFooter1
      // 
      this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
      this.GroupFooter1.HeightF = 25F;
      this.GroupFooter1.Name = "GroupFooter1";
      // 
      // xrTable2
      // 
      this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTable2.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
      this.xrTable2.Name = "xrTable2";
      this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
      this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
      this.xrTable2.SizeF = new System.Drawing.SizeF(1350F, 25F);
      // 
      // xrTableRow2
      // 
      this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell16,
            this.xrTableCell14,
            this.xrTableCell10,
            this.xrTableCell15,
            this.xrTableCell13,
            this.celcWithMoney,
            this.xrTableCell12,
            this.xrTableCell17,
            this.xrTableCell11,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.celGrossPrice,
            this.xrTableCell25,
            this.xrTableCell22,
            this.xrTableCell21,
            this.xrTableCell23,
            this.xrTableCell26});
      this.xrTableRow2.Name = "xrTableRow2";
      this.xrTableRow2.Weight = 1D;
      // 
      // xrTableCell9
      // 
      this.xrTableCell9.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell9.Multiline = true;
      this.xrTableCell9.Name = "xrTableCell9";
      this.xrTableCell9.StylePriority.UseFont = false;
      this.xrTableCell9.Text = "Total for";
      this.xrTableCell9.Weight = 0.21844659768621533D;
      // 
      // xrTableCell16
      // 
      this.xrTableCell16.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ResvType]")});
      this.xrTableCell16.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell16.Multiline = true;
      this.xrTableCell16.Name = "xrTableCell16";
      this.xrTableCell16.StylePriority.UseFont = false;
      this.xrTableCell16.Text = "xrTableCell16";
      this.xrTableCell16.Weight = 0.14320383383648228D;
      // 
      // xrTableCell14
      // 
      this.xrTableCell14.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumCount([ReservationNumber])")});
      this.xrTableCell14.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell14.Multiline = true;
      this.xrTableCell14.Name = "xrTableCell14";
      this.xrTableCell14.StylePriority.UseFont = false;
      xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell14.Summary = xrSummary1;
      this.xrTableCell14.Text = "xrTableCell14";
      this.xrTableCell14.Weight = 0.22410170502477311D;
      // 
      // xrTableCell10
      // 
      this.xrTableCell10.Multiline = true;
      this.xrTableCell10.Name = "xrTableCell10";
      this.xrTableCell10.Text = "With Money:";
      this.xrTableCell10.Weight = 0.35299509833776854D;
      // 
      // xrTableCell15
      // 
      this.xrTableCell15.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([ResvWithMoney])")});
      this.xrTableCell15.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell15.Multiline = true;
      this.xrTableCell15.Name = "xrTableCell15";
      this.xrTableCell15.StylePriority.UseFont = false;
      xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell15.Summary = xrSummary2;
      this.xrTableCell15.Text = "xrTableCell15";
      this.xrTableCell15.Weight = 0.19838856434185659D;
      // 
      // xrTableCell13
      // 
      this.xrTableCell13.Multiline = true;
      this.xrTableCell13.Name = "xrTableCell13";
      this.xrTableCell13.Text = "Without Money:";
      this.xrTableCell13.Weight = 0.47208719379210606D;
      // 
      // celcWithMoney
      // 
      this.celcWithMoney.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([ResvWithoutMoney])")});
      this.celcWithMoney.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.celcWithMoney.Multiline = true;
      this.celcWithMoney.Name = "celcWithMoney";
      this.celcWithMoney.StylePriority.UseFont = false;
      xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.celcWithMoney.Summary = xrSummary3;
      this.celcWithMoney.Text = "celcWithMoney";
      this.celcWithMoney.Weight = 0.21116537047551276D;
      // 
      // xrTableCell12
      // 
      this.xrTableCell12.Multiline = true;
      this.xrTableCell12.Name = "xrTableCell12";
      this.xrTableCell12.Text = "# of Nights:";
      this.xrTableCell12.Weight = 0.33131073346112316D;
      // 
      // xrTableCell17
      // 
      this.xrTableCell17.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([DaysCharged])")});
      this.xrTableCell17.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell17.Multiline = true;
      this.xrTableCell17.Name = "xrTableCell17";
      this.xrTableCell17.StylePriority.UseFont = false;
      xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell17.Summary = xrSummary4;
      this.xrTableCell17.Text = "xrTableCell17";
      this.xrTableCell17.Weight = 0.19538839016055931D;
      // 
      // xrTableCell11
      // 
      this.xrTableCell11.Multiline = true;
      this.xrTableCell11.Name = "xrTableCell11";
      this.xrTableCell11.Text = "Avg Price:";
      this.xrTableCell11.Weight = 0.28155386729945076D;
      // 
      // xrTableCell18
      // 
      this.xrTableCell18.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumAvg([calcGrossPrice])")});
      this.xrTableCell18.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell18.Multiline = true;
      this.xrTableCell18.Name = "xrTableCell18";
      this.xrTableCell18.StylePriority.UseFont = false;
      xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell18.Summary = xrSummary5;
      this.xrTableCell18.Text = "xrTableCell18";
      this.xrTableCell18.TextFormatString = "{0:c0}";
      this.xrTableCell18.Weight = 0.45263672005204325D;
      // 
      // xrTableCell19
      // 
      this.xrTableCell19.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTableCell19.Multiline = true;
      this.xrTableCell19.Name = "xrTableCell19";
      this.xrTableCell19.StylePriority.UseFont = false;
      this.xrTableCell19.Text = "Avg Price/Day:";
      this.xrTableCell19.Weight = 0.3558141147702219D;
      // 
      // xrTableCell20
      // 
      this.xrTableCell20.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "(Iif(sumSum([calcNoOfNightWithMoney]) > 0, sumSum([calcGrossPrice])/sumSum([calcN" +
                    "oOfNightWithMoney]) ,0 ))")});
      this.xrTableCell20.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell20.Multiline = true;
      this.xrTableCell20.Name = "xrTableCell20";
      this.xrTableCell20.StylePriority.UseFont = false;
      xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell20.Summary = xrSummary6;
      this.xrTableCell20.TextFormatString = "{0:c0}";
      this.xrTableCell20.Weight = 0.40874944128388996D;
      // 
      // celGrossPrice
      // 
      this.celGrossPrice.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([calcGrossPrice])")});
      this.celGrossPrice.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.celGrossPrice.Multiline = true;
      this.celGrossPrice.Name = "celGrossPrice";
      this.celGrossPrice.StylePriority.UseFont = false;
      this.celGrossPrice.StylePriority.UseTextAlignment = false;
      xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.celGrossPrice.Summary = xrSummary7;
      this.celGrossPrice.Text = "celGrossPrice";
      this.celGrossPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.celGrossPrice.TextFormatString = "{0:c0}";
      this.celGrossPrice.Weight = 0.34489177230296575D;
      // 
      // xrTableCell25
      // 
      this.xrTableCell25.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([calcTotalCharge])")});
      this.xrTableCell25.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell25.Multiline = true;
      this.xrTableCell25.Name = "xrTableCell25";
      this.xrTableCell25.StylePriority.UseFont = false;
      this.xrTableCell25.StylePriority.UseTextAlignment = false;
      xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell25.Summary = xrSummary8;
      this.xrTableCell25.Text = "xrTableCell25";
      this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.xrTableCell25.TextFormatString = "{0:c0}";
      this.xrTableCell25.Weight = 0.34489177230296575D;
      // 
      // xrTableCell22
      // 
      this.xrTableCell22.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([calcPMTotalRecdClient])")});
      this.xrTableCell22.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell22.Multiline = true;
      this.xrTableCell22.Name = "xrTableCell22";
      this.xrTableCell22.StylePriority.UseFont = false;
      this.xrTableCell22.StylePriority.UseTextAlignment = false;
      xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell22.Summary = xrSummary9;
      this.xrTableCell22.Text = "xrTableCell22";
      this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.xrTableCell22.TextFormatString = "{0:c0}";
      this.xrTableCell22.Weight = 0.34728369106808332D;
      // 
      // xrTableCell21
      // 
      this.xrTableCell21.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([calcWimcoTotalRecdClient])")});
      this.xrTableCell21.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell21.Multiline = true;
      this.xrTableCell21.Name = "xrTableCell21";
      this.xrTableCell21.StylePriority.UseFont = false;
      this.xrTableCell21.StylePriority.UseTextAlignment = false;
      xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell21.Summary = xrSummary10;
      this.xrTableCell21.Text = "xrTableCell21";
      this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.xrTableCell21.TextFormatString = "{0:c0}";
      this.xrTableCell21.Weight = 0.37903311233925691D;
      // 
      // xrTableCell23
      // 
      this.xrTableCell23.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([calcDepositDue])")});
      this.xrTableCell23.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell23.Multiline = true;
      this.xrTableCell23.Name = "xrTableCell23";
      this.xrTableCell23.StylePriority.UseFont = false;
      this.xrTableCell23.StylePriority.UseTextAlignment = false;
      xrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell23.Summary = xrSummary11;
      this.xrTableCell23.Text = "xrTableCell23";
      this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.xrTableCell23.TextFormatString = "{0:c0}";
      this.xrTableCell23.Weight = 0.33029062490766947D;
      // 
      // xrTableCell26
      // 
      this.xrTableCell26.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumAvg([calcDepositPercent])")});
      this.xrTableCell26.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell26.Multiline = true;
      this.xrTableCell26.Name = "xrTableCell26";
      this.xrTableCell26.StylePriority.UseFont = false;
      this.xrTableCell26.StylePriority.UseTextAlignment = false;
      xrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell26.Summary = xrSummary12;
      this.xrTableCell26.Text = "xrTableCell26";
      this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.xrTableCell26.TextFormatString = "{0:0.00%}";
      this.xrTableCell26.Weight = 0.30582557908348207D;
      // 
      // calcNoOfNightWithMoney
      // 
      this.calcNoOfNightWithMoney.DataMember = "spDashboardNewAndInquiry";
      this.calcNoOfNightWithMoney.Expression = "Iif([ResvWithMoney] = 1, [DaysCharged], 0)";
      this.calcNoOfNightWithMoney.Name = "calcNoOfNightWithMoney";
      // 
      // groupHeaderBand1
      // 
      this.groupHeaderBand1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("UserName", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
      this.groupHeaderBand1.HeightF = 9.000015F;
      this.groupHeaderBand1.Level = 1;
      this.groupHeaderBand1.Name = "groupHeaderBand1";
      // 
      // GroupFooter2
      // 
      this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
      this.GroupFooter2.HeightF = 35.00001F;
      this.GroupFooter2.Level = 1;
      this.GroupFooter2.Name = "GroupFooter2";
      // 
      // xrTable4
      // 
      this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
      this.xrTable4.Font = new System.Drawing.Font("Arial", 8.25F);
      this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
      this.xrTable4.Name = "xrTable4";
      this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
      this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
      this.xrTable4.SizeF = new System.Drawing.SizeF(1350F, 25F);
      this.xrTable4.StylePriority.UseBorders = false;
      this.xrTable4.StylePriority.UseFont = false;
      // 
      // xrTableRow4
      // 
      this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell24,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell54,
            this.celcGrossPrice2,
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60});
      this.xrTableRow4.Name = "xrTableRow4";
      this.xrTableRow4.Weight = 1D;
      // 
      // xrTableCell43
      // 
      this.xrTableCell43.Multiline = true;
      this.xrTableCell43.Name = "xrTableCell43";
      this.xrTableCell43.Text = "Total for";
      this.xrTableCell43.Weight = 0.21844660601929178D;
      // 
      // xrTableCell24
      // 
      this.xrTableCell24.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[UserName]")});
      this.xrTableCell24.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell24.Multiline = true;
      this.xrTableCell24.Name = "xrTableCell24";
      this.xrTableCell24.StylePriority.UseFont = false;
      this.xrTableCell24.Text = "xrTableCell24";
      this.xrTableCell24.Weight = 0.25970865071466065D;
      // 
      // xrTableCell44
      // 
      this.xrTableCell44.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumCount([ReservationNumber])")});
      this.xrTableCell44.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell44.Multiline = true;
      this.xrTableCell44.Name = "xrTableCell44";
      this.xrTableCell44.StylePriority.UseFont = false;
      xrSummary13.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell44.Summary = xrSummary13;
      this.xrTableCell44.Text = "xrTableCell14";
      this.xrTableCell44.Weight = 0.198616073995159D;
      // 
      // xrTableCell45
      // 
      this.xrTableCell45.Multiline = true;
      this.xrTableCell45.Name = "xrTableCell45";
      this.xrTableCell45.Text = "With Money:";
      this.xrTableCell45.Weight = 0.30566524069284684D;
      // 
      // xrTableCell46
      // 
      this.xrTableCell46.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([ResvWithMoney])")});
      this.xrTableCell46.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell46.Multiline = true;
      this.xrTableCell46.Name = "xrTableCell46";
      this.xrTableCell46.StylePriority.UseFont = false;
      xrSummary14.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell46.Summary = xrSummary14;
      this.xrTableCell46.Text = "xrTableCell15";
      this.xrTableCell46.Weight = 0.21659291982892476D;
      // 
      // xrTableCell47
      // 
      this.xrTableCell47.Multiline = true;
      this.xrTableCell47.Name = "xrTableCell47";
      this.xrTableCell47.Text = "Without Money:";
      this.xrTableCell47.Weight = 0.41019350176831887D;
      // 
      // xrTableCell48
      // 
      this.xrTableCell48.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([ResvWithoutMoney])")});
      this.xrTableCell48.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell48.Multiline = true;
      this.xrTableCell48.Name = "xrTableCell48";
      this.xrTableCell48.StylePriority.UseFont = false;
      xrSummary15.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell48.Summary = xrSummary15;
      this.xrTableCell48.Text = "xrTableCell16";
      this.xrTableCell48.Weight = 0.21116537047551276D;
      // 
      // xrTableCell49
      // 
      this.xrTableCell49.Multiline = true;
      this.xrTableCell49.Name = "xrTableCell49";
      this.xrTableCell49.Text = "# of Nights:";
      this.xrTableCell49.Weight = 0.33131073346112316D;
      // 
      // xrTableCell50
      // 
      this.xrTableCell50.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([DaysCharged])")});
      this.xrTableCell50.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell50.Multiline = true;
      this.xrTableCell50.Name = "xrTableCell50";
      this.xrTableCell50.StylePriority.UseFont = false;
      xrSummary16.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell50.Summary = xrSummary16;
      this.xrTableCell50.Text = "xrTableCell17";
      this.xrTableCell50.Weight = 0.19538839016055931D;
      // 
      // xrTableCell51
      // 
      this.xrTableCell51.Multiline = true;
      this.xrTableCell51.Name = "xrTableCell51";
      this.xrTableCell51.Text = "Avg Price:";
      this.xrTableCell51.Weight = 0.28155376730253329D;
      // 
      // xrTableCell52
      // 
      this.xrTableCell52.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumAvg([calcGrossPrice])")});
      this.xrTableCell52.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell52.Multiline = true;
      this.xrTableCell52.Name = "xrTableCell52";
      this.xrTableCell52.StylePriority.UseFont = false;
      xrSummary17.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell52.Summary = xrSummary17;
      this.xrTableCell52.Text = "xrTableCell18";
      this.xrTableCell52.TextFormatString = "{0:c0}";
      this.xrTableCell52.Weight = 0.45263672005204331D;
      // 
      // xrTableCell53
      // 
      this.xrTableCell53.Multiline = true;
      this.xrTableCell53.Name = "xrTableCell53";
      this.xrTableCell53.Text = "Avg Price/Day:";
      this.xrTableCell53.Weight = 0.35581411477022196D;
      // 
      // xrTableCell54
      // 
      this.xrTableCell54.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "(Iif(sumSum([calcNoOfNightWithMoney]) > 0, sumSum([calcGrossPrice])/sumSum([calcN" +
                    "oOfNightWithMoney]) ,0 ))\n")});
      this.xrTableCell54.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell54.Multiline = true;
      this.xrTableCell54.Name = "xrTableCell54";
      this.xrTableCell54.StylePriority.UseFont = false;
      xrSummary18.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell54.Summary = xrSummary18;
      this.xrTableCell54.TextFormatString = "{0:c0}";
      this.xrTableCell54.Weight = 0.40874954128080737D;
      // 
      // celcGrossPrice2
      // 
      this.celcGrossPrice2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([calcGrossPrice])")});
      this.celcGrossPrice2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.celcGrossPrice2.Multiline = true;
      this.celcGrossPrice2.Name = "celcGrossPrice2";
      this.celcGrossPrice2.StylePriority.UseFont = false;
      this.celcGrossPrice2.StylePriority.UseTextAlignment = false;
      xrSummary19.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.celcGrossPrice2.Summary = xrSummary19;
      this.celcGrossPrice2.Text = "xrTableCell24";
      this.celcGrossPrice2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.celcGrossPrice2.TextFormatString = "{0:c0}";
      this.celcGrossPrice2.Weight = 0.34489177230296575D;
      // 
      // xrTableCell56
      // 
      this.xrTableCell56.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([calcTotalCharge])")});
      this.xrTableCell56.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell56.Multiline = true;
      this.xrTableCell56.Name = "xrTableCell56";
      this.xrTableCell56.StylePriority.UseFont = false;
      this.xrTableCell56.StylePriority.UseTextAlignment = false;
      xrSummary20.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell56.Summary = xrSummary20;
      this.xrTableCell56.Text = "xrTableCell25";
      this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.xrTableCell56.TextFormatString = "{0:c0}";
      this.xrTableCell56.Weight = 0.34489177230296575D;
      // 
      // xrTableCell57
      // 
      this.xrTableCell57.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([calcPMTotalRecdClient])")});
      this.xrTableCell57.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell57.Multiline = true;
      this.xrTableCell57.Name = "xrTableCell57";
      this.xrTableCell57.StylePriority.UseFont = false;
      this.xrTableCell57.StylePriority.UseTextAlignment = false;
      xrSummary21.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell57.Summary = xrSummary21;
      this.xrTableCell57.Text = "xrTableCell22";
      this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.xrTableCell57.TextFormatString = "{0:c0}";
      this.xrTableCell57.Weight = 0.34728369106808332D;
      // 
      // xrTableCell58
      // 
      this.xrTableCell58.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([calcWimcoTotalRecdClient])")});
      this.xrTableCell58.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell58.Multiline = true;
      this.xrTableCell58.Name = "xrTableCell58";
      this.xrTableCell58.StylePriority.UseFont = false;
      this.xrTableCell58.StylePriority.UseTextAlignment = false;
      xrSummary22.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell58.Summary = xrSummary22;
      this.xrTableCell58.Text = "xrTableCell21";
      this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.xrTableCell58.TextFormatString = "{0:c0}";
      this.xrTableCell58.Weight = 0.37903311233925691D;
      // 
      // xrTableCell59
      // 
      this.xrTableCell59.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([calcDepositDue])")});
      this.xrTableCell59.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell59.Multiline = true;
      this.xrTableCell59.Name = "xrTableCell59";
      this.xrTableCell59.StylePriority.UseFont = false;
      this.xrTableCell59.StylePriority.UseTextAlignment = false;
      xrSummary23.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell59.Summary = xrSummary23;
      this.xrTableCell59.Text = "xrTableCell23";
      this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.xrTableCell59.TextFormatString = "{0:c0}";
      this.xrTableCell59.Weight = 0.33029062490766947D;
      // 
      // xrTableCell60
      // 
      this.xrTableCell60.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumAvg([calcDepositPercent])")});
      this.xrTableCell60.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
      this.xrTableCell60.Multiline = true;
      this.xrTableCell60.Name = "xrTableCell60";
      this.xrTableCell60.StylePriority.UseFont = false;
      this.xrTableCell60.StylePriority.UseTextAlignment = false;
      xrSummary24.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
      this.xrTableCell60.Summary = xrSummary24;
      this.xrTableCell60.Text = "xrTableCell26";
      this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
      this.xrTableCell60.TextFormatString = "{0:0.00%}";
      this.xrTableCell60.Weight = 0.30582557908348207D;
      // 
      // calcTotalCharge
      // 
      this.calcTotalCharge.DataMember = "spDashboardNewAndInquiry";
      this.calcTotalCharge.Expression = "Iif([CurrencyID]=1, [TotalCharge],Iif([CurrencyID]=13, [TotalCharge] * [PoundToDo" +
    "llar], [TotalCharge] *[EuroToDollar]) )";
      this.calcTotalCharge.Name = "calcTotalCharge";
      // 
      // calcPMTotalRecdClient
      // 
      this.calcPMTotalRecdClient.DataMember = "spDashboardNewAndInquiry";
      this.calcPMTotalRecdClient.Expression = "Iif([CurrencyID]=1,[PMTotalRecdClient],Iif([CurrencyID]=13, [PMTotalRecdClient] *" +
    " [PoundToDollar], [PMTotalRecdClient] *[EuroToDollar]) )\n";
      this.calcPMTotalRecdClient.Name = "calcPMTotalRecdClient";
      // 
      // calcDepositDue
      // 
      this.calcDepositDue.DataMember = "spDashboardNewAndInquiry";
      this.calcDepositDue.Expression = "Iif([CurrencyID]=1, [DepositDue],Iif([CurrencyID]=13, [DepositDue] * [PoundToDoll" +
    "ar], [DepositDue] *[EuroToDollar]) )";
      this.calcDepositDue.Name = "calcDepositDue";
      // 
      // calcWimcoTotalRecdClient
      // 
      this.calcWimcoTotalRecdClient.DataMember = "spDashboardNewAndInquiry";
      this.calcWimcoTotalRecdClient.Expression = "Iif([CurrencyID]=1, [WimcoTotalRecdClient],Iif([CurrencyID]=13, [WimcoTotalRecdCl" +
    "ient] * [PoundToDollar], [WimcoTotalRecdClient] *[EuroToDollar]) )\n";
      this.calcWimcoTotalRecdClient.Name = "calcWimcoTotalRecdClient";
      // 
      // calcGrossPrice
      // 
      this.calcGrossPrice.DataMember = "spDashboardNewAndInquiry";
      this.calcGrossPrice.Expression = "Iif([CurrencyID]=1, [GrossPrice],Iif([CurrencyID]=13, [GrossPrice] * [PoundToDoll" +
    "ar], [GrossPrice] *[EuroToDollar]) )";
      this.calcGrossPrice.Name = "calcGrossPrice";
      // 
      // xrControlStyle1
      // 
      this.xrControlStyle1.Name = "xrControlStyle1";
      this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
      // 
      // NewResvAndInquiry
      // 
      this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupHeader1,
            this.Detail,
            this.PageHeader,
            this.GroupFooter1,
            this.groupHeaderBand1,
            this.GroupFooter2});
      this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.calcDepositPercent,
            this.calcNoOfNightWithMoney,
            this.calcTotalCharge,
            this.calcPMTotalRecdClient,
            this.calcDepositDue,
            this.calcWimcoTotalRecdClient,
            this.calcGrossPrice});
      this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.sqlDataSource1});
      this.DataMember = "spDashboardNewAndInquiry";
      this.DataSource = this.sqlDataSource1;
      this.Font = new System.Drawing.Font("Arial", 9.75F);
      this.Landscape = true;
      this.Margins = new System.Drawing.Printing.Margins(25, 25, 25, 28);
      this.PageHeight = 850;
      this.PageWidth = 1400;
      this.PaperKind = System.Drawing.Printing.PaperKind.Legal;
      this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.prmStartDateBooked,
            this.prmEndDateBooked,
            this.prmResvAgent});
      this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.DetailCaption1,
            this.DetailData1,
            this.DetailData3_Odd,
            this.PageInfo,
            this.xrControlStyle1});
      this.Version = "19.2";
      ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion

    private DevExpress.DataAccess.Sql.SqlDataSource sqlDataSource1;
    private DevExpress.XtraReports.UI.XRControlStyle Title;
    private DevExpress.XtraReports.UI.XRControlStyle DetailCaption1;
    private DevExpress.XtraReports.UI.XRControlStyle DetailData1;
    private DevExpress.XtraReports.UI.XRControlStyle DetailData3_Odd;
    private DevExpress.XtraReports.UI.XRControlStyle PageInfo;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private DevExpress.XtraReports.UI.XRPageInfo pageInfo1;
    private DevExpress.XtraReports.UI.XRPageInfo pageInfo2;
    private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
    private DevExpress.XtraReports.UI.XRLabel label1;
    private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.Parameters.Parameter prmStartDateBooked;
    private DevExpress.XtraReports.Parameters.Parameter prmEndDateBooked;
    private DevExpress.XtraReports.Parameters.Parameter prmResvAgent;
    private DevExpress.XtraReports.UI.CalculatedField calcDepositPercent;
    private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
    private DevExpress.XtraReports.UI.XRTable xrTable1;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
    private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
    private DevExpress.XtraReports.UI.XRTable xrTable2;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
    private DevExpress.XtraReports.UI.XRTableCell celcWithMoney;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
    private DevExpress.XtraReports.UI.XRTableCell celGrossPrice;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
    private DevExpress.XtraReports.UI.XRTable xrTable3;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
    private DevExpress.XtraReports.UI.XRTable xrTable5;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
    private DevExpress.XtraReports.UI.CalculatedField calcNoOfNightWithMoney;
    private DevExpress.XtraReports.UI.GroupHeaderBand groupHeaderBand1;
    private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
    private DevExpress.XtraReports.UI.XRTable xrTable4;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
    private DevExpress.XtraReports.UI.XRTableCell celcGrossPrice2;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.CalculatedField calcTotalCharge;
        private DevExpress.XtraReports.UI.CalculatedField calcPMTotalRecdClient;
        private DevExpress.XtraReports.UI.CalculatedField calcDepositDue;
        private DevExpress.XtraReports.UI.CalculatedField calcWimcoTotalRecdClient;
        private DevExpress.XtraReports.UI.CalculatedField calcGrossPrice;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
    }
}
