﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintDetailLead.aspx.cs" Inherits="Dashboard.PrintDetailLead" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Print Detail Lead</title>
	<script src="js/jquery-1.8.2.js"></script>
	<script>
		$(document).ready(function () {
			window.print();
		});
	</script>
</head>
<body>
	<form id="form1" runat="server">
		<div>
			<br />
			<asp:Label ID="Label16" runat="server" Font-Bold="True" Font-Size="Larger" Text="Detail Lead"></asp:Label>
			<br />
			<br />

			<asp:GridView ID="grdPrintDetail"
				runat="server"
				AutoGenerateColumns="False"
				DataSourceID="odsDetailLead"
				PageSize="15"
				BorderStyle="None"
				GridLines="Horizontal" DataKeyNames="LeadID" ShowHeader="False">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>

							<asp:DetailsView ID="dvLeadDetail" runat="server" AutoGenerateRows="False" EnableModelValidation="True" GridLines="None" Height="50px" Width="500px" DataKeyNames="LeadID" DataSourceID="odsDetailLead">
								<FieldHeaderStyle HorizontalAlign="Right" Width="170px" Wrap="False" />
								<Fields>
									<asp:TemplateField HeaderText="Received Date:&amp;nbsp;&amp;nbsp;" SortExpression="RequestDate">
										<ItemTemplate>
											<asp:Label ID="lblRequestDate" runat="server" Text='<%# string.Format("{0:MMM dd, yyyy h:mm tt}", Eval("RequestDate")) %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="First Name:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Last Name:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Email Address:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblMailAddress" runat="server" Text='<%# Eval("ClientEMailAddress") %>'> </asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Phone Number:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblPhoneNumber" runat="server" Text='<%# Eval("PhoneNumber") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Villa Code/Hotel Name:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblVillaCode" runat="server" Text='<%# Eval("VillaCode") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="First Choice:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblDestinationName" runat="server" Text='<%# Eval("DestinationName") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Second Choice:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblSecondDestination" runat="server" Text='<%# Eval("SecondDestination") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Begin Date:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblDateFrom" runat="server" Text='<%# string.Format("{0:MMM dd, yyyy}", Eval("DateFrom")) %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="End Date:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblDateTo" runat="server" Text='<%# string.Format("{0:MMM dd, yyyy}", Eval("DateTo")) %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Number of Bedrooms:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblNumberOfBedroom" runat="server" Text='<%# Eval("NumberOfBedroom") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Minimum Price:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblMinimumPrice" runat="server" Text='<%# Eval("MinimumPrice") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Maximum Price:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblMaximumPrice" runat="server" Text='<%# Eval("MaximumPrice") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Must Have a Pool:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblRequirePool" runat="server" Text='<%# (Boolean.Parse(Eval("RequirePool").ToString())) ? "Yes" : string.Empty %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Travel with Children:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblTravelWithChildren" runat="server" Text='<%# Boolean.Parse(Eval("TravelWithChildren").ToString()) ? "Yes" : string.Empty %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Client Source Code:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblContactCodeName" runat="server" Text='<%# Eval("ContactCode") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Web-Inq Source Code:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblWebInqContactSourceCodeName" runat="server" Text='<%# Eval("WebInqContactSourceCode") %>'></asp:Label>
										</ItemTemplate>
										<HeaderStyle Wrap="False" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Recent Agent:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblRecentResvAgentName" runat="server" Text='<%# Eval("RecentResvAgentName") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Claim Agent:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblOwned" runat="server" Text='<%# Eval("ResvAgent") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Claim Agent:&amp;nbsp;&amp;nbsp;" Visible="false">
										<ItemTemplate>
											<asp:Label ID="lblMagUserID1" runat="server" Text='<%# Bind("MagUserID") %>' Visible="false" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Claim Date:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblAssignDate" runat="server" Text='<%# string.Format("{0:MMM dd, yyyy h:mm tt}", Eval("AssignDate")) %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Status:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblLeadStatusCode" runat="server" Text='<%# Eval("LeadStatusCode") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Comment:&amp;nbsp;&amp;nbsp;">
										<ItemTemplate>
											<asp:Label ID="lblComment" runat="server" Text='<%# Eval("Comment") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
								</Fields>
								<RowStyle Width="325px" />
							</asp:DetailsView>

						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
				<PagerSettings Position="TopAndBottom" />
			</asp:GridView>

			<asp:ObjectDataSource ID="odsDetailLead" runat="server" SelectMethod="GetDetailLead" TypeName="DataAccessLayer.LeadData">
				<SelectParameters>
					<asp:SessionParameter Name="emailAddress" SessionField="ClientEmail" Type="String" />
				</SelectParameters>
			</asp:ObjectDataSource>

		</div>
	</form>
</body>
</html>
