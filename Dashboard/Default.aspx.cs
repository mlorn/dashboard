﻿using Dashboard.Reports;
using DataAccessLayer;
using DevExpress.XtraReports.UI;
using QuickRes.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dashboard
{
  public partial class Default : System.Web.UI.Page
  {
    private const string finalPaymentKind = "P";
    private const string nonDepositKind = "D";
    private const string returningClientCallbackKind = "R";
    private const string futureClientCallbackKind = "F";
    private const string leadKind = "L";
    private const string rliKind = "R";
    private const string rliWaitlistKind = "W";
    private const string rliInqKind = "I";
    private const string rliBkdKind = "B";

    private const string admin = "ADMIN";
    private const int salesAssistGroup = 203;
    private const string reviewOK = "ROK";
    private const string reviewNotOK = "RNK";
    private const string noChange = "NC";
    private bool refreshState;
    private bool isRefresh;
    private const int RCCTemplate = 1;  // Returning client callback template
    private const int FCCTemplate = 2;  // Future Client Callback template

    private const string letterRevision = "R";
    private const string letterFinalPayment = "F";
    private const string letterNonDeposit = "D";

    private string filterString = string.Empty;
    private string sortBy = string.Empty;
    private string sortExpression = string.Empty;
    private int userID = 0;
    private int groupID = 0;
    private string userName = string.Empty;
    private string adminUserName = string.Empty;
    private string fullName = string.Empty;

    //private NumberFormatInfo nfi;
    DashboardData dashboard = null;
    dsDashboard ds = null;
    dsSalesMgrBoard dsSMB = null;
    int rowIndex;

    //string filterString = string.Empty;
    //store all the grid names.
    List<string> gridList = new List<string>
      {
        "CurrentFinalPayment",
        "PastFinalPayment",
        "CurrentPastNonDeposit",
        "CurrentReturnClientCallback",
        "PastReturnClientCallback",
        "CurrentFCC",
        "PastFCC",
        "CurrentLead",
        "PastLead",
        "InqRLI",
        "BkdRLI",
        "WtlRLI",
        "RAClient",
        "AllGrids"

      };

    int currentFinalPayment = 0;
    int pastFinalPayment = 0;
    int currentPastNonDeposit = 0;
    int currentReturnClientCallback = 0;
    int pastReturnClientCallback = 0;
    int currentFCC = 0;
    int pastFCC = 0;
    int currentLead = 0;
    int pastLead = 0;
    int inqRLI = 0;
    int bkdRLI = 0;
    int wtlRLI = 0;
    int rowTotal = 0;



    protected void Page_Load(object sender, EventArgs e)
    {

      try
      {
        userID = (int)Session["UserID"];
        groupID = (int)Session["GroupID"];
        userName = Session["UserName"].ToString();
        fullName = Session["FullName"].ToString();
        adminUserName = Session["AdminUserName"].ToString();
      }
      catch
      {
        //TODO: handle exception correctly
        Response.Redirect("login.aspx");
      }

      //if a user is belong to admin group, use ADMIN user name.
      if (adminUserName.Length > 0)
      {
        userName = adminUserName;
      }


      if (!IsPostBack)
      {
        loadData();
      };
      /*
      else
      {
        //ds = Session["DataSet"] as dsDashboard;
        //dsSMB = Session["SMBDataSet"] as dsSalesMgrBoard;
        //grdRA.LoadClientLayout(grdRA.SettingsCookies.CookiesID);
        //loadData();
      }

      if (ds != null)
      {
        grdRA.DataSource = ds.Tables["tblResAgent"];
        //grdRA.KeyFieldName = "UserID";

        grdRA.DataBind();

      }
      */
      /*
      try
      {
        startSmbtDate = Convert.ToDateTime(dtStartDate.Text);
      }
      catch
      {
        startSmbtDate = DateTime.Today;
      }
      try
      {
        endSmbDate = Convert.ToDateTime(dtEndDate.Text);
      }
      catch
      {
        endSmbDate = DateTime.Today;
      }
      */
    }

    /// <summary>
    /// Loads the data from a database
    /// </summary>
    private void loadData()
    {
      DateTime startSmbtDate;
      DateTime endSmbDate;
      DateTime startFCCDate;
      DateTime endFCCDate;
      DateTime startActivityDate;
      DateTime endActivityDate;

      int salesAssistID;

      //  registering a startup JavaScript which is a jQuery event handler that executes btnSubmit button 
      // click client side as soon as the document is loaded.
      //string script = "$(document).ready(function () { $('[id*=btnSubmit]').click(); });";
      //ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);

      lblLoggedInAs.Text = $"Logging in as: {userName}";
      startSmbtDate = DateTime.Today.AddDays(-7);
      dtStartDate.Text = startSmbtDate.ToString("MMM dd, yyyy");

      endSmbDate = DateTime.Today.AddDays(1);
      dtEndDate.Text = endSmbDate.ToString("MMM dd, yyyy");
      HttpContext.Current.Session.Add("StartSmbDate", startSmbtDate);
      HttpContext.Current.Session.Add("EndSmbDate", endSmbDate);

      startFCCDate = DateTime.Today.AddMonths(-1);  //default start date looks back for 1 month
      endFCCDate = DateTime.Today;
      dtStartFCCDate.Text = startFCCDate.ToString("MMM dd, yyyy");
      dtEndFCCDate.Text = endFCCDate.ToString("MMM dd, yyyy");
      HttpContext.Current.Session.Add("StartFCCDate", startFCCDate);
      HttpContext.Current.Session.Add("EndFCCDate", endFCCDate);

      startActivityDate = DateTime.Today;
      endActivityDate = startActivityDate;

      // a user is belonged to the Sales Assistant Group, make drop-down-list of reservation agents visible.
      // so he or she can pick to the Resv Agent's dashboard to work on.
      if (groupID == salesAssistGroup)
      {
        Session["SalesAssistID"] = userID;
        //Response.Redirect("ResvAgentAssist.aspx");
        GetResvAgentForAssist(userID);

        lblSelectResvAgent.Visible = true;
        ddlSelectResvAgent.Visible = true;
        pnlSMB.Visible = false;
        pnlFCC.Visible = false;
        btnRefressLetter.Visible = false;
        return;
      }

      dashboard = new DashboardData(userID, groupID, userName);
      Session["DashboardData"] = dashboard;


      // if a user logged in as Sales Assistant, make the drop-down-list visible.
      try
      {
        salesAssistID = Convert.ToInt32(Session["SalesAssistID"]);
      }
      catch
      {
        salesAssistID = 0;
      }

      if (salesAssistID > 0)
      {
        dashboard = Session["DashboardData"] as DashboardData;
        if (dashboard != null)
        {
          ddlSelectResvAgent.DataSource = dashboard.DashboardDataset.tblRAForAssistant;
          ddlSelectResvAgent.DataBind();
          ddlSelectResvAgent.Items.Insert(0, new ListItem(string.Empty, string.Empty));
          lblSelectResvAgent.Visible = true;
          ddlSelectResvAgent.Visible = true;
          pnlSMB.Visible = true;
          pnlFCC.Visible = true;
          btnRefressLetter.Visible = true;
        }
      }
      else
      {
        lblSelectResvAgent.Visible = false;
        ddlSelectResvAgent.Visible = false;
        pnlSMB.Visible = true;
        pnlFCC.Visible = true;
        btnRefressLetter.Visible = true;
      }

      //testing
      //dashboard.DashboardDataset.WriteXml("c:/dashTemp.xml");
      //testing 
      //dashboard = new DashboardData();
      //dashboard.DashboardDataset.ReadXml("c:/dashTemp.xml");

      //startSmbtDate = Convert.ToDateTime("12/05/2016");
      //endSmbDate = Convert.ToDateTime("12/06/2016");

      //startSmbtDate = DateTime.Today;
      //endSmbDate = startSmbtDate;
      // default look back date from today is 15 days.

      ds = dashboard.DashboardDataset;
      dsSMB = dashboard.SalesMgrBoardDataset;

      Session["SMBDataSet"] = dsSMB;

      Session["DataSet"] = ds;
      grdRA.DataSource = ds.Tables["tblResAgent"];
      grdRA.DataBind();

      //grdActivityCounter.DataSource = ds.tblResAgent;
      //grdActivityCounter.DataBind();

      //grdRA.KeyFieldName = "UserID";

      // add FCC Type to the FCC type drop-down-list
      assignData(dlFCCType, "tblFCCType");
      bindGrid();
    }

    /// <summary>
    /// Binds the grid with data
    /// </summary>
    private void bindGrid()
    {

      //if it is the Sales Agent Group, do not show foot total
      if (groupID == 4 || groupID == 195)  // admin group
      {
        //grdRA.FooterRow.Visible = true;
        grdRA.Visible = true;
        pnlSMB.Visible = false;
        pnlFCC.Visible = false;
        btnRefressLetter.Visible = false;
        //plnActivityCounter.Visible = true;
      }
      else
      {
        grdRA.Visible = false;
        pnlSMB.Visible = true;
        pnlFCC.Visible = true;
        btnRefressLetter.Visible = true;
        //plnActivityCounter.Visible = false;

        filterString = $"UserID = {userID}";
        //grdRA.FooterRow.Visible = false;

        try
        {
          sortExpression = Session["FinalAndNonDepositSort"].ToString();
        }
        catch
        {
          sortExpression = string.Empty;
        }
        // user default sorting, if there a column has not been pressed yet.
        if (string.IsNullOrEmpty(sortExpression))
        {
          sortBy = "DateFrom ASC,DateTo ASC";
        }
        else
        {
          sortBy = sortExpression;
        }
        assignData(grdFinalAndNonDepositRA, "tblFinalAndNonDeposit", filterString, sortBy);
        grdFinalAndNonDepositRA.ToolTip = filterString;

        try
        {
          sortExpression = Session["SalesManagementBoard"].ToString();
        }
        catch
        {
          sortExpression = string.Empty;
        }
        // user default sorting, if there a column has not been pressed yet.
        if (string.IsNullOrEmpty(sortExpression))
        {
          sortBy = string.Empty;
        }
        else
        {
          sortBy = sortExpression;
        }
        assignSMBData(grdSalesManagementBoard, "tblSalesManagementBoard", filterString, sortBy);
        grdSalesManagementBoard.ToolTip = filterString;


        try
        {
          sortExpression = Session["ReturnClientCallbackSort"].ToString();
        }
        catch
        {
          sortExpression = string.Empty;
        }
        // user default sorting, if there a column has not been pressed yet.
        if (string.IsNullOrEmpty(sortExpression))
        {
          sortBy = string.Empty;
        }
        else
        {
          sortBy = sortExpression;
        }
        assignData(grdReturnClientCallbackRA, "tblClientCallback", filterString, sortBy);
        grdReturnClientCallbackRA.ToolTip = filterString;

        try
        {
          sortExpression = Session["FCCSort"].ToString();
        }
        catch
        {
          sortExpression = "FCCType ASC,CallbackDate DESC";
        }
        // user default sorting, if there a column has not been pressed yet.
        if (string.IsNullOrEmpty(sortExpression))
        {
          sortBy = string.Empty; // "FCCType,CallbackDate";
        }
        else
        {
          sortBy = sortExpression;
        }
        assignData(grdFCCRA, "tblFCC", filterString, sortBy);
        grdFCCRA.ToolTip = filterString;

        try
        {
          sortExpression = Session["InqAndWaitListSort"].ToString();
        }
        catch
        {
          sortExpression = string.Empty;
        }
        // user default sorting, if there a column has not been pressed yet.
        if (string.IsNullOrEmpty(sortExpression))
        {
          sortBy = "Status ASC,DateFrom ASC,DateTo ASC";
        }
        else
        {
          sortBy = sortExpression;
        }

        //filterString = $"UserID = {userID} AND (Status = 'INQ' OR Status = 'WTL' OR Status = 'WTX' OR Status = 'OKD' OR Status = 'NOK')";
        filterString = $"UserID = {userID}";
        assignData(grdInqAndWaitListRA, "tblReservationWaitlist", filterString, sortBy);
        grdInqAndWaitListRA.ToolTip = filterString;

        /*
        try
        {
          sortExpression = Session["InHouseAndArrivalSort"].ToString();
        }
        catch
        {
          sortExpression = string.Empty;
        }
        // user default sorting, if there a column has not been pressed yet.
        if (string.IsNullOrEmpty(sortExpression))
        {
          sortBy = string.Empty;
        }
        else
        {
          sortBy = sortExpression;
        }
        filterString = $"UserID = {userID} AND (Status = 'BKD' OR Status = 'NEW')";
        assignData(grdInHouseAndArrivalRA, "tblInHouseAndArrival", filterString, sortBy);
        grdInHouseAndArrivalRA.ToolTip = filterString;
        */

        filterString = $"UserID = {userID} AND (Status = 'BKD' OR Status = 'NEW')";
        try
        {
          sortExpression = Session["ReservationSort"].ToString();
        }
        catch
        {
          sortExpression = string.Empty;
        }
        // user default sorting, if there a column has not been pressed yet.
        if (string.IsNullOrEmpty(sortExpression))
        {
          sortBy = "DateFrom";
        }
        else
        {
          sortBy = sortExpression;
        }
        assignData(grdReservationRA, "tblReservation", filterString, sortBy);
        grdReservationRA.ToolTip = filterString;

        //ML 12/13/2017 : Added
        try
        {
          sortExpression = Session["HurricaneReservationSort"].ToString();
        }
        catch
        {
          sortExpression = string.Empty;
        }
        // user default sorting, if there a column has not been pressed yet.
        if (string.IsNullOrEmpty(sortExpression))
        {
          sortBy = "Villa";
        }
        else
        {
          sortBy = sortExpression;
        }
        filterString = $"UserID = {userID}";
        assignData(grdHurricaneReservation, "tblHurricaneReservation", filterString, sortBy);
        grdHurricaneReservation.ToolTip = filterString;


        filterString = $"UserID = {userID}";
        try
        {
          sortExpression = Session["LetterSort"].ToString();
        }
        catch
        {
          sortExpression = string.Empty;
        }
        // user default sorting, if there a column has not been pressed yet.
        if (string.IsNullOrEmpty(sortExpression))
        {
          sortBy = "DateFrom";
        }
        else
        {
          sortBy = sortExpression;
        }
        assignData(grdLetterRA, "tblLetter", filterString, sortBy);
        grdLetterRA.ToolTip = filterString;

        //ML - 07/27/2018 : commented out
        /*
        sortBy = string.Empty;
        filterString = string.Empty;
        assignData(grdCounter, "tblCounter", filterString, sortBy);
        */
        // add FCC Type to the FCC type drop-down-list
        //assignData(dlFCCType, "tblFCCType");

        filterString = $"UserID = {userID}";
        // user default sorting, if there a column has not been pressed yet.
        if (string.IsNullOrEmpty(sortExpression))
        {
          sortBy = "Month";
        }
        else
        {
          sortBy = sortExpression;
        }
        assignData(grdActivitySummary, "tblActivitySummary", filterString, sortBy);
        grdActivitySummary.ToolTip = filterString;
      }
    }

    protected void GetResvAgentForAssist(int salesAssistID)
    {
      dashboard = Session["DashboardData"] as DashboardData;
      if (dashboard == null)
      {
        dashboard = new DashboardData();
        using (MagnumDataContext magnumDb = new MagnumDataContext())
        {
          if (salesAssistID > 0)
          {
            dashboard.FillResvAgentAssist(magnumDb, salesAssistID);
            ds = dashboard.DashboardDataset;
            Session["DataSet"] = ds;
          }
        }
      }

      ddlSelectResvAgent.DataSource = dashboard.DashboardDataset.tblRAForAssistant;
      ddlSelectResvAgent.DataBind();
      ddlSelectResvAgent.Items.Insert(0, new ListItem(string.Empty, string.Empty));
    }

    /// <summary>
    /// Shows or hides all grids when plus/minus button is pressed.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideAllGrids(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;
      int userID;

      //remove the current the active grid from the list so that it will not be closed,
      //but close the rest of the opened grid.
      gridList.Remove("AllGrids");
      closeOpenedGrid();

      ImageButton imgbtnShowHide = (sender as ImageButton);
      GridViewRow row = (imgbtnShowHide.NamingContainer as GridViewRow);
      if (imgbtnShowHide.CommandArgument == "Show")
      {
        row.FindControl("pnlAllGrids").Visible = true;
        imgbtnShowHide.CommandArgument = "Hide";
        imgbtnShowHide.ImageUrl = "~/images/minus.gif";

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID}";

        /*
        GridView grdClient = row.FindControl("grdClient") as GridView;
        assignData(grdClient, "tblClient", filterString);
        grdClient.ToolTip = filterString;

        GridView grdFinalPayment = row.FindControl("grdFinalPayment") as GridView;
        assignData(grdFinalPayment, "tblFinalPayment", filterString);
        grdFinalPayment.ToolTip = filterString;

        GridView grdNonDeposit = row.FindControl("grdNonDeposit") as GridView;
        assignData(grdNonDeposit, "tblNonDeposit", filterString);
        grdNonDeposit.ToolTip = filterString;

        GridView grdReturnClientCallback = row.FindControl("grdReturnClientCallback") as GridView;
        assignData(grdReturnClientCallback, "tblClientCallback", filterString);
        grdReturnClientCallback.ToolTip = filterString;

        GridView grdLead = row.FindControl("grdLead") as GridView;
        assignData(grdLead, "tblClientCallbackLead", filterString);
        grdLead.ToolTip = filterString;

        GridView grdFCC = row.FindControl("grdFCC") as GridView;
        assignData(grdFCC, "tblFCC", filterString);
        grdFCC.ToolTip = filterString;


        GridView grdResvHistory = row.FindControl("grdResvHistory") as GridView;
        assignData(grdResvHistory, "tblReservation", filterString);
        grdResvHistory.ToolTip = filterString;
        */

        GridView grdFinalAndNonDeposit = row.FindControl("grdFinalAndNonDepositAdmin") as GridView;
        assignData(grdFinalAndNonDeposit, "tblFinalAndNonDeposit", filterString, sortBy);
        grdFinalAndNonDeposit.ToolTip = filterString;

        GridView grdReturnClientCallback = row.FindControl("grdReturnClientCallbackAdmin") as GridView;
        assignData(grdReturnClientCallback, "tblClientCallback", filterString, sortBy);
        grdReturnClientCallback.ToolTip = filterString;

        GridView grdFCC = row.FindControl("grdFCCAdmin") as GridView;
        assignData(grdFCC, "tblFCC", filterString, sortBy);
        grdFCC.ToolTip = filterString;

        //filterString = $"UserID = {userID} AND (Status = 'INQ' OR Status = 'WTL' OR Status = 'WTX')";
        filterString = $"UserID = {userID}";
        GridView grdInqAndWaitlist = row.FindControl("grdInqAndWaitlistAdmin") as GridView;
        assignData(grdInqAndWaitlist, "tblReservationWaitlist", filterString, sortBy);
        grdInqAndWaitlist.ToolTip = filterString;

        filterString = $"UserID = {userID} AND (Status = 'BKD' OR Status = 'NEW')";
        GridView grdInHouseAndArrival = row.FindControl("grdInHouseAndArrivalAdmin") as GridView;
        assignData(grdInHouseAndArrival, "tblInHouseAndArrival", filterString, sortBy);
        grdInHouseAndArrival.ToolTip = filterString;

        filterString = $"UserID = {userID} AND (Status = 'BKD' OR Status = 'NEW')";
        GridView grdReservation = row.FindControl("grdReservationAdmin") as GridView;
        sortBy = "DateFrom";
        assignData(grdReservation, "tblReservation", filterString, sortBy);
        grdReservation.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlAllGrids").Visible = false;
        imgbtnShowHide.CommandArgument = "Show";
        imgbtnShowHide.ImageUrl = "~/images/plus.gif";
      }


    }

    protected void grdClient_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      //int userID;

      GridView grdClient = (sender as GridView);
      grdClient.PageIndex = e.NewPageIndex;
      //userID = Convert.ToInt32(grdClient.ToolTip);
      filterString = grdClient.ToolTip;
      assignData(grdClient, "tblClient", filterString);

      //grdClient.ToolTip = userID.ToString();
    }


    protected void grdFinalPayment_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      GridView grdFinalPayment = (sender as GridView);
      grdFinalPayment.PageIndex = e.NewPageIndex;
      //int userID = Convert.ToInt32(grdFinalPayment.ToolTip);
      filterString = grdFinalPayment.ToolTip;
      assignData(grdFinalPayment, "tblFinalPayment", filterString);

      //grdFinalPayment.ToolTip = filterString;
    }

    protected void grdSalesManagementBoard_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      GridView grdSalesManagementBoard = (sender as GridView);
      grdSalesManagementBoard.PageIndex = e.NewPageIndex;
      //int userID = Convert.ToInt32(grdFinalPayment.ToolTip);
      filterString = grdSalesManagementBoard.ToolTip;
      assignSMBData(grdSalesManagementBoard, "tblSalesManagementBoard", filterString);

      //grdFinalPayment.ToolTip = filterString;
    }

    protected void grdNonDeposit_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;

      GridView grdNonDeposit = (sender as GridView);
      grdNonDeposit.PageIndex = e.NewPageIndex;

      filterString = grdNonDeposit.ToolTip;
      assignData(grdNonDeposit, "tblNonDeposit", filterString);

      //grdNonDeposit.ToolTip = userID.ToString();
    }

    protected void grdReturnClientCallback_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;

      GridView grdReturnClientCallback = (sender as GridView);
      grdReturnClientCallback.PageIndex = e.NewPageIndex;

      filterString = grdReturnClientCallback.ToolTip;
      assignData(grdReturnClientCallback, "tblClientCallback", filterString);

    }

    protected void grdLead_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;

      GridView grdLead = (sender as GridView);
      grdLead.PageIndex = e.NewPageIndex;

      filterString = grdLead.ToolTip;
      assignData(grdLead, "tblClientCallbackLead", filterString);
    }


    protected void grdFCC_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;

      GridView grdFCC = (sender as GridView);
      grdFCC.PageIndex = e.NewPageIndex;
      filterString = grdFCC.ToolTip;
      assignData(grdFCC, "tblFCC", filterString);
    }

    protected void grdResvHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;

      GridView grdResvHistory = (sender as GridView);
      grdResvHistory.PageIndex = e.NewPageIndex;

      filterString = grdResvHistory.ToolTip;
      assignData(grdResvHistory, "tblReservation", filterString);
    }


    /// <summary>
    /// Shows or hides all grids that belong to a client.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideClientGrids(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int clientID;
      string emailAddress = string.Empty;

      ImageButton imgShowHide = (sender as ImageButton);
      GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
      Session["ClientRow"] = row;
      if (imgShowHide.CommandArgument == "Show")
      {
        row.FindControl("pnlClientGrids").Visible = true;
        imgShowHide.CommandArgument = "Hide";
        imgShowHide.ImageUrl = "~/images/minus.gif";

        clientID = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Values["ClientID"]);
        emailAddress = (row.NamingContainer as GridView).DataKeys[row.RowIndex].Values["EMailAddress"].ToString();

        filterString = $"ClientID = {clientID}";

        GridView grdFinalPayment = row.FindControl("grdFinalPayment2") as GridView;
        assignData(grdFinalPayment, "tblFinalPayment", filterString);
        grdFinalPayment.ToolTip = filterString;

        GridView grdNonDeposit = row.FindControl("grdNonDeposit2") as GridView;
        assignData(grdNonDeposit, "tblNonDeposit", filterString);
        grdNonDeposit.ToolTip = filterString;

        GridView grdReturnClientCallback = row.FindControl("grdReturnClientCallback2") as GridView;
        assignData(grdReturnClientCallback, "tblClientCallback", filterString);
        grdReturnClientCallback.ToolTip = filterString;

        GridView grdLead = row.FindControl("grdLead2") as GridView;
        assignData(grdLead, "tblClientCallbackLead", filterString);
        grdLead.ToolTip = filterString;

        GridView grdFCC = row.FindControl("grdFCC2") as GridView;
        assignData(grdFCC, "tblFCC", filterString);
        grdFCC.ToolTip = filterString;


        GridView grdResvHistory = row.FindControl("grdResvHistory2") as GridView;
        assignData(grdResvHistory, "tblReservation", filterString);
        grdResvHistory.ToolTip = filterString;

        // find GroupWise custom control and assign e-mail address for searching by email address.
        GWCustom.GWWimco gw = row.FindControl("gwGrid2") as GWCustom.GWWimco;

        if (gw != null)
        {
          try
          {
            gw.SeachByClientID = false;
            gw.SearchByEmail = true;
            gw.EmailToSearch = emailAddress;
            gw.ReplyButtonVisible = true;

            Session["EMailAddress"] = emailAddress;
            //gw.EmailFromDisplayName = "Mao Lorn";
            //gw.EmailFromEmailAddress = "mlorn@wimco.com";

            if (gw.EmailToSearch != null)
            {
              gw.SearchEmail();
            }

          }
          catch
          { }
        }
      }
      else
      {
        row.FindControl("pnlClientGrids").Visible = false;
        imgShowHide.CommandArgument = "Show";
        imgShowHide.ImageUrl = "~/images/plus.gif";
      }
    }


    /// <summary>
    /// Shows or hides Reservation history.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideGrdResvHistory2(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int clientID;

      ImageButton imgShowHide = (sender as ImageButton);
      GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
      if (imgShowHide.CommandArgument == "Show")
      {
        row.FindControl("pnlResvHistory2").Visible = true;
        imgShowHide.CommandArgument = "Hide";
        imgShowHide.ImageUrl = "~/images/minus.gif";

        clientID = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Value);

        filterString = $"ClientID = {clientID}";

        GridView grdResvHistory = row.FindControl("grdResvHistory2") as GridView;
        assignData(grdResvHistory, "tblReservation", filterString);
        grdResvHistory.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlResvHistory2").Visible = false;
        imgShowHide.CommandArgument = "Show";
        imgShowHide.ImageUrl = "~/images/plus.gif";
      }
    }

    protected void grdFinalPayment2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      int clientID;

      GridView grdView = (sender as GridView);
      grdView.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdView.ToolTip);
      filterString = $"ClientID = {clientID}";
      assignData(grdView, "tblFinalPayment", filterString);

      grdView.ToolTip = filterString;
    }

    protected void grdNonDeposit2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      int clientID;

      GridView grdView = (sender as GridView);
      grdView.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdView.ToolTip);
      filterString = $"ClientID = {clientID}";
      assignData(grdView, "tblNodDeposit", filterString);

      grdView.ToolTip = filterString;
    }

    protected void grdReturnClientCallback2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      int clientID;

      GridView grdView = (sender as GridView);
      grdView.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdView.ToolTip);
      filterString = $"ClientID = {clientID}";
      assignData(grdView, "tblClientCallback", filterString);

      grdView.ToolTip = filterString;
    }

    protected void grdLead2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      int clientID;

      GridView grdView = (sender as GridView);
      grdView.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdView.ToolTip);
      filterString = $"ClientID = {clientID}";
      assignData(grdView, "tblClientCallbackLead", filterString);

      grdView.ToolTip = filterString;
    }

    protected void grdFCC2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      int clientID;

      GridView grdView = (sender as GridView);
      grdView.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdView.ToolTip);
      filterString = $"ClientID = {clientID}";
      assignData(grdView, "tblFCC", filterString);

      grdView.ToolTip = filterString;
    }

    protected void grdResvHistory2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      int clientID;

      GridView grdResvHistory = (sender as GridView);
      grdResvHistory.PageIndex = e.NewPageIndex;
      clientID = Convert.ToInt32(grdResvHistory.ToolTip);
      filterString = $"ClientID = {clientID}";
      assignData(grdResvHistory, "tblReservation", filterString);

      grdResvHistory.ToolTip = filterString;
    }


    /// <summary>
    /// Hide grids
    /// </summary>
    protected void closeOpenedGrid()
    {
      ImageButton imgbtnControl;
      //find a link button control
      LinkButton lnkbtnControl;

      foreach (string ctr in gridList)
      {
        lnkbtnControl = ControlExtensions.FindControlRecursive(grdRA, "lnkbtn" + ctr) as LinkButton;
        if (lnkbtnControl != null)
        {
          GridViewRow row = (lnkbtnControl.NamingContainer as GridViewRow);
          //find a control panel and make it invisible
          row.FindControl("pnl" + ctr).Visible = false;
          lnkbtnControl.CommandArgument = "Show";
        }
        else
        {
          imgbtnControl = ControlExtensions.FindControlRecursive(grdRA, "imgbtn" + ctr) as ImageButton;
          if (imgbtnControl != null)
          {
            GridViewRow row = (imgbtnControl.NamingContainer as GridViewRow);
            //find a control panel and make it invisible
            row.FindControl("pnl" + ctr).Visible = false;
            imgbtnControl.CommandArgument = "Show";
            imgbtnControl.ImageUrl = "~/images/plus.gif";
          }
        }
      }

      // reset 
      /*
      if (!gridList.Contains("AllGrids"))
      {
        imgbtnControl = ControlExtensions.FindControlRecursive(grdRA, "AllGrids") as ImageButton;
        if (imgbtnControl != null)
        {
          GridViewRow row = (imgbtnControl.NamingContainer as GridViewRow);
          //find a control panel and make it invisible
          row.FindControl("pnlAllGrids").Visible = false;
          imgbtnControl.CommandArgument = "Show";
          imgbtnControl.ImageUrl = "~/images/plus.gif";
        }
      }
      */
    }

    /// <summary>
    /// Shows or hides current final payment.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideCurrentFinalPayment(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("CurrentFinalPayment");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);

      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";
        //lnkbtnShowHide.ImageUrl = "~/images/minus.gif";

        row.FindControl("pnlCurrentFinalPayment").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{finalPaymentKind}'";

        GridView grdFinalPayment = row.FindControl("grdCurrentFinalPayment") as GridView;
        assignData(grdFinalPayment, "tblFinalPayment", filterString);
        grdFinalPayment.ToolTip = filterString;

      }
      else
      {
        row.FindControl("pnlCurrentFinalPayment").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
        //lnkbtnShowHide.ImageUrl = "~/images/plus.gif";
      }

    }


    /// <summary>
    /// Shows or hides past final payment.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHidePastFinalPayment(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;
      gridList.Remove("PastFinalPayment");
      closeOpenedGrid();
      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlPastFinalPayment").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{finalPaymentKind}' AND [PastOrFuture] = 'Y'";

        GridView grdFinalPayment = row.FindControl("grdPastFinalPayment") as GridView;
        assignData(grdFinalPayment, "tblFinalPayment", filterString);
        grdFinalPayment.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlPastFinalPayment").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides current past non deposit.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideCurrentPastNonDeposit(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("CurrentPastNonDeposit");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlCurrentPastNonDeposit").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{nonDepositKind}'";

        GridView grdFinalPayment = row.FindControl("grdCurrentPastNonDeposit") as GridView;
        assignData(grdFinalPayment, "tblNonDeposit", filterString);
        grdFinalPayment.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlCurrentPastNonDeposit").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides current return client callback.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideCurrentReturnClientCallback(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("CurrentReturnClientCallback");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlCurrentReturnClientCallback").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{returningClientCallbackKind}'";

        GridView grdFinalPayment = row.FindControl("grdCurrentReturnClientCallback") as GridView;
        assignData(grdFinalPayment, "tblClientCallback", filterString);
        grdFinalPayment.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlCurrentReturnClientCallback").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides past return client callback.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHidePastReturnClientCallback(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("PastReturnClientCallback");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlPastReturnClientCallback").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{futureClientCallbackKind}'";

        GridView grdFinalPayment = row.FindControl("grdPastReturnClientCallback") as GridView;
        assignData(grdFinalPayment, "tblClientCallback", filterString);
        grdFinalPayment.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlPastReturnClientCallback").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides current FCC.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideCurrentFCC(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("CurrentFCC");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlCurrentFCC").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{futureClientCallbackKind}'";

        GridView grdView = row.FindControl("grdCurrentFCC") as GridView;
        assignData(grdView, "tblFCC", filterString);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlCurrentFCC").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides past FCC.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHidePastFCC(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("PastFCC");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlPastFCC").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{futureClientCallbackKind}'";

        GridView grdView = row.FindControl("grdPastFCC") as GridView;
        assignData(grdView, "tblFCC", filterString);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlPastFCC").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides current lead.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideCurrentLead(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("CurrentLead");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlCurrentLead").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{leadKind}'";

        GridView grdView = row.FindControl("grdCurrentLead") as GridView;
        assignData(grdView, "tblClientCallbackLead", filterString);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlCurrentLead").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides past lead.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHidePastLead(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("PastLead");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlPastLead").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{leadKind}'";

        GridView grdView = row.FindControl("grdPastLead") as GridView;
        assignData(grdView, "tblClientCallbackLead", filterString);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlPastLead").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides inq rli.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideInqRLI(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("InqRLI");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlInqRLI").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{rliInqKind}'";

        GridView grdView = row.FindControl("grdInqRLI") as GridView;
        assignData(grdView, "tblReservation", filterString);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlInqRLI").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides BKD rli.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideBkdRLI(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("BkdRLI");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlBkdRLI").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{rliBkdKind}'";

        GridView grdView = row.FindControl("grdBkdRLI") as GridView;
        assignData(grdView, "tblReservation", filterString);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlBkdRLI").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Shows or hides WTL rli.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideWtlRLI(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("WtlRLI");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlWtlRLI").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID} AND [Kind]='{rliWaitlistKind}'";

        GridView grdView = row.FindControl("grdWtlRLI") as GridView;
        assignData(grdView, "tblReservation", filterString);
        grdView.ToolTip = filterString;
      }
      else
      {
        row.FindControl("pnlWtlRLI").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }


    /// <summary>
    /// Shows or hides client grid for a specific RA
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideRAClient(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int userID;

      gridList.Remove("RAClient");
      closeOpenedGrid();

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlRAClient").Visible = true;

        userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"UserID = {userID}";

        GridView grdView = row.FindControl("grdRAClient") as GridView;
        assignData(grdView, "TblClient", filterString);
        grdView.ToolTip = filterString;

      }
      else
      {
        row.FindControl("pnlRAClient").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }


    /// <summary>
    /// Shows or hides ra client grids.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHideRAClientGrids(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      int clientID;
      string emailAddress = string.Empty;

      ImageButton imgShowHide = (sender as ImageButton);
      GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
      if (imgShowHide.CommandArgument == "Show")
      {
        row.FindControl("pnlRAClientGrids").Visible = true;
        imgShowHide.CommandArgument = "Hide";
        imgShowHide.ImageUrl = "~/images/minus.gif";

        clientID = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Values["ClientID"]);
        emailAddress = (row.NamingContainer as GridView).DataKeys[row.RowIndex].Values["EMailAddress"].ToString();

        filterString = $"ClientID = {clientID}";

        Label lblTesting = row.FindControl("lblTest") as Label;

        //GWCustom.GWWimco gw = (GWCustom.GWWimco)row.FindControl("gwGrid");

        //gw.EmailToSearch = "sdsd";
        //gw.SearchEmail();

        GridView grdFinalPayment = row.FindControl("grdRAClntFinalPayment2") as GridView;
        assignData(grdFinalPayment, "tblFinalPayment", filterString);
        grdFinalPayment.ToolTip = filterString;

        GridView grdNonDeposit = row.FindControl("grdRAClntNonDeposit2") as GridView;
        assignData(grdNonDeposit, "tblNonDeposit", filterString);
        grdNonDeposit.ToolTip = filterString;

        GridView grdReturnClientCallback = row.FindControl("grdRAClntReturnClientCallback2") as GridView;
        assignData(grdReturnClientCallback, "tblClientCallback", filterString);
        grdReturnClientCallback.ToolTip = filterString;

        GridView grdLead = row.FindControl("grdRAClntLead2") as GridView;
        assignData(grdLead, "tblClientCallbackLead", filterString);
        grdLead.ToolTip = filterString;

        GridView grdFCC = row.FindControl("grdRAClntFCC2") as GridView;
        assignData(grdFCC, "tblFCC", filterString);
        grdFCC.ToolTip = filterString;


        GridView grdResvHistory = row.FindControl("grdRAClntResvHistory2") as GridView;
        assignData(grdResvHistory, "tblReservation", filterString);
        grdResvHistory.ToolTip = filterString;
        /*
        filterString = string.Format("EmailAddress = '{0}'", "lfrankfort@coach.com");
        GridView gwEmail = row.FindControl("grdRAClntGroupWiseEmail") as GridView;
        assignData(gwEmail, "tblClient", filterString);
        gwEmail.ToolTip = filterString;
        */

        // find GroupWise custom control and assign e-mail address for searching by email address.
        GWCustom.GWWimco gw = row.FindControl("gwGrid") as GWCustom.GWWimco;
        if (gw != null)
        {
          try
          {

            gw.SeachByClientID = false;
            gw.SearchByEmail = true;
            gw.ReplyButtonVisible = true;
            gw.EmailToSearch = emailAddress;


            Session["EMailAddress"] = emailAddress;

            //gw.EmailFromDisplayName = "Mao Lorn";
            //gw.EmailFromEmailAddress = "mlorn@wimco.com";

            if (gw.EmailToSearch != null)
            {
              gw.SearchEmail();
            }
          }
          catch
          { }

        }
      }
      else
      {
        row.FindControl("pnlRAClientGrids").Visible = false;
        imgShowHide.CommandArgument = "Show";
        imgShowHide.ImageUrl = "~/images/plus.gif";
      }
    }


    /// <summary>
    /// Assigns the data.
    /// </summary>
    /// <param name="gridView">The grid view.</param>
    /// <param name="tableName">Name of the table.</param>
    /// <param name="filterString">The filter string.</param>
    private void assignData(GridView gridView, string tableName, string filterString)
    {
      DataTable dt;
      DataView dv;


      ds = Session["DataSet"] as dsDashboard;


      if (ds == null)
      {
        return;
      }

      try
      {
        //get a data table
        dt = ds.Tables[tableName];
        //filter data table 
        dv = dt.DefaultView;
        dv.RowFilter = filterString;
        //assign detail grid with the filtered DataView
        if (dv.Count > 0)
        {
          gridView.DataSource = dv;
          gridView.DataBind();
          gridView.Visible = true;
        }
        else
        {
          gridView.Visible = false;
        }
      }
      catch (Exception ex)
      {
        string errMsg = ex.Message;
        //TODO:  better handle exception
      }

    }


    /// <summary>
    /// Assigns the SMB data.
    /// </summary>
    /// <param name="gridView">The grid view.</param>
    /// <param name="tableName">Name of the table.</param>
    /// <param name="filterString">The filter string.</param>
    private void assignSMBData(GridView gridView, string tableName, string filterString)
    {
      DataTable dt;
      DataView dv;


      dsSMB = Session["SMBDataSet"] as dsSalesMgrBoard;


      if (dsSMB == null)
      {
        gridView.DataSource = null;
        gridView.DataBind();
        return;
      }

      try
      {
        //get a data table
        dt = dsSMB.Tables[tableName];
        //filter data table 
        dv = dt.DefaultView;
        dv.RowFilter = filterString;
        //assign detail grid with the filtered DataView
        if (dv.Count > 0)
        {
          gridView.DataSource = dv;
          gridView.DataBind();
          //gridView.Visible = true;
        }
        else
        {
          //gridView.Visible = false;
          gridView.DataSource = null;
          gridView.DataBind();
        }
      }
      catch (Exception ex)
      {
        string errMsg = ex.Message;
        //TODO:  better handle exception
      }

    }

    /// <summary>
    /// Assigns the data.  This method has a sort parameter.
    /// </summary>
    /// <param name="gridView">The grid view.</param>
    /// <param name="tableName">Name of the table.</param>
    /// <param name="filterString">The filter string.</param>
    private void assignData(GridView gridView, string tableName, string filterString, string sortBy)
    {
      DataTable dt;
      DataView dv;

      gridView.Visible = true;
      //get DataSet from the session
      ds = Session["DataSet"] as dsDashboard;

      if (ds == null)
      {
        gridView.DataSource = null;
        gridView.DataBind();
        return;
      }

      try
      {
        //get a data table
        dt = ds.Tables[tableName];

        dv = dt.DefaultView;

        if (sortBy.Length > 0)
        {
          dv.Sort = sortBy;
        }
        //filter data table 
        dv.RowFilter = filterString;
        //assign detail grid with the filtered DataView
        if (dv.Count > 0)
        {
          gridView.DataSource = dv;
          gridView.DataBind();
          //gridView.Visible = true;
        }
        else
        {
          //gridView.Visible = false;
          gridView.DataSource = null;
          gridView.DataBind();
        }
      }
      catch (Exception ex)
      {
        string errMsg = ex.Message;
        //TODO:  better handle exception
      }
    }


    private void assignData(DropDownList dropDownList, string tableName)
    {
      DataTable dt;

      //get DataSet from the session
      ds = Session["DataSet"] as dsDashboard;

      if (ds == null)
      {
        dropDownList.DataSource = null;
        dropDownList.DataBind();
        return;
      }

      try
      {
        //get a data table
        dt = ds.Tables[tableName];
        dropDownList.DataSource = dt;
        dropDownList.DataBind();
      }
      catch (Exception ex)
      {
        string errMsg = ex.Message;
        //TODO:  better handle exception
      }
    }

    /// <summary>
    /// Assigns the SMB data.
    /// </summary>
    /// <param name="gridView">The grid view.</param>
    /// <param name="tableName">Name of the table.</param>
    /// <param name="filterString">The filter string.</param>
    /// <param name="sortBy">The sort by.</param>
    private void assignSMBData(GridView gridView, string tableName, string filterString, string sortBy)
    {
      DataTable dt;
      DataView dv;

      //get DataSet from the session
      dsSMB = Session["SMBDataSet"] as dsSalesMgrBoard;

      if (dsSMB == null)
      {
        gridView.DataSource = null;
        gridView.DataBind();
        return;
      }

      try
      {
        //get a data table
        dt = dsSMB.Tables[tableName];

        dv = dt.DefaultView;

        if (sortBy.Length > 0)
        {
          dv.Sort = sortBy;
        }
        //filter data table 
        dv.RowFilter = filterString;
        //assign detail grid with the filtered DataView
        if (dv.Count > 0)
        {
          gridView.DataSource = dv;
          gridView.DataBind();
          //gridView.Visible = true;
        }
        else
        {
          //gridView.Visible = false;
          gridView.DataSource = null;
          gridView.DataBind();
        }
      }
      catch (Exception ex)
      {
        string errMsg = ex.Message;
        //TODO:  better handle exception
      }
    }


    /// <summary>
    /// Sorts data.
    /// </summary>
    /// <param name="gridView">The grid view.</param>
    /// <param name="tableName">Name of the table.</param>
    /// <param name="filterString">The filter string.</param>
    /// <param name="sortExpression">The sort expression.</param>
    private void sortData(GridView gridView, string tableName, string filterString, string sortExpression)
    {
      DataTable dt;
      DataView dv;

      //get DataSet from the session
      ds = Session["DataSet"] as dsDashboard;

      if (ds == null)
      {
        return;
      }

      try
      {
        //get a data table
        dt = ds.Tables[tableName];
        //filter data table 
        dv = dt.DefaultView;
        dv.RowFilter = filterString;
        dv.Sort = sortExpression;
        //assign detail grid with the filtered DataView
        if (dv.Count > 0)
        {
          gridView.DataSource = dv;
          gridView.DataBind();
          gridView.Visible = true;
        }
        else
        {
          gridView.Visible = false;
        }
      }
      catch (Exception ex)
      {
        string errMsg = ex.Message;
        //TODO:  better handle exception
      }

    }


    /// <summary>
    /// Sorts the SMB data.
    /// </summary>
    /// <param name="gridView">The grid view.</param>
    /// <param name="tableName">Name of the table.</param>
    /// <param name="filterString">The filter string.</param>
    /// <param name="sortExpression">The sort expression.</param>
    private void sortSMBData(GridView gridView, string tableName, string filterString, string sortExpression)
    {
      DataTable dt;
      DataView dv;

      //get DataSet from the session
      dsSMB = Session["SMBDataSet"] as dsSalesMgrBoard;

      if (dsSMB == null)
      {
        return;
      }

      try
      {
        //get a data table
        dt = dsSMB.Tables[tableName];
        //filter data table 
        dv = dt.DefaultView;
        dv.RowFilter = filterString;
        dv.Sort = sortExpression;
        //assign detail grid with the filtered DataView
        if (dv.Count > 0)
        {
          gridView.DataSource = dv;
          gridView.DataBind();
          gridView.Visible = true;
        }
        else
        {
          gridView.Visible = false;
        }
      }
      catch (Exception ex)
      {
        string errMsg = ex.Message;
        //TODO:  better handle exception
      }

    }

    /// <summary>
    /// Creates the URL for Resv No.
    /// </summary>
    /// <param name="refType">Type of the ref.</param>
    /// <param name="refNumber">The ref number.</param>
    /// <returns></returns>
    private string CreateURL(string refType, string refNumber)
    {
      string userName;
      string url = string.Empty;

      userName = Session["UserName"].ToString();
      url = SecureJump.JumpToNomad.CreateJumpURL(userName, refType, refNumber);
      return url;
    }

    /// <summary>
    /// Jumps to other site.
    /// </summary>
    /// <param name="refType">Type of the ref.</param>
    /// <param name="refNumber">The ref number.</param>
    private void jumpToOtherSite(string refType, string refNumber)
    {
      string userName;
      if (isRefresh == false)
      {
        userName = Session["UserName"].ToString();
        string url = SecureJump.JumpToNomad.CreateJumpURL(userName, refType, refNumber);
        //Response.Write("<script>window.open('" + url + "','_blank');</script>");
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "','_blank');", true);
        //ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "','_blank',left=0,width=860);", true);
      }
      else
      {
        Response.Redirect(Request.RawUrl);
      }
    }

    /// <summary>
    /// Handles the RowDataBound event of the grdRA control for adding each cell 
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void grdRA_RowDataBound(object sender, GridViewRowEventArgs e)
    {

      int temp;

      temp = DataBinder.Eval(e.Row.DataItem, "FinalPayment") == DBNull.Value ? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FinalPayment"));
      currentFinalPayment += temp;

      //temp = DataBinder.Eval(e.Row.DataItem, "FinalPaymentPast") == DBNull.Value? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FinalPaymentPast"));
      //pastFinalPayment += temp;

      temp = DataBinder.Eval(e.Row.DataItem, "NonDeposit") == DBNull.Value ? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "NonDeposit"));
      currentPastNonDeposit += temp;

      temp = DataBinder.Eval(e.Row.DataItem, "ReturnClientCallback") == DBNull.Value ? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ReturnClientCallback"));
      currentReturnClientCallback += temp;

      //temp = DataBinder.Eval(e.Row.DataItem, "ReturnClientCallbackPast") == DBNull.Value? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ReturnClientCallbackPast"));
      //pastReturnClientCallback += temp;

      temp = DataBinder.Eval(e.Row.DataItem, "FCCCurrent") == DBNull.Value ? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FCCCurrent"));
      currentFCC += temp;

      //temp = DataBinder.Eval(e.Row.DataItem, "FCCPast") == DBNull.Value? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FCCPast"));
      //pastFCC += temp;

      temp = DataBinder.Eval(e.Row.DataItem, "CurrentLead") == DBNull.Value ? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "CurrentLead"));
      currentLead += temp;

      //temp = DataBinder.Eval(e.Row.DataItem, "PastLead") == DBNull.Value? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "PastLead"));
      //pastLead += temp;

      temp = DataBinder.Eval(e.Row.DataItem, "InqRLI") == DBNull.Value ? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "InqRLI"));
      inqRLI += temp;

      temp = DataBinder.Eval(e.Row.DataItem, "BkdRLI") == DBNull.Value ? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "BkdRLI"));
      bkdRLI += temp;

      temp = DataBinder.Eval(e.Row.DataItem, "WtlRLI") == DBNull.Value ? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "WtlRLI"));
      wtlRLI += temp;

      temp = DataBinder.Eval(e.Row.DataItem, "RowTotal") == DBNull.Value ? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "RowTotal"));
      rowTotal += temp;

      //update row total 
      if (e.Row.RowType == DataControlRowType.Footer)
      {
        GridViewRow row = e.Row;
        row.Cells[2].Text = currentFinalPayment.ToString();
        //row.Cells[3].Text = pastFinalPayment.ToString();
        row.Cells[3].Text = currentPastNonDeposit.ToString();
        row.Cells[4].Text = currentReturnClientCallback.ToString();
        //row.Cells[6].Text = pastReturnClientCallback.ToString();
        row.Cells[5].Text = currentFCC.ToString();
        //row.Cells[8].Text = pastFCC.ToString();
        row.Cells[6].Text = currentLead.ToString();
        //row.Cells[10].Text = pastLead.ToString();
        row.Cells[7].Text = inqRLI.ToString();
        row.Cells[8].Text = bkdRLI.ToString();
        row.Cells[9].Text = wtlRLI.ToString();
        row.Cells[10].Text = rowTotal.ToString();
      }
    }



    protected void grdRA_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {

      int index = Convert.ToInt32(e.CommandArgument);
      GridViewRow gvRow = grdRA.Rows[index];
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
      int ID = Convert.ToInt32(((LinkButton)sender).CommandArgument);
    }


    /// <summary>
    /// Handles the Click event of the lnkbtnItinerary control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void lnkbtnItinerary_Click(object sender, EventArgs e)
    {
      string itinerary = ((LinkButton)sender).CommandArgument;
      jumpToOtherSite("I", itinerary);
    }

    /// <summary>
    /// Handles the Click event of the lnkbtnResvNo control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void lnkbtnResvNo_Click(object sender, EventArgs e)
    {
      string resvNo = ((LinkButton)sender).CommandArgument;
      jumpToOtherSite("R", resvNo);
    }

    protected void lnkbtnEmail_Click(object sender, EventArgs e)
    {
      string clientID = ((LinkButton)sender).CommandArgument;
      Session["ClientID"] = clientID;

      Response.Redirect("sendemail.aspx");
    }

    /// <summary>
    /// Handles the Click event of the lnkbtnClientID control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void lnkbtnClientID_Click(object sender, EventArgs e)
    {
      string jumpToNomad = string.Empty;
      /*
      try
      {
        jumpToNomad = Session["JumpToNomad"].ToString();
      }
      catch
      {
        jumpToNomad = string.Empty;
      }

      if (jumpToNomad != string.Empty)
      {

        if (jumpToNomad == ((LinkButton)sender).ClientID)
        {
          Session.Remove("JumpToNomad");
          //  loadData();
          return;
        }
        //Session.Remove("JumpToNomad");
      }

      Session["JumpToNomad"] = ((LinkButton)sender).ClientID;
      */
      string clientID = ((LinkButton)sender).CommandArgument;
      jumpToOtherSite("C", clientID);

    }

    /// <summary>
    /// Shows the hidelnkbtn group wise email.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void showHidelnkbtnGroupWiseEmail(object sender, EventArgs e)
    {
      string filterString = string.Empty;
      string emailAddress = string.Empty;

      LinkButton lnkbtnShowHide = (sender as LinkButton);
      GridViewRow row = (lnkbtnShowHide.NamingContainer as GridViewRow);
      if (lnkbtnShowHide.CommandArgument == "Show")
      {
        lnkbtnShowHide.CommandArgument = "Hide";

        row.FindControl("pnlRAClntGroupWiseEmail").Visible = true;
        //emailAddress = (row.NamingContainer as GridView).DataKeys[row.RowIndex].Value.ToString();
        //GWCustom.GWWimco grdView = row.FindControl("GWWimcoEmailGrid1") as GWCustom.GWWimco;
        //grdView.SearchByEmail = true;
        //grdView.EmailToSearch = "louisduquesne@hotmail.com";
        //grdView.ReplyButtonVisible = true;
        //grdView.SearchEmail();

        //userID = (int)grdRA.DataKeys[row.RowIndex].Value;

        filterString = $"EmailAddress = '{"lfrankfort@coach.com"}'";

        GridView grdView = row.FindControl("grdRAClntGroupWiseEmail") as GridView;
        assignData(grdView, "tblClient", filterString);
        grdView.ToolTip = filterString;

      }
      else
      {
        row.FindControl("pnlRAClntGroupWiseEmail").Visible = false;
        lnkbtnShowHide.CommandArgument = "Show";
      }
    }

    /// <summary>
    /// Gets or sets the grid view sort direction.
    /// </summary>
    /// <value>
    /// The grid view sort direction.
    /// </value>
    private string GridViewSortDirection
    {
      get
      {
        return ViewState["SortDirection"] as string ?? "ASC";
      }
      set
      {
        ViewState["SortDirection"] = value;
      }

    }

    /// <summary>
    /// Changes the sort direction (toggle)
    /// </summary>
    /// <returns></returns>
    private string ChangeSortDirection()
    {
      switch (GridViewSortDirection)
      {
        case "ASC":
          GridViewSortDirection = "DESC";
          break;
        case "DESC":
          GridViewSortDirection = "ASC";
          break;
        default:
          GridViewSortDirection = "ASC";
          break;
      }

      return GridViewSortDirection;
    }

    /// <summary>
    /// Handles the Sorting event of the grdClient control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdClient_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdClient_ = (sender as GridView);
      filterString = grdClient_.ToolTip;

      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdClient_, "tblClient", filterString, sortExpression);
      ChangeSortDirection();
    }


    /// <summary>
    /// Handles the Sorting event of the grdFinalPayment control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdFinalPayment_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdFinalPayment = (sender as GridView);
      filterString = grdFinalPayment.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }

      sortData(grdFinalPayment, "tblFinalPayment", filterString, sortExpression);
      ChangeSortDirection();
    }

    /// <summary>
    /// Handles the Sorting event of the grdSalesManagementBoard control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdSalesManagementBoard_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdSalesManagementBoard = (sender as GridView);
      filterString = grdSalesManagementBoard.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortSMBData(grdSalesManagementBoard, "tblSalesManagementBoard", filterString, sortExpression);
      ChangeSortDirection();
    }

    /// <summary>
    /// Handles the Sorting event of the grdNonDeposit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdNonDeposit_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;
      GridView grdNonDeposit = (sender as GridView);
      filterString = grdNonDeposit.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdNonDeposit, "tblNonDeposit", filterString, sortExpression);
      ChangeSortDirection();
    }

    /// <summary>
    /// Handles the Sorting event of the grdReturnClientCallback control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdReturnClientCallback_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdReturnClientCallback = (sender as GridView);
      filterString = grdReturnClientCallback.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdReturnClientCallback, "tblClientCallback", filterString, sortExpression);
      ChangeSortDirection();
      Session["ReturnClientCallbackSort"] = sortExpression;
    }

    /// <summary>
    /// Handles the Sorting event of the grdLead control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdLead_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdLead = (sender as GridView);
      filterString = grdLead.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdLead, "tblClientCallbackLead", filterString, sortExpression);
      ChangeSortDirection();
    }


    /// <summary>
    /// Handles the Sorting event of the grdFCC control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdFCC_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      // if the Print header is pressed, print grid instead of sorting.
      if (sortExpression == "Print")
      {
        string URL = "PrintFCC.aspx";
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + URL + "','_blank');", true);
      }
      else
      {
        GridView grdFCC = (sender as GridView);
        filterString = grdFCC.ToolTip;
        sortDir = GridViewSortDirection;
        sortExpression = e.SortExpression;
        if (sortExpression.Contains("LastName,FirstName"))
        {
          sortExpression = $"LastName {sortDir},FirstName {sortDir}";
        }
        else
        {
          if (sortExpression.Contains("FCCType,CallbackDate"))
          {
            sortExpression = $"FCCType {sortDir},CallbackDate DESC";
          }
          else
          {
            sortExpression += " " + sortDir;
          }
        }
        sortData(grdFCC, "tblFCC", filterString, sortExpression);
        ChangeSortDirection();
        Session["FCCSort"] = sortExpression;
      }
    }

    /// <summary>
    /// Handles the Sorting event of the grdResvHistory control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdResvHistory_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdResvHistory = (sender as GridView);
      filterString = grdResvHistory.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdResvHistory, "tblReservation", filterString, sortExpression);
      ChangeSortDirection();
    }

    protected void lnkbtnCurrentFinalPayment_DataBinding(object sender, EventArgs e)
    {
      //LinkButton temp = sender as LinkButton;
      //currentFinalPayment += Convert.ToInt32(temp.Text); // This will add to running total of all.
    }

    /// <summary>
    /// Handles the PageIndexChanging event of the grdFinalAndNonDeposit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewPageEventArgs"/> instance containing the event data.</param>
    protected void grdFinalAndNonDeposit_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;

      GridView grdFinalAndNonDeposit = (sender as GridView);
      grdFinalAndNonDeposit.PageIndex = e.NewPageIndex;
      //int userID = Convert.ToInt32(grdFinalPayment.ToolTip);
      filterString = grdFinalAndNonDeposit.ToolTip;
      assignData(grdFinalAndNonDeposit, "tblFinalAndNonDeposit", filterString, sortBy);

      //grdFinalPayment.ToolTip = filterString;
    }

    /// <summary>
    /// Handles the Sorting event of the grdFinalPayment control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdFinalAndNonDeposit_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdFinalAndNonDeposit = (sender as GridView);
      filterString = grdFinalAndNonDeposit.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdFinalAndNonDeposit, "tblFinalAndNonDeposit", filterString, sortExpression);
      ChangeSortDirection();
      Session["FinalAndNonDepositSort"] = sortExpression;
    }

    /// <summary>
    /// Handles the PageIndexChanging event of the grdInqAndWaitList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewPageEventArgs"/> instance containing the event data.</param>
    protected void grdInqAndWaitList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;

      GridView grdInqAndWaitList = (sender as GridView);
      grdInqAndWaitList.PageIndex = e.NewPageIndex;

      filterString = grdInqAndWaitList.ToolTip;
      assignData(grdInqAndWaitList, "tblReservationWaitlist", filterString, sortBy);
    }

    /// <summary>
    /// Handles the Sorting event of the grdInqAndWaitList.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdInqAndWaitList_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdInqAndWaitList = (sender as GridView);
      filterString = grdInqAndWaitList.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdInqAndWaitList, "tblReservationWaitlist", filterString, sortExpression);
      ChangeSortDirection();
      Session["InqAndWaitListSort"] = sortExpression;
    }

    /// <summary>
    /// Handles the PageIndexChanging event of the grdInHouseAndArrival control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewPageEventArgs"/> instance containing the event data.</param>
    protected void grdInHouseAndArrival_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = string.Empty;

      GridView grdInHouseAndArrival = (sender as GridView);
      grdInHouseAndArrival.PageIndex = e.NewPageIndex;

      filterString = grdInHouseAndArrival.ToolTip;
      assignData(grdInHouseAndArrival, "tblInHouseAndArrival", filterString, sortBy);

      //grdNonDeposit.ToolTip = userID.ToString();
    }

    /// <summary>
    /// Handles the Sorting event of the grdInHouseAndArrival control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdInHouseAndArrival_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdInHouseAndArrival = (sender as GridView);
      filterString = grdInHouseAndArrival.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdInHouseAndArrival, "tblInHouseAndArrival", filterString, sortExpression);
      ChangeSortDirection();
      Session["InHouseAndArrivalSort"] = sortExpression;
    }

    /// <summary>
    /// Handles the PageIndexChanging event of the grdReservation control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewPageEventArgs"/> instance containing the event data.</param>
    protected void grdReservation_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = "DateFrom";

      GridView grdReservation = (sender as GridView);
      grdReservation.PageIndex = e.NewPageIndex;

      filterString = grdReservation.ToolTip;
      assignData(grdReservation, "tblReservation", filterString, sortBy);

      //grdNonDeposit.ToolTip = userID.ToString();
    }

    /// <summary>
    /// Handles the Sorting event of the grdReservation control.
    /// sorting the grid if a column heading is clicked.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void grdReservation_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdReservation = (sender as GridView);
      filterString = grdReservation.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdReservation, "tblReservation", filterString, sortExpression);
      ChangeSortDirection();
      Session["ReservationSort"] = sortExpression;
    }

    /// <summary>
    /// Handles the RowDataBound event of the grdFCCRA control.
    /// The Color field stores the acctual color name, e.g. Red, Yellow, and White.
    /// and convert these color names to the actual color.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void grdFCCRA_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      RadioButtonList rdbDisplay;
      string displayRadioButtonList = string.Empty;
      string adminUser = string.Empty;
      string previousState = string.Empty;

      try
      {
        adminUser = Session["Admin"].ToString();
      }
      catch
      {
        //TODO: better handle exception
      }

      string temp;
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        temp = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Color"));
        if (temp != null)
        {
          // convert the actual color name, e.g. Red to a color and change row background
          // color accordingly.
          if (temp == "red" || temp == "yellow")  // only change background color, if is red or yellow.
          {
            e.Row.BackColor = System.Drawing.Color.FromName(temp);
            e.Row.ControlStyle.CssClass = "tblRowPrint";
          }
        }

        /* ML - 12/04/2018 : commented out because it is no longer used.
        //find control using
        rdbDisplay = e.Row.FindControl("rdbOKNotOK") as RadioButtonList; //control id which is in gridview
        if (rdbDisplay != null)
        {

          displayRadioButtonList = DataBinder.Eval(e.Row.DataItem, "DisplayRadioButtonList").ToString();
          // if review not OK, remember the selection
          if (displayRadioButtonList == reviewNotOK)  // review not ok
          {
            rdbDisplay.SelectedValue = reviewNotOK;
            rdbDisplay.Visible = true;
          }
          else
          {
            if (displayRadioButtonList == noChange)  // no change
            {
              rdbDisplay.SelectedValue = string.Empty;
              rdbDisplay.Visible = true;
            }
            else
            {
              rdbDisplay.SelectedValue = string.Empty;
              rdbDisplay.Visible = false;
            }
          }

          if (rdbDisplay.Visible)
          {
            previousState = DataBinder.Eval(e.Row.DataItem, "PreviousState").ToString();
            if (previousState == reviewNotOK)
            {
              rdbDisplay.ForeColor = System.Drawing.Color.Red;
              rdbDisplay.Font.Bold = true;
            }
            // if admin logged in, enable the check box.
            if (adminUser == admin)
            {
              rdbDisplay.Enabled = true;
            }
            else
            {
              rdbDisplay.Enabled = false;
            }
          }
        }
        */

        string key = grdFCCRA.DataKeys[e.Row.RowIndex].Value.ToString();
        e.Row.Attributes.Add("id", key);
      }
    }

    /// <summary>
    /// Handles the RowDataBound event of the grdSalesManagementBoard control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void grdSalesManagementBoard_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      string temp = string.Empty;
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        temp = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "EmailTraffic"));
        if (temp != null)
        {
          if (temp == "1")  // change background color
          {
            e.Row.Cells[0].BackColor = System.Drawing.Color.Green;
          }
          else
          {
            if (temp == "2")
            {
              e.Row.Cells[0].BackColor = System.Drawing.Color.Red;
            }

          }
        }
      }
    }

    /// <summary>
    /// Handles the Click event of the btnPrintLetterFinalAndNonDepositRA control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPrintLetterFinalAndNonDepositRA_Click(object sender, EventArgs e)
    {
      //Response.Clear();
      string finalURL = string.Empty;
      string targetWebSite = "http://nomad.wimco.com/";
      string userName = string.Empty;
      string url = string.Empty;
      string temp = string.Empty;
      string[] stringValues;
      string resvNo = string.Empty;
      string resvNumberList = string.Empty;
      string kind = string.Empty;
      string totalReceived = string.Empty;

      if (isRefresh == false)
      {
        GlobalSetting.LetterObj letterObj;
        SecureJump.GenericSecureJump<GlobalSetting.LetterObj> secureJump;
        letterObj = new GlobalSetting.LetterObj();
        secureJump = new SecureJump.GenericSecureJump<GlobalSetting.LetterObj>();

        // get the final URL page from the config file.
        finalURL = Properties.Settings.Default.FinalURL;
        targetWebSite = Properties.Settings.Default.targetWebSite;

        temp = ((LinkButton)sender).CommandArgument;
        // the commandArgument stored both reservation, kind (P = final payment, D = deposit), total received
        // and separate resv no and kind by :
        stringValues = temp.Split(':');
        resvNo = stringValues[0];
        kind = stringValues[1];
        totalReceived = stringValues[2];

        ds = Session["DataSet"] as dsDashboard;
        dashboard = new DashboardData(ds);

        // get all reservation numbers that belong to an itinerary
        //resvNumberList = dashboard.GetResvWithinItinerary(resvNo);

        letterObj.LetterID = 200;
        letterObj.ReservationNumber = resvNo;
        letterObj.ResvNumberList = resvNumberList;
        letterObj.FullName = string.Empty;
        letterObj.FinalURL = finalURL;
        if (kind == "P")
        {
          if (totalReceived != "0.00")
          {
            //letterObj.LetterName = "Final Payment Letter";
            letterObj.ProcessLetterKind = GlobalSetting.LetterGroup.FinalPayment;
          }
          else
          {
            letterObj.ProcessLetterKind = GlobalSetting.LetterGroup.Revision;
          }

        }
        else
        {
          //letterObj.LetterName = "Revision Letter";
          letterObj.ProcessLetterKind = GlobalSetting.LetterGroup.Revision;
        }

        try
        {
          letterObj.MagUserName = Session["UserName"].ToString();
        }
        catch
        {
          //TODO: better handle exception
          letterObj.MagUserName = string.Empty;
        }

        url = secureJump.GenerateJumpURL(letterObj, letterObj.MagUserName, targetWebSite, letterObj.FinalURL);
        url = "window.open('" + url + "', '_blank');";
        //Response.Write("<script>window.open('" + url + "','_blank');</script>");
        //Response.Write("<script>window.open('" + url + "','_blank');</script>");
        //Response.Redirect(url);
        ClientScript.RegisterStartupScript(this.GetType(), "script", url, true);
      }
      else
      {
        Response.Redirect(Request.RawUrl);
      }
    }

    /// <summary>
    /// Handles the Click event of the btnPrintLetterReservationRA control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPrintLetterReservationRA_Click(object sender, EventArgs e)
    {
      /*
      if (Session["JumpToNomad"] != null)
      {

        if (Session["JumpToNomad"].ToString() != ((LinkButton)sender).ClientID)
        {

        }
        else
        {
          Session.Remove("JumpToNomad");
          loadData();
          return;
        }
        Session.Remove("JumpToNomad");
      }
      Session["JumpToNomad"] = ((LinkButton)sender).ClientID;
      */

      //Response.Clear(); //for debug
      string finalURL = string.Empty;
      string targetWebSite = "http://nomad.wimco.com/";
      string userName = string.Empty;
      string url = string.Empty;
      string resvNo = string.Empty;
      string resvNumberList = string.Empty;
      string filterString = string.Empty;
      string sortExpression = string.Empty;

      if (isRefresh == false)
      {

        GlobalSetting.LetterObj letterObj;
        SecureJump.GenericSecureJump<GlobalSetting.LetterObj> secureJump;
        letterObj = new GlobalSetting.LetterObj();
        secureJump = new SecureJump.GenericSecureJump<GlobalSetting.LetterObj>();

        finalURL = Properties.Settings.Default.FinalURL;
        targetWebSite = Properties.Settings.Default.targetWebSite;

        //LinkButton lnkLetter = (sender as LinkButton);
        //GridViewRow row = (lnkLetter.NamingContainer as GridViewRow);
        //resvNo = grdReservationRA.DataKeys[row.RowIndex].Values["ReservationNumber"].ToString();

        ds = Session["DataSet"] as dsDashboard;
        dashboard = new DashboardData(ds);

        resvNo = ((LinkButton)sender).CommandArgument;

        // get all reservation numbers that belong to an itinerary
        //resvNumberList = dashboard.GetResvWithinItinerary(resvNo);

        letterObj.LetterID = 200;
        letterObj.ReservationNumber = resvNo;
        letterObj.ResvNumberList = resvNumberList;
        letterObj.FinalURL = finalURL;
        letterObj.LetterName = string.Empty;
        letterObj.ProcessLetterKind = GlobalSetting.LetterGroup.Revision;

        try
        {
          letterObj.MagUserName = Session["UserName"].ToString();
        }
        catch
        {
          //TODO: better handle exception
          letterObj.MagUserName = string.Empty;
        }

        try
        {
          letterObj.FullName = Session["FullName"].ToString();
        }
        catch
        {
          //TODO: better handle exception
          letterObj.FullName = string.Empty;
        }


        url = secureJump.GenerateJumpURL(letterObj, letterObj.MagUserName, targetWebSite, letterObj.FinalURL);
        url = "window.open('" + url + "', '_blank');";
        //Response.Write("<script>window.open('" + url + "','_blank');</script>");
        //Response.Write("<script>window.open('" + url + "','_blank');</script>");
        ClientScript.RegisterStartupScript(this.GetType(), "script", url, true);
        //Response.Redirect(url);
      }
      else
      {
        Response.Redirect(Request.RawUrl);
      }
    }

    /// <summary>
    /// Loads the state of the view.
    /// </summary>
    /// <param name="savedState">State of the saved.</param>
    protected override void LoadViewState(object savedState)
    {
      object[] AllStates = (object[])savedState;
      base.LoadViewState(AllStates[0]);
      refreshState = bool.Parse(AllStates[1].ToString());
      if (Session["ISREFRESH"] != null && Session["ISREFRESH"] != string.Empty)
        isRefresh = (refreshState == (bool)Session["ISREFRESH"]);
    }

    protected override object SaveViewState()
    {
      Session["ISREFRESH"] = refreshState;
      object[] AllStates = new object[3];
      AllStates[0] = base.SaveViewState();
      AllStates[1] = !(refreshState);
      return AllStates;
    }

    protected void grdRA_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnReadEmail_Click(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Handles the Click event of the Email control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Email_Click(object sender, EventArgs e)
    {
      string url = string.Empty;
      LinkButton lnkbtnEmail = (sender as LinkButton);
      string clientID = Convert.ToString(lnkbtnEmail.CommandArgument);
      string email = lnkbtnEmail.Text;

      Session["ClientID"] = clientID;
      Session["EmailSendToAddress"] = email;

      Session["DID"] = null;
      Session["TemplateKindID"] = null;
      Session["FCCContactSourceID"] = null;
      Session["FCCDateInitial"] = null;
      Session["TemplateID"] = null;

      url = "sendemail.aspx?new=true";
      //window.open('sendemail.aspx?new=true, '_blank', 'toolbar = no, scrollbars = yes, resizable = yes, top = 50, left = 50, width = 860, height = 400', '');
      Response.Write("<script>window.open('" + url + "','_blank', 'toolbar = no, scrollbars = yes, resizable = yes, top = 50, left = 50, width = 746, height = 660', '');</script>");
    }

    /// <summary>
    /// Handles the Click event of the grdFCCRAEmail control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void grdFCCRAEmail_Click(object sender, EventArgs e)
    {
      // ML - 1/30/2018 : commented out and replace with statements below
      /*
      string url = string.Empty;
      LinkButton lnkbtnEmail = (sender as LinkButton);
      string clientID = Convert.ToString(lnkbtnEmail.CommandArgument);
      string email = lnkbtnEmail.Text;

      Session["ClientID"] = clientID;
      Session["EmailToSearch"] = email;

      Session["DID"] = null;
      Session["TemplateKindID"] = FCCTemplate;
      Session["FCCContactSourceID"] = null;
      Session["FCCDateInitial"] = null;
      Session["TemplateID"] = null;
      jumpToOtherSite("C", clientID);
      */
      Session["TemplateKindID"] = FCCTemplate;
      string[] stringValues;
      string clientID = string.Empty;
      string email = string.Empty;
      string temp = string.Empty;

      clientID = ((LinkButton)sender).CommandArgument; //Convert.ToString(lnkbtnDetail.CommandArgument);
      temp = ((LinkButton)sender).CommandArgument;
      // the commandArgument stored both client ID and email 
      // and separated by :
      stringValues = temp.Split(':');
      clientID = stringValues[0];
      email = stringValues[1];

      try
      {
        Session.Remove("EmailBody");
      }
      catch
      {
        //TODO: better handle exception
      }
      Session["ClientID"] = clientID;
      Session["EmailSendToAddress"] = email;

      //url = "sendemail.aspx?new=true";
      //window.open('sendemail.aspx?new=true, '_blank', 'toolbar = no, scrollbars = yes, resizable = yes, top = 50, left = 50, width = 860, height = 400', '');
      //Response.Write("<script>window.open('" + url + "','_blank', 'toolbar = no, scrollbars = yes, resizable = yes, top = 50, left = 50, width = 746, height = 660', '');</script>");
    }


    /// <summary>
    /// Handles the Click event of the ReturnClientCallbackEmail control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ReturnClientCallbackEmail_Click(object sender, EventArgs e)
    {
      string url = string.Empty;
      string temp = string.Empty;
      string[] stringSeparators = new string[] { ";" };
      string[] values;
      string clientID = string.Empty;
      string destinationID = string.Empty;
      string email = string.Empty;
      string dateTo = string.Empty;

      LinkButton lnkbtnEmail = (sender as LinkButton);
      //commandArgument stores ClientID and DestinationID and seperated by ;
      temp = Convert.ToString(lnkbtnEmail.CommandArgument);
      if (temp != null)
      {
        values = temp.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
        if (values.Length == 2)
        {
          clientID = values[0];
          destinationID = values[1];
        }
        else
        {
          if (values.Length == 3)
          {
            clientID = values[0];
            destinationID = values[1];
            dateTo = values[2];
          }
          else
          {
            if (values.Length == 4)
            {
              clientID = values[0];
              destinationID = values[1];
              dateTo = values[2];
              email = values[3];
            }
          }
        }
      }

      try
      {
        Session.Remove("EmailBody");
      }
      catch
      {
        //TODO: better handle exception
      }

      Session["ClientID"] = clientID;
      Session["EmailSendToAddress"] = email;
      Session["DID"] = destinationID;
      Session["TemplateKindID"] = RCCTemplate;
      Session["FCCContactSourceID"] = 700;
      Session["FCCDateInitial"] = dateTo;
      Session["TemplateID"] = null;

      url = "sendemail.aspx?new=true";
      //Response.Write("<script>window.open('" + url + "','_blank', 'toolbar = no, scrollbars = yes, resizable = yes, top = 50, left = 50, width = 780, height = 680', '');</script>");
    }

    /// <summary>
    /// Handles the Click event of the Detail control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Detail_Click(object sender, EventArgs e)
    {
      DashboardData dashboardData = new DashboardData();
      string url = string.Empty;
      string clientEmail = string.Empty;
      //LinkButton lnkbtnDetail = (LinkButton)sender;
      string clientID = ((LinkButton)sender).CommandArgument; //Convert.ToString(lnkbtnDetail.CommandArgument);
                                                              //string email = lnkbtnEmail.Text;
      try
      {
        Session.Remove("EmailBody");
      }
      catch
      {
        //TODO: better handle exception
      }
      Session["ClientID"] = clientID;
      clientEmail = dashboardData.GetClientEmailAddress(Convert.ToInt32(clientID));
      Session["EmailSendToAddress"] = clientEmail;
      Session["EmailToSearch"] = clientEmail;
      //url = "sendemail.aspx?showall=true";

      // use JavaScript link
      //window.open('sendemail.aspx?new=true, '_blank', 'toolbar = no, scrollbars = yes, resizable = yes, top = 50, left = 50, width = 860, height = 400', '');
      //  Response.Write("<script>window.open('" + url + "','_blank', 'toolbar = no, scrollbars = yes, resizable = yes, top = 50, left = 50, width = 746, height = 660', '');event.stopPropagation();</script>");

    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    protected void dtStartDate_TextChanged(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Handles the Click event of the btnSubmit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSubmitSMB_Click(object sender, EventArgs e)
    {
      string sortExpression = string.Empty;
      string sortBy = string.Empty;
      string filterString = string.Empty;
      int userID = 0;
      DateTime startDate;
      DateTime endDate;
      DashboardData dashboard = new DashboardData();

      try
      {
        startDate = Convert.ToDateTime(dtStartDate.Text);
      }
      catch
      {
        startDate = DateTime.Today;
      }

      try
      {
        endDate = Convert.ToDateTime(dtEndDate.Text);
      }
      catch
      {
        endDate = DateTime.Today.AddDays(1);
      }

      try
      {
        userID = (int)Session["UserID"];
      }
      catch
      {
        //TODO: handle exception correctly
        Response.Redirect("login.aspx");
      }

      filterString = $"UserID = {userID}";

      try
      {
        sortExpression = Session["SalesManagementBoard"].ToString();
      }
      catch
      {
        sortExpression = string.Empty;
      }
      // user default sorting, if there a column has not been pressed yet.
      if (string.IsNullOrEmpty(sortExpression))
      {
        sortBy = string.Empty;
      }
      else
      {
        sortBy = sortExpression;
      }

      dashboard.FillSalesManagermnetBoard(startDate, endDate);
      dsSMB = dashboard.SalesMgrBoardDataset;

      Session["SMBDataSet"] = dsSMB;

      assignSMBData(grdSalesManagementBoard, "tblSalesManagementBoard", filterString, sortBy);
      grdSalesManagementBoard.ToolTip = filterString;
    }

    /// <summary>
    /// Handles the RowCreated event of the grdFCCRA control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void grdFCCRA_RowCreated(object sender, GridViewRowEventArgs e)
    {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        e.Row.Attributes.Add("onclick", "onGridViewRowSelected('" + rowIndex.ToString() + "')");
      }
      rowIndex++;
    }

    /// <summary>
    /// Handles the RowDataBound event of the grdInqAndWaitListRA control.
    /// Draws background color according a value storing in the RowColor field
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void grdInqAndWaitListRA_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      Label lblStatus;
      string temp;
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        temp = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "RowColor"));
        if (temp != null)
        {
          // convert the actual color name, e.g. Green to a color and change row background
          // color accordingly.
          if (temp == "yellow")  // only change background color, if is green or yellow.
          {
            e.Row.BackColor = System.Drawing.Color.FromName(temp);
            //e.Row.ControlStyle.CssClass = "tblRowPrint";
          }
          else
          {
            if (temp == "green")  // only change background color, if is green or yellow.
            {
              lblStatus = e.Row.FindControl("grdInqAndWaitListRAStatus") as Label;
              if (lblStatus != null)
              {
                lblStatus.Text = $"{lblStatus.Text} - Available";
              }
            }
          }
        }
      }
    }
    protected void btnSubmitFCC_Click(object sender, EventArgs e)
    {
      reloadFCC();
    }

    /// <summary>
    /// Reloads the FCC records.
    /// </summary>
    private void reloadFCC()
    {
      string sortExpression = string.Empty;
      string sortBy = string.Empty;
      string filterString = string.Empty;
      int userID = 0;
      string userName = string.Empty;
      int fccType = 0;
      string temperature = string.Empty;
      DateTime startFCCDate;
      DateTime endFCCDate;

      //DashboardData dashboard = new DashboardData();

      try
      {
        startFCCDate = Convert.ToDateTime(dtStartFCCDate.Text);
      }
      catch
      {
        startFCCDate = DateTime.Today.AddMonths(-1);
      }

      try
      {
        endFCCDate = Convert.ToDateTime(dtEndFCCDate.Text);
      }
      catch
      {
        endFCCDate = DateTime.Today;
      }

      try
      {
        userID = (int)Session["UserID"];
      }
      catch
      {
        //TODO: handle exception correctly
        Response.Redirect("login.aspx");
      }

      fccType = Convert.ToInt32(dlFCCType.SelectedValue);
      temperature = dlTemperature.SelectedValue;

      filterString = $"UserID = {userID}";
      if (fccType > 0)
      {
        filterString += $" AND FCCContactSourceID = {fccType}";
      }

      if (temperature == "Cold" || temperature == "Hot" || temperature == "Warm")
      {
        filterString += $" AND LeadScoreDescription = '{temperature}'";
      }

      try
      {
        sortExpression = Session["FCCSort"].ToString();
      }
      catch
      {
        sortExpression = "FCCType ASC,CallbackDate DESC";
      }

      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        dashboard = Session["DashboardData"] as DashboardData;
        dashboard.FillFutureClientCallback(magnumDb, startFCCDate, endFCCDate);
        //dashboardDataset.tblFCC
        //Remove FCCs if they are already in the Return Client Callback grid.
        dashboard.RemoveFCC();

        ds = dashboard.DashboardDataset;
        Session["DataSet"] = ds;
      }

      assignData(grdFCCRA, "tblFCC", filterString, sortBy);
      grdFCCRA.ToolTip = filterString;
      //assignData(dlFCCType, "tblFCCType");
    }

    protected void ddlSelectResvAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
      string logInAs = ddlSelectResvAgent.SelectedValue.ToString();
      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        var logIn = magnumDb.spLogInGetUser(logInAs).FirstOrDefault();
        if (logIn != null)
        {
          Session["UserID"] = logIn.UserID;
          Session["UserName"] = logIn.UserName;
          Session["GroupID"] = (int)logIn.GroupID;
          Session["AdminUserName"] = string.Empty;
          Session["FullName"] = logIn.FullName;

          UserData ud = new UserData();
          ud.UserID = logIn.UserID;
          ud.username = logIn.UserName;
          ud.FullName = logIn.FullName;
          ud.EmailAddress = logIn.EmailAddress;
          ud.SalesAgent = true;
          ud.OrganizationID = 1; //wimco
          ud.Loggedin = true;
          HttpContext.Current.Session.Add("UserData", ud);

          string uds = BaseLogic.SerializeAnObject(ud);
          string b64 = BaseLogic.EncodeTo64(uds);
          HttpCookie c = new HttpCookie("udv");
          c.Expires = DateTime.Now.AddHours(12);
          c.Value = b64;
          c.Path = "/";
          Response.Cookies.Add(c);

          Response.Redirect("Default.aspx");
        }
      }
    }

    protected void grdHurricaneReservation_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = "DateFrom";

      GridView grdHurricaneReservation = (sender as GridView);
      grdHurricaneReservation.PageIndex = e.NewPageIndex;

      filterString = grdHurricaneReservation.ToolTip;
      assignData(grdHurricaneReservation, "tblHurricaneReservation", filterString, sortBy);

      //grdNonDeposit.ToolTip = userID.ToString();
    }

    protected void grdHurricaneReservation_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdHurricaneReservation = (sender as GridView);
      filterString = grdHurricaneReservation.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdHurricaneReservation, "tblHurricaneReservation", filterString, sortExpression);
      ChangeSortDirection();
      Session["HurricaneReservationSort"] = sortExpression;
    }

    protected void grdCounter_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      string temp = string.Empty;
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        temp = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Description"));
        if (temp != null)
        {
          if (temp == "NEW WORK" || temp == "PAST DUE WORK")  // change font to bold
          {
            e.Row.Cells[0].Font.Bold = true;
            e.Row.Cells[0].Font.Underline = true;
          }
          else
          {
            e.Row.Cells[0].Font.Bold = false;
            e.Row.Cells[0].Font.Underline = false;
          }

          if (temp.Contains("TOTAL"))
          {
            e.Row.Cells[0].ForeColor = System.Drawing.Color.Red;
          }

        }
      }
    }


    /// <summary>
    /// Handles the Click event of the btnSubmitChange control.
    /// Loop throught the FCC grid and find out if there are any check marks
    /// and call the web service method to update the FCCManagement record.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSubmitChange_Click(object sender, EventArgs e)
    {
      RadioButtonList rdbOKNotOK;
      int callbackID;
      bool result = false;
      string temp = string.Empty;
      string previousState = string.Empty;

      foreach (GridViewRow gvr in grdFCCRA.Rows)
      {
        if (gvr.RowType == DataControlRowType.DataRow)
        {
          rdbOKNotOK = gvr.Cells[0].FindControl("rdbOKNotOK") as RadioButtonList;
          temp = rdbOKNotOK.SelectedValue;

          previousState = grdFCCRA.DataKeys[gvr.RowIndex].Values["PreviousState"].ToString();

          if (temp.Length > 0)
          {
            callbackID = Convert.ToInt32(grdFCCRA.DataKeys[gvr.RowIndex].Values["CallbackID"]);
            result = WimcoBaseLogic.BaseLogic.fcc_UpdateFCCCheckBox(callbackID, temp);
          }

        }
      }
      // refresh the grid
      reloadFCC();
    }

    protected void lnkbtnSalesManagementBoardDetail_Command(object sender, CommandEventArgs e)
    {
      int.TryParse(e.CommandArgument.ToString(), out int clientID);
    }

    protected void grdReservationRA_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      string temp;
      int oddEven;
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
        temp = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ReservationType"));
        if (temp != null)
        {
          oddEven = e.Row.RowIndex % 2;
          //if a reservation is In House or Arrival change background color
          if (temp == "InHouse")
          {
            if (oddEven == 0)
            {
              // even row
              e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#D8BFD8");
            }
            else
            {
              // odd row
              e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#EE82EE");
            }
          }
          else
          {
            // change backgroud color for other reservations
            if (oddEven == 0)
            {
              // even row
              e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#ADD8E6");
            }
            else
            {
              //odd row
              e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#AFEEEE");
            }
          }
        }
      }

    }

    protected void grdLetterRA_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;
      string sortBy = "Kind";

      GridView grdLetterRA = (sender as GridView);
      grdLetterRA.PageIndex = e.NewPageIndex;

      filterString = grdLetterRA.ToolTip;
      assignData(grdLetterRA, "tblLetter", filterString, sortBy);
    }

    protected void grdLetterRA_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdLetterRA = (sender as GridView);
      filterString = grdLetterRA.ToolTip;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;
      if (sortExpression.Contains("LastName,FirstName"))
      {
        sortExpression = $"LastName {sortDir},FirstName {sortDir}";
      }
      else
      {
        sortExpression += " " + sortDir;
      }
      sortData(grdLetterRA, "tblLetter", filterString, sortExpression);
      upnlGrdLetterRA.Update();
      ChangeSortDirection();
      Session["LetterSort"] = sortExpression;
    }

    /// <summary>
    /// Handles the Click event of the btnPrintLetterFinalAndNonDepositRA control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnPrintLetterRA_Click(object sender, EventArgs e)
    {
      //Response.Clear();
      string finalURL = string.Empty;
      string targetWebSite = "http://nomad.wimco.com/";
      string userName = string.Empty;
      string url = string.Empty;
      string temp = string.Empty;
      string[] stringValues;
      string resvNo = string.Empty;
      string resvNumberList = string.Empty;
      string kind = string.Empty;
      string totalReceived = string.Empty;

      if (isRefresh == false)
      {
        GlobalSetting.LetterObj letterObj;
        SecureJump.GenericSecureJump<GlobalSetting.LetterObj> secureJump;
        letterObj = new GlobalSetting.LetterObj();
        secureJump = new SecureJump.GenericSecureJump<GlobalSetting.LetterObj>();

        // get the final URL page from the config file.
        finalURL = Properties.Settings.Default.FinalURL;
        targetWebSite = Properties.Settings.Default.targetWebSite;

        temp = ((LinkButton)sender).CommandArgument;
        // the commandArgument stored both reservation, kind (R = Revision, F = Final Payment, D = NonDeposit), total received
        // and separate resv no, kind, total received by :
        stringValues = temp.Split(':');
        resvNo = stringValues[0];
        kind = stringValues[1];
        totalReceived = stringValues[2];

        ds = Session["DataSet"] as dsDashboard;
        dashboard = new DashboardData(ds);

        // get all reservation numbers that belong to an itinerary
        //resvNumberList = dashboard.GetResvWithinItinerary(resvNo);

        letterObj.LetterID = 200;
        letterObj.ReservationNumber = resvNo;
        letterObj.ResvNumberList = resvNumberList;
        letterObj.FullName = string.Empty;
        letterObj.FinalURL = finalURL;
        switch (kind)
        {
          case letterFinalPayment:
            {
              letterObj.ProcessLetterKind = GlobalSetting.LetterGroup.FinalPayment;
              break;
            }
          // for NonDeposit letter will be processed in the Revision rules because RAs
          // will not send the Non-Deposit letter (Deposit Invoice).
          case letterNonDeposit:
          case letterRevision:
            {
              letterObj.ProcessLetterKind = GlobalSetting.LetterGroup.Revision;
              break;
            }
          default:
            {
              letterObj.ProcessLetterKind = GlobalSetting.LetterGroup.Revision;
              break;
            }
        }

        try
        {
          letterObj.MagUserName = Session["UserName"].ToString();
        }
        catch
        {
          //TODO: better handle exception
          letterObj.MagUserName = string.Empty;
        }

        url = secureJump.GenerateJumpURL(letterObj, letterObj.MagUserName, targetWebSite, letterObj.FinalURL);
        //url = "window.open('" + url + "', '_blank');";
        //Response.Write("<script>window.open('" + url + "','_blank');</script>");
        //Response.Write("<script>window.open('" + url + "','_blank');</script>");
        //Response.Redirect(url);
        //ClientScript.RegisterStartupScript(this.GetType(), "script", url, true);
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "','_blank');", true);

      }
      else
      {
        Response.Redirect(Request.RawUrl);
      }
    }

    /// <summary>
    /// Handles the Click event of the btnRemoveLetterRA control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnRemoveLetterRA_Click(object sender, EventArgs e)
    {
      string temp = string.Empty;
      string[] stringValues;
      string resvNo = string.Empty;
      string kind = string.Empty;


      if (isRefresh == false)
      {
        temp = ((LinkButton)sender).CommandArgument;
        // the commandArgument stored both reservation, kind (R = Revision, F = Final Payment)
        // and separate resv no and kind by :
        stringValues = temp.Split(':');
        resvNo = stringValues[0];
        kind = stringValues[1];
        LetterDataBridge letterDataBridge = new LetterDataBridge();
        letterDataBridge.ResetResvAgentLetter(resvNo, kind);
        ReloadLetter();
        upnlGrdLetterRA.Update();
      }
    }

    protected void btnRefressLetter_Click(object sender, EventArgs e)
    {
      ReloadLetter();
    }

    private void ReloadLetter()
    {
      string sortExpression = string.Empty;
      string sortBy = string.Empty;
      string filterString = string.Empty;
      int userID = 0;
      string userName = string.Empty;

      DateTime startDate = DateTime.Today;
      DateTime endDate = startDate;

      //DashboardData dashboard = new DashboardData();

      try
      {
        userID = (int)Session["UserID"];
      }
      catch
      {
        //TODO: handle exception correctly
        Response.Redirect("login.aspx");
      }

      filterString = $"UserID = {userID}";
      try
      {
        sortExpression = Session["LetterSort"].ToString();
      }
      catch
      {
        sortExpression = string.Empty;
      }
      // user default sorting, if there a column has not been pressed yet.
      if (string.IsNullOrEmpty(sortExpression))
      {
        sortBy = "DateFrom";
      }
      else
      {
        sortBy = sortExpression;
      }

      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        dashboard = Session["DashboardData"] as DashboardData;
        dashboard.FillLetter(magnumDb, startDate, endDate);


        ds = dashboard.DashboardDataset;
        Session["DataSet"] = ds;
      }

      assignData(grdLetterRA, "tblLetter", filterString, sortBy);
      grdLetterRA.ToolTip = filterString;
      //assignData(dlFCCType, "tblFCCType");
    }

    protected void grdActivitySummary_Sorting(object sender, GridViewSortEventArgs e)
    {
      string filterString = string.Empty;
      string sortExpression = string.Empty;
      string sortDir = string.Empty;

      GridView grdActivitySummary = (sender as GridView);
      //filterString = grdActivityCounter.ToolTip;
      filterString = string.Empty;
      sortDir = GridViewSortDirection;
      sortExpression = e.SortExpression;

      sortExpression += " " + sortDir;
      sortData(grdActivitySummary, "tblActivitySummary", filterString, sortExpression);
      ChangeSortDirection();
      Session["ActivitySummarySort"] = sortExpression;


    }

    protected void grdActivitySummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      string filterString = string.Empty;

      GridView grdActivitySummary = (sender as GridView);
      grdActivitySummary.PageIndex = e.NewPageIndex;
      filterString = grdActivitySummary.ToolTip;
      assignData(grdActivitySummary, "tblActivitySummary", filterString);
    }

    protected void btnCurrentMonthSales_Click(object sender, EventArgs e)
    {
      DataSet ds;
      XtraReport rpt;
      string url;
      DateTime currentDate = DateTime.Today;
      DateTime firstDayOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);

      dsDashboard data = Session["DataSet"] as dsDashboard;
      dashboard = new DashboardData(data);
      ds = dashboard.GetNewResvAndInquiry(firstDayOfMonth, currentDate, userName);

      rpt = new NewResvAndInquiry();
      rpt.Parameters["prmStartDateBooked"].Value = firstDayOfMonth;
      rpt.Parameters["prmEndDateBooked"].Value = currentDate;

      rpt.DataSource = ds;
      rpt.DataMember = "Table";
      rpt.CreateDocument();
      Session.Add("ReportObject", rpt);
      //Response.Redirect("~/LetterMainViewer.aspx");
      url = $"{Properties.Settings.Default.DashboardSite}/{"LetterMainViewer.aspx"}";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
    }

    protected void btnLastMonthSales_Click(object sender, EventArgs e)
    {
      DataSet ds;
      XtraReport rpt;
      string url;
      var today = DateTime.Today;
      //get the previous month, start date and end date
      var currentMonth = new DateTime(today.Year, today.Month, 1);
      var startDateBooked = currentMonth.AddMonths(-1);
      var endDateBooked = currentMonth.AddDays(-1);

      dsDashboard data = Session["DataSet"] as dsDashboard;
      dashboard = new DashboardData(data);
      ds = dashboard.GetNewResvAndInquiry(startDateBooked, endDateBooked, userName);

      rpt = new NewResvAndInquiry();
      rpt.Parameters["prmStartDateBooked"].Value = startDateBooked;
      rpt.Parameters["prmEndDateBooked"].Value = endDateBooked;

      rpt.DataSource = ds;
      rpt.DataMember = "Table";
      rpt.CreateDocument();
      Session.Add("ReportObject", rpt);
      //Response.Redirect("~/LetterMainViewer.aspx");
      url = $"{Properties.Settings.Default.DashboardSite}/{"LetterMainViewer.aspx"}";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);

    }

    protected void btnPrintReport_Click(object sender, EventArgs e)
    {
      string currentMonth = "1";
      string month;
      month = ((LinkButton)sender).CommandArgument;
      if (month == currentMonth)
      {
        CurrentMonthSales();
      }
      else
      {
        LastMonthSales();
      }
    }

    protected void CurrentMonthSales()
    {
      DataSet ds;
      XtraReport rpt;
      string url;
      DateTime currentDate = DateTime.Today;
      DateTime firstDayOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);

      dsDashboard data = Session["DataSet"] as dsDashboard;
      dashboard = new DashboardData(data);
      ds = dashboard.GetNewResvAndInquiry(firstDayOfMonth, currentDate, userName);

      rpt = new NewResvAndInquiry();
      rpt.Parameters["prmStartDateBooked"].Value = firstDayOfMonth;
      rpt.Parameters["prmEndDateBooked"].Value = currentDate;

      rpt.DataSource = ds;
      rpt.DataMember = "Table";
      rpt.CreateDocument();
      Session.Add("ReportObject", rpt);
      //Response.Redirect("~/LetterMainViewer.aspx");
      url = $"{Properties.Settings.Default.DashboardSite}/{"LetterMainViewer.aspx"}";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
    }

    protected void LastMonthSales()
    {
      DataSet ds;
      XtraReport rpt;
      string url;
      var today = DateTime.Today;
      //get the previous month, start date and end date
      var currentMonth = new DateTime(today.Year, today.Month, 1);
      var startDateBooked = currentMonth.AddMonths(-1);
      var endDateBooked = currentMonth.AddDays(-1);

      dsDashboard data = Session["DataSet"] as dsDashboard;
      dashboard = new DashboardData(data);
      ds = dashboard.GetNewResvAndInquiry(startDateBooked, endDateBooked, userName);

      rpt = new NewResvAndInquiry();
      rpt.Parameters["prmStartDateBooked"].Value = startDateBooked;
      rpt.Parameters["prmEndDateBooked"].Value = endDateBooked;

      rpt.DataSource = ds;
      rpt.DataMember = "Table";
      rpt.CreateDocument();
      Session.Add("ReportObject", rpt);
      //Response.Redirect("~/LetterMainViewer.aspx");
      url = $"{Properties.Settings.Default.DashboardSite}/{"LetterMainViewer.aspx"}";
      ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('" + url + "','_blank');", true);
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {
      // clear cookies and back to login.

      Session.Clear();
      Session.Abandon();
      //remove cookie
      Request.Cookies["udv"]?.Expires.Equals(DateTime.Now.AddYears(-20));

      Response.Redirect("/login.aspx");
    }
  }
}