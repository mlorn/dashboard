﻿#region Copyright (c) West Indies Management Company
/*
{*******************************************************************}
{       Copyright (c) West Indies Management Company                }
{       ALL RIGHTS RESERVED                                         }
{                                                                   }
{  Author: ML                                                       }
{  Date: 02/22/2010                                                 }
{  Discription:                                                     }
{  Send e-mail by using SMTP Client.  It reads an app configuration }
{  file to get basic info, such as SMTP Server (Host).  This class  }
{  can sent multiple e-mail addresses to To, CC, BCC by separating  }
{  e-mail addresses by using semicolon.                             }
{                                                                   }
{*******************************************************************}
*/
#endregion

#region using declaration
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
#endregion

namespace DataAccessLayer
{
	public class SmtpEmail
	{
		private MailMessage message;
		private SmtpClient client;
		private NetworkCredential userCredential;

		/// <summary>
		/// Initializes a new instance of the <see cref="SmtpEmail"/> class.
		/// Default contructor for the class.  Initialize all neccessary field by
		/// reading app configuration file.
		/// </summary>
		public SmtpEmail()
		{
			Properties.Settings appSettings = new Properties.Settings();
			message = new MailMessage();
			client = new SmtpClient();
			userCredential = new NetworkCredential();
			this.Host = appSettings.SmtpServer;
			this.Port = 25;
			this.IsSsl = appSettings.IsSsl;
			this.UserName = string.Empty;
			this.Password = string.Empty;

			this.FromEmailAddres = string.Empty;
			this.ToEmailAddress = appSettings.PostmasterAddress;

			this.CcEmailAddress = string.Empty;
			this.BccEmailAddress = string.Empty;
			this.ReplyEmailAddress = appSettings.ReplyAddress;
			this.ReplyFriendlyName = appSettings.ReplyFriendlyName1;
			this.IsBodyHtml = false;
			this.Subject = appSettings.Subject1;
			this.AttachedFiles = null;
		}

		#region smtp email property
		public string Name
		{
			get;
			set;
		}

		public string FromEmailAddres
		{
			get;
			set;
		}
		public string ToEmailAddress
		{
			get;
			set;
		}

		public string CcEmailAddress
		{
			get;
			set;
		}

		public string BccEmailAddress
		{
			get;
			set;
		}

		public string ReplyEmailAddress
		{
			get;
			set;
		}

		public string ReplyFriendlyName
		{
			get;
			set;
		}

		public string Subject
		{
			get;
			set;
		}

		//use indexer instead of array because array is stored as reference type
		public ArrayList AttachedFiles
		{
			get;
			set;
		}

		public bool IsBodyHtml
		{
			get;
			set;
		}

		public string MessageBody
		{
			get;
			set;
		}

		public string Host
		{
			get;
			set;
		}

		public bool IsSsl
		{
			get;
			set;
		}

		public int Port
		{
			get;
			set;
		}

		public string UserName
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}
		#endregion

		/// <summary>
		/// Initials the SMTP client.
		/// </summary>
		private void initialSmtpClient()
		{
			//client.Credentials = new NetworkCredential(this.UserName, this.Password);
			client.Host = this.Host;
			//client.Port = this.Port;
			client.EnableSsl = this.IsSsl;
			client.UseDefaultCredentials = true;
			client.Timeout = 15000;
		}

		/// <summary>
		/// Initials the mail client.
		/// </summary>
		private void initialMailClient()
		{
			message.From = new MailAddress(this.FromEmailAddres);
			if (this.ToEmailAddress.Length > 0)
			{
				foreach (var emailAddress in this.ToEmailAddress.Split(';'))
				{
					message.To.Add(new MailAddress(emailAddress));
				}
			}
			if (this.CcEmailAddress.Length > 0)
			{
				foreach (var emailAddress in this.CcEmailAddress.Split(';'))
				{
					message.CC.Add(new MailAddress(emailAddress));
				}
			}
			if (this.BccEmailAddress.Length > 0)
			{
				foreach (var emailAddress in this.BccEmailAddress.Split(';'))
				{
					message.Bcc.Add(new MailAddress(emailAddress));
				}
			}
			if (this.ReplyEmailAddress.Length > 0)
			{
				message.ReplyTo = new MailAddress(this.ReplyEmailAddress);
			}

			// The attachments array should point to a file location     
			// where the attachment resides - add the attachments to the
			// message
			if (this.AttachedFiles != null)
			{
				foreach (string attach in this.AttachedFiles)
				{
					Attachment attached = new Attachment(attach, MediaTypeNames.Application.Octet);
					message.Attachments.Add(attached);
				}
			}

			message.Subject = this.Subject;
			message.IsBodyHtml = this.IsBodyHtml;
			message.Body = this.MessageBody;
		}

		/// <summary>
		/// Sends the email.  Return exception if there is a problem;
		/// otherwise, return emty string.
		/// </summary>
		/// <returns></returns>
		public string SendEmail()
		{
			string result = string.Empty;
			try
			{
				initialSmtpClient();
				initialMailClient();
				client.Send(message);
			}
			catch (FormatException ex)
			{
				result = string.Format("Format Exception: {0}", ex.Message);
			}
			catch (SmtpException ex)
			{
				result = string.Format("SMTP Exception: {0}", ex.Message);
			}
			catch (Exception ex)
			{
				result = string.Format("General Exception: {0}", ex.Message);
			}

			return result;
		}
	}
}
