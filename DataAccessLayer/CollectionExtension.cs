﻿//========================================================================
// Copyright © 2009 WIMCO.  All rights reserved.
//
//========================================================================
//      Author:  mlorn
//        Date:  4/24/2009
// Description:
// 
//========================================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace WimcoHelper
{

  /// <summary>
  /// Convert any object that implements the IEnumerable interface. 
  /// It creates a DataSet with one table that has the name you pass in.
  /// </summary>
  /// <example>
  /// <code>
  ///   DataSet ds;
  ///   List<spLetterFinalPaymentResult> finalPaymentData = new List<spLetterFinalPaymentResult>();
  ///   using (LetterServiceClient letterSvc = new LetterServiceClient())
  ///   {
  ///     finalPaymentData = letterSvc.GetFinalPaymentData(string.Empty, "12/15/2008", "12/15/2008").ToList();
  ///   }
  ///   ds = CollectionExtension.ToDataSet<spLetterFinalPaymentResult>(finalPaymentData, "FinalPayment");
  /// </code>
  /// </example>
  public static class CollectionExtension
  {

    /// <summary>
    /// Covert a collect to a DataSet.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection">The collection.</param>
    /// <param name="dataTableName">Name of the data table.</param>
    /// <returns></returns>
    public static DataSet ToDataSet<T>(this IEnumerable<T> collection, string dataTableName)
    {
      if (collection == null)
      {
        throw new ArgumentNullException("collection is required.");
      }

      if (string.IsNullOrEmpty(dataTableName))
      {
        throw new ArgumentNullException("DataTable Name is required.");
      }

      DataSet data = new DataSet("NewDataSet");
      data.Tables.Add(FillDataTable(dataTableName, collection));
      return data;
    }

    /// <summary>
    /// Convert a Type Collection to a Data Table.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection">The collection.</param>
    /// <param name="dataTableName">Name of the Data Table.</param>
    /// <returns></returns>
    public static DataTable ToDataTable<T>(this IEnumerable<T> collection, string dataTableName)
    {
      if (collection == null)
      {
        throw new ArgumentNullException("collection is required.");
      }

      if (string.IsNullOrEmpty(dataTableName))
      {
        throw new ArgumentNullException("DataTable Name is required.");
      }

      return FillDataTable(dataTableName, collection);
    }

    /// <summary>
    /// Fills the data table.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="tableName">Name of the table.</param>
    /// <param name="collection">The collection.</param>
    /// <returns></returns>
    public static DataTable FillDataTable<T>(string tableName, IEnumerable<T> collection)
    {
      PropertyInfo[] properties = typeof(T).GetProperties();

      DataTable dt = CreateDataTable<T>(tableName,
      collection, properties);

      IEnumerator<T> enumerator = collection.GetEnumerator();
      while (enumerator.MoveNext())
      {
        dt.Rows.Add(FillDataRow<T>(dt.NewRow(),
       enumerator.Current, properties));
      }

      return dt;
    }

    /// <summary>
    /// Fills the data row.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="dataRow">The data row.</param>
    /// <param name="item">The item.</param>
    /// <param name="properties">The properties.</param>
    /// <returns></returns>
    private static DataRow FillDataRow<T>(DataRow dataRow, T item, PropertyInfo[] properties)
    {
      foreach (PropertyInfo property in properties)
      {
        if (property.GetValue(item, null) == null)
        {
          dataRow[property.Name.ToString()] = DBNull.Value;
        }
        else
        {
          dataRow[property.Name.ToString()] = property.GetValue(item, null);
        }
      }

      return dataRow;
    }

    /// <summary>
    /// Creates the data table.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="tableName">Name of the table.</param>
    /// <param name="collection">The collection.</param>
    /// <param name="properties">The properties.</param>
    /// <returns></returns>
    private static DataTable CreateDataTable<T>(string tableName, IEnumerable<T> collection, PropertyInfo[] properties)
    {
      DataTable dt = new DataTable(tableName);

      foreach (PropertyInfo property in properties)
      {
        if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
        {
          NullableConverter converter = new NullableConverter(property.PropertyType);
          dt.Columns.Add(property.Name, converter.UnderlyingType);
        }
        else
          dt.Columns.Add(property.Name, property.PropertyType);
      }

      return dt;
    }
  }
}
