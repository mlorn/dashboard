﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace DataAccessLayer
{
  public struct UserData
  {
    public bool Loggedin;
    public string username;
    public string password;
    public int UserID;
    public int OrganizationID;
    public bool NomadAdmin;
    public bool Manager;
    public bool SalesAgent;
    public string EmailAddress;
    public string FullName;
    public int[] Destinations;
    public string[] ExtendedOptions;
    public string IPAddress;

    public static string SerializeAnObject(object AnObject)
    {
      XmlSerializer Xml_Serializer = new XmlSerializer(AnObject.GetType());
      StringWriter Writer = new StringWriter();
      Xml_Serializer.Serialize(Writer, AnObject);
      return Writer.ToString();
    }

    public static string EncodeTo64(string toEncode)
    {
      byte[] toEncodeAsBytes = System.Text.Encoding.Unicode.GetBytes(toEncode);
      string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
      return returnValue;
    }
  }



}
