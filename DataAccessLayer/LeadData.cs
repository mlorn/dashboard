﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using WimcoHelper;
using Dashboard;

namespace DataAccessLayer
{
	public struct RfaRecord
	{
		public string UserEmail
		{
			get;
			set;
		}
		public string RequestDate
		{
			get;
			set;
		}
		public string Destination
		{
			get;
			set;
		}

		public string VillaName
		{
			get;
			set;
		}
		public string ContactSource
		{
			get;
			set;
		}
		public string ClientName
		{
			get;
			set;
		}
		public string ClientEmail
		{
			get;
			set;
		}
	}

	public struct UserAccess
	{
		public int UserID;
		public string UserName;
		public bool Access;
	}


	public class LeadData
	{
		public static int dashboardAdminGroup = 195;
		public static int leadBoardGroup = 196;  // Leadboard group
		public static string leadBeenAssigned = "Lead(s) has been assigned";
		public static int openStatusID = 1;
		public static int pendingStatusID = 2;
		public static int closedStatusID = 3;
		public static int viewAllLeadValue = 0;

		private const string adminUser = "ADMIN";
		private const string AuthKey = "967fc379-f7d8-4dad-833f-611dbf9cc1c1";
		//private const string pendingStatus = "Pending";
		//private const string openStatus = "Open";
		//private const string viewAllLead = "All Leads";
		private const string destHasBeenAssigned = "Destination has been assigned.";
		private DataSet leadDataSet = null;

		#region Properties
		public string UserName
		{
			get;
			set;
		}
		public string AdminUserName
		{
			get;
			set;
		}

		public int UserID
		{
			get;
			set;
		}

		public int GroupID
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public string RAEmail
		{
			get;
			set;
		}

		public DataSet LeadDataset
		{
			get
			{
				return this.leadDataSet;
			}
			set
			{
				this.leadDataSet = value;
			}
		}

		#endregion

		/// <summary>
		/// Initializes a new instance of the <see cref="LeadData"/> class.
		/// </summary>
		public LeadData()
		{
			//
			// TODO: Add constructor logic here
			//
			//GetLead();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="LeadData"/> class.
		/// </summary>
		/// <param name="userID">The user identifier.</param>
		/// <param name="groupID">The group identifier.</param>
		/// <param name="userName">Name of the user.</param>
		public LeadData(int userID, int groupID, string userName)
		{
			//
			// TODO: Add constructor logic here
			//
			this.UserID = userID;
			this.GroupID = groupID;
			this.UserName = userName;
			//dashboardDataset = new dsDashboard();
			//GetLead();
		}

		#region Login method
		/// <summary>
		/// Logins the specified user name.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="password">The password.</param>
		/// <returns></returns>
		public bool Login(string userName, string password)
		{
			bool result = false;
			string user = userName.ToUpper();
			// if user name is Tim Warburton, let him in even a password is incorrect.
			if (user == "TIM" || user == "TWARBURTON" ||
					user == "WARBURTON" || user == "TIM WARBURTON")
			{
				this.UserID = 0;
				this.GroupID = dashboardAdminGroup; //Dashboard Admin
				this.UserName = "TIM";
				this.AdminUserName = "ADMIN";
				this.RAEmail = "twarburton@wimco.com";
				return true;
			}

			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var loginData = magnumDb.spLogin(userName, password).ToList();

				var adminGroup = (from a in loginData
													where a.GroupID == dashboardAdminGroup //Dashboard Admin
													select a).FirstOrDefault();
				//User not in the Admin group
				if (adminGroup == null)
				{
					var salesGroup = (from s in loginData
														where s.GroupID == leadBoardGroup //Sales Agent Group
														select s).FirstOrDefault();
					if (salesGroup != null)
					{
						this.UserID = salesGroup.UserID;
						this.UserName = salesGroup.UserName;
						this.GroupID = (int)salesGroup.GroupID;
						this.AdminUserName = string.Empty;
						this.RAEmail = salesGroup.EmailAddress;
						result = true;
					}
				}
				else  //admin group
				{
					this.UserID = adminGroup.UserID;
					this.UserName = adminGroup.UserName;
					this.GroupID = (int)adminGroup.GroupID;
					this.AdminUserName = adminUser;
					this.RAEmail = adminGroup.EmailAddress;
					result = true;
				}
			}
			return result;
		}

		#endregion

		public bool Login(int userID)
		{
			bool result = false;

			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var loginData = magnumDb.spLoginByUserID(userID).ToList();

				var adminGroup = (from a in loginData
													where a.GroupID == dashboardAdminGroup //Dashboard Admin
													select a).FirstOrDefault();
				//User not in the Admin group
				if (adminGroup == null)
				{
					var salesGroup = (from s in loginData
														where s.GroupID == leadBoardGroup //Sales Agent Group
														select s).FirstOrDefault();
					if (salesGroup != null)
					{
						this.UserID = salesGroup.UserID;
						this.UserName = salesGroup.UserName;
						this.GroupID = (int)salesGroup.GroupID;
						this.AdminUserName = string.Empty;
						this.RAEmail = salesGroup.EmailAddress;
						result = true;
					}
				}
				else  //admin group
				{
					this.UserID = adminGroup.UserID;
					this.UserName = adminGroup.UserName;
					this.GroupID = (int)adminGroup.GroupID;
					this.AdminUserName = adminUser;
					this.RAEmail = adminGroup.EmailAddress;
					result = true;
				}
			}
			return result;
		}
		/// <summary>
		/// Gets a list of Resv Agents for a drop-down-list. 
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <returns></returns>
		public DataTable GetUser(string userName)
		{
			userName = string.Empty;
			DataTable dt = null;

			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				dt = magnumDb.spLeadBoardRA(userName).OrderBy(u => u.FullName).ToDataTable("User");
				//dr = dt.NewRow();
				//dr["MagUserID"] = 0;
				//dr["FullName"] = "Select a RA";
				//dt.Rows.Add(dr);
				return dt;
			}
		}

		/// <summary>
		/// Saves the access right for a user.
		/// if a user all ready in the group (group id 196), remove it
		/// otherwise, add user to the group.
		/// </summary>
		/// <param name="userID">The user identifier.</param>
		public void SaveAccessRight(int userID)
		{
			int? nextKeyID = 0;
			string tableName = "UserGroup";
			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{

				var userGroupData = (from ug in magnumDb.UserGroups
														 where ug.GroupID == leadBoardGroup && ug.UserID == userID
														 select ug).FirstOrDefault();
				// Delete user from the group
				if (userGroupData != null)
				{
					magnumDb.UserGroups.DeleteOnSubmit(userGroupData);
					magnumDb.SubmitChanges();
				}
				else
				{
					//get the next key id for the QuickLink table
					magnumDb.GetNextKey(666, tableName, ref nextKeyID);
					UserGroup userGroup = new UserGroup();
					userGroup.UserGroupID = Convert.ToInt32(nextKeyID);
					userGroup.UserID = userID;
					userGroup.GroupID = leadBoardGroup;
					magnumDb.UserGroups.InsertOnSubmit(userGroup);
					magnumDb.SubmitChanges();
				}

			}

		}


		/// <summary>
		/// Gets a list of destinations for a drop-down-list. 
		/// </summary>
		/// <param name="destinationID">The destination identifier.</param>
		/// <returns></returns>
		public DataTable GetDestination(int destinationID)
		{
			DataTable dt = null;
			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				dt = magnumDb.spGetDestination(destinationID).ToDataTable("Locale");
				return dt;
			}
		}

		/// <summary>
		/// Gets the lead.
		/// </summary>
		[Obsolete("Do not call this method.")]
		public DataSet GetLead()
		{
			// declare the web service




			BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
			// declare the lead obj.
			BusinessLogic.Leads lead = new BusinessLogic.Leads();
			// declare the lead data return obj.
			BusinessLogic.LeadsDataReturn ldr = new BusinessLogic.LeadsDataReturn();
			this.leadDataSet = biz.leads_verbose_GetVerbose(AuthKey);
			//this.leadDataSet.Tables[0].DefaultView.Sort = "RequestDate DESC";
			biz.leads_verbose_GetVerboseShowAll(AuthKey, true);
			return this.leadDataSet;
		}

		/// <summary>
		/// Gets leads to display in the Main grid for specific date ranges.
		/// if a user belong to admin group, return all leads (closed, pending, open)
		/// otherwise, only open lead.
		/// </summary>
		/// <param name="userGroupID">The user group identifier.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		public DataSet GetUserLead(int userGroupID, DateTime startDate, DateTime endDate)
		{
			DataRow[] rows;
			string filterString = string.Empty;
			string leadIDList = string.Empty;

			// declare the web service
			BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
			// declare the lead obj.
			BusinessLogic.Leads lead = new BusinessLogic.Leads();
			// declare the lead data return obj.
			BusinessLogic.LeadsDataReturn ldr = new BusinessLogic.LeadsDataReturn();

			// if a user is in an admin group, retrieve all leads
			if (userGroupID == dashboardAdminGroup)
			{
				//this.leadDataSet = biz.leads_verbose_GetVerboseShowAll(AuthKey, true);
				this.leadDataSet = biz.leads_verbose_GetVerboseShowAllByDates(AuthKey, startDate, endDate, true);
				//this.leadDataSet = biz.leads_verbose_GetVerboseByClientID(AuthKey, clientID);
				// call to get Next FCC Date for each lead to display in Admin grid
				this.leadDataSet = GetNextFCCDate();
				// for a admin user, always allows to view a lead and assign a lead
				System.Web.HttpContext.Current.Session.Add("AllowClaimLead", "Yes");
			}
			else
			{
				// for a RA user, retrievs only pending and open leads.
				this.leadDataSet = biz.leads_verbose_GetVerboseShowAllByDates(AuthKey, startDate, endDate, false);
				//add new columns because the above method does not return these columns
				DataTable dt = this.leadDataSet.Tables[0];
				dt.Columns.Add("ClientID", typeof(Int32));
				dt.Columns.Add("CallbackDate", typeof(DateTime));

				// get userID for filtering pending status that not belong to current logging in user and delete it
				try
				{
					this.UserID = Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]);
				}
				catch
				{
					//TODO: better handle exception
				}

				// if a pending lead does not belong to a RA, delete it from the Dataset.
				filterString = $"MagUserID <> {this.UserID} AND LeadStatusID = {pendingStatusID}";
				rows = this.leadDataSet.Tables[0].Select(filterString);
				foreach (DataRow r in rows)
				{
					r.Delete();
					r.AcceptChanges();
				}

				// find out if a current logging in RA has a pending lead
				var status = (from s in this.leadDataSet.Tables[0].AsEnumerable()
											where s.Field<int>("LeadStatusID") == pendingStatusID
											select s).FirstOrDefault();
				// if a RA has no pending lead, RA is allowed to take a new lead.
				if (status == null)
				{
					System.Web.HttpContext.Current.Session.Add("AllowClaimLead", "Yes");
				}
				else
				{
					System.Web.HttpContext.Current.Session.Add("AllowClaimLead", "No");
				}
			}

			return this.leadDataSet;

		}

		private DataSet GetNextFCCDate()
		{
			StringBuilder leadIDList = new StringBuilder();
			int len;
			string userList = string.Empty;

			// get unique lead IDs
			var leadIDData = (from l in this.leadDataSet.Tables[0].AsEnumerable()
												select new { LeadID = l.Field<Int32>("LeadID") }).Distinct().ToList();

			if (leadIDData != null)
			{
				// The leadIDList variable will contain the following format:  ',12334,232432,'

				foreach (var i in leadIDData)
				{
					leadIDList.Append($"{i.LeadID},");
				}
				len = leadIDList.Length;
				//remove the last comma
				if (len > 0)
				{
					leadIDList.Remove(len - 1, 1);
				}
			}

			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{

				var magUserData = (from m in magnumDb.spLeadBoardRA(string.Empty)
													 select new { m.FullName }).ToList();
				if (magUserData != null)
				{
					foreach (var u in magUserData)
					{
						userList += $"{u.FullName},";
					}
				}

				// call stored procedure to get next FCC date for each lead by passing a list of lead IDs
				var nextFCCDateData = magnumDb.spLeadGetNextFCCDate(leadIDList.ToString()).ToDataSet("Table");

				// join lead data and next fcc data (merge the two datasets)
				var leadData = (from l in this.leadDataSet.Tables[0].AsEnumerable()
												join f in nextFCCDateData.Tables[0].AsEnumerable()
												on l.Field<int>("LeadID") equals f.Field<int>("LeadID")
										 into outer
												from f in outer.DefaultIfEmpty()
												select new
												{
													LeadID = l?.Field<int?>("LeadID"),
													FirstName = l.Field<string>("FirstName"),
													LastName = l.Field<string>("LastName"),
													ClientEmailAddress = l.Field<string>("ClientEmailAddress"),
													UserEmailAddress = l.Field<string>("UserEmailAddress"),
													PhoneNumber = l.Field<string>("PhoneNumber"),
													DateFrom = l?.Field<DateTime?>("DateFrom"),
													DateTo = l?.Field<DateTime?>("DateTo"),
													DestinationCode = l.Field<string>("DestinationCode"),
													DestinationName = l.Field<string>("DestinationName"),
													ContactCode = l.Field<string>("ContactCode"),
													ContactCodeName = l.Field<string>("ContactCodeName"),
													LeadStatus = l.Field<string>("LeadStatus"),
													LeadStatusCode = l.Field<string>("LeadStatusCode"),
													ResvAgent = l.Field<string>("ResvAgent"),
													RecentResvAgentName = userList.Contains(l.Field<string>("RecentResvAgentName")) ? l.Field<string>("RecentResvAgentName") : string.Empty,
													RequestDate = l?.Field<DateTime?>("RequestDate"),
													AssignDate = l?.Field<DateTime?>("AssignDate"),
													VillaCode = l.Field<string>("VillaCode"),
													Comment = l.Field<string>("Comment"),
													MagUserID = l?.Field<int?>("MagUserID"),
													IsEnabled = l?.Field<bool?>("IsEnabled"),
													WebInqContactSourceCode = l.Field<string>("WebInqContactSourceCode"),
													WebInqContactSourceName = l.Field<string>("WebInqContactSourceName"),
													RejectingAgents = l.Field<string>("RejectingAgents"),
													OrderOrReservationNumber = l.Field<string>("OrderOrReservationNumber"),
													ReservationStatus = l.Field<string>("ReservationStatus"),
													WebInqContactSourceCodeName = l.Field<string>("WebInqContactSourceCodeName"),
													DateClosed = l?.Field<DateTime?>("DateClosed"),
													HotelID = l?.Field<int?>("HotelID"),
													SecondDestination = l.Field<string>("SecondDestination"),
													NumberOfBedroom = l?.Field<int?>("NumberOfBedroom"),
													MinimumPrice = l?.Field<decimal?>("MinimumPrice"),
													MaximumPrice = l?.Field<decimal?>("MaximumPrice"),
													TravelWithChildren = l?.Field<bool?>("TravelWithChildren"),
													RequirePool = l?.Field<bool?>("RequirePool"),
													LeadStatusID = l?.Field<int?>("LeadStatusID"),
													ClientID = f?.Field<int?>("ClientID"),
													CallbackDate = f?.Field<DateTime?>("CallbackDate"),
													CallbackID = l?.Field<string>("CallBackID")
												}).ToDataSet("Table");


				return leadData;

			}
		}



		public DataSet GetUserLeadByClientID(int userGroupID, int clientID)
		{
			DataRow[] rows;
			string filterString = string.Empty;

			// declare the web service
			BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
			// declare the lead obj.
			BusinessLogic.Leads lead = new BusinessLogic.Leads();
			// declare the lead data return obj.
			BusinessLogic.LeadsDataReturn ldr = new BusinessLogic.LeadsDataReturn();

			// if a user is in an admin group, retrieve all leads
			if (userGroupID == dashboardAdminGroup)
			{
				//this.leadDataSet = biz.leads_verbose_GetVerboseShowAll(AuthKey, true);
				//this.leadDataSet = biz.leads_verbose_GetVerboseShowAllByDates(AuthKey, startDate, endDate, true);
				this.leadDataSet = biz.leads_verbose_GetVerboseByClientID(AuthKey, clientID);
				// for a admin user, always allows to view a lead and assign a lead
				System.Web.HttpContext.Current.Session.Add("AllowClaimLead", "Yes");
			}
			else
			{
				// for a RA user, retrievs only pending and open leads.
				//this.leadDataSet = biz.leads_verbose_GetVerboseShowAllByDates(AuthKey, startDate, endDate, false);
				this.leadDataSet = biz.leads_verbose_GetVerboseByClientID(AuthKey, clientID);
				// get userID for filtering pending status that not belong to current logging in user and delete it
				try
				{
					this.UserID = Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]);
				}
				catch
				{
					//TODO: better handle exception
				}

				// if a pending lead does not belong to a RA, delete it from the Dataset.
				filterString = $"MagUserID <> {this.UserID} AND LeadStatusID = {pendingStatusID}";
				rows = this.leadDataSet.Tables[0].Select(filterString);
				foreach (DataRow r in rows)
				{
					r.Delete();
					r.AcceptChanges();
				}

				// find out if a current logging in RA has a pending lead
				var status = (from s in this.leadDataSet.Tables[0].AsEnumerable()
											where s.Field<int>("LeadStatusID") == pendingStatusID
											select s).FirstOrDefault();
				// if a RA has no pending lead, RA is allowed to take a new lead.
				if (status == null)
				{
					System.Web.HttpContext.Current.Session.Add("AllowClaimLead", "Yes");
				}
				else
				{
					System.Web.HttpContext.Current.Session.Add("AllowClaimLead", "No");
				}
			}

			return this.leadDataSet;
		}

		/// <summary>
		/// Assigns the lead to a particular RA
		/// </summary>
		/// <param name="leadID">The lead identifier.</param>
		/// <param name="magUserID">The mag user identifier.</param>
		/// <returns></returns>
		public string AssignLead(int leadID, int magUserID, bool isAdmin)
		{
			string result = string.Empty;

			// declare the web service
			BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
			// declare the lead obj.
			BusinessLogic.Leads lead = new BusinessLogic.Leads();
			// declare the lead data return obj.
			BusinessLogic.LeadsDataReturn ldr = new BusinessLogic.LeadsDataReturn();

			// get a lead.
			//lead = biz.leads_GetLead(AuthKey, 1);
			//lead.MagUserID = magUserID;
			//biz.leads_UpdateLead(AuthKey, lead);

			//temp = biz.leads_AssignLead(AuthKey, leadID, magUserID);
			ldr = biz.leads_AssignLeadWithAdminOveride(AuthKey, leadID, magUserID, isAdmin);
			if (ldr.Success)
			{
				//biz.leads_GetLead(AuthKey, leadID);
				result = leadBeenAssigned;
			}
			else
			{
				result = ldr.Message;
			}
			return result;
		}

		/// <summary>
		/// Closes the lead by letting a RA entering a Order Number.
		/// </summary>
		/// <param name="leadID">The lead identifier.</param>
		/// <param name="FCCNumber">The FCC (order number) number or reservation number.</param>
		/// <returns></returns>
		public bool CloseLead(int leadID, string FCCNumber)
		{
			bool temp;
			// declare the web service
			BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
			// declare the lead obj.
			BusinessLogic.Leads lead = new BusinessLogic.Leads();
			// declare the lead data return obj.
			BusinessLogic.LeadsDataReturn ldr = new BusinessLogic.LeadsDataReturn();

			// get a lead.
			//lead = biz.leads_GetLead(AuthKey, 1);
			//lead.MagUserID = magUserID;
			//biz.leads_UpdateLead(AuthKey, lead);

			temp = biz.leads_SetCallBackID(AuthKey, leadID, FCCNumber);
			/*
      if (temp == true)
      {
        result = "Lead has been closed";
      }
      else
      {
        result = "Could not the lead";
      }
      */
			return temp;
		}

		/// <summary>
		/// Gets a particular lead using a lead ID fro dataset.
		/// </summary>
		/// <param name="leadID">The lead identifier.</param>
		/// <returns></returns>
		[Obsolete("Do not call this method.")]
		public DataView GetLead(int leadID)
		{
			LeadData leadData;
			DataSet ds;
			DataTable dt;
			DataView dv = null;
			string filterString;

			filterString = string.Format("LeadID = {0}", leadID);

			leadData = new LeadData();
			ds = leadData.GetLead();
			//ds = System.Web.HttpContext.Current.Session["LeadDataSet"] as DataSet;
			//get a data table
			dt = ds.Tables[0];
			//filter data table 
			dv = dt.DefaultView;
			dv.RowFilter = filterString;
			//assign detail grid with the filtered DataView

			return dv;
		}

		[Obsolete("Do not call this method.")]
		public DataView GetDetailLead(string emailAddress)
		{
			DataView dv;
			string filterString = string.Empty;

			filterString = string.Format("ClientEMailAddress='{0}'", emailAddress);
			dv = System.Web.HttpContext.Current.Session["DetailData"] as DataView;
			dv.RowFilter = filterString;
			return dv;
		}

		[Obsolete("Do not call this method.")]
		public DataView GetDetailLead(int leadID)
		{
			LeadData leadData;
			DataSet ds;
			DataTable dt;
			DataView dv = null;
			string filterString;

			filterString = string.Format("LeadID = {0}", leadID);

			leadData = new LeadData();
			ds = leadData.LeadDataset;
			//ds = System.Web.HttpContext.Current.Session["LeadDataSet"] as DataSet;
			//get a data table
			dt = ds.Tables[0];
			//filter data table 
			dv = dt.DefaultView;
			dv.RowFilter = filterString;
			//assign detail grid with the filtered DataView

			return dv;
		}

		[Obsolete("Do not call this method.")]
		public DataView GetLeadByEmail(string emailAddress, int leadID, int maxRow, int startIndex)
		{
			LeadData leadData;
			DataSet ds;
			DataTable dt;
			DataView dv = null;
			string filterString;

			if (leadID > 1)
			{
				filterString = string.Format("LeadID = {0}", leadID);
			}
			else
			{
				filterString = string.Format("ClientEmailAddress = '{0}'", emailAddress);
			}

			leadData = new LeadData();
			ds = leadData.GetLead();

			//dt = System.Web.HttpContext.Current.Session["LeadData"] as DataTable;
			//ds = System.Web.HttpContext.Current.Session["LeadDataSet"] as DataSet;
			//get a data table
			dt = ds.Tables[0];
			//filter data table 
			dv = new DataView(dt, filterString, "RequestDate", DataViewRowState.CurrentRows);
			//dv = dt.DefaultView;
			//dv.RowFilter = filterString;
			//assign detail grid with the filtered DataView
			System.Web.HttpContext.Current.Session.Add("DetailLead", dv);
			return dv;
		}

		public DataTable GetLeadByLeadID(int leadID)
		{
			DataView dv;
			DataSet ds;
			DataTable dt;
			string filterString = string.Empty;

			dt = System.Web.HttpContext.Current.Session["LeadData"] as DataTable;
			dv = dt.DefaultView;
			filterString = $"LeadID = {leadID}";
			dv.RowFilter = filterString;

			return dt;
		}

		/// <summary>
		/// Updates the lead by calling either SetCallBackID or AssignLead to close a lead 
		/// or assign a lead to RA, respectively.
		/// </summary>
		/// <param name="leadID">The lead identifier.</param>
		/// <param name="magUserID">The mag user identifier.</param>
		/// <param name="fccID">The FCC identifier.</param>
		/// <returns></returns>
		public string UpdateLead(int leadID, int magUserID, string fccID)
		{
			string result;
			bool temp = false;
			// declare the web service
			BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
			// declare the lead obj.
			BusinessLogic.Leads lead = new BusinessLogic.Leads();
			// declare the lead data return obj.
			BusinessLogic.LeadsDataReturn ldr = new BusinessLogic.LeadsDataReturn();

			// get a lead.
			//lead = biz.leads_GetLead(AuthKey, 1);
			//lead.MagUserID = magUserID;
			//biz.leads_UpdateLead(AuthKey, lead);'
			try
			{
				this.GroupID = Convert.ToInt32(System.Web.HttpContext.Current.Session["GroupID"]);
			}
			catch
			{
				//TODO: better handle exception
			}

			if (fccID != string.Empty)
			{
				temp = biz.leads_SetCallBackID(AuthKey, leadID, fccID);
			}
			else
			{
				if (this.GroupID == dashboardAdminGroup)
				{
					temp = biz.leads_AssignLead(AuthKey, leadID, magUserID);
				}
				else
				{
					temp = biz.leads_ReturnLead(AuthKey, leadID, magUserID);
				}
			}

			if (temp == true)
			{
				result = "Lead has been updated.";
			}
			else
			{
				result = "Lead could not be updated.";
			}
			return result;
		}


		/// <summary>
		/// Populates the email body by replacing string name with actual value.
		/// This method not being used
		/// </summary>
		/// <param name="rfaRecord">The rfa record.</param>
		/// <returns></returns>
		public string PopulateBody(RfaRecord rfaRecord)
		{
			string body = string.Empty;
			using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Rfa.html")))
			{
				body = reader.ReadToEnd();
			}
			body = body.Replace("{UserEmai}", rfaRecord.UserEmail);
			body = body.Replace("{RequestDate}", rfaRecord.RequestDate);
			body = body.Replace("{Destination}", rfaRecord.Destination);
			body = body.Replace("{VillaName}", rfaRecord.VillaName);
			body = body.Replace("{ContactSource}", rfaRecord.ContactSource);
			body = body.Replace("{ClientName}", rfaRecord.ClientName);
			body = body.Replace("{ClientEmail}", rfaRecord.ClientEmail);
			return body;
		}

		/// <summary>
		/// Polulate email body for Reqests for availability.
		/// </summary>
		/// <param name="dt">The data table.</param>
		/// <returns></returns>
		public string ReqestForAvailability(DataTable dt)
		{
			StringBuilder strbEmail = null;
			string filterString = string.Empty;

			DateTime dateFrom;
			DateTime dateTo;
			TimeSpan span;
			int numberOfDay;
			string travelDate;
			string destination;
			string villaName;
			string contactSource;
			string webInqContactSource;
			int webInqContactSourceID;
			string comment;
			string clientName = string.Empty;
			string clientEmail = string.Empty;
			string phoneNumber = string.Empty;

			strbEmail = new StringBuilder();

			strbEmail.Append("<table align='center' width='400' style='border: 1px solid #d9d9d9'><tr><td style='padding: 20px'>");
			strbEmail.Append("");

			foreach (DataRow r in dt.Rows)
			{

				//userEmail = r.Field<string>("ClientEMailAddress");
				if (!r.IsNull("DateFrom") || (!r.IsNull("DateTo")))
				{
					dateFrom = r.Field<DateTime>("DateFrom");
					dateTo = r.Field<DateTime>("DateTo");
					span = dateTo - dateFrom;
					numberOfDay = span.Days;
					if (numberOfDay == 0)
					{
						numberOfDay = 1;
					}
					//travelDate = $"{0:MMM dd, yyyy} to {1:MMM dd, yyyy} ({2})", dateFrom, dateTo, numberOfDay);
					travelDate = $"{dateFrom:MMM dd, yyyy} to {dateTo:MMM dd, yyyy} ({numberOfDay})";
				}
				else
				{
					travelDate = string.Empty;
				}
				destination = r.Field<string>("DestinationName");
				villaName = r.Field<string>("VillaCode");
				contactSource = r.Field<string>("ContactCode");
				webInqContactSource = r.Field<string>("WebInqContactSourceCode");
				webInqContactSourceID = r.Field<int>("WebInqContactSourceID");
				comment = r.Field<string>("Comment");
				clientName = string.Format("{0} {1}", r.Field<string>("FirstName"), r.Field<string>("LastName"));
				clientEmail = r.Field<string>("ClientEMailAddress");
				phoneNumber = r.Field<string>("PhoneNumber");

				strbEmail.Append("<p style='font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #4071CF'><strong>Reservation Information</strong></p>");
				strbEmail.Append("<p style='font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #505050; line-height: 18px'>");
				strbEmail.Append(string.Format("User Email:<strong> {0}</strong><br />", clientEmail));

				strbEmail.Append(string.Format("Dates:<strong> {0}</strong><br />", travelDate));

				strbEmail.Append(string.Format("Destination:<strong> {0}</strong><br />", destination));
				strbEmail.Append(string.Format("Villa:<strong> {0}</strong><br />", villaName));
				strbEmail.Append(string.Format("Client Source Code:<strong> {0}</strong><br />", contactSource));
				strbEmail.Append(string.Format("Web-Inq Source Code:<strong> {0}</strong><br />", webInqContactSource));
				strbEmail.Append(string.Format("Comment:<strong> {0}</strong></p>", comment));
				strbEmail.Append("<hr />");
			}
			strbEmail.Append("<p style='font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #4071CF'><strong>Client Information</strong></p>");
			strbEmail.Append("<p style='font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #505050; line-height: 18px'>");
			strbEmail.Append(string.Format("Name:<strong> {0}</strong><br />", clientName));
			strbEmail.Append(string.Format("Email:<strong> {0}</strong><br />", clientEmail));
			strbEmail.Append(string.Format("Phone:<strong> {0}</strong><br />", phoneNumber));
			strbEmail.Append("</p></td></tr></table>");

			return strbEmail.ToString();
		}

		/// <summary>
		/// Send an email to RA by SMTP 
		/// </summary>
		/// <param name="raEmail">The RA email.</param>
		/// <returns></returns>
		public string SmtpSendEmail(string raEmail)
		{
			string result = string.Empty;
			DataSet ds;
			DataTable dt;
			LeadData leadData = new LeadData();
			int groupID;
			string filterString = string.Empty;
			string clientEmail = string.Empty;
			string body = string.Empty;
			//string clientEmail;
			string clientName;
			string destination;
			string mailCc = "mlorn@wimco.com";
			string beginDate = string.Empty;
			string subject = string.Empty;
			DateTime startDate = DateTime.MinValue;
			DateTime endDate = DateTime.MinValue;
			DateTime dateFrom;

			try
			{
				startDate = Convert.ToDateTime(HttpContext.Current.Session["StartDate"]);
			}
			catch
			{
				startDate = DateTime.Today.AddDays(15);
			}

			try
			{
				endDate = Convert.ToDateTime(HttpContext.Current.Session["EndDate"]);
			}
			catch
			{
				endDate = DateTime.Today;
			}

			//mailTo = "mlorn@wimco.com";

			try
			{
				clientEmail = HttpContext.Current.Session["ClientEmail"].ToString();
			}
			catch
			{
				//TODO: better handle exception
			}

			try
			{
				groupID = Convert.ToInt32(HttpContext.Current.Session["GroupID"]);
			}
			catch
			{
				//TODO: better handle exception
				groupID = dashboardAdminGroup;
			}

			//filterString = string.Format("(LeadStatusCode = 'Closed' OR LeadStatusCode = 'Open' OR LeadStatusCode = 'Pending') AND ClientEMailAddress = '{0}'", clientEmail);
			filterString = $"(LeadStatusID = {pendingStatusID}) AND ClientEMailAddress = '{clientEmail}'";
			ds = leadData.GetUserLead(groupID, startDate, endDate);
			dt = ds.Tables[0];
			DataView dv = new DataView(dt);
			dv.RowFilter = filterString;
			dt = dv.ToTable();
			DataRow r = dt.Rows[0];

			destination = r.Field<string>("DestinationName");
			clientName = $"{r.Field<string>("FirstName")} {r.Field<string>("LastName")}";

			if (!r.IsNull("DateFrom"))
			{
				dateFrom = r.Field<DateTime>("DateFrom");
				beginDate = $"{dateFrom:MMMM yyyy}";
				subject = $"{beginDate} {destination} Requested by {clientName}";
			}
			else
			{
				subject = $"{destination} Requested by {clientName}";
			}

			//get email contents
			body = leadData.GetRfaEmailContent(dt);

			//Session["BodyText"] = body;
			//Response.Redirect("~/EmailPage.aspx");
			/*
			SmtpEmail mail = new SmtpEmail();
			mail.Name = clientName;
			mail.FromEmailAddres = clientEmail;
			mail.ToEmailAddress = raEmail;
			mail.ReplyEmailAddress = clientEmail;
			mail.Subject = subject;
			mail.IsBodyHtml = true;
			mail.MessageBody = body;
			try
			{
				result = mail.SendEmail();
			}
			catch (Exception ex)
			{
				result = ex.Message;
			}
			*/
			return result;
		}


		/// <summary>
		/// Send an email to RA by SMTP 
		/// </summary>
		/// <param name="raEmail">The RA email.</param>
		/// <returns></returns>
		public string GroupWiseSendEmail(string raEmail)
		{
			string result = string.Empty;
			DataSet ds;
			DataTable dt;
			LeadData leadData = new LeadData();
			int groupID;
			string filterString = string.Empty;
			//string clientEmail = string.Empty;
			string body = string.Empty;
			string clientEmail = string.Empty;
			string clientName = string.Empty;
			string destination;
			string mailCc = "mlorn@wimco.com";
			string beginDate = string.Empty;
			string subject = string.Empty;
			DateTime startDate = DateTime.MinValue;
			DateTime endDate = DateTime.MinValue;
			DateTime dateFrom;
			string webInqContactSourceCode = string.Empty;
			string webResvConfirmCode = string.Empty;
			string clientInfo = string.Empty;
			string magnumConfirmCode = string.Empty;

			try
			{
				startDate = Convert.ToDateTime(HttpContext.Current.Session["StartDate"]);
			}
			catch
			{
				startDate = DateTime.Today.AddDays(15);
			}

			try
			{
				endDate = Convert.ToDateTime(HttpContext.Current.Session["EndDate"]);
			}
			catch
			{
				endDate = DateTime.Today;
			}

			//mailTo = "mlorn@wimco.com";

			try
			{
				clientEmail = HttpContext.Current.Session["ClientEmail"].ToString();
			}
			catch
			{
				//TODO: better handle exception
			}

			try
			{
				groupID = Convert.ToInt32(HttpContext.Current.Session["GroupID"]);
			}
			catch
			{
				//TODO: better handle exception
				groupID = dashboardAdminGroup;
			}

			//filterString = string.Format("(LeadStatusCode = 'Closed' OR LeadStatusCode = 'Open' OR LeadStatusCode = 'Pending') AND ClientEMailAddress = '{0}'", clientEmail);
			filterString = $"(LeadStatusID = {pendingStatusID}) AND ClientEMailAddress = '{clientEmail}'";
			ds = leadData.GetUserLead(groupID, startDate, endDate);
			dt = ds.Tables[0];
			DataView dv = new DataView(dt);
			dv.RowFilter = filterString;
			dt = dv.ToTable();
			DataRow r = dt.Rows[0];

			destination = r.Field<string>("DestinationName");
			clientName = $"{r.Field<string>("FirstName")} {r.Field<string>("LastName")}";
			webInqContactSourceCode = r.Field<string>("webInqContactSourceCode");

			// if a request is a Web Resv
			if (webInqContactSourceCode == "REZ2_ED")
			{
				if (!r.IsNull("DateFrom"))
				{
					dateFrom = r.Field<DateTime>("DateFrom");
					beginDate = $"{dateFrom:MMMM yyyy}";
					subject = $"{beginDate} {destination} Requested by {clientName}";
				}
				else
				{
					subject = $"{destination} Requested by {clientName}";
				}
			}
			else
			{
				if (!r.IsNull("DateFrom"))
				{
					dateFrom = r.Field<DateTime>("DateFrom");
					beginDate = $"{dateFrom:MMMM yyyy}";
					subject = $"{beginDate} {destination} Requested by {clientName}";
				}
				else
				{
					subject = $"{destination} Requested by {clientName}";
				}
			}

			if (!r.IsNull("DateFrom"))
			{
				dateFrom = r.Field<DateTime>("DateFrom");
				beginDate = $"{dateFrom:MMMM yyyy}";
				subject = $"{beginDate} {destination} Requested by {clientName}";
			}
			else
			{
				subject = $"{destination} Requested by {clientName}";
			}

			// if a request is a Web Resv
			if (webInqContactSourceCode == "REZ2_ED")
			{
				//get email contents for RFA
				body = leadData.GetWebResvEmailContent(dt);
				try
				{
					magnumConfirmCode = HttpContext.Current.Session["MagnumConfirmCode"].ToString();
					subject += $", Confirm Code:  {magnumConfirmCode}";
				}
				catch
				{
					magnumConfirmCode = string.Empty;
				}
			}
			else
			{
				//get email contents for RFA
				body = leadData.GetRfaEmailContent(dt);
			}

			//Session["BodyText"] = body;
			//Response.Redirect("~/EmailPage.aspx");

			SmtpEmail mail = new SmtpEmail();
			mail.Name = clientName;
			mail.FromEmailAddres = clientEmail;
			mail.ToEmailAddress = raEmail;
			mail.ReplyEmailAddress = clientEmail;
			mail.Subject = subject;
			mail.IsBodyHtml = true;
			mail.MessageBody = body;
			try
			{
				result = mail.SendEmail();
			}
			catch (Exception ex)
			{
				result = ex.Message;
			}

			return result;
		}

		/// <summary>
		/// format a content of email that will be sent to RA
		/// </summary>
		/// <param name="dt">The DataTable.</param>
		/// <returns></returns>
		public string GetRfaEmailContent(DataTable dt)
		{
			StringBuilder strbEmail = null;
			string filterString = string.Empty;

			DateTime dateFrom;
			DateTime dateTo;
			TimeSpan span;
			int numberOfDay;
			string travelDate;
			string destination;
			string villaName;
			string contactSource = string.Empty;
			string comment;
			string clientName = string.Empty;
			string clientEmail = string.Empty;
			string phoneNumber = string.Empty;
			string requestDate = string.Empty;
			string firstDestination = string.Empty;
			string secondDestination = string.Empty;
			string numberOfBedroom = string.Empty;
			string webInqContactSource = string.Empty;
			string webInqContactSourceCode = string.Empty;
			decimal minimumPrice = 0.0m;
			decimal maximumPrice = 0.0m;
			//string travelWithChildren = string.Empty;
			//string requirePool = string.Empty;
			bool travelWithChildren = false;
			bool requirePool = false;


			strbEmail = new StringBuilder();

			foreach (DataRow r in dt.Rows)
			{
				//userEmail = r.Field<string>("ClientEMailAddress");
				if (!r.IsNull("DateFrom") || (!r.IsNull("DateTo")))
				{
					dateFrom = r.Field<DateTime>("DateFrom");
					dateTo = r.Field<DateTime>("DateTo");
					span = dateTo - dateFrom;
					numberOfDay = span.Days;
					if (numberOfDay == 0)
					{
						numberOfDay = 1;
					}
					travelDate = $"{dateFrom:MMM dd, yyyy} to {dateTo:MMM dd, yyyy} ({numberOfDay})";
				}
				else
				{
					travelDate = string.Empty;
				}
				if (r.IsNull("RequestDate"))
				{
					requestDate = string.Empty;
				}
				else
				{
					requestDate = $"{r.Field<DateTime>("RequestDate"):MMM dd, yyyy h:mm tt}";
				}

				destination = r.Field<string>("DestinationName");
				villaName = r.Field<string>("VillaCode");
				contactSource = r.Field<string>("ContactCodeName");
				comment = r.Field<string>("Comment");
				clientName = $"{r.Field<string>("FirstName")} {r.Field<string>("LastName")}";
				clientEmail = r.Field<string>("ClientEMailAddress");
				phoneNumber = r.Field<string>("PhoneNumber");
				webInqContactSource = r.Field<string>("WebInqContactSourceCodeName");
				webInqContactSourceCode = r.Field<string>("webInqContactSourceCode");

				strbEmail.Append("<table align='center' width='400' style='border: 1px solid #d9d9d9;'><tr><td style='padding: 20px'>");
				strbEmail.Append("");
				strbEmail.Append("<p style='font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #4071CF'><strong>Request Information</strong></p>");
				strbEmail.Append("<p style='font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #505050; line-height: 18px'>");
				strbEmail.Append($"Name:<strong> {clientName}</strong><br />");
				if (!string.IsNullOrEmpty(clientEmail))
				{
					strbEmail.Append($"Email:<strong> {clientEmail}</strong><br />");
				}
				if (!string.IsNullOrEmpty(phoneNumber))
				{
					strbEmail.Append($"Phone:<strong> {phoneNumber}</strong><br />");
				}
				if (!string.IsNullOrEmpty(requestDate))
				{
					strbEmail.Append($"Request Date:<strong> {requestDate}</strong><br />");
				}
				if (!string.IsNullOrEmpty(villaName))
				{
					strbEmail.Append($"Villa Name:<strong> {villaName}</strong><br />");
				}
				if (!string.IsNullOrEmpty(destination))
				{
					strbEmail.Append($"First Choice:<strong> {destination}</strong><br />");
				}
				if (!string.IsNullOrEmpty(secondDestination))
				{
					strbEmail.Append($"Second Choice:<strong> {secondDestination}</strong><br />");
				}

				if (!string.IsNullOrEmpty(travelDate))
				{
					strbEmail.Append($"Dates:<strong> {travelDate}</strong><br />");
				}

				if (!string.IsNullOrEmpty(numberOfBedroom))
				{
					strbEmail.Append($"Number Of Bedrooms:<strong> {numberOfBedroom}</strong><br />");
				}
				if (minimumPrice > 0 || maximumPrice > 0)
				{
					strbEmail.Append($"Price Range:<strong> {minimumPrice:C2} to {maximumPrice:C2}</strong><br />");
				}
				if (travelWithChildren == true)
				{
					strbEmail.Append("Traveling with Children:<strong> Yes</strong><br />");
				}
				if (requirePool == true)
				{
					strbEmail.Append("Must Have a Pool:<strong> Yes</strong><br />");
				}
				if (!string.IsNullOrEmpty(comment))
				{
					strbEmail.Append($"Comments:<strong> {comment}</strong><br />");
				}
				if (!string.IsNullOrEmpty(contactSource))
				{
					strbEmail.Append($"How did you hear of WIMCO:<strong> {contactSource}</strong><br />");
				}
				// ML - Requested by Ann Marie Caye not to include
				/*
        if (!string.IsNullOrEmpty(webInqContactSource))
        {
          strbEmail.Append(string.Format("Inquiry Source:<strong> {0}</strong></p>", webInqContactSource));
        }
        */

				strbEmail.Append("</p></td></tr></table>");
			}

			return strbEmail.ToString();
		}


		public string GetWebResvEmailContent(DataTable dt)
		{
			StringBuilder strbEmail = null;
			string filterString = string.Empty;

			DateTime dateFrom;
			DateTime dateTo;
			TimeSpan span;
			int numberOfDay;
			string travelDate;
			string destination;
			string villaName;
			string confirmCode = string.Empty;
			string contactSource;
			string comment;

			string webInqContactSource = string.Empty;
			string webInqContactSourceCode = string.Empty;

			//string travelWithChildren = string.Empty;
			//string requirePool = string.Empty;
			bool travelWithChildren = false;
			bool requirePool = false;
			string webResvNumber;
			decimal grossPrice;
			decimal ServiceCharge;
			decimal Tax;
			decimal price;
			string estimatedPrice = string.Empty;
			string numberOfBedroom = string.Empty;
			string numberOfAdult = string.Empty;
			string numberOfChild = string.Empty;
			string childAdge = string.Empty;
			string clientName = string.Empty;
			string clientEmail = string.Empty;
			string phoneNumber = string.Empty;
			string address = string.Empty;
			string countryName = string.Empty;

			strbEmail = new StringBuilder();

			strbEmail.Append("<table align='center' width='400' style='border: 1px solid #d9d9d9'><tr><td style='padding: 20px'>");
			strbEmail.Append("");

			foreach (DataRow r in dt.Rows)
			{
				webResvNumber = r.Field<string>("CallbackID");
				using (MagnumDataContext magnumDb = new MagnumDataContext())
				{
					var resvData = magnumDb.spGetWebReservation(string.Empty, webResvNumber, string.Empty).FirstOrDefault();
					if (resvData != null)
					{
						confirmCode = resvData.MagnumConfirmCode;
						grossPrice = Convert.ToDecimal(resvData?.GrossPrice ?? 0);
						ServiceCharge = Convert.ToDecimal(resvData?.ServiceChargeAmt ?? 0);
						Tax = Convert.ToDecimal(resvData?.TaxAmt ?? 0);
						price = grossPrice + ServiceCharge + Tax;
						estimatedPrice = price == 0 ? string.Empty : $"${price:N2}";
						numberOfAdult = resvData?.NumAdults.ToString() ?? string.Empty;
						numberOfChild = resvData?.NumChildren.ToString() ?? string.Empty;
						childAdge = resvData?.ChildrenAges.ToString() ?? string.Empty;
						numberOfBedroom = resvData.NumRooms.ToString() ?? string.Empty;
						HttpContext.Current.Session["MagnumConfirmCode"] = resvData?.MagnumConfirmCode ?? string.Empty;
						address = FormatMailingAddress(resvData.Street, resvData.City, resvData.State, resvData.PostalCode);
						countryName = resvData.CountryName;
					}
				}

				//userEmail = r.Field<string>("ClientEMailAddress");
				if (!r.IsNull("DateFrom") || (!r.IsNull("DateTo")))
				{
					dateFrom = r.Field<DateTime>("DateFrom");
					dateTo = r.Field<DateTime>("DateTo");
					span = dateTo - dateFrom;
					numberOfDay = span.Days;
					if (numberOfDay == 0)
					{
						numberOfDay = 1;
					}
					travelDate = $"{dateFrom:MMM dd, yyyy} to {dateTo:MMM dd, yyyy} ({numberOfDay})";
				}
				else
				{
					travelDate = string.Empty;
				}

				destination = r.Field<string>("DestinationName");
				villaName = r.Field<string>("VillaCode");
				contactSource = r.Field<string>("ContactCodeName");
				comment = r.Field<string>("Comment");
				clientName = $"{r.Field<string>("FirstName")} {r.Field<string>("LastName")}";
				clientEmail = r.Field<string>("ClientEMailAddress");
				phoneNumber = r.Field<string>("PhoneNumber");
				webInqContactSource = r.Field<string>("WebInqContactSourceCodeName");
				webInqContactSourceCode = r.Field<string>("webInqContactSourceCode");

				strbEmail.Append("<p style='font-size: 14px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #4071CF'><strong>Reservation Information</strong></p>");
				strbEmail.Append("<p style='font-size: 11px; font-family: Verdana, Arial, Helvetica, sans-serif; color: #505050; line-height: 18px'>");
				strbEmail.Append($"Name:<strong> {clientName}</strong><br />");
				strbEmail.Append($"Email:<strong> {clientEmail}</strong><br />");
				strbEmail.Append($"Phone Number:<strong> {phoneNumber}</strong><br />");
				if (!string.IsNullOrEmpty(travelDate))
				{
					strbEmail.Append($"Dates:<strong> {travelDate}</strong><br />");
				}
				strbEmail.Append("<br/>");
				strbEmail.Append($"Destination:<strong> {destination}</strong><br />");
				strbEmail.Append($"Villa:<strong> {villaName}</strong><br />");
				strbEmail.Append($"Number of Bedrooms:<strong> {numberOfBedroom}</strong><br />");
				strbEmail.Append($"Number of Adults:<strong> {numberOfAdult}</strong><br />");
				strbEmail.Append($"Number of Children:<strong> {numberOfChild}</strong><br />");
				strbEmail.Append($"Child Age(s):<strong> {childAdge}</strong><br />");
				strbEmail.Append($"Estimated Price:<strong> {estimatedPrice}</strong><br />");
				strbEmail.Append($"Client Source Code:<strong> {contactSource}</strong><br />");
				strbEmail.Append("Web-Inq Source Code:<strong> Web Res</strong><br />");
				strbEmail.Append($"Comments:<strong> {comment}</strong></p>");
			}

			strbEmail.Append("</p></td></tr></table>");

			return strbEmail.ToString();
		}

		/// <summary>
		/// Gets the RA's email.
		/// </summary>
		/// <param name="userID">The user identifier.</param>
		/// <returns></returns>
		public string GetRAEmail(int userID)
		{
			DataTable dt = null;
			string raEmail = string.Empty;

			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				dt = magnumDb.spGetMagUser(userID).ToDataTable("User");
				raEmail = dt.Rows[0].Field<string>("EmailAddress");
			}

			return raEmail;
		}

		/// <summary>
		/// Gets the lead status for populating a drop-down-list
		/// </summary>
		/// <returns></returns>
		public DataTable GetLeadStatus()
		{
			DataTable dt = null;
			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var status = from s in magnumDb.LeadStatus
										 orderby s.LeadStatusCode
										 select s;
				dt = status.ToDataTable("LeadStus");
			}
			return dt;
		}

		public string GetFilterString(int groupID, int leadStatusID, string clientEmail)
		{
			string filterString = string.Empty;

			// if a RA is belong to an admin group, allows to view closed leads
			if (groupID == dashboardAdminGroup)
			{
				// check drop-down-list value for an admin user and
				// filter accordingly
				if (leadStatusID == viewAllLeadValue)
				{
					filterString = $"(LeadStatusID = {openStatusID} OR LeadStatusID = {pendingStatusID} OR LeadStatusID = {closedStatusID}) AND ClientEMailAddress = '{clientEmail}'";
				}
				else
				{
					// take care of filter by a drop-down-list value
					filterString = $"LeadStatusID = {leadStatusID} AND ClientEMailAddress = '{clientEmail}'";
				}
			}
			else
			{
				// if RA is logging in, display only Open Status and his or her Pending Status
				filterString = $"(LeadStatusID = {openStatusID} OR LeadStatusID = {pendingStatusID}) AND ClientEMailAddress = '{clientEmail}'";
			}

			return filterString;
		}

		public string GetFilterString(int groupID, string clientEmail)
		{
			string filterString = string.Empty;

			// if a RA is belong to an admin group, allows to view closed leads
			if (groupID == dashboardAdminGroup)
			{
				filterString = $"(LeadStatusID = {openStatusID} OR LeadStatusID = {pendingStatusID} OR LeadStatusID = {closedStatusID}) AND ClientEMailAddress = '{clientEmail}'";
			}
			else
			{
				// if RA is logging in, display only Open Status and his or her Pending Status
				filterString = $"(LeadStatusID = {openStatusID} OR LeadStatusID = {pendingStatusID}) AND ClientEMailAddress = '{clientEmail}'";
			}

			return filterString;
		}

		/// <summary>
		/// Assigns the destination to a lead.
		/// </summary>
		/// <param name="leadID">The lead identifier.</param>
		/// <param name="destinationID">The destination identifier.</param>
		/// <returns></returns>
		public string AssignDestination(int leadID, int destinationID)
		{
			string result = string.Empty;

			// declare the web service
			BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
			// declare the lead obj.
			BusinessLogic.Leads lead = new BusinessLogic.Leads();
			// declare the lead data return obj.
			BusinessLogic.LeadsDataReturn ldr = new BusinessLogic.LeadsDataReturn();

			// get a lead.
			lead = biz.leads_GetLead(AuthKey, leadID);
			lead.DestinationID = destinationID;
			ldr = biz.leads_UpdateLead(AuthKey, lead);

			if (ldr.Success)
			{
				result = destHasBeenAssigned;
			}
			else
			{
				result = ldr.Message;
			}
			return result;
		}

		/// <summary>
		/// Gets the client identifier.
		/// </summary>
		/// <param name="leadID">The lead identifier.</param>
		/// <returns></returns>
		public int GetClientID(int leadID)
		{

			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var clientID = magnumDb.spLeadGetClientID(leadID);
				return clientID;
			}
		}

		public UserAlertLink ProcessLeadLink(string linkID)
		{
			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var alertLinkData = (from l in magnumDb.UserAlertLinks
														 where l.LinkID == linkID && l.IsEnabled == true
														 select l).FirstOrDefault();
				return alertLinkData;
			}
		}

		public (string clientName, string clientEmail) GetClientEmail(int leadID)
		{
			string clientEmail = string.Empty;
			string clientName = string.Empty;
			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var lead = (from l in magnumDb.Leads
										where l.LeadID == leadID
										select l).FirstOrDefault();
				if (lead != null)
				{
					clientEmail = lead.ClientEmailAddress;
					clientName = $"{lead.FirstName} {lead.LastName}";
				}
			}
			return (clientName, clientEmail);
		}

		public string FormatMailingAddress
		(
			string addressLine1,
			string city,
			string state,
			string postalCode
		)
		{
			string mailingAddress = string.Empty;
			string addressLine2 = string.Empty;

			addressLine2 = (city != string.Empty ? city + ", " : string.Empty) +
				(state != string.Empty ? state + " " : string.Empty) +
				(postalCode != string.Empty ? postalCode : string.Empty);

			mailingAddress = (addressLine1 != string.Empty ? addressLine1 + Environment.NewLine : string.Empty) +
				(addressLine2 != string.Empty ? addressLine2 + Environment.NewLine : string.Empty);
			return mailingAddress;
		}


		public static string GetGridLayout(string gridName)
		{
			int userID;

			string result = string.Empty;
			try
			{
				userID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
			}
			catch
			{
				//TODO: Better handle exception
				userID = 0;
			}

			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var gridLayoutData = (from p in magnumDb.GridLayouts
															where p.UserID == userID && p.GridName == gridName
															select p).FirstOrDefault();
				if (gridLayoutData != null)
				{
					result = gridLayoutData.Layout;
				}
			}

			return result;
		}

		public static void SaveGridLayout(string gridName, string gridLayout)
		{
			int userID;

			try
			{
				userID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
			}
			catch
			{
				//TODO: Better handle exception
				userID = 0;
			}

			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var gridLayoutData = (from p in magnumDb.GridLayouts
															where p.UserID == userID && p.GridName.ToLower() == gridName.ToLower()
															select p).FirstOrDefault();
				if (gridLayoutData != null)
				{
					gridLayoutData.Layout = gridLayout;
				}
				else
				{
					GridLayout newLayout = new GridLayout();
					newLayout.UserID = userID;
					newLayout.GridName = gridName;
					newLayout.Layout = gridLayout;
					magnumDb.GridLayouts.InsertOnSubmit(newLayout);
				}

				magnumDb.SubmitChanges();
			}
		}

		public static string GetResvAgentEmail(string fullName)
		{
			string result = string.Empty;
			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var userData = (from u in magnumDb.spGetResvAgentEmailByFullName(fullName)
												select u).FirstOrDefault();
				if (userData != null)
				{
					result = userData.EmailAddress;
				}
			}
			return result;
		}

		public static int GetClientIDByOrderOrResvNumber(string OrderOrReservationNumber)
		{
			int result = 0;
			/*
			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var clientData = (from c in magnumDb.spGetClientIDByOrderOrResv(OrderOrReservationNumber)
													select c).FirstOrDefault();
				if (clientData != null)
				{
					result = clientData.ClientID;
				}
			}
			*/
			return result;
		}


		public static int GetClientIDByEmail(string EmailAddress)
		{
			int result = 0;
			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				var clientData = (from c in magnumDb.spGetClientID(EmailAddress)
													select c).FirstOrDefault();
				if (clientData != null)
				{
					result = clientData.ClientID;
				}
			}

			return result;
		}

		public DataSet GetFCC(int clientID)
		{
			DataSet ds = null;
			using (MagnumDataContext magnumDb = new MagnumDataContext())
			{
				ds = magnumDb.spLeadGetFCC(clientID).ToDataSet("Table");
			}

			return ds;
		}

	}

}
