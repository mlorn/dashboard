﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer
{
  struct LoginInfo
  {
    public int UserID
    {
      get;
      set;
    }
    public string UserName
    {
      get;
      set;
    }
    public int GroupID
    {
      get;
      set;
    }
  }
}
