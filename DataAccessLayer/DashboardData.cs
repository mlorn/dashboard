﻿#region Copyright (c) West Indies Management Company
/*
{*******************************************************************}
{       Copyright (c) West Indies Management Company                }
{       ALL RIGHTS RESERVED                                         }
{                                                                   }
{  Author: ML                                                       }
{  Date: 1/1/2010                                                   }
{  Description:                                                     }
{  Retrieve data from the database by using entity framework and    }
{  populate data tables.                                            }
{                                                                   }
{*******************************************************************}
*/
#endregion

#region using statements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using WimcoHelper;
#endregion


namespace DataAccessLayer
{
  /// <summary>
  /// Summary description for ReservationAgent
  /// </summary>
  public class DashboardData
  {
    private const string finalPaymentKind = "P";
    private const string nonDepositKind = "D";
    private const string returningClientCallbackKind = "R";
    private const string futureClientCallbackKind = "F";
    private const string leadKind = "L";
    private const string rliKind = "R";
    private const string rliWaitlistKind = "W";
    private const string rliInqKind = "I";
    private const string rliBkdKind = "B";

    private const int dashboardAdmin = 195;
    private const int resvAgent = 186;
    private const int salesAssist = 203; // sales Assistant

    private const int opTypeLead = 983;  // FCC for lead
    private const int opTypeClientProspect = 390;
    private const int opTypeService = 982;

    private const string letterRevision = "R";
    private const string letterFinalPayment = "F";
    private const string letterNonDeposit = "D";

    private const string op1 = "OP1"; // First opportunity
    private const int redAlert = 2; //red alert

    private const string adminUser = "ADMIN";
    private const string authKey = "967fc379-f7d8-4dad-833f-611dbf9cc1c1";
    private int leadClaimedInSevenDay = 0;
    private int pastDueFccForLead = 0;
    private int pastDueFccForClientProspect = 0;
    private int pastDueFccForService = 0;
    private int pastDueFccForUnassigned = 0;
    private int pastDueRccb = 0;
    // pending FCC
    private int pendingFccOpp = 0;
    private int pendingFccClp = 0;
    private int pendingFccSvc = 0;
    // Completed FCC
    private int CompletedFccOpp = 0;
    private int CompletedFccClp = 0;
    private int CompletedFccSvc = 0;
    // pending Reservation
    private int pendingResvInq = 0;
    private int pendingResvBkd = 0;
    private int pendingResvOkd = 0;

    private decimal totalSales = 0.0m;
    private decimal totalBuds = 0.0m;


    private int rccbInSevenDay = 0;
    private int leadAtOp1OrRedAlert = 0;
    private int todayFCC = 0;

    private dsDashboard dashboardDataset;
    private dsSalesMgrBoard salesMgrBoardDatabaset;

    #region Properties
    public string UserName
    {
      get;
      set;
    }
    public string AdminUserName
    {
      get;
      set;
    }

    public int UserID
    {
      get;
      set;
    }

    public int GroupID
    {
      get;
      set;
    }

    public string Password
    {
      get;
      set;
    }

    public string FullName
    {
      get;
      set;
    }

    public int SalesAssistID
    {
      get;
      set;
    }

    public string EmailAddress
    {
      get;
      set;
    }

    public dsDashboard DashboardDataset
    {
      get
      {
        return dashboardDataset;
      }
      set
      {
        dashboardDataset = value;
      }
    }

    public dsSalesMgrBoard SalesMgrBoardDataset
    {
      get
      {
        return salesMgrBoardDatabaset;
      }
      set
      {
        salesMgrBoardDatabaset = value;
      }
    }
    #endregion

    #region Contructors
    /// <summary>
    /// Initializes a new instance of the <see cref="DashboardData"/> class.
    /// This contructor method being called from the Login page and
    /// it does populate the dataset.
    /// </summary>
    public DashboardData()
    {

      //
      // TODO: Add constructor logic here
      //
      dashboardDataset = new dsDashboard();
      salesMgrBoardDatabaset = new dsSalesMgrBoard();

    }

    public DashboardData(dsDashboard ds)
    {
      dashboardDataset = ds;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DashboardData"/> class.
    /// This construct method being called from the Default page to retrieve data
    /// and populate dataset.
    /// </summary>
    /// <param name="userID">The user ID.</param>
    /// <param name="groupID">The group ID.</param>
    public DashboardData(int userID, int groupID, string userName)
    {
      //
      // TODO: Add constructor logic here
      //
      this.UserID = userID;
      this.GroupID = groupID;
      this.UserName = userName;
      dashboardDataset = new dsDashboard();
      salesMgrBoardDatabaset = new dsSalesMgrBoard();
      //HttpContext.Current.Session["UserName"] = this.UserName;
      FillDataTables();
    }
    #endregion

    #region Login method
    /// <summary>
    /// Logins the specified user name.
    /// </summary>
    /// <param name="userName">Name of the user.</param>
    /// <param name="password">The password.</param>
    /// <returns></returns>
    public bool Login(string userName, string password, string logInAs)
    {
      bool result = false;
      string user = userName.ToUpper();
      /*
      // if user name is Tim Warburton, let him in even a password is incorrect.
      if (user == "TIM" || user == "TWARBURTON" ||
          user == "WARBURTON" || user == "TIM WARBURTON")
      {
          this.UserID = 0;
          this.GroupID = dashboardAdmin; //Dashboard Admin
          this.UserName = "TIM";
          this.AdminUserName = "ADMIN";
          this.FullName = "Tim Warburton";
        
          return true;
      }
      */

      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        var loginData = magnumDb.spLogin(userName, password).ToList();

        var adminGroup = (from a in loginData
                          where a.GroupID == dashboardAdmin //Dashboard Admin
                          select a).FirstOrDefault();

        //User not in the Admin group
        if (adminGroup == null)
        {
          HttpContext.Current.Session["Admin"] = string.Empty;

          var salesAssistGroup = (from s in loginData
                                  where s.GroupID == salesAssist //Sales Agent Group
                                  select s).FirstOrDefault();
          if (salesAssistGroup != null)  // log in as Sales Assistant group
          {
            this.UserID = salesAssistGroup.UserID;
            this.UserName = salesAssistGroup.UserName;
            this.GroupID = (int)salesAssistGroup.GroupID;
            this.AdminUserName = string.Empty;
            this.FullName = salesAssistGroup.FullName;
            this.SalesAssistID = this.UserID;
            this.EmailAddress = salesAssistGroup.EmailAddress;
            result = true;
          }
          else
          {
            var salesGroup = (from s in loginData
                              where s.GroupID == resvAgent //Sales Agent Group
                              select s).FirstOrDefault();
            if (salesGroup != null)  // log in as Sales Agent
            {
              this.UserID = salesGroup.UserID;
              this.UserName = salesGroup.UserName;
              this.GroupID = (int)salesGroup.GroupID;
              this.AdminUserName = string.Empty;
              this.FullName = salesGroup.FullName;
              this.EmailAddress = salesGroup.EmailAddress;
              result = true;
            }
          }
        }
        else  //admin group which can log in as Admin or impersonat a RA
        {
          HttpContext.Current.Session["Admin"] = adminUser;
          if (logInAs == "All")  //logging as Admin user
          {
            this.UserID = adminGroup.UserID;
            this.UserName = adminGroup.UserName;
            this.GroupID = (int)adminGroup.GroupID;
            this.AdminUserName = adminUser;
            this.FullName = adminGroup.FullName;
            result = true;
          }
          else   //logging as individual (impersonat) 
          {
            var logIn = magnumDb.spLogInGetUser(logInAs).FirstOrDefault();
            if (logIn != null)
            {
              this.UserID = logIn.UserID;
              this.UserName = logIn.UserName;
              this.GroupID = (int)logIn.GroupID;
              this.AdminUserName = string.Empty;
              this.FullName = logIn.FullName;
              this.EmailAddress = logIn.EmailAddress;
              result = true;
            }
          }
        }
      }
      //HttpContext.Current.Session["UserName"] = this.UserName;
      /*
      UserData ud = new UserData();
      ud.EmailAddress = this.EmailAddress;
      ud.UserID = this.UserID;
      ud.Loggedin = true;
      ud.OrganizationID = 1; // Wimco
      ud.username = this.UserName;
      ud.SalesAgent = true;
      ud.FullName = this.FullName;
      HttpContext.Current.Session.Add("UserData", ud);
      */
      return result;
    }
    #endregion

    public DataTable GetUser(string userName)
    {
      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        return magnumDb.spDashboardRA(userName).OrderBy(u => u.FullName).ToDataTable("User");
      }
    }


    public string GetClientEmailAddress(int clientID)
    {
      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        var clientData = (from c in magnumDb.spGetClientEmailAddress(clientID)
                          select c).FirstOrDefault();
        if (clientData != null)
        {
          return clientData.EMailAddress;
        }
        else
        {
          return string.Empty;
        }

      }

    }

    #region Retrieve Client
    /// <summary>
    /// Gets clients for a specific Resv Agent.
    /// </summary>
    /// <param name="ResAgentID">The res agent ID.</param>
    /// <returns></returns>
    public DataTable GetClient(int ResAgentID)
    {

      var clientData = (from c in dashboardDataset.tblClient
                        where c.UserID == ResAgentID
                        select new
                        {
                          ClientID = c.Field<int>("ClientID"),
                          FirstName = c.Field<string>("FirstName"),
                          LastName = c.Field<string>("LastName"),
                          UserID = c.Field<int>("UserID"),
                          UserName = c.Field<string>("UserName"),
                          EMailAddress = c.Field<string>("EMailAddress"),
                          LeadScore = c.Field<string>("LeadScore"),
                          LastContact = c.Field<string>("LastContact"),
                          Kind = c.Field<string>("Kind"),
                          AllKind = c.Field<string>("AllKind")
                        }).Distinct();
      return clientData.ToDataTable("tblClient");

    }
    #endregion



    public void RemoveFCC()
    {
      string filterString = string.Empty;

      var clientCallback = (from c in dashboardDataset.tblClientCallback
                            select c.ClientID).ToArray();
      var fcc = (from f in dashboardDataset.tblFCC
                 select f.ClientID).ToArray();

      //find ClientIDs that are both in the ClientCallback dataset and FCC dataset.
      var commonNumbers = fcc.Intersect(clientCallback);
      foreach (var c in commonNumbers)
      {
        DataRow[] dr;
        // Remove FCC from the FCC dataset if a client is already in the Client Callback dataset.
        //filterString = string.Format("ClientID={0}", c);
        filterString = $"ClientID={c}";
        dr = dashboardDataset.tblFCC.Select(filterString);
        if (dr != null)
        {
          foreach (var r in dr)
          {
            dashboardDataset.tblFCC.Rows.Remove(r);
          }
        }
      }
    }

    #region Reservation History data table
    /// <summary>
    /// Fill Reseration History data table.
    /// </summary>
    /// <returns></returns>
    public void FillWaitListed(MagnumDataContext magnumDb)
    {
      DataRow dr;
      TimeSpan day;
      int numberDay;
      DateTime eachDay;
      bool available = true;
      string result = string.Empty;
      string kind = string.Empty;

      //call the stored procedure and store the result in a list
      var WaitlistedRLI = magnumDb.spDashboardStatusRLI(this.UserName).ToList();
      //get all waitlisted reservations
      var wtl = from l in WaitlistedRLI
                where (l.Status == "WTL" || l.Status == "WTX")
                select l;

      //loop through all wait list reservations
      foreach (var w in wtl)
      {
        //get begin date
        eachDay = Convert.ToDateTime(w.DateFrom);
        //find a number of day within a reservation
        day = Convert.ToDateTime(w.DateTo) - eachDay;
        numberDay = day.Days;

        //loop through each day within a reservation and find any reservation
        //between eachDay (WaitListed reservation day) 
        for (int i = 1; i <= numberDay; i++)
        {
          var noWtl = (from n in WaitlistedRLI
                       where n.Status != "WTL" &&
                       (
                        ((eachDay > n.DateFrom) && (eachDay < n.DateTo)) &&
                        (n.VendorBaseID == w.VendorBaseID) &&
                        (n.ProductBaseID == w.ProductBaseID)
                       )
                       select n).FirstOrDefault();
          //return null if data is not found in the list.
          if (noWtl == null)
          {
            available = true;
          }
          else
          {
            available = false;
            break;
          }
          //move to the next day
          eachDay = eachDay.AddDays(1);
        }
        //if not available, change the status
        if (available)
        {
          w.Status = "WTL";
        }
        else
        {
          w.Status = "NOT";
        }
      }

      //Reservation info
      kind = string.Empty;
      var statusRLI = from s in WaitlistedRLI
                      where s.Status == "INQ" || s.Status == "NEW" ||
                      s.Status == "BKD" || s.Status == "WTL"
                      select s;
      foreach (var r in statusRLI)
      {
        if (r.Status == "INQ")
        {
          kind = rliInqKind;
        }
        else
        {
          if (r.Status == "NEW" || r.Status == "BKD")
          {
            kind = rliBkdKind;
          }
          else
          {
            if (r.Status == "WTL")
            {
              kind = rliWaitlistKind;
            }
          }
        }

        if (kind == rliWaitlistKind || kind == rliInqKind || kind == rliBkdKind)
        {
          dr = null;
          dr = dashboardDataset.tblReservation.NewRow();
          dr["ReservationLineItemID"] = r.ReservationLineItemID;
          dr["ItineraryID"] = r.ItineraryID;
          dr["ReservationNumber"] = r.ReservationNumber;
          dr["ClientID"] = r.ClientID;
          dr["FirstName"] = r.FirstName;
          dr["LastName"] = r.LastName;
          dr["EMailAddress"] = r.EMailAddress;
          dr["DateFrom"] = r.DateFrom;
          dr["DateTo"] = r.DateTo;
          dr["Villa"] = r.VillaName;
          dr["Status"] = r.Status;
          dr["UserID"] = r.UserID;
          dr["UserName"] = r.FullName;
          dr["CurrencySymbol"] = r.CurrencySymbol;
          dr["GrossPrice"] = r.GrossPrice;
          dr["DepositDueDate"] = r.DepositDueDate.ToString();
          dr["Kind"] = kind;
          dr["NetOnHand"] = r.NetOnHand;
          dashboardDataset.tblReservation.Rows.Add(dr);
          kind = string.Empty;
        }
      }
      //return result;
    }
    #endregion

    #region Resv Agent data table.
    /// <summary>
    /// Fills the resv agent data table.
    /// </summary>
    /// <param name="magnumDb">The magnum db.</param>
    public void FillResvAgent(MagnumDataContext magnumDb)
    {
      DataRow dr;
      var RAData = magnumDb.spDashboardRA(this.UserName);
      if (this.GroupID == resvAgent) //Sales Agent Group
      {
        var userData = from r in RAData
                       where r.UserID == this.UserID
                       select r;

        foreach (var r in userData)
        {
          dr = null;
          dr = dashboardDataset.tblResAgent.NewRow();
          dr["UserID"] = r.UserID;
          dr["UserName"] = r.UserName;
          dr["FullName"] = r.FullName;
          dr["FinalPayment"] = r.FinalPayment;
          dr["Nondeposit"] = r.NonDeposit;
          dr["ReturnClientCallback"] = r.ReturnClientCallback;
          dr["FCCCurrent"] = r.FutureClientCallback;
          dr["InqRLI"] = r.InqRLI;
          dr["NewRLI"] = r.NewRLI;
          dr["BkdRLI"] = r.BkdRLI;
          dr["WtlRLI"] = r.WtlRLI;
          //for activity counter
          dr["PendingFCC"] = string.Empty;
          dr["CompletedFCC"] = string.Empty;
          dr["ClaimedLead"] = 0;
          dr["ConversionLead"] = 0.0m;
          dr["TotalSales"] = 0.0m;
          dr["TotalBud"] = 0.0m;
          dashboardDataset.tblResAgent.Rows.Add(dr);
        }
      }
      else  //Dashboard Admin Group
      {
        var userData = RAData.ToList();  //get all reservation agents
        foreach (var r in userData)
        {
          dr = null;
          dr = dashboardDataset.tblResAgent.NewRow();
          dr["UserID"] = r.UserID;
          dr["UserName"] = r.UserName;
          dr["FullName"] = r.FullName;
          dr["FinalPayment"] = r.FinalPayment;
          dr["Nondeposit"] = r.NonDeposit;
          dr["ReturnClientCallback"] = r.ReturnClientCallback;
          dr["FCCCurrent"] = r.FutureClientCallback;
          dr["InqRLI"] = r.InqRLI;
          dr["NewRLI"] = r.NewRLI;
          dr["BkdRLI"] = r.BkdRLI;
          dr["WtlRLI"] = r.WtlRLI;
          //for activity counter
          dr["PendingFCC"] = string.Empty;
          dr["CompletedFCC"] = string.Empty;
          dr["ClaimedLead"] = 0;
          dr["ConversionLead"] = 0.0m;
          dr["TotalSales"] = 0.0m;
          dr["TotalBud"] = 0.0m;
          dashboardDataset.tblResAgent.Rows.Add(dr);
        }
      }
    }
    #endregion

    #region Final Payment data table
    /// <summary>
    /// Fills the final payment data table.
    /// </summary>
    /// <param name="magnumDb">The magnum db.</param>
    public void FillFinalPayment(MagnumDataContext magnumDb)
    {
      DataRow dr;
      //**** Final Payment
      DateTime? msgDate = null;

      var finalPaymentData = magnumDb.spDashboardFinalPayment(this.UserName);
      foreach (var f in finalPaymentData)
      {
        dr = null;
        dr = dashboardDataset.tblFinalPayment.NewRow();
        dr["ReservationLineItemID"] = f.ReservationLineItemID;
        dr["ItineraryID"] = f.ItineraryID;
        dr["ReservationNumber"] = f.ReservationNumber;
        dr["DateFrom"] = f.DateFrom;
        dr["DateTo"] = f.DateTo;
        dr["Villa"] = f.Villa;
        dr["UserID"] = f.UserID;
        dr["UserName"] = f.UserName;
        dr["CurrencySymbol"] = f.CurrencySymbol;
        if (f.FinalDueDate != null)
        {
          dr["FinalDueDate"] = f.FinalDueDate;
        }
        //dr["FinalDueDate"] = Convert.IsDBNull(f.FinalDueDate) ? DBNull.Value : f.FinalDueDate;
        //balanceDue = f.TotalCharge -
        //  f.WimcoTotalRecdClient - f.PMTotalRecdClient - f.TACommAmt + f.TotalPaidClient;

        dr["FinalDueAmt"] = f.BalanceDue;
        //dr["FinalDueDay"] = f.FinalDueDay;
        dr["FinalDueDay"] = 0;
        dr["GrossPrice"] = f.GrossPrice;
        dr["ClientID"] = f.ClientID;
        dr["LastName"] = f.LastName;
        dr["FirstName"] = f.FirstName;
        dr["EMailAddress"] = f.EMailAddress;
        //dr["Message"] = f.Body;
        dr["Message"] = string.Empty;
        //dr["PastOrFuture"] = f.FinalDueDay > 0 ? "Y" : "N";
        dr["PastOrFuture"] = "N";
        dr["DepositDueAmt"] = 0.0m;
        dr["Kind"] = finalPaymentKind;
        dr["PaymentDueDate"] = Convert.ToDateTime(f.FinalDueDate).ToShortDateString();
        dr["TotalCharge"] = f.TotalCharge;
        dr["TotalReceived"] = f.TotalReceived;
        dr["BalanceDue"] = f.BalanceDue;

        try
        {
          msgDate = f.LetterPrintedDate;
          if (msgDate == null || msgDate == Convert.ToDateTime("1/1/1900"))
          {
            dr["LetterPrintedDate"] = DateTime.MinValue;
          }
          else
          {
            dr["LetterPrintedDate"] = msgDate;
          }
        }
        catch
        {
          dr["LetterPrintedDate"] = DateTime.MinValue;
        }

        try
        {
          msgDate = f.LetterSentDate;
          if (msgDate == null || msgDate == Convert.ToDateTime("1/1/1900"))
          {
            dr["LetterSentDate"] = DateTime.MinValue;
          }
          else
          {
            dr["LetterSentDate"] = msgDate;
          }
        }
        catch
        {
          dr["LetterSentDate"] = DateTime.MinValue;
        }

        dashboardDataset.tblFinalPayment.Rows.Add(dr);
      }

    }
    #endregion

    #region Non-Deposit data table
    /// <summary>
    /// Fills the non deposit data table.
    /// </summary>
    /// <param name="magnumDb">The magnum db.</param>
    /// <param name="startDate">The start date.</param>
    public void FillNonDeposit(MagnumDataContext magnumDb, DateTime startDate)
    {
      Decimal? balanceDue;
      DateTime currentDate;
      TimeSpan dayLate;
      DateTime? msgDate = null;
      string villaName = string.Empty;
      int numberDayLate;

      var nonDepositData = magnumDb.spDashboardNonDeposit(startDate, this.UserName);
      if (nonDepositData != null)
      {
        foreach (var n in nonDepositData)
        {
          if (n.DepositDue > 0)
          {
            DataRow dr;
            dr = dashboardDataset.tblNonDeposit.NewRow();
            dr["ReservationLineItemID"] = n.ReservationLineItemID;
            dr["ItineraryID"] = n.ItineraryID;
            dr["ReservationNumber"] = n.ReservationNumber;
            dr["ClientID"] = n.ClientID;
            dr["FirstName"] = n.FirstName;
            dr["LastName"] = n.LastName;
            dr["EMailAddress"] = n.EMailAddress;
            dr["DateFrom"] = n.DateFrom;
            dr["DateTo"] = n.DateTo;
            villaName = n.VillaName;
            if (string.IsNullOrEmpty(villaName))
            {
              dr["Villa"] = string.Empty;
            }
            else
            {
              dr["Villa"] = villaName;
            }
            dr["UserID"] = n.UserID;
            dr["UserName"] = n.UserName;
            //ml commented out on 10/26/2012
            //dr["DepositDueDate"] = Convert.ToDateTime(n.DepositDueDate).ToShortDateString();
            dr["DepositDueDate"] = Convert.ToDateTime(n.DepositDueDate).ToShortDateString();
            //ml commented out on 10/26/2012
            //currentDate = Convert.ToDateTime(n.DepositDueDate);
            currentDate = Convert.ToDateTime(n.DepositDueDate);
            dayLate = startDate - currentDate;
            numberDayLate = dayLate.Days > 0 ? dayLate.Days : 0;
            dr["NumberOfDayLate"] = numberDayLate;
            dr["CurrencySymbol"] = n.CurrencySymbol;
            dr["DepositAmt"] = n.DepositDue;
            balanceDue = n.DepositDue - n.TotalReceived;
            dr["DepositDueAmt"] = balanceDue;
            //ml commented out on 10/26/2012
            //dr["PastOrFuture"] = numberDayLate > 0 ? "Y" : "N";
            dr["PastOrFuture"] = numberDayLate >= 0 ? "N" : "Y";
            dr["FinalDueAmt"] = 0.0m;
            dr["Kind"] = nonDepositKind;
            dr["PaymentDueDate"] = Convert.ToDateTime(n.DepositDueDate).ToShortDateString();
            if (n.TotalCharge != null)
            {
              dr["TotalCharge"] = n.TotalCharge;
            }
            else
            {
              dr["TotalCharge"] = 0.0m;
            }
            dr["TotalReceived"] = n.TotalReceived;
            dr["BalanceDue"] = balanceDue;

            try
            {
              msgDate = n.LetterPrintedDate;
              if (msgDate == null || msgDate == Convert.ToDateTime("1/1/1900"))
              {
                dr["LetterPrintedDate"] = DateTime.MinValue;
              }
              else
              {
                dr["LetterPrintedDate"] = msgDate;
              }
            }
            catch
            {
              dr["LetterPrintedDate"] = DateTime.MinValue;
            }

            try
            {
              msgDate = n.LetterSentDate;
              if (msgDate == null || msgDate == Convert.ToDateTime("1/1/1900"))
              {
                dr["LetterSentDate"] = DateTime.MinValue;
              }
              else
              {
                dr["LetterSentDate"] = msgDate;
              }
            }
            catch
            {
              dr["LetterSentDate"] = DateTime.MinValue;
            }

            dashboardDataset.tblNonDeposit.Rows.Add(dr);
          }
        }
      }
    }
    #endregion

    #region Remove Final Payment Record
    /// <summary>
    /// Removes the final payment if a reservation is in bother
    /// final payment and non-deposit.
    /// </summary>
    public void RemoveFinalPayment()
    {
      var final = (from f in dashboardDataset.tblFinalPayment
                   select f.ReservationNumber).ToArray();
      var deposit = (from n in dashboardDataset.tblNonDeposit
                     select n.ReservationNumber).ToArray();

      //find reservations that are both in final and non-deposit
      var commonNumbers = final.Intersect(deposit);
      foreach (var r in commonNumbers)
      {
        DataRow dr;
        //remove reservation from the non-deposit datatable
        dr = dashboardDataset.tblNonDeposit.Rows.Find(r);
        dashboardDataset.tblNonDeposit.Rows.Remove(dr);
        //remove reservation from the final datatable
        //dr = dashboardDataset.tblFinalPayment.Rows.Find(r);
        //dashboardDataset.tblFinalPayment.Rows.Remove(dr);
      }
    }
    #endregion

    #region Fill Final Payment and Non-Deposit
    /// <summary>
    /// Fills the final and non deposit table by union the FinalPayment and
    /// Non-deposit tables in the dataset.
    /// </summary>
    public void FillFinalAndNonDeposit()
    {
      DataRow dr;
      DateTime currentDate = DateTime.Today;
      TimeSpan ts;

      var finalAndNonDeposit = (from c in dashboardDataset.tblFinalPayment
                                select new
                                {
                                  c.ReservationLineItemID,
                                  c.ItineraryID,
                                  c.ClientID,
                                  c.FirstName,
                                  c.LastName,
                                  c.UserID,
                                  c.UserName,
                                  c.EMailAddress,
                                  c.PastOrFuture,
                                  LastContact = string.Empty,
                                  LeadScoreID = 99,
                                  LeadScoreDescription = string.Empty,
                                  Status = string.Empty,
                                  DepositDueDate = string.Empty,
                                  c.FinalDueAmt,
                                  DepositDueAmt = 0.0m,
                                  c.CurrencySymbol,
                                  c.ReservationNumber,
                                  c.DateFrom,
                                  c.DateTo,
                                  c.Villa,
                                  DetailInfo = string.Empty,
                                  RCBDayPast = 0,
                                  c.Kind,
                                  Destination = string.Empty,
                                  c.PaymentDueDate,
                                  c.TotalCharge,
                                  c.TotalReceived,
                                  c.BalanceDue,
                                  c.LetterPrintedDate,
                                  c.LetterSentDate

                                }).Union
                          (from c in dashboardDataset.tblNonDeposit
                           select new
                           {
                             c.ReservationLineItemID,
                             c.ItineraryID,
                             c.ClientID,
                             c.FirstName,
                             c.LastName,
                             c.UserID,
                             c.UserName,
                             c.EMailAddress,
                             c.PastOrFuture,
                             LastContact = string.Empty,
                             LeadScoreID = 99,
                             LeadScoreDescription = string.Empty,
                             Status = string.Empty,
                             c.DepositDueDate,
                             FinalDueAmt = 0.0m,
                             c.DepositDueAmt,
                             c.CurrencySymbol,
                             c.ReservationNumber,
                             c.DateFrom,
                             c.DateTo,
                             c.Villa,
                             DetailInfo = string.Empty,
                             RCBDayPast = 0,
                             c.Kind,
                             Destination = string.Empty,
                             c.PaymentDueDate,
                             c.TotalCharge,
                             c.TotalReceived,
                             c.BalanceDue,
                             c.LetterPrintedDate,
                             c.LetterSentDate
                           });

      if (finalAndNonDeposit != null)
      {
        foreach (var f in finalAndNonDeposit)
        {
          dr = null;
          dr = dashboardDataset.tblFinalAndNonDeposit.NewRow();

          dr["ReservationLineItemID"] = f.ReservationLineItemID;
          dr["ItineraryID"] = f.ItineraryID;
          dr["ReservationNumber"] = f.ReservationNumber;
          dr["ClientID"] = f.ClientID;
          dr["FirstName"] = f.FirstName;
          dr["LastName"] = f.LastName;
          dr["EMailAddress"] = f.EMailAddress;
          dr["DateFrom"] = f.DateFrom;
          dr["DateTo"] = f.DateTo;
          dr["Villa"] = f.Villa;
          dr["UserID"] = f.UserID;
          dr["UserName"] = f.UserName;
          dr["CurrencySymbol"] = f.CurrencySymbol;
          if (f.PaymentDueDate != null)
          {
            dr["PaymentDueDate"] = f.PaymentDueDate;
          }

          if (f.PaymentDueDate == null)
          {
            dr["NumberOfDayDue"] = 0;
          }
          else
          {
            ts = Convert.ToDateTime(f.PaymentDueDate) - currentDate;
            dr["NumberOfDayDue"] = ts.Days;
          }

          dr["BalanceDue"] = f.BalanceDue;
          dr["TotalCharge"] = f.TotalCharge;
          dr["TotalReceived"] = f.TotalReceived;
          dr["Kind"] = f.Kind; if (f.LetterPrintedDate != null)
          {
            if (f.LetterPrintedDate == DateTime.MinValue)
            {
              dr["LetterPrintedDate"] = DBNull.Value;
            }
            else
            {
              dr["LetterPrintedDate"] = f.LetterPrintedDate;
            }
          }
          if (f.LetterSentDate != null)
          {
            if (f.LetterSentDate == DateTime.MinValue)
            {
              dr["LetterSentDate"] = DBNull.Value;
            }
            else
            {
              dr["LetterSentDate"] = f.LetterSentDate;
            }
          }


          try
          {
            //include duplicate clients so it can be count all records
            //dashboardDataset.tblClient1.Rows.Add(dr);
            //unique clients for individual RA because it has primary keys
            dashboardDataset.tblFinalAndNonDeposit.Rows.Add(dr);
          }
          catch (ConstraintException ex)
          {
            //do nothing when there are duplicate client
          }
        }
      }
    }
    #endregion

    #region Sales Management Board
    /// <summary>
    /// Fills the sales managermnet board dataset by calling the Web Service method.
    /// </summary>
    public void FillSalesManagermnetBoard(DateTime startDate, DateTime endDate)
    {
      DataRow dr;
      int? emailTraffic = 0;
      string opportunity = string.Empty;
      DateTime? emailDate;
      string status = string.Empty;
      //DataSet ds = null;
      // int userID = 0;

      // declare the web service

      try
      {
        this.UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
      }
      catch
      {
        this.UserID = 0;
      }

      BusinessLogic.WimcoBusinessLogic biz = new BusinessLogic.WimcoBusinessLogic();
      BusinessLogic.ReservationsDS smbData = new BusinessLogic.ReservationsDS();
      smbData = biz.Reports_GetSMBData(authKey, startDate, endDate, this.UserID);

      foreach (DataRow r in smbData.Tables[0].Rows)
      {
        dr = null;
        status = r.Field<string>("Status");
        if (status != "BKD")
        {
          dr = salesMgrBoardDatabaset.tblSalesManagementBoard.NewRow();
          dr["ClientID"] = r.Field<Int32>("ClientID");
          dr["ClientName"] = r.Field<string>("ClientName");
          dr["UserID"] = this.UserID;
          dr["UserName"] = r.Field<string>("Agent");
          dr["HQIDate"] = r.Field<DateTime>("HQIDate");
          dr["Destination"] = r.Field<string>("Destination");
          dr["VillaCode"] = r.Field<string>("Villa Code");
          dr["TravelDate"] = r.Field<string>("TravelDates");
          dr["FCCNote"] = r.Field<string>("LastFCCNote");
          //dr["FCCCallbackDate"] = Convert.IsDBNull(r.Field<DateTime>("LastFCCDate")) ? DBNull.Value : r.Field<DateTime>("LastFCCDate");
          try
          {
            dr["FCCCallbackDate"] = r.Field<DateTime>("LastFCCDate");
          }
          catch
          {
            dr["FCCCallbackDate"] = DBNull.Value;
          }

          try
          {
            emailTraffic = r.Field<int>("EmailTraffic");
            dr["EmailTraffic"] = emailTraffic;
          }
          catch
          {
            dr["EmailTraffic"] = 0;
            emailTraffic = 0;
          }

          try
          {
            opportunity = r.Field<string>("OP");
          }
          catch
          {
            opportunity = string.Empty;
          }

          if (opportunity == op1 || emailTraffic == redAlert)
          {
            leadAtOp1OrRedAlert++;
          }

          dr["OP"] = opportunity;

          dr["Status"] = r.Field<string>("Status");
          try
          {
            dr["LastEmailInDate"] = r.Field<DateTime>("LastEmailInDate");
          }
          catch
          {
            dr["LastEmailInDate"] = DBNull.Value;
          }
          try
          {
            emailDate = r.Field<DateTime>("LastEmailOutDate");
            if (emailDate == null || emailDate == DateTime.MinValue)
            {
              dr["LastEmailOutDate"] = DBNull.Value;
            }
            else
            {
              dr["LastEmailOutDate"] = emailDate;
            }
          }
          catch
          {
            dr["LastEmailOutDate"] = DBNull.Value;
          }

          //dr["EmailTraffic"] = r.Field<int>("EmailTraffic");
          dr["SecureJumpClient"] = r.Field<string>("SecureJumpClient");
          dr["FCCType"] = r.Field<string>("FCCKindVerbose");
          salesMgrBoardDatabaset.tblSalesManagementBoard.Rows.Add(dr);
        }
      }

    }

    #endregion

    #region Returning Client Callback data table
    /// <summary>
    /// Fills the returning client callback data table.
    /// </summary>
    /// <param name="magnumDb">The magnum db.</param>
    /// <param name="startDate">The start date.</param>
    private void FillReturningClientCallback(MagnumDataContext magnumDb, DateTime startDate)
    {
      DataRow dr;
      DateTime startDateClientCallback = startDate.AddDays(-7);
      TimeSpan ts;
      //DateTime endDateClienCallback = endDate.AddDays(-7);
      try
      {
        var clientCallbackData = magnumDb.spDashboardClientCallback(this.UserName).OrderBy(c => c.DateTo);
        foreach (var c in clientCallbackData)
        {
          dr = null;
          dr = dashboardDataset.tblClientCallback.NewRow();
          dr["ReservationLineItemID"] = c.ReservationLineItemID;
          dr["ReservationNumber"] = c.ReservationNumber;
          dr["ClientID"] = c.ClientID;
          dr["FirstName"] = c.FirstName;
          dr["LastName"] = c.LastName;
          dr["EMailAddress"] = c.EMailAddress;
          dr["DateFrom"] = c.DateFrom;
          dr["DateTo"] = c.DateTo;
          dr["Villa"] = c.Villa;
          dr["UserID"] = c.UserID;
          dr["UserName"] = c.UserName;
          dr["CurrencySymbol"] = c.CurrencySymbol;
          dr["TotalReceived"] = c.TotalReceived;
          //if the end travel date is greater then or equals to current date - 7 and 
          //less then current date, it is not past yet.
          dr["PastOrFuture"] = (c.DateTo >= startDateClientCallback && c.DateTo < startDate) ? "N" : "Y";
          dr["FinalDueAmt"] = 0.0m;
          dr["DepositDueAmt"] = 0.0m;
          ts = startDateClientCallback - Convert.ToDateTime(c.DateTo);
          dr["RCBDayPast"] = ts.Days;
          dr["Kind"] = returningClientCallbackKind;
          dr["DestinationID"] = c.DestinationID;
          if (ts.Days > 0)
          {
            dr["CallbackDate"] = startDate.AddDays(-ts.Days);
            dr["CallbackStatus"] = "Past due";
            pastDueRccb++;
          }
          else
          {
            dr["CallbackDate"] = startDate;
            dr["CallbackStatus"] = "To do now";
            rccbInSevenDay++;
          }
          dashboardDataset.tblClientCallback.Rows.Add(dr);
        }
      }
      catch
      {
        //TODO: have a better handle exception
      }

      //gridControl1.DataSource = dashboardDataset.tblClientCallback;

    }
    #endregion

    #region Future Client Callback data table
    /// <summary>
    /// Fills the future client callback data table.
    /// </summary>
    /// <param name="magnumDb">The magnum db.</param>
    /// <param name="startDate">The start date.</param>
    /// <param name="endDate">The end date.</param>
    public void FillFutureClientCallback(MagnumDataContext magnumDb, DateTime startFCCDate, DateTime endFCCDate)
    {
      string temp = string.Empty;
      DateTime callbackDate;
      DateTime fccCreateDate;
      int? fccContactSourceID = null;
      DateTime? lastTravelDate = null;
      DateTime? lastEmailSent = null;
      DateTime currentDate = Convert.ToDateTime(DateTime.Today.ToShortDateString());
      DateTime pastDate = currentDate.AddDays(-7);
      string checkState;

      // delele all records in table.
      dashboardDataset.tblFCC.Clear();

      Func<int, string, bool> isTooLong = (int x, string s) => s.Length > x;

      //var FCCData = (magnumDb.spDashboardFCC_New(startFCCDate, endFCCDate, this.UserName).Where(s => s.CheckState != "Y").OrderBy(t => t.ContactSourceName).OrderByDescending(d => d.CallbackDate)
      //         .GroupBy(g => g.ClientID).Select(g => g.FirstOrDefault())).ToList();

      var FCCData = (magnumDb.spDashboardFCC_New(startFCCDate, endFCCDate, this.UserName)).ToList();


      if (FCCData != null)
      {
        foreach (var f in FCCData)
        {
          DataRow dr;
          dr = (dsDashboard.tblFCCRow)dashboardDataset.tblFCC.NewRow();
          dr["CallbackID"] = f.CallbackID;
          dr["CallbackDate"] = f.CallbackDate;
          fccCreateDate = Convert.ToDateTime(f.CreateDate);
          /*
          if (fccCreateDate >= pastDate) // lead that have claimed 7 days ago.
          {
            leadClaimedInSevenDay++;
          }
          */
          dr["CreateDate"] = fccCreateDate;
          dr["ClientID"] = f.ClientID;
          dr["FirstName"] = f.FirstName;
          dr["LastName"] = f.LastName;
          dr["EMailAddress"] = f.EMailAddress;
          dr["UserID"] = f.UserID;
          dr["UserName"] = f.UserName;
          dr["OrderNumber"] = f.OrderNumber;
          dr["LeadScoreID"] = f.LeadScoreID;
          dr["LeadScoreDescription"] = f.LeadScoreDescription;
          dr["Remarks"] = f.Remarks;
          // "P" - past FCC, "C" - current FCC, and "N" - Future FCC
          callbackDate = Convert.ToDateTime(f.CallbackDate);
          if (callbackDate == currentDate)
          {
            temp = "C";
          }
          else
          {
            if (callbackDate < currentDate)
            {
              temp = "P";
            }
            else
            {
              //ML -added on 9/26/2012 to fix future FCCs showing as past FCCs.
              if (callbackDate > currentDate)
              {
                temp = "N";
              }
            }

          }

          dr["PastOrFuture"] = temp;
          dr["FinalDueAmt"] = 0.0m;
          dr["DepositDueAmt"] = 0.0m;
          dr["Kind"] = futureClientCallbackKind;
          dr["Destination"] = f.LocaleName;
          dr["Color"] = f.Color;

          lastTravelDate = f.LastTravelDate;
          if (lastTravelDate == Convert.ToDateTime("1/1/1900"))
          {
            dr["LastTravelDate"] = DBNull.Value;
          }
          else
          {
            dr["LastTravelDate"] = lastTravelDate;
          }

          lastEmailSent = f.LastEmailSent;
          if (lastEmailSent == null)
          {
            dr["LastEmailSent"] = DBNull.Value;
          }
          else
          {
            dr["LastEmailSent"] = lastEmailSent;
          }
          fccContactSourceID = f.ContactSourceID;
          if (fccContactSourceID == null)
          {
            //dr["FCCContactSourceID"] = DBNull.Value;
            dr["FCCContactSourceID"] = 0;
            dr["FCCType"] = string.Empty;
          }
          else
          {
            dr["FCCContactSourceID"] = fccContactSourceID;
            dr["FCCType"] = f.ContactSourceCode;
          }
          checkState = f.CheckState;
          dr["DisplayRadioButtonList"] = checkState;
          dr["PreviousState"] = f.PreviousState;
          dashboardDataset.tblFCC.Rows.Add(dr);
          //ML - added on 9/26/2012
          temp = string.Empty;
        }
      }
    }
    #endregion

    #region Fill Reservation data table
    public void FillReservation(MagnumDataContext magnumDb, string userName)
    {
      DataRow dr;
      DateTime? msgDate = null;
      DateTime? dateBooked = null;
      DateTime? statusDate = null;

      //call the stored procedure and store the result in a list
      var reservationData = magnumDb.spDashboardStatusRLI(userName);
      if (reservationData != null)
      {
        foreach (var r in reservationData)
        {
          dr = null;
          dr = dashboardDataset.tblReservation.NewRow();
          dr["ReservationLineItemID"] = r.ReservationLineItemID;
          dr["ItineraryID"] = r.ItineraryID;
          dr["ReservationNumber"] = r.ReservationNumber;
          dr["ClientID"] = r.ClientID;
          dr["FirstName"] = r.FirstName;
          dr["LastName"] = r.LastName;
          dr["EMailAddress"] = r.EMailAddress;
          dr["DateFrom"] = r.DateFrom;
          dr["DateTo"] = r.DateTo;
          dr["Villa"] = r.VillaName;
          dr["Status"] = r.Status;
          dr["UserID"] = r.UserID;
          dr["UserName"] = r.UserName;
          dr["FullName"] = r.FullName;
          dr["CurrencySymbol"] = r.CurrencySymbol;
          dr["GrossPrice"] = r.GrossPrice;
          dr["DepositDueDate"] = r.DepositDueDate.ToString();
          dr["Kind"] = rliKind;
          dr["DateEntered"] = r.DateEntered;
          dr["VendorBaseID"] = r.VendorBaseID;
          dr["ProductBaseID"] = r.ProductBaseID;
          //dr["StatusDate"] = r.StatusDate;
          statusDate = r.StatusDate;
          if (statusDate == Convert.ToDateTime("1/1/1900"))
          {
            dr["statusDate"] = DBNull.Value;
          }
          else
          {
            dr["statusDate"] = statusDate;
          }

          msgDate = r.LetterPrintedDate;
          if (msgDate == Convert.ToDateTime("1/1/1900"))
          {
            dr["LetterPrintedDate"] = DBNull.Value;
          }
          else
          {
            dr["LetterPrintedDate"] = msgDate;
          }
          msgDate = r.LetterSentDate;
          if (msgDate == Convert.ToDateTime("1/1/1900"))
          {
            dr["LetterSentDate"] = DBNull.Value;
          }
          else
          {
            dr["LetterSentDate"] = msgDate;
          }
          dr["Car"] = r.Car;
          if (r.CarRequest != null)  // if there is something has been entered, assign Y as well
          {
            if (r.CarRequest.Length > 0)
            {
              dr["Car"] = 'Y';
            }
          }
          dr["Arrival"] = r.Arrival;
          dr["CellularPhoneNumber"] = r.CellularPhoneNumber;
          dr["AdditionalPassenger"] = r.AdditionalPassenger;
          dateBooked = r.DateBooked;
          try
          {
            dr["DateBooked"] = dateBooked;
          }
          catch
          {
            dr["DateBooked"] = DBNull.Value;
          }
          dr["ReservationType"] = r.ReservationType;
          dr["NetOnHand"] = r.NetOnHand;

          dashboardDataset.tblReservation.Rows.Add(dr);
        }
      }
    }

    #endregion

    #region Fill Reservation Waitlist data table
    public void FillReservationWaitList(MagnumDataContext magnumDb, string userName)
    {
      DataRow dr;
      DateTime currentDate = DateTime.Today;
      DateTime? msgDate = null;
      List<string> status = new List<string>() { "INQ", "WTL", "WTX", "OKD", "NOK" };
      string vendorBaseID = ";";
      string productBaseID = ";";
      int temp = 0;
      TimeSpan day;
      int numberDay;
      DateTime eachDay;
      bool available = true;


      //Select all reservations that have the following status: "INQ", "WTL", "WTX", "OKD", "NOK"
      //from the dataset
      var resvWaitlistData = from w in dashboardDataset.tblReservation.AsEnumerable()
                             where w.UserName == userName && status.Contains(w.Status)
                             select w;

      if (resvWaitlistData != null)
      {
        // add to dataset
        foreach (var r in resvWaitlistData)
        {
          dr = null;
          dr = dashboardDataset.tblReservationWaitlist.NewRow();
          dr["ReservationLineItemID"] = r.ReservationLineItemID;
          dr["ItineraryID"] = r.ItineraryID;
          dr["ReservationNumber"] = r.ReservationNumber;
          dr["ClientID"] = r.ClientID;
          dr["FirstName"] = r.FirstName;
          dr["LastName"] = r.LastName;
          dr["EMailAddress"] = r.EMailAddress;
          dr["DateFrom"] = r.DateFrom;
          dr["DateTo"] = r.DateTo;
          dr["Villa"] = r.Villa;
          dr["Status"] = r.Status;
          dr["UserID"] = r.UserID;
          dr["UserName"] = r.UserName;
          dr["CurrencySymbol"] = r.CurrencySymbol;
          dr["GrossPrice"] = r.GrossPrice;
          dr["DepositDueDate"] = r.DepositDueDate.ToString();
          dr["Kind"] = rliKind;
          dr["DateEntered"] = r.DateEntered;
          dr["VendorBaseID"] = r.VendorBaseID;
          dr["ProductBaseID"] = r.ProductBaseID;
          dr["StatusDate"] = r.StatusDate;
          //change row color if a resv is not a waitlist resv
          if (r.Status != "WTL" && r.Status != "WTX")
          {
            // if status date has not been changed within 2 days, change background color to yellow
            if (currentDate > r.StatusDate.AddDays(1))
            {
              dr["RowColor"] = "yellow";
            }
            else
            {
              dr["RowColor"] = "black";
            }
            dr["NetOnHand"] = 0.0m;
          }
          else
          {
            dr["RowColor"] = "black";
            dr["NetOnHand"] = r.NetOnHand;
          }

          dashboardDataset.tblReservationWaitlist.Rows.Add(dr);
        }

        // get all vendor base ID and product base ID for waitlist resvs
        var resvWaitListData = (from p in dashboardDataset.tblReservationWaitlist.AsEnumerable()
                                where p.Status == "WTL" || p.Status == "WTX"
                                orderby p.VendorBaseID, p.ProductBaseID
                                select p).ToList();

        if (resvWaitListData != null)
        {
          foreach (var p in resvWaitListData)
          {
            // get on unique vendor base ID/product base ID
            // and format vendor base ID as ;7684;8025;... and product base ID as ;902711;902712;902713;...
            // this will pass to a stored procedure for where clause.
            if (temp != p.VendorBaseID)
            {
              vendorBaseID += $"{p.VendorBaseID};";
              temp = p.VendorBaseID;
            }
            productBaseID += $"{p.ProductBaseID};";
          }
        }

        // make sure that is a vendor base id not empty
        if (vendorBaseID.Length > 1)
        {
          // get all booked reservations for vendor/product in the list
          var resvBKDData = magnumDb.spDashboardBKDRLI(vendorBaseID, productBaseID).ToList();
          if (resvBKDData != null)
          {
            //loop through all wait list reservations
            foreach (var w in resvWaitListData)
            {
              //get begin date
              eachDay = Convert.ToDateTime(w.DateFrom);
              //find a number of day within a reservation
              day = Convert.ToDateTime(w.DateTo) - eachDay;
              // number of days for a waitlist resv.
              numberDay = day.Days;

              // if there is only one day, use >= and <= 
              if (numberDay == 1)
              {
                numberDay++;
              }

              //loop through each day within a reservation and find any reservation
              //between eachDay (WaitListed reservation day) 
              for (int i = 1; i <= numberDay; i++)
              {
                var noWtl = (from n in resvBKDData
                             where
                             ((eachDay > n.DateFrom) && (eachDay < n.DateTo)) &&
                             (n.VendorBaseID == w.VendorBaseID) &&
                             (n.ProductBaseID == w.ProductBaseID)
                             select n).FirstOrDefault();
                //return null if data is not found in the list; therefore, date is available
                if (noWtl == null)
                {
                  available = true;
                }
                else
                {
                  // if there any day is occupied, it considers as not available
                  available = false;
                  break;
                }

                //move to the next day of the waitlist resv
                eachDay = eachDay.AddDays(1);
              }

              //if not available, change the status
              if (available)
              {
                w.RowColor = "green";
              }
              else
              {
                w.RowColor = "black";
              }
            }
          }
        }
      }
    }

    #endregion

    #region Fill In House and Arrival data table
    /// <summary>
    /// Fills the in house and arrival table that will arrive in the 
    /// next 10 days.
    /// </summary>
    /// <param name="magnumDb">The magnum database.</param>
    public void FillInHouseAndArrival(MagnumDataContext magnumDb)
    {
      string kind = "R";
      DataRow dr;

      var reservationData = magnumDb.spDashboardInHouseAndArrival(this.UserName).OrderBy(d => d.DateFrom);

      if (reservationData != null)
      {
        foreach (var r in reservationData)
        {
          dr = null;
          dr = dashboardDataset.tblInHouseAndArrival.NewRow();
          dr["ReservationLineItemID"] = r.ReservationLineItemID;
          dr["ItineraryID"] = r.ItineraryID;
          dr["ReservationNumber"] = r.ReservationNumber;
          dr["ClientID"] = r.ClientID;
          dr["FirstName"] = r.FirstName;
          dr["LastName"] = r.LastName;
          dr["EMailAddress"] = r.EMailAddress;
          dr["DateFrom"] = r.DateFrom;
          dr["DateTo"] = r.DateTo;
          dr["Villa"] = r.VillaName;
          dr["Status"] = r.Status;
          dr["UserID"] = r.UserID;
          dr["UserName"] = r.FullName;
          dr["CurrencySymbol"] = r.CurrencySymbol;
          dr["GrossPrice"] = r.GrossPrice;
          dr["DepositDueDate"] = r.DepositDueDate.ToString();
          dr["Car"] = r.Car;
          if (r.CarRequest != null)  // if there is something has been entered, assign Y
          {
            if (r.CarRequest.Length > 0)
            {
              dr["Car"] = 'Y';
            }
          }
          dr["Arrival"] = r.Arrival;
          dr["CellularPhoneNumber"] = r.CellularPhoneNumber;
          dr["AdditionalPassenger"] = r.AdditionalPassenger;
          dr["Kind"] = rliKind;
          dr["ReservationType"] = r.ReservationType;
          dashboardDataset.tblInHouseAndArrival.Rows.Add(dr);
        }
      }
    }

    #endregion

    #region Client Callback Lead data table
    /// <summary>
    /// Fills the client callback lead table in the Dataset.
    /// </summary>
    /// <param name="magnumDb">The magnum database.</param>
    [Obsolete("No longer being used", false)]
    public void FillClientCallbackLead(MagnumDataContext magnumDb)
    {
      DataRow dr;
      string temp = string.Empty;

      var CCBLeadData = magnumDb.spDashboardCCBLead(this.UserName);
      foreach (var c in CCBLeadData)
      {
        dr = null;
        dr = (dsDashboard.tblClientCallbackLeadRow)dashboardDataset.tblClientCallbackLead.NewRow();

        dr["OrderID"] = c.OrderID;
        dr["ClientID"] = c.ClientID;
        dr["FirstName"] = c.FirstName;
        dr["LastName"] = c.LastName;
        dr["EMailAddress"] = c.EMailAddress;
        dr["LeadScoreID"] = c.LeadScoreID;
        dr["LeadScoreDescription"] = c.LeadScoreDescription;
        dr["OrderNumber"] = c.OrderNumber;
        dr["CreateDate"] = c.CreateDate;
        dr["UserName"] = c.UserName;
        dr["OrderContactSource"] = c.OrderContactSource;
        dr["UserID"] = c.UserID;
        dr["UserName"] = c.UserName;
        dr["Remarks"] = c.Remarks;
        dr["PastOrFuture"] = c.PastOrFuture;
        dr["FinalDueAmt"] = 0.0m;
        dr["DepositDueAmt"] = 0.0m;
        dr["Kind"] = leadKind;
        dr["Destination"] = c.LocaleName;
        dashboardDataset.tblClientCallbackLead.Rows.Add(dr);
      }
    }
    #endregion

    #region Letter data table.
    /// <summary>
    /// Fills the Letter data table.
    /// </summary>
    /// <param name="magnumDb">The magnum db.</param>
    public void FillLetter(MagnumDataContext magnumDb, DateTime startDate, DateTime endDate)
    {
      DataRow dr;
      DateTime? msgDate;
      DateTime currentDate = DateTime.Today;
      TimeSpan ts;
      string letterType;

      // delele all records in table.
      dashboardDataset.tblLetter.Clear();

      // process NonDeposit letter
      // ML - 07/06/2018 : commented out
      /*
      var nonDepositData = magnumDb.spDashboardLetterNonDeposit(startDate, endDate, this.UserName);
      if (nonDepositData != null)
      {
        foreach (var f in nonDepositData)
        {
          dr = null;
          dr = dashboardDataset.tblLetter.NewRow();

          dr["ReservationLineItemID"] = f.ReservationLineItemID;
          dr["ItineraryID"] = f.ItineraryID;
          dr["ReservationNumber"] = f.ReservationNumber;
          dr["ClientID"] = f.ClientID;
          dr["FirstName"] = f.FirstName;
          dr["LastName"] = f.LastName;
          dr["EMailAddress"] = f.EMailAddress;
          dr["DateFrom"] = f.DateFrom;
          dr["DateTo"] = f.DateTo;
          dr["Villa"] = f.VillaCode;
          dr["UserID"] = f.UserID;
          dr["UserName"] = f.UserName;
          dr["CurrencySymbol"] = f.ResvCurrencySymbol;

          if (f.DepositDueDate != null)
          {
            dr["PaymentDueDate"] = f.DepositDueDate;
          }

          if (f.DepositDueDate == null)
          {
            dr["NumberOfDayDue"] = 0;
          }
          else
          {
            ts = Convert.ToDateTime(f.DepositDueDate) - currentDate;
            dr["NumberOfDayDue"] = ts.Days;
          }

          //dr["BalanceDue"] = f.AmountDue;
          dr["BalanceDue"] = f.DepositDue - f.TotalReceived;
          if (f.TotalCharge != null)
          {
            dr["TotalCharge"] = f.TotalCharge;
          }
          else
          {
            dr["TotalCharge"] = 0.0m;
          }
          dr["TotalReceived"] = f.TotalReceived;
          dr["Kind"] = f.Kind;
          msgDate = f.MessageDateTime;
          // the MessageDateTime has been assigned to "1/1/1900", if it is NULL
          if (msgDate == Convert.ToDateTime("1/1/1900"))
          {
            dr["MessageDateTime"] = DBNull.Value;
          }
          else
          {
            dr["MessageDateTime"] = f.MessageDateTime;
          }
          dr["LetterType"] = "Deposit Request";
          dashboardDataset.tblLetter.Rows.Add(dr);
        }
      }
      */

      // process Final Payment letter
      var finalPaymentData = magnumDb.spDashboardLetterFinalPayment(startDate, endDate, this.UserName);

      if (finalPaymentData != null)
        foreach (var f in finalPaymentData)
        {
          dr = null;
          dr = dashboardDataset.tblLetter.NewRow();

          dr["ReservationLineItemID"] = f.ReservationLineItemID;
          dr["ItineraryID"] = f.ItineraryID;
          dr["ReservationNumber"] = f.ReservationNumber;
          dr["ClientID"] = f.ClientID;
          dr["FirstName"] = f.FirstName;
          dr["LastName"] = f.LastName;
          dr["EMailAddress"] = f.EMailAddress;
          dr["DateFrom"] = f.DateFrom;
          dr["DateTo"] = f.DateTo;
          dr["Villa"] = f.VillaCode;
          dr["UserID"] = f.UserID;
          dr["UserName"] = f.UserName;
          dr["CurrencySymbol"] = f.ResvCurrencySymbol;

          if (f.FinalDueDate != null)
          {
            dr["PaymentDueDate"] = f.FinalDueDate;
          }

          if (f.FinalDueDate == null)
          {
            dr["NumberOfDayDue"] = 0;
          }
          else
          {
            ts = Convert.ToDateTime(f.FinalDueDate) - currentDate;
            dr["NumberOfDayDue"] = ts.Days;
          }

          dr["BalanceDue"] = f.AmountDue;
          dr["TotalCharge"] = f.TotalCharge;
          dr["TotalReceived"] = f.TotalReceived;
          dr["Kind"] = f.Kind;
          try
          {
            msgDate = f.LetterPrintedDate;
            if (msgDate == null || msgDate == Convert.ToDateTime("1/1/1900"))
            {
              dr["LetterPrintedDate"] = string.Empty;
            }
            else
            {
              dr["LetterPrintedDate"] = msgDate;
            }
          }
          catch
          {
            dr["LetterPrintedDate"] = string.Empty;
          }

          try
          {
            msgDate = f.LetterSentDate;
            if (msgDate == null || msgDate == Convert.ToDateTime("1/1/1900"))
            {
              dr["LetterSentDate"] = string.Empty;
            }
            else
            {
              dr["LetterSentDate"] = msgDate;
            }
          }
          catch
          {
            dr["LetterSentDate"] = string.Empty;
          }
          letterType = f?.LetterType ?? string.Empty;
          if (letterType.Contains("Magnum - "))
          {
            letterType = letterType.Remove(0, 9);
          }
          dr["LetterType"] = letterType;
          dashboardDataset.tblLetter.Rows.Add(dr);
        }


      // process Revision Letter
      // Revision Process has to call a method to determine what kind of a letter will be produced.
      ProcessLetterRevision letterRevision;

      // get revision reservations.  There can be multiple lines of a reservation because of including DepositLineItem
      var revisionLetterData = magnumDb.spDashboardLetterRevision(this.UserName).ToDataSet("Table");

      if (revisionLetterData != null)
      {
        // pass the Dataset to the class constructor
        letterRevision = new ProcessLetterRevision(revisionLetterData);

        // get unique reservations which is exclude the DepositLineItem
        var resvData = (from r in revisionLetterData.Tables[0].AsEnumerable()
                        select new
                        {

                          ReservationLineItemID = r.Field<int>("ReservationLineItemID"),
                          ItineraryID = r.Field<int>("ItineraryID"),
                          ReservationNumber = r.Field<string>("ReservationNumber"),
                          ClientID = r.Field<int>("ClientID"),
                          FirstName = r.Field<string>("FirstName"),
                          LastName = r.Field<string>("LastName"),
                          EMailAddress = r.Field<string>("EMailAddress"),
                          DateFrom = r.Field<DateTime>("DateFrom"),
                          DateTo = r.Field<DateTime>("DateTo"),
                          VillaCode = r.Field<string>("VillaCode"),
                          UserID = r.Field<int>("UserID"),
                          UserName = r.Field<string>("UserName"),
                          ResvCurrencySymbol = r.Field<string>("ResvCurrencySymbol"),
                          AmountDue = r.Field<decimal>("AmountDue"),
                          TotalCharge = r.Field<decimal>("TotalCharge"),
                          TotalReceived = r.Field<decimal>("TotalReceived"),
                          Kind = r.Field<string>("Kind"),
                          LetterPrintedDate = r.Field<DateTime>("LetterPrintedDate"),
                          LetterSentDate = r.Field<DateTime>("LetterSentDate"),
                          LetterType = r.Field<string>("LetterType")
                        }).Distinct().ToList();

        if (resvData != null)
        {
          foreach (var r in resvData)
          {
            dr = null;
            dr = dashboardDataset.tblLetter.NewRow();

            dr["ReservationLineItemID"] = r.ReservationLineItemID;
            dr["ItineraryID"] = r.ItineraryID;
            dr["ReservationNumber"] = r.ReservationNumber;
            dr["ClientID"] = r.ClientID;
            dr["FirstName"] = r.FirstName;
            dr["LastName"] = r.LastName;
            dr["EMailAddress"] = r.EMailAddress;
            dr["DateFrom"] = r.DateFrom;
            dr["DateTo"] = r.DateTo;
            dr["Villa"] = r.VillaCode;
            dr["UserID"] = r.UserID;
            dr["UserName"] = r.UserName;
            dr["CurrencySymbol"] = r.ResvCurrencySymbol;

            //if (r.PaymentDueDate != null)
            //{
            //  dr["PaymentDueDate"] = r.PaymentDueDate;
            //}


            dr["PaymentDueDate"] = DBNull.Value;

            //if (r.PaymentDueDate == null)
            //{
            //  dr["NumberOfDayDue"] = 0;
            //}
            //else
            //{
            //  ts = Convert.ToDateTime(r.PaymentDueDate) - currentDate;
            //  dr["NumberOfDayDue"] = ts.Days;
            //}

            dr["NumberOfDayDue"] = 0;

            dr["BalanceDue"] = r.AmountDue;
            dr["TotalCharge"] = r.TotalCharge;
            dr["TotalReceived"] = r.TotalReceived;
            dr["Kind"] = r.Kind;

            try
            {
              msgDate = r.LetterPrintedDate;
              if (msgDate == null || msgDate == Convert.ToDateTime("1/1/1900"))
              {
                dr["LetterPrintedDate"] = string.Empty;
              }
              else
              {
                dr["LetterPrintedDate"] = msgDate;
              }
            }
            catch
            {
              dr["LetterPrintedDate"] = string.Empty;
            }

            try
            {
              msgDate = r.LetterSentDate;
              if (msgDate == null || msgDate == Convert.ToDateTime("1/1/1900"))
              {
                dr["LetterSentDate"] = string.Empty;
              }
              else
              {
                dr["LetterSentDate"] = msgDate;
              }
            }
            catch
            {
              dr["LetterSentDate"] = string.Empty;
            }

            // call the method to get letter type
            //dr["LetterType"] = letterRevision.GetRevisionLetter(r.ReservationNumber);
            //remove the "Magnum - " from the letter type.            
            letterType = r.LetterType;
            if (letterType.Contains("Magnum - "))
            {
              letterType = letterType.Remove(0, 9);
            }
            dr["LetterType"] = letterType;
            dashboardDataset.tblLetter.Rows.Add(dr);
          }

        }
      }

    }
    #endregion

    #region Fill Client data table
    /// <summary>
    /// Fills the client data table. Combine all clients into the Client data table
    /// and figure out what kinds of reservations a client has, such as Final Payment, Non-Deposit, Client Callback,
    /// future client callback, etc.
    /// </summary>
    private void FillClient()
    {
      DataRow dr;
      string temp = string.Empty;
      string detailInfo = string.Empty;

      var clientData = (from c in dashboardDataset.tblFinalPayment
                        select new
                        {
                          c.ClientID,
                          c.FirstName,
                          c.LastName,
                          c.UserID,
                          c.UserName,
                          c.EMailAddress,
                          c.PastOrFuture,
                          LastContact = string.Empty,
                          LeadScoreID = 99,
                          LeadScoreDescription = string.Empty,
                          Status = string.Empty,
                          DepositDueDate = string.Empty,
                          c.FinalDueAmt,
                          DepositDueAmt = 0.0m,
                          c.CurrencySymbol,
                          c.ReservationNumber,
                          c.DateFrom,
                          c.DateTo,
                          c.Villa,
                          DetailInfo = string.Empty,
                          RCBDayPast = 0,
                          c.Kind,
                          Destination = string.Empty
                        }).Union
                                (from c in dashboardDataset.tblNonDeposit
                                 select new
                                 {
                                   c.ClientID,
                                   c.FirstName,
                                   c.LastName,
                                   c.UserID,
                                   c.UserName,
                                   c.EMailAddress,
                                   c.PastOrFuture,
                                   LastContact = string.Empty,
                                   LeadScoreID = 99,
                                   LeadScoreDescription = string.Empty,
                                   Status = string.Empty,
                                   c.DepositDueDate,
                                   FinalDueAmt = 0.0m,
                                   c.DepositDueAmt,
                                   c.CurrencySymbol,
                                   c.ReservationNumber,
                                   c.DateFrom,
                                   c.DateTo,
                                   c.Villa,
                                   DetailInfo = string.Empty,
                                   RCBDayPast = 0,
                                   c.Kind,
                                   Destination = string.Empty
                                 }).Union
                                 (from c in dashboardDataset.tblClientCallback
                                  select new
                                  {
                                    c.ClientID,
                                    c.FirstName,
                                    c.LastName,
                                    c.UserID,
                                    c.UserName,
                                    c.EMailAddress,
                                    c.PastOrFuture,
                                    LastContact = string.Empty,
                                    LeadScoreID = 99,
                                    LeadScoreDescription = string.Empty,
                                    Status = string.Empty,
                                    DepositDueDate = string.Empty,
                                    FinalDueAmt = 0.0m,
                                    DepositDueAmt = 0.0m,
                                    c.CurrencySymbol,
                                    c.ReservationNumber,
                                    c.DateFrom,
                                    c.DateTo,
                                    c.Villa,
                                    DetailInfo = string.Empty,
                                    c.RCBDayPast,
                                    c.Kind,
                                    Destination = string.Empty
                                  }).Union
                                 (from c in dashboardDataset.tblFCC
                                  select new
                                  {
                                    c.ClientID,
                                    c.FirstName,
                                    c.LastName,
                                    c.UserID,
                                    c.UserName,
                                    c.EMailAddress,
                                    c.PastOrFuture,
                                    LastContact = c.CreateDate.ToShortDateString(),
                                    c.LeadScoreID,
                                    c.LeadScoreDescription,
                                    Status = string.Empty,
                                    DepositDueDate = string.Empty,
                                    FinalDueAmt = 0.0m,
                                    DepositDueAmt = 0.0m,
                                    CurrencySymbol = string.Empty,
                                    ReservationNumber = string.Empty,
                                    DateFrom = DateTime.MinValue,
                                    DateTo = DateTime.MinValue,
                                    Villa = string.Empty,
                                    DetailInfo = string.Empty,
                                    RCBDayPast = 0,
                                    c.Kind,
                                    c.Destination
                                  }).Union
                                 (from c in dashboardDataset.tblClientCallbackLead
                                  select new
                                  {
                                    c.ClientID,
                                    c.FirstName,
                                    c.LastName,
                                    c.UserID,
                                    c.UserName,
                                    c.EMailAddress,
                                    c.PastOrFuture,
                                    LastContact = c.CreateDate.ToShortDateString(),
                                    c.LeadScoreID,
                                    c.LeadScoreDescription,
                                    Status = string.Empty,
                                    DepositDueDate = string.Empty,
                                    FinalDueAmt = 0.0m,
                                    DepositDueAmt = 0.0m,
                                    CurrencySymbol = string.Empty,
                                    ReservationNumber = string.Empty,
                                    DateFrom = DateTime.MinValue,
                                    DateTo = DateTime.MinValue,
                                    Villa = string.Empty,
                                    DetailInfo = string.Empty,
                                    RCBDayPast = 0,
                                    c.Kind,
                                    c.Destination
                                  }).Union
                                (from c in dashboardDataset.tblReservation
                                 select new
                                 {
                                   c.ClientID,
                                   c.FirstName,
                                   c.LastName,
                                   c.UserID,
                                   c.UserName,
                                   c.EMailAddress,
                                   c.PastOrFuture,
                                   LastContact = string.Empty,
                                   LeadScoreID = 99,
                                   LeadScoreDescription = string.Empty,
                                   c.Status,
                                   c.DepositDueDate,
                                   FinalDueAmt = 0.0m,
                                   DepositDueAmt = 0.0m,
                                   CurrencySymbol = string.Empty,
                                   c.ReservationNumber,
                                   c.DateFrom,
                                   c.DateTo,
                                   c.Villa,
                                   DetailInfo = string.Empty,
                                   RCBDayPast = 0,
                                   c.Kind,
                                   Destination = string.Empty
                                 });

      //add all client to the client table within the dataset
      foreach (var c in clientData)
      {
        dr = null;
        int len;
        int leadScoreID = 99;
        decimal final = 0.0m;
        decimal deposit = 0.0m;
        string lastContact = DateTime.MinValue.ToShortDateString();
        //string DateTemp;

        var kindData = from k in clientData
                       where k.ClientID == c.ClientID
                       select k;
        //find out what kind of data each client has (D, F, P, N, R, I, W, B, L)
        foreach (var t in kindData)
        {
          if (!temp.Contains(t.Kind.ToString())) //if not already contain (D, F, P, N, R, I, W, B, L) add it
          {
            temp += t.Kind + ",";
          }
          //if it is FCC
          if (t.Kind == futureClientCallbackKind)
          {
            //leadscordID (1-hot, 2-warm, 3-cold)
            if (leadScoreID > t.LeadScoreID)
            {
              leadScoreID = t.LeadScoreID;
            }
            if (lastContact == string.Empty)
            {
              lastContact = DateTime.MinValue.ToShortDateString();
            }
            if (Convert.ToDateTime(lastContact) < Convert.ToDateTime(t.LastContact))
            {
              lastContact = t.LastContact.ToString();
            }

            if (detailInfo.Length == 0)
            {
              detailInfo = $"{t.FirstName} {t.LastName}%0A";
            }
          }
          else
          {
            if (t.Kind == finalPaymentKind)
            {
              final += t.FinalDueAmt;
              detailInfo = $"Reservation Number:  {t.ReservationNumber}%0ABegin Date:  {t.DateFrom:MM/dd/yyyy}%0AEnd Date:  {t.DateTo:MM/dd/yyyy}%0AVilla Name:  {t.Villa}%0AFinal Due Amount:  {t.CurrencySymbol}{t.FinalDueAmt:F2}%0A{t.FirstName} {t.LastName}%0A";
            }
            else
            {
              if (t.Kind == nonDepositKind)
              {
                deposit += t.DepositDueAmt;
                /*
                detailInfo = string.Format("Reservation Number:  {0}%0ABegin Date:  {1}%0AEnd Date:  {2}%0AVilla Name:  {3}%0ADeposit Due Amount:  {4}{5}%0ADeposit Due Date:  {6}%0A{7} {8}%0A",
                  t.ReservationNumber,
                  t.DateFrom.ToShortDateString(),
                  t.DateTo.ToShortDateString(),
                  t.Villa,
                  t.CurrencySymbol,
                  t.DepositDueAmt.ToString("N"),
                  t.DepositDueDate,
                  t.FirstName,
                  t.LastName);
                  */
                detailInfo = $"Reservation Number:  {t.ReservationNumber}%0ABegin Date:  {t.DateFrom:MM/dd/yyyy}%0AEnd Date:  {t.DateTo:MM/dd/yyyy}%0AVilla Name:  {t.Villa}%0AFinal Due Amount:  {t.CurrencySymbol}{t.DepositDueAmt:F2}%0ADeposit Due Date:  {t.DepositDueDate}%0A{t.FirstName} {t.LastName}%0A";

              }
            }
            lastContact = string.Empty;
          }

          if (t.Kind == rliKind || t.Kind == rliInqKind || t.Kind == rliWaitlistKind || t.Kind == rliBkdKind)
          {
            //assign a value if there is no deposit or final payment
            if (detailInfo.Length == 0)
            {
              detailInfo = $"{t.FirstName} {t.LastName}%0A";
            }
          }
          //leads
          if (t.Kind == leadKind)
          {
            if (leadScoreID > t.LeadScoreID)
            {
              leadScoreID = t.LeadScoreID;
            }
            //assign a value if there is no deposit or final payment
            if (detailInfo.Length == 0)
            {
              detailInfo = $"{t.FirstName} {t.LastName}%0A";
            }
          }
        }
        //dr = dashboardDataset.tblClient1.NewRow();
        dr = dashboardDataset.tblClient.NewRow();
        dr["ClientID"] = c.ClientID;
        dr["FirstName"] = c.FirstName;
        dr["LastName"] = c.LastName;
        dr["UserID"] = c.UserID;
        dr["UserName"] = c.UserName;
        dr["EMailAddress"] = c.EMailAddress;
        dr["PastOrFuture"] = c.PastOrFuture;
        switch (leadScoreID)
        {
          case 1:
            dr["LeadScore"] = "1-Hot";
            break;
          case 2:
            dr["LeadScore"] = "2-Warm";
            break;
          case 3:
            dr["LeadScore"] = "3-Cold";
            break;
          default:
            dr["LeadScore"] = "";
            break;
        }
        dr["FinalDueAmt"] = final;
        dr["DepositDueAmt"] = deposit;
        dr["Kind"] = c.Kind;
        dr["DetailInfo"] = detailInfo;
        //take out the last comma
        len = temp.Length;
        if (len > 0)
        {
          dr["AllKind"] = temp.Remove(len - 1, 1);
        }
        else
        {
          dr["AllKind"] = temp;
        }
        dr["lastContact"] = lastContact;
        dr["RCBDayPast"] = c.RCBDayPast;
        dr["Display"] = "Y";
        //when there are more then one kind, display only

        try
        {
          //include duplicate clients so it can be count all records
          //dashboardDataset.tblClient1.Rows.Add(dr);
          dashboardDataset.tblClient.Rows.Add(dr);
          //unique clients for individual RA because it has primary keys
          dashboardDataset.tblClient.ImportRow(dr);
        }
        catch (ConstraintException ex)
        {
          //do nothing when there are duplicate client
        }
        //reset temp variables
        temp = string.Empty;
        detailInfo = string.Empty;
        leadScoreID = 99;
        lastContact = DateTime.MinValue.ToShortDateString();
        final = 0.0m;
        deposit = 0.0m;
      }
    }
    #endregion

    #region Calculate Total Items for each Resv Agent
    /// <summary>
    /// Calculates the total items for each Resv Agent.
    /// as well as grand totals.
    /// </summary>
    void CalculateTotals(MagnumDataContext magnumDb)
    {
      int rowTotal = 0;
      int finalPayment = 0;
      int finalPaymentPast = 0;
      int nonDeposit = 0;
      int nonDepositPast = 0;
      int clientCallback = 0;
      int clientCallbackPast = 0;
      int FccCurrent = 0;
      int FccPast = 0;
      int currentLead = 0;
      int pastLead = 0;
      //int futureFcc = 0;
      int inqRLI = 0;
      int newRLI = 0;
      int bkdRLI = 0;
      int wtlRLI = 0;
      //Calc total numbers of Final Payment, Non-Deposit, Returning Client Callback, and Future Client Callback
      if (dashboardDataset.tblResAgent.Rows.Count == 0)
      {
        FillResvAgent(magnumDb);
      }

      foreach (DataRow r in dashboardDataset.tblResAgent.Rows)
      {
        r.BeginEdit();
        var clientData = from c in dashboardDataset.tblClient
                         where c.UserID == (int)r["UserID"]
                         select c;
        foreach (var c in clientData)
        {
          //Final Payment
          if (c.Kind == finalPaymentKind)
          {
            //if (c.PastOrFuture == "Y")
            //{
            //  finalPaymentPast += 1;
            //}
            //else
            //{
            //  finalPayment += 1;
            //}

            finalPayment++;
          }
          //Non-Deposit
          if (c.Kind == nonDepositKind)
          {
            //if (c.PastOrFuture == "Y")
            //{
            //  nonDepositPast += 1;
            //}
            //else
            //{
            //  nonDeposit += 1;
            //}

            nonDeposit++;
          }
          //Returning Client Callback
          if (c.Kind == returningClientCallbackKind)
          {
            //if (c.PastOrFuture == "Y")
            //{
            //  clientCallbackPast += 1;
            //}
            //else
            //{
            //  clientCallback += 1;
            //}

            clientCallback++;
          }
          //Future Client Callback
          if (c.Kind == futureClientCallbackKind)
          {
            ////if current date FCC, add 1
            //if (c.PastOrFuture == "C")
            //{
            //  FccCurrent += 1;
            //}
            //else
            //{
            //  if (c.PastOrFuture == "P")
            //  {
            //    FccPast += 1;
            //  }
            //  //else
            //  //{
            //  //  if (c.PastOrFuture == "F")
            //  //  {
            //  //    futureFcc += 1;
            //  //  }
            //  //}
            //}

            FccCurrent++;
          }
          //Resv:  INQ, NEW, and Booked
          if (c.Kind == rliInqKind)  //INQ
          {
            inqRLI++;
          }
          else
          {
            if (c.Kind == rliBkdKind) //BKD and NEW as 10/24/2012
            {
              bkdRLI++;
            }
            else
            {
              if (c.Kind == rliWaitlistKind) //WTL
              {
                wtlRLI++;
              }
            }
          }

          //calculate total of leads
          //if (c.Kind == leadKind)
          //{
          //  if (c.PastOrFuture == "C")
          //  {
          //    currentLead++;
          //  }
          //  else
          //  {
          //    if (c.PastOrFuture == "P")
          //    {
          //      pastLead++;
          //    }
          //  }
          //  currentLead++;
          //}
        }

        rowTotal = finalPayment + nonDeposit + clientCallback +
          FccCurrent + inqRLI + bkdRLI + wtlRLI + currentLead;
        r["FinalPayment"] = finalPayment;
        r["FinalPaymentPast"] = finalPaymentPast;
        r["Nondeposit"] = nonDeposit;
        r["NondepositPast"] = nonDepositPast;
        r["ReturnClientCallback"] = clientCallback;
        r["ReturnClientCallbackPast"] = clientCallbackPast;
        r["FCCCurrent"] = FccCurrent;
        r["FCCPast"] = FccPast;
        //r["FutureFCC"] = futureFcc;
        r["InqRLI"] = inqRLI;
        r["NewRLI"] = newRLI;
        r["BkdRLI"] = bkdRLI;
        r["WtlRLI"] = wtlRLI;
        r["CurrentLead"] = currentLead;
        r["PastLead"] = pastLead;
        r["RowTotal"] = rowTotal;
        r.EndEdit();

        finalPayment = 0;
        finalPaymentPast = 0;
        nonDeposit = 0;
        nonDepositPast = 0;
        clientCallback = 0;
        clientCallbackPast = 0;
        FccCurrent = 0;
        FccPast = 0;
        //futureFcc = 0;
        inqRLI = 0;
        newRLI = 0;
        bkdRLI = 0;
        wtlRLI = 0;
        currentLead = 0;
        pastLead = 0;
      }
    }
    #endregion

    #region Fill Final payment and non-deposit table
    /// <summary>
    /// Finals the and non deposit.  This method is not used.
    /// Call stored procedure that returns the UNION of Final Payment and NonDeposit
    /// </summary>
    /// <param name="magnumDb">The magnum database.</param>
    /// <param name="startDate">The start date.</param>
    /*
    public void FinalAndNonDeposit(MagnumDataContext magnumDb, DateTime startDate)
    {
      DataRow dr;
      DateTime currentDate = DateTime.Today;
      TimeSpan ts;
      var finalAndNonDeposit = from c in magnumDb.spDashboardFinalAndNonDeposit(startDate, this.UserName)
                               orderby c.PaymentDueDate
                               select new
                               {
                                 c.ReservationLineItemID,
                                 c.ReservationNumber,
                                 c.ClientID,
                                 c.FirstName,
                                 c.LastName,
                                 c.EMailAddress,
                                 c.DateFrom,
                                 c.DateTo,
                                 c.Villa,
                                 c.UserID,
                                 c.UserName,
                                 c.CurrencySymbol,
                                 c.PaymentDueDate,
                                 c.BalanceDue,
                                 c.TotalCharge,
                                 c.TotalReceived,
                                 c.Kind,
                               };

      if (finalAndNonDeposit != null)
      {
        foreach (var f in finalAndNonDeposit)
        {
          dr = null;
          dr = dashboardDataset.tblFinalAndNonDeposit.NewRow();

          dr["ReservationLineItemID"] = f.ReservationLineItemID;
          dr["ReservationNumber"] = f.ReservationNumber;
          dr["ClientID"] = f.ClientID;
          dr["FirstName"] = f.FirstName;
          dr["LastName"] = f.LastName;
          dr["EMailAddress"] = f.EMailAddress;
          dr["DateFrom"] = f.DateFrom;
          dr["DateTo"] = f.DateTo;
          dr["Villa"] = f.Villa;
          dr["UserID"] = f.UserID;
          dr["UserName"] = f.UserName;
          dr["CurrencySymbol"] = f.CurrencySymbol;
          if (f.PaymentDueDate != null)
          {
            dr["PaymentDueDate"] = f.PaymentDueDate;
          }
          if (f.PaymentDueDate == null)
          {
            dr["NumberOfDayDue"] = 0;
          }
          else
          {
            ts = Convert.ToDateTime(f.PaymentDueDate) - currentDate;
            dr["NumberOfDayDue"] = ts.Days;
          }
          dr["BalanceDue"] = f.BalanceDue;
          dr["TotalCharge"] = f.TotalCharge;
          dr["TotalReceived"] = f.TotalReceived;
          dr["Kind"] = f.Kind;
          try
          {
            //include duplicate clients so it can be count all records
            //dashboardDataset.tblClient1.Rows.Add(dr);
            //unique clients for individual RA because it has primary keys
            dashboardDataset.tblFinalAndNonDeposit.Rows.Add(dr);
          }
          catch (ConstraintException ex)
          {
            //do nothing when there are duplicate client
          }
        }

      }
    }
    */
    #endregion

    #region Client callback summary data
    public void ClientCallbackSummary(dsDashboard dashboardDataset)
    {
      DataRow dr;
      DateTime currentDate = DateTime.Today;
      DateTime today = currentDate.AddDays(-7);
      DateTime lastWeek = today.AddDays(-7);
      DateTime thirtyDay = today.AddDays(-30);
      DateTime sixtyDay = today.AddDays(-60);
      DateTime ninetyDay = today.AddDays(-90);
      DateTime hundredTwenty = today.AddDays(-120);

      //empty the table
      dashboardDataset.tblClientCallbackSummary.Rows.Clear();

      var clientCallbackData = from c in dashboardDataset.tblClientCallback
                               select c;
      foreach (var c in clientCallbackData)
      {
        dr = null;
        dr = dashboardDataset.tblClientCallbackSummary.NewRow();
        dr["ReservationLineItemID"] = c.ReservationLineItemID;
        dr["ReservationNumber"] = c.ReservationNumber;
        dr["ClientID"] = c.ClientID;
        dr["FirstName"] = c.FirstName;
        dr["LastName"] = c.LastName;
        dr["DateFrom"] = thirtyDay;
        dr["DateTo"] = c.DateTo;
        dr["UserID"] = c.UserID;
        dr["UserName"] = c.UserName;
        dr["Today"] = ((c.DateTo >= today) ? 1 : 0);
        dr["LastWeek"] = ((c.DateTo >= thirtyDay && c.DateTo < lastWeek) ? 1 : 0);
        dr["ThirtyDay"] = ((c.DateTo >= sixtyDay && c.DateTo < thirtyDay) ? 1 : 0);
        dr["SixtyDay"] = ((c.DateTo >= ninetyDay && c.DateTo < sixtyDay) ? 1 : 0);
        dr["NinetyDay"] = ((c.DateTo >= hundredTwenty && c.DateTo < ninetyDay) ? 1 : 0);
        dr["HundredTwentyOrMore"] = ((c.DateTo < hundredTwenty) ? 1 : 0);
        dashboardDataset.tblClientCallbackSummary.Rows.Add(dr);
      }
    }

    #endregion

    #region Fill FCC Type
    public void FillFCCType(MagnumDataContext magnumDb)
    {
      DataRow dr;


      var fccTypeData = magnumDb.spDashboardFCCType();
      dr = dashboardDataset.tblFCCType.NewRow();
      dr["ContactSourceID"] = 0;
      dr["ContactSourceCode"] = "All";
      dr["ContactSourceName"] = "All";
      dashboardDataset.tblFCCType.Rows.Add(dr);

      foreach (var f in fccTypeData)
      {
        dr = null;
        dr = dashboardDataset.tblFCCType.NewRow();
        dr["ContactSourceID"] = f.ContactSourceID;
        dr["ContactSourceCode"] = f.ContactSourceCode;
        dr["ContactSourceName"] = f.ContactSourceName;
        dashboardDataset.tblFCCType.Rows.Add(dr);
      }
    }

    #endregion


    #region Fill Assistant Reservation Agent 
    /// <summary>
    /// Fill the resv agent assist table for assist.
    /// </summary>
    /// <param name="magnumDb">The magnum database.</param>
    /// <param name="salesAssistID">The sales assist identifier.</param>
    public void FillResvAgentAssist(MagnumDataContext magnumDb, int salesAssistID)
    {
      DataRow dr;
      var RAData = magnumDb.spDashboardSalesAssist(salesAssistID);

      foreach (var r in RAData)
      {
        dr = null;
        dr = dashboardDataset.tblRAForAssistant.NewRow();
        dr["UserID"] = r.UserID;
        dr["UserName"] = r.UserName;
        dr["FullName"] = r.FullName;
        dr["EmailAddress"] = r.EmailAddress;
        dashboardDataset.tblRAForAssistant.Rows.Add(dr);
      }

    }
    #endregion;

    #region Fill Hurricane Reservations
    /// <summary>
    /// Fills the hurricane reservations.
    /// </summary>
    /// <param name="magnumDb">The magnum database.</param>
    /// <param name="userName">Name of the user.</param>
    public void FillHurricaneVilla(MagnumDataContext magnumDb, string userName)
    {
      DataRow dr;
      DateTime hurricaneDate = Convert.ToDateTime("9/5/2017");

      var hurricaneVilla = from h in magnumDb.spDashboardHurricaneVilla()
                           from r in dashboardDataset.tblReservation.AsEnumerable()
                           where h.ProductBaseID == r.ProductBaseID &&
                           (r.DateFrom < h.DateTo && r.DateTo > h.DateFrom) &&
                           r.UserName == userName
                           select new { h, r };

      foreach (var v in hurricaneVilla)
      {
        dr = null;
        dr = dashboardDataset.tblHurricaneReservation.NewRow();
        dr["ReservationLineItemID"] = v.r.ReservationLineItemID;
        dr["ItineraryID"] = v.r.ItineraryID;
        dr["ReservationNumber"] = v.r.ReservationNumber;
        dr["ClientID"] = v.r.ClientID;
        dr["FirstName"] = v.r.FirstName;
        dr["LastName"] = v.r.LastName;
        dr["EMailAddress"] = v.r.EMailAddress;
        dr["DateFrom"] = v.r.DateFrom;
        dr["DateTo"] = v.r.DateTo;
        dr["Villa"] = v.r.Villa;
        dr["Status"] = v.r.Status;
        dr["UserID"] = v.r.UserID;
        dr["UserName"] = v.r.UserName;
        dr["FullName"] = v.r.FullName;
        dr["CurrencySymbol"] = v.r.CurrencySymbol;
        dr["GrossPrice"] = v.r.GrossPrice;
        //dr["Kind"] = kind;
        dr["VendorBaseID"] = v.r.VendorBaseID;
        dr["ProductBaseID"] = v.r.ProductBaseID;
        dr["DateHurricaneEnd"] = v.h.DateTo;
        dashboardDataset.tblHurricaneReservation.Rows.Add(dr);
      }
    }
    #endregion


    #region Count Leads that have been claimed in the last 7 days
    public void CountLead()
    {
      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        leadClaimedInSevenDay = magnumDb.spDashboardLead(this.UserName).Count();
      }

    }

    #endregion

    #region Count FCC
    /// <summary>
    /// Counts the FCC.
    /// </summary>
    public void CountFCC()
    {

      DateTime callbackDate;
      DateTime currentDate = DateTime.Today;
      pastDueFccForLead = 0;
      pastDueFccForClientProspect = 0;
      pastDueFccForService = 0;
      pastDueFccForUnassigned = 0;

      var fccData = from c in dashboardDataset.tblFCC.AsEnumerable()
                    where c.UserName == this.UserName
                    select c;

      if (fccData != null)
      {
        foreach (var f in fccData)
        {
          callbackDate = Convert.ToDateTime(f.CallbackDate);
          if (callbackDate == currentDate)
          {
            todayFCC++;
            switch (f.FCCContactSourceID)
            {
              case opTypeLead:
                pendingFccOpp++;
                break;
              case opTypeClientProspect:
                pendingFccClp++;
                break;
              case opTypeService:
                pendingFccSvc++;
                break;
              default:
                pastDueFccForUnassigned++;
                break;
            }

          }
          else
          {
            if (callbackDate < currentDate)
            {

              switch (f.FCCContactSourceID)
              {
                case opTypeLead:
                  pastDueFccForLead++;
                  CompletedFccOpp++;
                  break;
                case opTypeClientProspect:
                  pastDueFccForClientProspect++;
                  CompletedFccClp++;
                  break;
                case opTypeService:
                  pastDueFccForService++;
                  CompletedFccSvc++;
                  break;
                default:
                  pastDueFccForUnassigned++;

                  break;
              }

            }
          }
        }
      }
    }
    #endregion

    public void FillActivitySummary(MagnumDataContext magnumDb, string userName)
    {
      DataRow dr = null;

      int currentMonth = 1;
      int lastMonth = 2;

      int userID = 0;
      string nameOfUser = string.Empty;
      string fullName = string.Empty;

      //Current Month
      string month1 = string.Empty;
      int numberOfBooking1 = 0;
      decimal totalBKD1 = 0;
      decimal totalBUD1 = 0;

      //Previous Month
      string month2 = string.Empty;
      int numberOfBooking2 = 0;
      decimal totalBKD2 = 0;
      decimal totalBUD2 = 0;

      var activitySummaryData = magnumDb.spDashboardActivitySummary(userName);
      if (activitySummaryData != null)
      {
        foreach (var a in activitySummaryData)
        {
          if (a.Month == currentMonth)
          {
            //get month and year values only from the first record
            if (month1 == string.Empty)
            {
              month1 = $"{Convert.ToDateTime(a.DateEntered).ToString("MMM")} {Convert.ToDateTime(a.DateEntered).ToString("yyyy")}";
              userID = (int)a.UserID;
              nameOfUser = a.UserName;
              fullName = a.FullName;
            }
            numberOfBooking1 += 1;
            if (a.Status == "BKD" || a.Status == "NEW")
            {
              totalBKD1 += (decimal)a.GrossPrice;
            }

            if (a.IsDepositPaid == 'Y')
            {
              totalBUD1 += (decimal)a.GrossPrice;
            }
          }
          else
          {
            //get month and year values only from the first record
            if (month2 == string.Empty)
            {
              month2 = $"{Convert.ToDateTime(a.DateEntered).ToString("MMM")} {Convert.ToDateTime(a.DateEntered).ToString("yyyy")}";
              userID = (int)a.UserID;
              nameOfUser = a.UserName;
              fullName = a.FullName;
            }
            numberOfBooking2 += 1;
            if (a.Status == "BKD" || a.Status == "NEW")
            {
              totalBKD2 += (decimal)a.GrossPrice;
            }

            if (a.IsDepositPaid == 'Y')
            {
              totalBUD2 += (decimal)a.GrossPrice;
            }
          }
        }

      }

      if (numberOfBooking1 > 0)
      {
        dr = null;
        dr = dashboardDataset.tblActivitySummary.NewRow();
        dr["UserID"] = userID;
        dr["UserName"] = nameOfUser;
        dr["FullName"] = fullName;
        dr["Month"] = month1;
        dr["NumberOfBooking"] = numberOfBooking1;
        dr["TotalBKD"] = totalBKD1;
        dr["TotalBUD"] = totalBUD1;
        dr["Report"] = currentMonth;
        dashboardDataset.tblActivitySummary.Rows.Add(dr);
      }

      if (numberOfBooking2 > 0)
      {
        dr = null;
        dr = dashboardDataset.tblActivitySummary.NewRow();
        dr["UserID"] = userID;
        dr["UserName"] = nameOfUser;
        dr["FullName"] = fullName;
        dr["Month"] = month2;
        dr["NumberOfBooking"] = numberOfBooking2;
        dr["TotalBKD"] = totalBKD2;
        dr["TotalBUD"] = totalBUD2;
        dr["Report"] = lastMonth;
        dashboardDataset.tblActivitySummary.Rows.Add(dr);
      }
    }


    public void FillActivityCounter(MagnumDataContext magnumDb, DateTime startDate, DateTime endDate)
    {
      // pending FCC
      int pendingFccOpp = 0;
      int pendingFccClp = 0;
      int pendingFccSvc = 0;
      // Completed FCC
      int completedFccOpp = 0;
      int completedFccClp = 0;
      int completedFccSvc = 0;
      // pending Reservation
      int pendingResvInq = 0;
      int pendingResvBkd = 0;
      int pendingResvOkd = 0;

      decimal totalSales = 0.0m;
      decimal totalBuds = 0.0m;

      decimal grossPrice;

      int leadClaimed = 0;
      decimal leadBooked = 0.0m;
      decimal temp = 0.0m;

      //if (dashboardDataset.tblFCC.Rows.Count == 0)
      //{
      //  FillFutureClientCallback(magnumDb, startDate, endDate);
      //}

      if (dashboardDataset.tblResAgent.Rows.Count == 0)
      {
        FillResvAgent(magnumDb);
      }

      //FillFutureClientCallback(magnumDb, startDate, endDate);

      var fccCompletedData = magnumDb.spDashboardFCCActivity(startDate, endDate, this.UserName).ToList();
      var resvData = magnumDb.spDashboardPendingResv(startDate, endDate, this.UserName).ToList();
      var leadData = magnumDb.spDashboardLeadConversion(startDate, endDate, this.UserName).ToList();

      if (this.GroupID == 4 || this.GroupID == dashboardAdmin)  // admin group
      {
        foreach (DataRow a in dashboardDataset.tblResAgent.Rows)
        {
          a.BeginEdit();

          //select resvs for each RA
          var resvDataAdmin = (from r in resvData.AsEnumerable()
                               where r.UserID == a.Field<int>("UserID")
                               select r).ToList();

          if (resvDataAdmin != null)
          {
            foreach (var r in resvDataAdmin)
            {
              if (r.Status == "INQ")
              {
                pendingResvInq++;
              }
              else
              {
                if (r.Status == "OKD")
                {
                  pendingResvOkd++;
                }
                else
                {
                  if (r.Status == "BKD" || r.Status == "NEW")
                  {
                    pendingResvBkd++;
                  }
                }
              }

              grossPrice = Convert.ToDecimal(r.GrossPrice);

              if (r.IsDepositPaid == "Y")
              {
                totalBuds += grossPrice;
              }

              totalSales += grossPrice;
            }
          }

          //add resv info to the res agent datatable
          a["PendingResv"] = $"{pendingResvInq} - {pendingResvOkd} - {pendingResvBkd}";
          a["TotalSales"] = totalSales;
          a["TotalBud"] = totalBuds;

          // Lead data, count number of leads claimed and find out many leads have been comverted to resv.
          if (leadData != null)
          {
            var leadDataAdmin = from l in leadData.AsEnumerable()
                                where l.UserID == a.Field<int>("UserID")
                                select l;
            if (leadDataAdmin != null)
            {
              leadClaimed = leadDataAdmin.Count();
              foreach (var c in leadDataAdmin)
              {
                leadBooked += c.Conversion;
              }

              a["ClaimedLead"] = leadClaimed;
              if (leadClaimed > 0)
              {
                a["ConversionLead"] = (leadBooked / leadClaimed);
              }
              else
              {
                a["ConversionLead"] = 0;
              }
            }
          }

          // Count Pending FCCs (opportunity, Client Prospective, and Service)
          if (dashboardDataset.tblFCC.Rows.Count > 0)
          {
            var fccPendingDataAdmin = from f in dashboardDataset.tblFCC.AsEnumerable()
                                      where f.UserID == a.Field<int>("UserID")
                                      select f;

            if (fccPendingDataAdmin != null)
            {
              foreach (var f in fccPendingDataAdmin)
              {
                if (f.FCCContactSourceID == opTypeLead)
                {
                  pendingFccOpp++;
                }
                else
                {
                  if (f.FCCContactSourceID == opTypeClientProspect)
                  {
                    pendingFccClp++;
                  }
                  else
                  {
                    if (f.FCCContactSourceID == opTypeService)
                    {
                      pendingFccSvc++;
                    }
                  }
                }
              }
              a["PendingFCC"] = $"{pendingFccOpp} - {pendingFccClp} - {pendingFccSvc}";
            }
          }

          //count Completed FCCs (opportunity, Client Prospective, and Service)
          if (fccCompletedData != null)
          {
            var fccDataAdmin = from f in fccCompletedData.AsEnumerable()
                               where f.UserID == a.Field<int>("UserID")
                               select f;

            if (fccDataAdmin != null)
            {
              foreach (var f in fccDataAdmin)
              {
                if (f.ContactSourceID == opTypeLead)
                {
                  completedFccOpp++;
                }
                else
                {
                  if (f.ContactSourceID == opTypeClientProspect)
                  {
                    completedFccClp++;
                  }
                  else
                  {
                    if (f.ContactSourceID == opTypeService)
                    {
                      completedFccSvc++;
                    }
                  }
                }
              }
              a["CompletedFCC"] = $"{completedFccOpp} - {completedFccClp} - {completedFccSvc}";
            }
          }

          //save the changes
          a.EndEdit();

          //reset variables
          pendingResvInq = 0;
          pendingResvBkd = 0;
          pendingResvOkd = 0;

          totalSales = 0.0m;
          totalBuds = 0.0m;

          // lead variables
          leadBooked = 0;

          // FCC variables
          completedFccOpp = 0;
          completedFccClp = 0;
          completedFccSvc = 0;
          pendingFccOpp = 0;
          pendingFccClp = 0;
          pendingFccSvc = 0;
        }
      }
      else
      {
        if (resvData != null)
        {
          var resvAgentData = (from a in dashboardDataset.tblResAgent.AsEnumerable()
                               where a.UserName == this.UserName
                               select a).FirstOrDefault();

          if (resvAgentData != null)
          {

            resvAgentData.BeginEdit();

            // Reservation info
            foreach (var r in resvData)
            {
              if (r.Status == "INQ")
              {
                pendingResvInq++;
              }
              else
              {
                if (r.Status == "OKD")
                {
                  pendingResvOkd++;
                }
                else
                {
                  if (r.Status == "BKD" || r.Status == "NEW")
                  {
                    pendingResvBkd++;
                  }
                }
              }

              grossPrice = Convert.ToDecimal(r.GrossPrice);

              if (r.IsDepositPaid == "Y")
              {
                totalBuds += grossPrice;
              }

              totalSales += grossPrice;
            }

            resvAgentData.PendingResv = $"{pendingResvInq} - {pendingResvOkd} - {pendingResvBkd}";
            resvAgentData.TotalSales = totalSales;
            resvAgentData.TotalBud = totalBuds;

            // Lead data, count number of leads claimed and find out many leads have been comverted to resv.
            if (leadData != null)
            {

              leadClaimed = leadData.Count();
              foreach (var c in leadData)
              {
                leadBooked += c.Conversion;
              }

              resvAgentData.ClaimedLead = leadClaimed;
              if (leadClaimed > 0 && leadBooked > 0)
              {
                resvAgentData.ConversionLead = (leadBooked / (decimal)leadClaimed);
              }
              else
              {
                resvAgentData.ConversionLead = 0.0m;
              }
            }

            // Count Pending FCCs (opportunity, Client Prospective, and Service)
            if (dashboardDataset.tblFCC.Rows.Count > 0)
            {
              var fccPendingData = from f in dashboardDataset.tblFCC.AsEnumerable()
                                   select f;

              if (fccPendingData != null)
              {
                foreach (var f in fccPendingData)
                {
                  if (f.FCCContactSourceID == opTypeLead)
                  {
                    pendingFccOpp++;
                  }
                  else
                  {
                    if (f.FCCContactSourceID == opTypeClientProspect)
                    {
                      pendingFccClp++;
                    }
                    else
                    {
                      if (f.FCCContactSourceID == opTypeService)
                      {
                        pendingFccSvc++;
                      }
                    }
                  }
                }
                resvAgentData.PendingFCC = $"{pendingFccOpp} - {pendingFccClp} - {pendingFccSvc}";
              }
            }

            //count Completed FCCs (opportunity, Client Prospective, and Service)
            if (fccCompletedData != null)
            {
              foreach (var f in fccCompletedData)
              {
                if (f.ContactSourceID == opTypeLead)
                {
                  completedFccOpp++;
                }
                else
                {
                  if (f.ContactSourceID == opTypeClientProspect)
                  {
                    completedFccClp++;
                  }
                  else
                  {
                    if (f.ContactSourceID == opTypeService)
                    {
                      completedFccSvc++;
                    }
                  }
                }
              }
              resvAgentData.CompletedFCC = $"{completedFccOpp} - {completedFccClp} - {completedFccSvc}";
            }


            //save the changes
            resvAgentData.EndEdit();

            //reset variables
            // Resv variables
            pendingResvInq = 0;
            pendingResvBkd = 0;
            pendingResvOkd = 0;

            totalSales = 0.0m;
            totalBuds = 0.0m;

            // lead variables
            leadBooked = 0;

            // FCC variables
            completedFccOpp = 0;
            completedFccClp = 0;
            completedFccSvc = 0;
            pendingFccOpp = 0;
            pendingFccClp = 0;
            pendingFccSvc = 0;


          }
        }

      }

    }

    #region Fill Counter 
    /// <summary>
    /// Fills the Counter data table.
    /// </summary>
    public void FillCounter()
    {
      DataRow dr = null;
      int leadFCC = 983; //Opportunity FCC
      DateTime currentDate = DateTime.Today;
      int userID;
      string userName;
      string fullName;
      string description;

      try
      {
        userID = (int)HttpContext.Current.Session["UserID"];
      }
      catch
      {
        userID = 0;
      }
      try
      {
        userName = HttpContext.Current.Session["UserName"].ToString();
      }
      catch
      {
        userName = string.Empty;
      }
      try
      {
        fullName = HttpContext.Current.Session["FullName"].ToString();
      }
      catch
      {
        fullName = string.Empty;
      }

      void insertRecord(string desc)
      {
        dr = dashboardDataset.tblCounter.NewRow();
        dr["UserID"] = userID;
        dr["UserName"] = userName;
        dr["FullName"] = fullName;
        dr["Description"] = desc;
        dashboardDataset.tblCounter.Rows.Add(dr);
      }

      /*
      var fccCount = (from f in dashboardDataset.tblFCC.AsEnumerable()
                      where f.CallbackDate < currentDate && f.FCCContactSourceID == leadFCC
                      select f).Count();
      if (fccCount > 0)
      {

        dr = dashboardDataset.tblCounter.NewRow();
        dr["UserID"] = userID;
        dr["UserName"] = userName;
        dr["FullName"] = fullName;
        dr["Description"] = "Past due FCCs for leads";
        dr["Counter"] = fccCount;
        dashboardDataset.tblCounter.Rows.Add(dr);
      }
      var otherFccCount = (from f in dashboardDataset.tblFCC.AsEnumerable()
                           where f.CallbackDate < currentDate && f.FCCContactSourceID != leadFCC
                           select f).Count();
      if (otherFccCount > 0)
      {
        dr = dashboardDataset.tblCounter.NewRow();
        dr["UserID"] = userID;
        dr["UserName"] = userName;
        dr["FullName"] = fullName;
        dr["Description"] = "Past due FCCs for all other types";
        dr["Counter"] = otherFccCount;
        dashboardDataset.tblCounter.Rows.Add(dr);
      }

      var callbackCount = (from c in dashboardDataset.tblClientCallback.AsEnumerable()
                           where c.PastOrFuture == "Y"
                           select c).Count();
      if (callbackCount > 0)
      {
        dr = dashboardDataset.tblCounter.NewRow();
        dr["UserID"] = userID;
        dr["UserName"] = userName;
        dr["FullName"] = fullName;
        dr["Description"] = "Past due RCCB";
        dr["Counter"] = callbackCount;
        dashboardDataset.tblCounter.Rows.Add(dr);
      }
      */
      description = "NEW WORK";
      insertRecord(description);
      description = $"Opportunities logged in last 7 days = {leadClaimedInSevenDay}";
      insertRecord(description);
      description = $"Today's FCCs = {todayFCC}";
      insertRecord(description);
      description = $"RCCB in last 7 days = {rccbInSevenDay}";
      insertRecord(description);
      description = $"TOTAL = {leadClaimedInSevenDay + rccbInSevenDay + todayFCC}";
      insertRecord(description);
      description = "PAST DUE WORK";
      insertRecord(description);
      description = $"Opportunities at OP1 or Red Alert status = {leadAtOp1OrRedAlert}";
      insertRecord(description);
      description = $"Past due RCCB = {pastDueRccb}";
      insertRecord(description);
      description = $"Past due FCCs for Opportunities = {pastDueFccForLead}";
      insertRecord(description);
      description = $"TOTAL PAST DUE WORK ON OPPORTUNITIES = {leadAtOp1OrRedAlert + pastDueRccb + pastDueFccForLead}";
      insertRecord(description);
      description = $"Past due FCCs for Client Prospect = {pastDueFccForClientProspect}";
      insertRecord(description);
      description = $"Past due FCCs for Service = {pastDueFccForService}";
      insertRecord(description);
      description = $"Past due FCCs for Unassigned Type = {pastDueFccForUnassigned}";
      insertRecord(description);

    }
    #endregion



    //*****************************************************************
    #region Call each method to fill each data tables in the dataset dsDashbaord
    /// <summary>
    /// Fills the data tables.  Call each method to fills eah data table.
    /// </summary>
    private void FillDataTables()
    {
      //string userName = string.Empty;
      string kind = string.Empty;
      string temp = string.Empty;
      //DateTime currentDate;
      //DateTime depositDueDate;
      //TimeSpan dayLate;
      //int numberDayLate;
      DateTime startDate;
      DateTime endDate;
      DateTime smbStartDate;
      DateTime smbEndDate;
      DateTime startFCCDate;
      DateTime endFCCDate;
      DateTime startActivityDate;
      DateTime endActivityDate;

      startDate = DateTime.Today;
      endDate = DateTime.Today;

      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        magnumDb.CommandTimeout = 0;
        if (this.GroupID == 4 || this.GroupID == dashboardAdmin)  // admin group
        {

          //fill final payment data table
          FillFinalPayment(magnumDb);

          //fill Non Deposit data table
          FillNonDeposit(magnumDb, startDate);

          //remove a reservation from a final payment datatable if it is also in Non-deposit datable
          RemoveFinalPayment();

          // Fill Final Payment and Nondeposit table by UNION the Final and Non-Deposit datasets
          FillFinalAndNonDeposit();

          #region Fill Sales Management Board table
          try
          {
            smbStartDate = Convert.ToDateTime(HttpContext.Current.Session["StartDate"]);
          }
          catch
          {
            smbStartDate = startDate.AddDays(-7);
          }
          try
          {
            smbEndDate = Convert.ToDateTime(HttpContext.Current.Session["EndDate"]);
          }
          catch
          {
            smbEndDate = endDate.AddDays(1);
          }
          FillSalesManagermnetBoard(smbStartDate, smbEndDate);
          #endregion

          //fill Returnin Client Callback data table
          FillReturningClientCallback(magnumDb, startDate);

          #region fill Future Client Callback data table
          try
          {
            startFCCDate = Convert.ToDateTime(HttpContext.Current.Session["StartFCCDate"]);
          }
          catch
          {
            startFCCDate = startDate;
          }
          try
          {
            endFCCDate = Convert.ToDateTime(HttpContext.Current.Session["EndFCCDate"]);
          }
          catch
          {
            endFCCDate = endDate;
          }
          FillFutureClientCallback(magnumDb, startFCCDate, endFCCDate);
          #endregion

          #region Fill Activity Summary
          //try
          //{
          //	startActivityDate = Convert.ToDateTime(HttpContext.Current.Session["StartActivityDate"]);
          //}
          //catch
          //{
          //	startActivityDate = startDate;
          //}
          //try
          //{
          //	endActivityDate = Convert.ToDateTime(HttpContext.Current.Session["EndActivityDate"]);
          //}
          //catch
          //{
          //	endActivityDate = endDate;
          //}
          //FillActivityCounter(magnumDb, startActivityDate, endActivityDate);
          FillActivitySummary(magnumDb, this.UserName);
          #endregion


          // Remove FCC from the FCC dataset if a client is already in the Callback dataset (grid)
          RemoveFCC();

          //Reservation data table
          FillWaitListed(magnumDb);

          // In House and Arrival (next 10 days)
          //FillInHouseAndArrival(magnumDb);

          // Reservation data table;
          //FillReservation(magnumDb);

          //Lead info
          //FillClientCallbackLead(magnumDb);

          //fill Reservation Agent data table
          if (dashboardDataset.tblResAgent.Rows.Count == 0)
          {
            FillResvAgent(magnumDb);
          }

          //fill Letter data table
          //FillLetter(magnumDb, startDate, endDate);

          //fill clients data table from each datatable
          FillClient();

          //calc total for each type (final payment, non-deposit, etc) for each RA
          CalculateTotals(magnumDb);
        }
        else
        {
          //fill final payment data table
          FillFinalPayment(magnumDb);

          //fill Non Deposit data table
          FillNonDeposit(magnumDb, startDate);

          //remove a reservation from a final payment datatable if it is also in Non-deposit datable
          RemoveFinalPayment();

          // Fill Final Payment and Nondeposit table by UNION the Final and Non-Deposit datasets
          FillFinalAndNonDeposit();

          #region Fill Sales Management Board table
          try
          {
            smbStartDate = Convert.ToDateTime(HttpContext.Current.Session["StartSmbDate"]);
          }
          catch
          {
            smbStartDate = startDate.AddDays(-7);
          }
          try
          {
            smbEndDate = Convert.ToDateTime(HttpContext.Current.Session["EndSmbDate"]);
          }
          catch
          {
            smbEndDate = endDate.AddDays(1);
          }
          FillSalesManagermnetBoard(smbStartDate, smbEndDate);
          #endregion

          //fill Returnin Client Callback data table
          FillReturningClientCallback(magnumDb, startDate);

          #region fill Future Client Callback data table
          try
          {
            startFCCDate = Convert.ToDateTime(HttpContext.Current.Session["StartFCCDate"]);
          }
          catch
          {
            startFCCDate = startDate;
          }
          try
          {
            endFCCDate = Convert.ToDateTime(HttpContext.Current.Session["EndFCCDate"]);
          }
          catch
          {
            endFCCDate = endDate;
          }

          FillFutureClientCallback(magnumDb, startFCCDate, endFCCDate);
          #endregion

          // Count FCC 
          // ML - 07/27/2018 - commented out
          //CountFCC();

          #region Fill Activity Summary
          //try
          //{
          //	startActivityDate = Convert.ToDateTime(HttpContext.Current.Session["StartActivityDate"]);
          //}
          //catch
          //{
          //	startActivityDate = startDate;
          //}
          //try
          //{
          //	endActivityDate = Convert.ToDateTime(HttpContext.Current.Session["EndActivityDate"]);
          //}
          //catch
          //{
          //	endActivityDate = endDate;
          //}
          //FillActivityCounter(magnumDb, startActivityDate, endActivityDate);

          FillActivitySummary(magnumDb, this.UserName);
          #endregion

          // Remove FCC from the FCC dataset if a client is already in the Callback dataset (grid)
          RemoveFCC();

          // In House and Arrival (next 10 days)
          //FillInHouseAndArrival(magnumDb);

          //Reservation data table;
          FillReservation(magnumDb, this.UserName);

          //Waitlist, OKD, and NOK
          FillReservationWaitList(magnumDb, this.UserName);

          #region Fill Sales Assist
          try
          {
            this.SalesAssistID = Convert.ToInt32(HttpContext.Current.Session["SalesAssistID"]);
          }
          catch
          {

            this.SalesAssistID = 0;
          }

          if (this.SalesAssistID > 0)
          {
            FillResvAgentAssist(magnumDb, this.SalesAssistID);
          }
          #endregion

          //fill hurricane Reservations
          FillHurricaneVilla(magnumDb, this.UserName);

          //fill Letter data table
          FillLetter(magnumDb, startDate, endDate);

          //Count Leads that have been claimed in the last 7 days
          // ML - 07/27/2018 : commented out
          //CountLead();

          //fill couter table
          // ML - 07/27/2018
          //FillCounter();
          //Lead info
          //fillClientCallbackLead(magnumDb);

          //remove a reservation from a final payment datatable if it is also in Non-deposit datable
          //RemoveFinalPayment();

          //fill Reservation Agent data table
          //fillResvAgent(magnumDb);

          //fill clients data table from each datatable
          //fillClient();

          //calc total for each type (final payment, non-deposit, etc) for each RA
          //calculateTotals();
        }
        // Fill FCC type for the drop-down-list
        FillFCCType(magnumDb);
      }



      //var countData = from c in dashboardDataset.tblClient.ToList()
      //                group c by c.UserID into g
      //                select new { UserID = g.Key, userCount = g.Group.Count() };
    }
    #endregion

    public string GetAllResvWithinItinerary(string resvNo)
    {
      string result = string.Empty;
      //get an itinerary ID for a specific resv #
      var itineraryID = (from i in dashboardDataset.tblFinalAndNonDeposit.AsEnumerable()
                         where i.ReservationNumber == resvNo
                         select i.ItineraryID).FirstOrDefault();

      // result will contain the following format:  ';M73072;M73071;M73250;M73070;M73075;'
      if (itineraryID > 0)
      {

        var resvList = from r in dashboardDataset.tblFinalAndNonDeposit.AsEnumerable()
                       where r.ItineraryID == itineraryID
                       select r;
        if (resvList != null)
        {
          result = ";";
          foreach (var resv in resvList)
          {
            result += $"{resv.ReservationNumber};";
          }
        }
      }
      // return a string with the following format: ;M73072;M73071;M73250;M73070;M73075;
      return result;
    }

    public string GetResvWithinItinerary(string resvNo)
    {
      string result = string.Empty;
      //get an itinerary ID for a specific resv #
      var itineraryID = (from i in dashboardDataset.tblReservation.AsEnumerable()
                         where i.ReservationNumber == resvNo
                         select i.ItineraryID).FirstOrDefault();

      // The result variable will contain the following format:  ',M73072,M73071,M73250,M73070,M73075,'
      if (itineraryID > 0)
      {

        var resvList = from r in dashboardDataset.tblReservation.AsEnumerable()
                       where r.ItineraryID == itineraryID
                       select r;
        if (resvList != null)
        {
          result = ",";
          foreach (var resv in resvList)
          {
            result += $"{resv.ReservationNumber},";
          }
        }
      }
      // return a string with the following format: ,M73072,M73071,M73250,M73070,M73075,
      return result;
    }

    public DataSet GetNewResvAndInquiry(DateTime startDateBooked, DateTime endDateBooked, string resvAgent)
    {
      DataSet ds = null;
      using (MagnumDataContext magnumDb = new MagnumDataContext())
      {
        ds = magnumDb.spDashboardNewAndInquiry(startDateBooked, endDateBooked, resvAgent).ToDataSet("Table");
      }
      return ds;
    }
  }
}